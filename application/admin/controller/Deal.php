<?php
namespace app\admin\controller;

use app\admin\service\NodeService;
use library\Controller;
use library\tools\Data;
use think\Db;
use PHPExcel;//tp5.1用法
use PHPExcel_IOFactory;
use PHPExcel_Reader_Excel2007;
use PHPExcel_Reader_Excel5;

/**
 * 交易中心
 * Class Users
 * @package app\admin\controller
 */
class Deal extends Controller
{

    /**
     * 订单列表
     *@auth true
     *@menu true
     */
    public function order_list()
    {
        $this->title = '订单列表';
        $where = [];
        if(input('oid/s','')) $where[] = ['xc.id','like','%'.input('oid','').'%'];
        if(input('username/s','')) $where[] = ['u.username','like','%' . input('username/s','') . '%'];
        if(input('tel/s',''))$where[] = ['u.tel','like','%' . input('tel/s','') . '%'];
        if(input('o_status/d','')==-1){$o_status=0;}else{$o_status=input('o_status/d','');}
        if(input('o_status/d',''))$where[] = ['xc.status','=',$o_status];
        if(input('addtime/s','')){
            $arr = explode(' - ',input('addtime/s',''));
            $where[] = ['xc.addtime','between',[strtotime($arr[0]),strtotime ("+1 day", strtotime($arr[0]))]];
        }

        $user = session('admin_user');
        if($user['authorize']==2  && !empty($user['nodes']) ){
            //获取直属下级
            $mobile = $user['phone'];
            $uid = db('xy_users')->where('tel', $mobile)->value('id');

            $ids1  = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');

            $ids1 ? $ids2  = db('xy_users')->where('parent_id','in', $ids1)->field('id')->column('id') : $ids2 = [];

            $ids2 ? $ids3  = db('xy_users')->where('parent_id','in', $ids2)->field('id')->column('id') : $ids3 = [];

            $ids3 ? $ids4  = db('xy_users')->where('parent_id','in', $ids3)->field('id')->column('id') : $ids4 = [];

            $idsAll = array_merge([$uid],$ids1,$ids2 ,$ids3 ,$ids4);  //所有ids
            $where[] = ['xc.uid','in',$idsAll];


            //echo '<pre>';
            //var_dump($where,$idsAll,$ids3,$ids4);die;
        }

        $this->_query('xy_convey')
            ->alias('xc')
            ->leftJoin('xy_users u','u.id=xc.uid')
            ->leftJoin('xy_goods_list_cn g','g.id=xc.goods_id')
            ->field('xc.*,u.username,u.tel,u.is_jia,u.is_free,u.parent_id,g.goods_name,g.goods_price')
            ->where($where)
            ->order('addtime desc')
            ->page();
    }

    /**
     * 手动解冻
     * @auth true
     * @menu true
     */
    public function jiedong()
    {
        $this->applyCsrfToken();
        $oid = input('post.id/s','');

        if ($oid) {
            $oinfo = Db::name('xy_convey')->where('id',$oid)->find();
            if ( $oinfo['status'] != 5 ) {
                return $this->error('该订单未冻结!');
            }
            Db::name('xy_convey')->where('id',$oinfo['id'])->update(['status'=>1,'c_status'=>2]);
			Db::name('xy_balance_log')->insert([
                    'uid'           => $oinfo['uid'],
                    'oid'           => $oinfo['id'],
                    'num'           => $oinfo['num'],
                    'type'          => 9,//订单冻结
                    'status'        => 1,
                    'addtime'       => time()
                ]);
            //
            $res1 = Db::name('xy_users')
                ->where('id', $oinfo['uid'])
                //->inc('balance',$oinfo['num']+$oinfo['commission'])
                //->dec('freeze_balance',$oinfo['num']+$oinfo['commission']) //冻结商品金额 + 佣金
                ->inc('balance',$oinfo['num'])
                ->dec('freeze_balance',$oinfo['num']) //冻结商品金额 + 佣金
                ->update(['deal_status'=>1]);
            //$this->deal_reward($oinfo['uid'],$oinfo['id'],$oinfo['num'],'');
            return $this->success('解冻成功!111');
        }
        return $this->success('解冻成功!222');
    }

    /**
     * 交易返佣
     * @return void
     */
    public function deal_reward($uid,$oid,$num,$cnum)
    {

        Db::name('xy_balance_log')->where('oid',$oid)->update(['status'=>1]);

        //将订单状态改为已返回佣金
        Db::name('xy_convey')->where('id',$oid)->update(['c_status'=>1]);
        Db::name('xy_reward_log')->insert(['oid'=>$oid,'uid'=>$uid,'num'=>$num,'addtime'=>time(),'type'=>2]);//记录充值返佣订单
        /************* 发放交易奖励 *********/
        //$userList = model('admin/Users')->parent_user($uid,5); //五级
        $userList = model('admin/Users')->parent_user($uid,3); //三级
        //echo '<pre>';
        //var_dump($userList);die;
        if($userList){
            foreach($userList as $v){
                if($v['status']===1){
                    Db::name('xy_reward_log')
                        ->insert([
                            'uid'       => $v['id'],
                            'sid'       => $v['pid'],
                            'oid'       => $oid,
                            'num'       => $num*config($v['lv'].'_d_reward'),
                            'lv'        => $v['lv'],
                            'type'      => 2,
                            'status'    => 1,
                            'addtime'   => time(),
                        ]);
                }

                //
                $num3 = $num*config($v['lv'].'_d_reward'); //佣金
                $res = Db::name('xy_users')->where('id',$v['id'])->where('status',1)->setInc('balance',$num3);
                $res2 = Db::name('xy_balance_log')->insert([
                    'uid'           => $v['id'],
                    'oid'           => $oid,
                    'num'           => $num3,
                    'type'          => 6,
                    'status'        => 1,
                    'addtime'       => time()
                ]);

            }
        }
        /************* 发放交易奖励 *********/
    }


    /**
     * 处理用户交易订单
     */
    public function do_user_order()
    {
        $this->applyCsrfToken();
        $oid = input('post.id/s','');
        $status = input('post.status/d',1);
        if(!\in_array($status,[3,4])) return $this->error('参数错误');
        $res = model('Convey')->do_order($oid,$status);
        if($res['code']===0)
            return $this->success('操作成功');
        else
            return $this->error($res['info']);
    }

    /**
     * 交易控制
     * @auth true
     * @menu true
     */
    public function deal_console()
    {
        $this->title = '交易控制';
        $appname = config('app_name');
        if (in_array($appname, ['dypf_indon'])) {
            $file = "yn_bank.txt";
        } else {
            $file = "cn_bank.txt";
        }
        if(request()->isPost()){
            $deal_min_balance = input('post.deal_min_balance/d',0);
            $deal_timeout     = input('post.deal_timeout/d',0);
//            $deal_min_num     = input('post.deal_min_num/d',0);
//            $deal_max_num     = input('post.deal_max_num/d',0);
            $deal_min_num1     = input('post.deal_min_num1/d',0);
            $deal_max_num1     = input('post.deal_max_num1/d',0);
            $deal_min_num2     = input('post.deal_min_num2/d',0);
            $deal_max_num2     = input('post.deal_max_num2/d',0);
            $deal_min_num3     = input('post.deal_min_num3/d',0);
            $deal_max_num3     = input('post.deal_max_num3/d',0);
            
            $deal_min_num4     = input('post.deal_min_num4/d',0);
            $deal_max_num4    = input('post.deal_max_num4/d',0);
            $deal_min_num5     = input('post.deal_min_num5/d',0);
            $deal_max_num5     = input('post.deal_max_num5/d',0);
            $deal_min_num6     = input('post.deal_min_num6/d',0);
            $deal_max_num6     = input('post.deal_max_num6/d',0);
            
            
            
            $deal_count       = input('post.deal_count/d',0);
            $deal_reward_count= input('post.deal_reward_count/d',0);
            $deal_feedze      = input('post.deal_feedze/d',0);
            $deal_error       = input('post.deal_error/d',0);
            $deal_commission  = input('post.deal_commission/f',0);
            $_1reward  = input('post.1_reward/f',0);
            $_2reward  = input('post.2_reward/f',0);
            $_3reward  = input('post.3_reward/f',0);
            $_1_d_reward  = input('post.1_d_reward/f',0);
            $_2_d_reward  = input('post.2_d_reward/f',0);
            $_3_d_reward  = input('post.3_d_reward/f',0);
            $_4_d_reward  = input('post.4_d_reward/f',0);
            $_5_d_reward  = input('post.5_d_reward/f',0);

            //可以加上限制条件
            if($deal_commission>1||$deal_commission<0) return $this->error('参数错误'); 
            setconfig(['deal_min_balance'],[$deal_min_balance]);
            setconfig(['deal_timeout'],[$deal_timeout]);
//            setconfig(['deal_min_num'],[$deal_min_num]);
//            setconfig(['deal_max_num'],[$deal_max_num]);
            setconfig(['deal_min_num1'],[$deal_min_num1]);
            setconfig(['deal_max_num1'],[$deal_max_num1]);
            setconfig(['deal_min_num2'],[$deal_min_num2]);
            setconfig(['deal_max_num2'],[$deal_max_num2]);
            setconfig(['deal_min_num3'],[$deal_min_num3]);
            setconfig(['deal_max_num3'],[$deal_max_num3]);
            
            
            setconfig(['deal_min_num4'],[$deal_min_num4]);
            setconfig(['deal_max_num4'],[$deal_max_num4]);
            setconfig(['deal_min_num5'],[$deal_min_num5]);
            setconfig(['deal_max_num5'],[$deal_max_num5]);
            setconfig(['deal_min_num6'],[$deal_min_num6]);
            setconfig(['deal_max_num6'],[$deal_max_num6]);
            
            
            setconfig(['deal_reward_count'],[$deal_reward_count]);
            setconfig(['deal_count'],[$deal_count]);
            setconfig(['deal_feedze'],[$deal_feedze]);
            setconfig(['deal_error'],[$deal_error]);
            setconfig(['deal_commission'],[$deal_commission]);
            setconfig(['1_reward'],[$_1reward]);
            setconfig(['2_reward'],[$_2reward]);
            setconfig(['3_reward'],[$_3reward]);
            setconfig(['1_d_reward'],[$_1_d_reward]);
            setconfig(['2_d_reward'],[$_2_d_reward]);
            setconfig(['3_d_reward'],[$_3_d_reward]);
            setconfig(['4_d_reward'],[$_4_d_reward]);
            setconfig(['5_d_reward'],[$_5_d_reward]);
            setconfig(['vip_1_commission'],[input('post.vip_1_commission/f')]);
            setconfig(['vip_2_commission'],[input('post.vip_2_commission/f')]);
            setconfig(['vip_2_num'],[input('post.vip_2_num/f')]);
            setconfig(['vip_3_commission'],[input('post.vip_3_commission/f')]);
            setconfig(['vip_3_num'],[input('post.vip_3_num/f')]);
            setconfig(['master_cardnum'],[input('post.master_cardnum')]);
            setconfig(['master_name'],[input('post.master_name')]);
            setconfig(['master_bank'],[input('post.master_bank')]);
            setconfig(['master_bk_address'],[input('post.master_bk_address')]);
            setconfig(['deal_zhuji_time'],[input('post.deal_zhuji_time')]);
            setconfig(['deal_shop_time'],[input('post.deal_shop_time')]);
            setconfig(['app_url'],[input('post.app_url')]);
            setconfig(['version'],[input('post.version')]);

            setconfig(['tixian_time_1'],[input('post.tixian_time_1')]);
            setconfig(['tixian_time_2'],[input('post.tixian_time_2')]);

            setconfig(['chongzhi_time_1'],[input('post.chongzhi_time_1')]);
            setconfig(['chongzhi_time_2'],[input('post.chongzhi_time_2')]);
            setconfig(['chongzhi_status'],[input('post.chongzhi_status')]);

            setconfig(['order_time_1'],[input('post.order_time_1')]);
            setconfig(['order_time_2'],[input('post.order_time_2')]);

            setconfig(['user'],[input('post.user')]);
            setconfig(['pass'],[input('post.pass')]);
            setconfig(['sign'],[input('post.sign')]);


            setconfig(['lxb_bili'],[input('post.lxb_bili')]);
            setconfig(['lxb_time'],[input('post.lxb_time')]);
            setconfig(['lxb_sy_bili1'],[input('post.lxb_sy_bili1')]);
            setconfig(['lxb_sy_bili2'],[input('post.lxb_sy_bili2')]);
            setconfig(['lxb_sy_bili3'],[input('post.lxb_sy_bili3')]);
            setconfig(['lxb_sy_bili4'],[input('post.lxb_sy_bili4')]);
            setconfig(['lxb_sy_bili5'],[input('post.lxb_sy_bili5')]);
            setconfig(['lxb_ru_max'],[input('post.lxb_ru_max')]);
            setconfig(['lxb_ru_min'],[input('post.lxb_ru_min')]);

            setconfig(['shop_status'],[input('post.shop_status')]);

            setconfig(['cn_bank'],[input('post.bank')]);
            //var_dump(input('post.bank'));die;
            //
            $appname = config('app_name');
            
            $fileurl = APP_PATH . "../config/$file";
            
            file_put_contents($fileurl, input('post.bank')); // 写入配置文件


            return $this->success('操作成功!');
        }

       // var_dump(config('master_name'));die;
        $fileurl = APP_PATH . "../config/$file";
        $this->bank = file_get_contents($fileurl); // 写入配置文件


        $user = config('app.smsbao.user');
        $pass = config('app.smsbao.pass') ;


		
        return $this->fetch();
    }

    /**
     * 商品管理
     *@auth true
     *@menu true
     */
    public function goods_list()
    {
        $this->title = '商品管理';

        // $this->cate = db('xy_goods_cate')->order('addtime asc')->select();
        $this->cate = [
            ['id'=>18482,'name'=>'美妆工具'],
            // ['id'=>18637,'name'=>'美容护肤/美体/精油'],
            ['id'=>17455,'name'=>'腕表'],
            ['id'=>5851,'name'=>'电脑 / 平板'],
            ['id'=>6128,'name'=>'生活电器'],
            ['id'=>1,'name'=>'服饰 / 包包'],
            ['id'=>2,'name'=>'豪华房车'],
            ['id'=>3,'name'=>'手机'],
            ['id'=>4,'name'=>'家居娱乐']
        ];
        $this->cate_ = [
            18482 => '美妆工具',
            // 18637 => '美容护肤/美体/精油',
            17455 => '腕表',
            5851 => '电脑 / 平板',
            6128 => '生活电器',
            1 => '服饰 / 包包',
            2 => '豪华房车',
            3 => '手机',
            4 => '家居娱乐'
        ];
        $where = [];
        //var_dump($this->cate);die;
        $query = $this->_query('xy_goods_list_cn');
        if(input('title/s',''))$where[] = ['goods_name','like','%' . input('title/s','') . '%'];
        if(input('cat_id/d',''))$where[] = ['cat_id','=',input('cat_id/d','')];

        //var_dump($where);die;
        $query->where($where)->page();;


    }


    /**
     * 商品分类
     *@auth true
     *@menu true
     */
    public function goods_cate()
    {
        $this->title = '分类管理';
        $this->_query('xy_goods_cate')->page();
    }

    /**
     * 添加商品
     *@auth true
     *@menu true
     */
    public function add_goods()
    {
        if(\request()->isPost()){
            $this->applyCsrfToken();//验证令牌
            $shop_name      = input('post.shop_name/s','');
            $goods_name     = input('post.goods_name/s','');
            $goods_price    = input('post.goods_price/f',0);
            $goods_pic      = input('post.goods_pic/s','');
            $goods_info     = input('post.goods_info/s','');
            $cat_id     = input('post.cat_id/d',1);
            // $goods_pic =ltrim(parse_url($goods_pic,PHP_URL_PATH),'');
            $goods_pic =ltrim($goods_pic,'');
            if (!$cat_id) {
                $this->error('请选择分类');
            }
            $res = model('GoodsList')->submit_goods($shop_name,$goods_name,$goods_price,$goods_pic,$goods_info,$cat_id);
            if($res['code']===0)
                return $this->success($res['info'],'/admin.html#/admin/deal/goods_list.html');
            else 
                return $this->error($res['info']);
        }
        // $this->cate = db('xy_goods_cate')->order('addtime asc')->select();
        $this->cate = [
            ['id'=>18482,'name'=>'美妆工具'],
            // ['id'=>18637,'name'=>'美容护肤/美体/精油'],
            ['id'=>17455,'name'=>'腕表'],
            ['id'=>5851,'name'=>'电脑 / 平板'],
            ['id'=>6128,'name'=>'生活电器'],
            ['id'=>1,'name'=>'服饰 / 包包'],
            ['id'=>2,'name'=>'豪华房车'],
            ['id'=>3,'name'=>'手机'],
            ['id'=>4,'name'=>'家居娱乐']
        ];
        return $this->fetch();
    }


    /**
     * 添加商品
     *@auth true
     *@menu true
     */
    public function add_cate()
    {
        if(\request()->isPost()){
            $this->applyCsrfToken();//验证令牌
            $name      = input('post.name/s','');
            $bili     = input('post.bili/s','');
            $info    = input('post.cate_info/s','');
            $min    = input('post.min/s','');
            $max    = '1000';
            $res = $this->submit_cate($name,$bili,$info,$min,0,$max);
            if($res['code']===0)
                return $this->success($res['info'],'/admin.html#/admin/deal/goods_cate.html');
            else
                return $this->error($res['info']);
        }
        return $this->fetch();
    }


    /**
     * 添加商品
     *
     * @param string $shop_name
     * @param string $goods_name
     * @param string $goods_price
     * @param string $goods_pic
     * @param string $goods_info
     * @param string $id 传参则更新数据,不传则写入数据
     * @return array
     */
    public function submit_cate($name,$bili,$info,$min,$id,$cate_pic,$max)
    {
        if(!$name) return ['code'=>1,'info'=>('请输入分类名称')];
        if(!$bili) return ['code'=>1,'info'=>('请输入比例')];

        $data = [
            'name'     => $name,
            'bili'    => $bili,
            'cate_info'   => $info,
            'addtime'       => time(),
            'min'           =>$min,
            'max'           =>$max,
            'cate_pic'  =>$cate_pic
        ];
        if(!$id){
            $res = Db::table('xy_goods_cate')->insert($data);
        }else{
            $res = Db::table('xy_goods_cate')->where('id',$id)->update($data);
        }
        if($res)
            return ['code'=>0,'info'=>'操作成功!'];
        else
            return ['code'=>1,'info'=>'操作失败!'];
    }

    /**
     * 编辑商品信息
     * @auth true
     * @menu true
     */
    public function edit_goods($id)
    {
        $id = (int)$id;
        if(\request()->isPost()){
            $this->applyCsrfToken();//验证令牌
            $shop_name      = input('post.shop_name/s','');
            $goods_name     = input('post.goods_name/s','');
            $goods_price    = input('post.goods_price/f',0);
            $goods_pic      = input('post.goods_pic/s','');
            $goods_info     = input('post.goods_info/s','');
            $id             = input('post.id/d',0);
            $cat_id             = input('post.cat_id/d',0);
            $res = model('GoodsList')->submit_goods($shop_name,$goods_name,$goods_price,$goods_pic,$goods_info,$cat_id,$id);
            if($res['code']===0)
                return $this->success($res['info'],'/admin.html#/admin/deal/goods_list.html');
            else 
                return $this->error($res['info']);
        }
        $info = db('xy_goods_list_cn')->find($id);
        // $this->cate = db('xy_goods_cate')->order('addtime asc')->select();
        $this->cate = [
            ['id'=>18482,'name'=>'美妆工具'],
            // ['id'=>18637,'name'=>'美容护肤/美体/精油'],
            ['id'=>17455,'name'=>'腕表'],
            ['id'=>5851,'name'=>'电脑 / 平板'],
            ['id'=>6128,'name'=>'生活电器'],
            ['id'=>1,'name'=>'服饰 / 包包'],
            ['id'=>2,'name'=>'豪华房车'],
            ['id'=>3,'name'=>'手机'],
            ['id'=>4,'name'=>'家居娱乐']
        ];
        $this->assign('cate',$this->cate);
        $this->assign('info',$info);
        return $this->fetch();
    }
 /**
     * 编辑商品信息
     * @auth true
     * @menu true
     */
    public function edit_cate($id)
    {
        $id = (int)$id;
        if(\request()->isPost()){
            $this->applyCsrfToken();//验证令牌
            $name      = input('post.name/s','');
            $bili     = input('post.bili/s','');
            $info    = input('post.cate_info/s','');
            $min    = input('post.min/s','');
            $max    = input('post.max/s','');
            $cate_pic = input('post.cate_pic','');

            $res = $this->submit_cate($name,$bili,$info,$min,$id,$cate_pic,$max);
            if($res['code']===0)
                return $this->success($res['info'],'/admin.html#/admin/deal/goods_cate.html');
            else
                return $this->error($res['info']);
        }
        $info = db('xy_goods_cate')->find($id);
        $this->assign('info',$info);

        $this->level = Db::table('xy_level')->select();

        return $this->fetch();
    }
 /**
     * 编辑商品信息
     * @auth true
     * @menu true
     */
    public function edit_cate_status($id)
    {
        $id = input('id/d',0);
        $status = input('status/d',0);
		$status == -1 ? $status = 0:'';
        if(!$id || !in_array($status,[0,1])) return $this->error('参数错误');
        $res = Db::table('xy_goods_cate')->where('id',$id)->update(['status'=>$status]);
        if(!$res){
            echo '<pre>';
            var_dump($res,$status,$_REQUEST);
            return $this->error('更新失败!');
        }
        return $this->success('更新成功');
 
    }

    /**
     * 更改商品状态
     * @auth true
     */
    public function edit_goods_status()
    {
        $this->applyCsrfToken();
        $this->_form('xy_goods_list', 'form');
    }

    /**
     * 删除商品
     * @auth true
     */
    public function del_goods()
    {
        $this->applyCsrfToken();
        $this->_delete('xy_goods_list_cn');
    }
    /**
     * 删除商品
     * @auth true
     */
    public function del_cate()
    {
        $this->applyCsrfToken();
        $this->_delete('xy_goods_cate');
    }

    /**
     * 充值管理
     * @auth true
     * @menu true
     */
    public function user_recharge()
    {
        $this->title = '充值管理';
        $query = $this->_query('xy_recharge')->alias('xr');
        $where = [];
        if(input('oid/s','')) $where[] = ['xr.id','like','%'.input('oid','').'%'];
        if(input('tel/s','')) $where[] = ['xr.tel','=',input('tel/s','')];
        if(input('username/s','')) $where[] = ['u.username','like','%' . input('username/s','') . '%'];
        if(input('real_name/s','')) $where[] = ['u.real_name','like','%' . input('real_name/s','') . '%'];
        if(input('r_status/d',''))$where[] = ['xr.status','=',input('r_status/d','')];
        if(input('shuijun/d','') == '') {
            $shuijun_down_userid = db('xy_users')->where("username IN ('shuijun', 'Shuijun01')")->value('down_userid');
            $where[] = ['xr.uid','NOT IN',$shuijun_down_userid];
        }
        if(input('upusername/s','') != '') {
            $down_userid = db('xy_users')->where("username", input('upusername'))->value('down_userid');
            $where[] = ['xr.uid','IN',$down_userid];
        }
        if(input('addtime/s','')){
            $arr = explode(' - ',input('addtime/s',''));
            $where[] = ['xr.addtime','between',[strtotime($arr[0]),strtotime ("+1 day", strtotime($arr[1]))]];
        }

        $user = session('admin_user');
        if($user['authorize'] ==2  && !empty($user['nodes']) ){
            //获取直属下级
            $mobile = $user['phone'];
            $uid = db('xy_users')->where('tel', $mobile)->value('id');

//            $ids1  = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
//
//            $ids1 ? $ids2  = db('xy_users')->where('parent_id','in', $ids1)->field('id')->column('id') : $ids2 = [];
//
//            $ids2 ? $ids3  = db('xy_users')->where('parent_id','in', $ids2)->field('id')->column('id') : $ids3 = [];
//
//            $ids3 ? $ids4  = db('xy_users')->where('parent_id','in', $ids3)->field('id')->column('id') : $ids4 = [];

//            $idsAll = array_merge([$uid],$ids1,$ids2 ,$ids3 ,$ids4);  //所有ids
            $idsAll = team_get_ids($uid, -1, $idsStr);
            $where[] = ['xr.uid','in',$idsStr];
        }


        $query->leftJoin('xy_users u','u.id=xr.uid')
            ->field('xr.*,u.username,u.is_jia,u.is_free,u.parent_id')
            ->where($where)
            ->order('addtime desc')
            ->page();
    }

    /**
     * 充值管理(代收)
     * @auth true
     * @menu true
     */
    public function user_recharge_pay()
    {
        $this->title = '充值管理(代收)';
        $query = $this->_query('xy_recharge_pay')->alias('xr');
        $where = [];
        if(input('oid/s','')) $where[] = ['xr.id','like','%'.input('oid','').'%'];
        if(input('tel/s','')) $where[] = ['xr.tel','=',input('tel/s','')];
        if(input('username/s','')) $where[] = ['u.username','like','%' . input('username/s','') . '%'];
        if(input('real_name/s','')) $where[] = ['u.real_name','like','%' . input('real_name/s','') . '%'];
        if(input('r_status/d',''))$where[] = ['xr.status','=',input('r_status/d','')];
        if(input('shuijun/d','') == '') {
            $shuijun_down_userid = db('xy_users')->where('username', 'shuijun')->value('down_userid');
            $where[] = ['xr.uid','NOT IN',$shuijun_down_userid];
        }
        if(input('addtime/s','')){
            $arr = explode(' - ',input('addtime/s',''));
            $where[] = ['xr.addtime','between',[strtotime($arr[0]),strtotime ("+1 day", strtotime($arr[1]))]];
        }

        $user = session('admin_user');
        if($user['authorize'] ==2  && !empty($user['nodes']) ){
            //获取直属下级
            $mobile = $user['phone'];
            $uid = db('xy_users')->where('tel', $mobile)->value('id');

//            $ids1  = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
//
//            $ids1 ? $ids2  = db('xy_users')->where('parent_id','in', $ids1)->field('id')->column('id') : $ids2 = [];
//
//            $ids2 ? $ids3  = db('xy_users')->where('parent_id','in', $ids2)->field('id')->column('id') : $ids3 = [];
//
//            $ids3 ? $ids4  = db('xy_users')->where('parent_id','in', $ids3)->field('id')->column('id') : $ids4 = [];

//            $idsAll = array_merge([$uid],$ids1,$ids2 ,$ids3 ,$ids4);  //所有ids
            $idsAll = team_get_ids($uid, -1, $idsStr);
            $where[] = ['xr.uid','in',$idsStr];
        }


        $query->leftJoin('xy_users u','u.id=xr.uid')
            ->field('xr.*,u.username,u.is_jia,u.is_free,u.parent_id')
            ->where($where)
            ->order('addtime desc')
            ->page();
    }

    /**
     * 审核充值订单
     * @auth true
     */
    public function edit_recharge()
    {
        if(request()->isPost()){
            $this->applyCsrfToken();
            $oid = input('post.id/s','');
            $remark = input('remark/s', '');
            $status = input('post.status/d',1);
            $oinfo = Db::name('xy_recharge')->find($oid);
if ($oinfo['status']==1 && empty($oinfo['endtime'])){
            Db::startTrans();
            $res = Db::name('xy_recharge')->where('id',$oid)->update(['endtime'=>time(),'status'=>$status,'error_msg' => $remark]);
            //var_dump($res,$oinfo,$status);die;
            if($status==2){

                //var_dump($res,$oinfo['is_vip'],$oinfo['level']);die;

                if ($oinfo['is_vip']) {
                    $res1 = Db::name('xy_users')->where('id',$oinfo['uid'])->update(['level'=>$oinfo['level']]);
                }else{
                    $res1 = Db::name('xy_users')->where('id',$oinfo['uid'])->setInc('balance',$oinfo['num']);
                }

                $res2 = Db::name('xy_balance_log')
                        ->insert([
                            'uid'=>$oinfo['uid'],
                            'oid'=>$oid,
                            'num'=>$oinfo['num'],
                            'type'=>1,
                            'status'=>1,
                            'addtime'=>time(),
                        ]);                

                //发放注册奖励
            }elseif($status==3){
                if (cookie('think_var')=='yn'){
                    $content = 'Pesanan top up'.$oid.'Telah dikirim kembali,Silahkan hubungi customer service jika anda memiliki pertanyaan';
                }else if (cookie('think_var')=='en'){
                    $content = 'The recharge order '.$oid.' has been returned, if you have any questions, please contact customer service';
                }else if(cookie('think_var')=='yd'){
                    $content = 'रिचार्ज ऑर्डर '.$oid.' वापस कर दिया गया है, यदि आपके कोई प्रश्न हैं, तो कृपया ग्राहक सेवा से संपर्क करें';
                }else if(cookie('think_var')=='cn'){
                    $content = '充值订单'.$oid.'已被退回，如有疑问请联系客服';
                }else{
                    $content = 'The recharge order '.$oid.' has been returned, if you have any questions, please contact customer service';
                }

                if ($remark) {
                    $content = '充值订单'.$oid.'已被退回。 (' . $remark . ')';
                }


                $res1 = Db::name('xy_message')
                        ->insert([
                            'uid'=>$oinfo['uid'],
                            'type'=>2,
                            'title'=>'Notice',
                            'content'=>$content,
                            'addtime'=>time()
                        ]);

            }
            if($res && $res1){
                Db::commit();
                if ($oinfo['is_vip']) {
                    goto end;
                }

                /************* 发放推广奖励 *********/
                //用户总充值
                $total_recharge = Db::name('xy_recharge')->where(['uid' => $oinfo['uid'], 'status' => 2])->sum('num');

                //首次充值奖励
                if ($status == 2 && $total_recharge == $oinfo['num'] && config('first_recharge_reward') > 0 && $oinfo['num'] >= config('first_recharge_reward_min_amount')) {
                    $rechargeReward = config('first_recharge_reward');
                    $res_rechargeReward = Db::name('xy_balance_log')->insert([
                        //记录邀请一个朋友三十元的现金奖励
                        'uid'       => $oinfo['uid'],
                        'oid'       =>$oid,
                        'num'       => $rechargeReward,
                        'type'      => 18,//首次充值奖励
                        'addtime'   => time(),
                    ]);
                    if ($res_rechargeReward) {
                        Db::name('xy_users')->where('id',$oinfo['uid'])->setInc('balance',$rechargeReward);
                    };
                }

                Db::name('xy_users')->where('id', $oinfo['uid'])->update(['total_recharge' => $total_recharge]);
                if ($total_recharge >= config('invite_reward_min_recharge')) {
                    if (!in_array(config('app_name'), ['dypf_indon', 'prime'])) {
                        $total_recharge = 0;
                    }
                    model('admin/Users')->invitationBonus($oinfo['uid'],$oid);
                }
                /************* 发放推广奖励 *********/

                end:

                $this->success('操作成功!');
            }else{
                Db::rollback();
                $this->error('操作失败!');
            }
}else{
	return $this->error('充值订单已被处理','/admin.html#/admin/deal/user_recharge');
}
        }

    }


    /**
     * 提现管理
     * @auth true
     * @menu true
     */
    public function deposit_list()
    {
        $this->title = '提现列表';
        $query = $this->_query('xy_deposit')->alias('xd');
        $tod  =  strtotime( date("Y-m-d 00:00:00") );
        $where =[];
        if(input('username/s','')) $where[] = ['u.username','like','%' . input('username/s','') . '%'];
        if(input('tel/s','')) $where[] = ['u.tel','=',input('tel/s','')];
        if(input('d_status/d',''))$where[] = ['xd.status','=',input('d_status/d','')];
        if(input('oid/s','')) $where[] = ['xd.id','like','%' . input('oid/s','') . '%'];
        if(input('khname/s','')) $where[] = ['bk.username|bk.tel|bk.bankname|bk.cardnum','like','%' . input('khname/s','') . '%'];
        if(input('yes_deposit/d','')==1){$where[] = ['xd.addtime','<',$tod];$where[] = ['xd.status','=',1];}
        if(input('addtime/s','')){
            $arr = explode(' - ',input('addtime/s',''));
            $where[] = ['xd.addtime','between',[strtotime($arr[0]),strtotime ($arr[1] . ' 23:59:59')]];
        }
        if(input('shuijun/d','') == '') {
            $shuijun_down_userid = db('xy_users')->where("username IN ('shuijun', 'Shuijun01')")->value('down_userid');
            $where[] = ['xd.uid','NOT IN',$shuijun_down_userid];
        }
        if(input('upusername/s','') != '') {
            $down_userid = db('xy_users')->where("username", input('upusername'))->value('down_userid');
            $where[] = ['xd.uid','IN',$down_userid];
        }
        $user = session('admin_user');
        if($user['authorize'] ==2 && !empty($user['nodes']) ){
            //获取直属下级
            $mobile = $user['phone'];
            $uid = db('xy_users')->where('tel', $mobile)->value('id');
//            $uid = db('xy_users')->where('tel', $mobile)->value('id');
//
//            $ids1  = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
//
//            $ids1 ? $ids2  = db('xy_users')->where('parent_id','in', $ids1)->field('id')->column('id') : $ids2 = [];
//
//            $ids2 ? $ids3  = db('xy_users')->where('parent_id','in', $ids2)->field('id')->column('id') : $ids3 = [];
//
//            $ids3 ? $ids4  = db('xy_users')->where('parent_id','in', $ids3)->field('id')->column('id') : $ids4 = [];
//
//            $idsAll = array_merge([$uid],$ids1,$ids2 ,$ids3 ,$ids4);  //所有ids
            $idsAll = team_get_ids($uid, -1, $idsStr);
            $where[] = ['xd.uid','in',$idsStr];
        }

        $whereo[] = ['u.is_jia','=',0];
        $whereo[] = ['u.is_free','=',0];
        //$whereo[] = ['u.parent_id','>',0];
        $depositsall=db('xy_deposit')->alias('xd')->leftJoin('xy_users u','u.id=xd.uid')->field('xd.*,u.parent_id,u.is_jia,u.is_free')->where('xd.status',1)->sum('xd.num');
        $deposits=db('xy_deposit')->alias('xd')->leftJoin('xy_users u','u.id=xd.uid')->field('xd.*,u.parent_id,u.is_jia,u.is_free')->where($whereo)->where('xd.status',1)->sum('xd.num');
        $yesdeposits=db('xy_deposit')->alias('xd')->leftJoin('xy_users u','u.id=xd.uid')->field('xd.*,u.parent_id,u.is_jia,u.is_free')->where('xd.status',1)->where('xd.addtime','<',$tod)->sum('xd.num');
        $this->assign('depositsall', $depositsall);
        $this->assign('deposits', $deposits);
        $this->assign('yesdeposits', $yesdeposits);
        $this->assign('user',$user['id']);


        $query->leftJoin('xy_users u','u.id=xd.uid')
            ->leftJoin('xy_bankinfo bk','bk.id=xd.bk_id')
            ->field('xd.*,u.username,u.tel as usertel,u.is_jia,u.is_free,u.parent_id,u.wx_ewm,u.zfb_ewm,bk.address,bk.bankname,bk.username as khname,bk.tel,bk.cardnum,u.id uid,bk.site')
            ->where($where)
            ->order('addtime desc,endtime desc')
            ->page();
    }




    /**
     * 利息宝管理
     * @auth true
     * @menu true
     */
    public function lixibao_log()
    {
        $this->title = '利息宝列表';
        $query = $this->_query('xy_lixibao')->alias('xd');
        $where =[];
        if(input('username/s','')) $where[] = ['u.username|u.tel','like','%' . input('username/s','') . '%'];
        if(input('type/s','')) $where[] = ['xd.type','=',input('type/s',0)];
        if(input('addtime/s','')){
            $arr = explode(' - ',input('addtime/s',''));
            $where[] = ['xd.addtime','between',[strtotime($arr[0]),strtotime ("+1 day", strtotime($arr[0]))]];
        }


        $user = session('admin_user');
        if($user['authorize'] ==2  && !empty($user['nodes']) ){
            //获取直属下级
            $mobile = $user['phone'];
            $uid = db('xy_users')->where('tel', $mobile)->value('id');

            $ids1  = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');

            $ids1 ? $ids2  = db('xy_users')->where('parent_id','in', $ids1)->field('id')->column('id') : $ids2 = [];

            $ids2 ? $ids3  = db('xy_users')->where('parent_id','in', $ids2)->field('id')->column('id') : $ids3 = [];

            $ids3 ? $ids4  = db('xy_users')->where('parent_id','in', $ids3)->field('id')->column('id') : $ids4 = [];

            $idsAll = array_merge([$uid],$ids1,$ids2 ,$ids3 ,$ids4);  //所有ids
            $where[] = ['xd.uid','in',$idsAll];
        }

        $query->leftJoin('xy_users u','u.id=xd.uid')
            ->leftJoin('xy_lixibao_list xll', 'xd.sid = xll.id')
            ->field('xd.*,u.username,u.wx_ewm,u.zfb_ewm,u.id uid,xll.name,xll.endtime AS xll_endtime')
            ->where($where)
            ->order('addtime desc,endtime desc')
            ->page();
    }

    /**
     * 添加利息宝
     * @auth true
     * @menu true
     */
    public function add_lixibao()
    {
        if(\request()->isPost()){
            $this->applyCsrfToken();//验证令牌
            $name      = input('post.name/s','');
            $day       = input('post.day/d','');
            $bili      = input('post.bili/f','');
            $min_num    = input('post.min_num/s','');
            $max_num    = input('post.max_num/s','');
            $shouxu    = input('post.shouxu/s','');

            $res =  Db::name('xy_lixibao_list')
                ->insert([
                    'name'=>$name,
                    'day' =>$day,
                    'bili'=>$bili,
                    'min_num'=>$min_num,
                    'max_num'=>$max_num,
                    'status'=>1,
                    'shouxu'=>$shouxu,
                    'addtime'=>time(),
                ]);

            if($res)
                return $this->success('提交成功','/admin.html#/admin/deal/lixibao_list.html');
            else
                return $this->error('提交失败');
        }
        return $this->fetch();
    }
    /**
     * 编辑利息宝
     * @auth true
     * @menu true
     */
    public function edit_lixibao($id)
    {
        $id = (int)$id;
        if(\request()->isPost()){
            $this->applyCsrfToken();//验证令牌
            $name      = input('post.name/s','');
            $day       = input('post.day/d','');
            $bili      = input('post.bili/f','');
            $min_num    = input('post.min_num/s','');
            $max_num    = input('post.max_num/s','');
            $shouxu    = input('post.shouxu/s','');

            $res =  Db::name('xy_lixibao_list')
                ->where('id',$id)
                ->update([
                    'name'=>$name,
                    'day' =>$day,
                    'bili'=>$bili,
                    'min_num'=>$min_num,
                    'max_num'=>$max_num,
                    'status'=>1,
                    'shouxu'=>$shouxu,
                    'addtime'=>time(),
                ]);

            if($res)
                return $this->success('提交成功','/admin.html#/admin/deal/lixibao_list.html');
            else
                return $this->error('提交失败');
        }
        $info = db('xy_lixibao_list')->find($id);
        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * 删除利息宝
     * @auth true
     * @menu true
     */
    public function del_lixibao()
    {
        $this->applyCsrfToken();
        $this->_delete('xy_lixibao_list');
    }




    /**
     * 利息宝管理
     * @auth true
     * @menu true
     */
    public function lixibao_list()
    {
        $this->title = '利息宝列表';
        $query = $this->_query('xy_lixibao_list')->alias('xd');
        $where =[];
        if(input('addtime/s','')){
            $arr = explode(' - ',input('addtime/s',''));
            $where[] = ['xd.addtime','between',[strtotime($arr[0]),strtotime ("+1 day", strtotime($arr[0]))]];
        }

        $query
            ->field('xd.*')
            ->where($where)
            ->order('id')
            ->page();
    }



    /**
     * 禁用利息宝产品
     * @auth true
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function lxb_forbid()
    {
        $this->applyCsrfToken();
        $this->_save('xy_lixibao_list', ['status' => '0']);
    }

    /**
     * 启用利息宝产品
     * @auth true
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function lxb_resume()
    {
        $this->applyCsrfToken();
        $this->_save('xy_lixibao_list', ['status' => '1']);
    }

    public function generateSign($data, $signKey)
    {
        // 按照ASCII码升序排序
        ksort($data);

        $str = '';
        foreach ($data as $key => $value) {
            $value = trim($value);
            if ('sign' != $key && '' != $value) {
                $str .= $key . '=' . $value . '&';
            }
        }

        $str .= $signKey;

        return strtolower(md5($str));
    }

    public function post($url, $requestData)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        //普通数据
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($requestData));
        $res = curl_exec($curl);

        //$info = curl_getinfo($ch);
        curl_close($curl);
        return json_decode($res,true);
    }


    /**
     * 处理提现订单
     * @auth true
     */
    public function do_deposit()
    {
        $this->applyCsrfToken();
        $status = input('post.status/d',1);
//        dump($status);exit;
        
            $oinfo = Db::name('xy_deposit')->where('id',input('post.id',0))->find();
//        if ($oinfo['status']==1 && empty($oinfo['endtime'])){
//                if($status==3){
//                    //驳回订单的业务逻辑
//                    Db::name('xy_users')->where('id',$oinfo['uid'])->setInc('balance',input('num/f',0));
//                }
//                if($status==2) {
//                    $oid = input('post.id',0);
//                    Db::name('xy_balance_log')->where('oid',$oid)->update(['status'=>1]);
//        //            $res2 = Db::name('xy_balance_log')
//        //                ->insert([
//        //                    'uid' => $oinfo['uid'],
//        //                    'oid' => $oinfo['id'],
//        //                    'num' => $oinfo['num'],
//        //                    'type' => 3,
//        //                    'status' => 1,
//        //                    'addtime' => time(),
//        //                ]);
//                }
//                $this->_save('xy_deposit', ['status' =>$status,'endtime'=>time()]);
//        }else{
//            return $this->error('提现订单已处理','/admin.html#/admin/deal/deposit_list');
//        }
        $admin_data = session('admin_user');

        //1待审核 2审核通过 3审核驳回 4审核锁定
        if ($oinfo['status']==1 && empty($oinfo['endtime'])){
            //订单未审核
            if($status==3) {
                //驳回订单的业务逻辑
                Db::name('xy_users')->where('id',$oinfo['uid'])->setInc('balance',$oinfo['num']);

                $res2 = Db::name('xy_balance_log')
                    ->insert([
                        'uid' => $oinfo['uid'],
                        'oid' => $oinfo['id'],
                        'num' => $oinfo['num'],
                        'type' => 17,
                        'status' => 1,
                        'addtime' => time(),
                        'remark' => input('remark')
                ]);

                $this->_save('xy_deposit', ['status' =>$status,'endtime'=>time(), 'remark' => input('remark/s','')]);
            } elseif ($status==4) {
                $bankinfo = Db::name('xy_bankinfo')->where('id',$oinfo['bk_id'])->find();

//                /////////////锋芒代付////////////////////////
//                $bankinfo = Db::name('xy_bankinfo')->where('id',$oinfo['bk_id'])->find();
//                $params['version']  =   'V1.0';
//                $params['partner_id']  =   '109449';
//                $params['order_no']  =   $oinfo['id'];
//                $params['amount']  =   $oinfo['real_num'];
//                $params['bank_code']  =   $bankinfo['bankname'];
//                $params['account_no']  =   $bankinfo['cardnum'];
//                $params['account_name']  =   $bankinfo['username'];
//                $params['sign'] = $this->generateSign($params,'fb9341656d380198438279f61f9a9d52');
//                $data = $this->post('https://gateway.linxi.shop/withdraw/gateway',$params);
//                if ($data['code'] != '00'){
//                    return json(['code'=>1,'info'=>'提现失败！稍后再试']);
//                }else{
//                    //锁定订单逻辑
//                    $this->_save('xy_deposit', ['status' =>$status,'endtime'=>time(),'act_user_id'=>$admin_data['id']]);
//                }
//
//                /////////////锋芒代付///////////////////

                /*印尼支付*/

//                $bankinfo = Db::name('xy_bankinfo')->where('id',$oinfo['bk_id'])->find();
//                $userinfo = Db::name('xy_users')->where('id',session('user_id'))->find();
//                $params['merchantCode'] = 't10027';
//                $params['merchantOrderId'] = $oinfo['id'];
//                $params['tradeAmount'] = $oinfo['real_num'];
//                $params['bankCode'] = $bankinfo['bankname'];
//                $params['subBank'] = $bankinfo['site'];
//                $params['account'] = $bankinfo['username'];
//                $params['cardNo'] = $bankinfo['cardnum'];
//                $params['idCard'] = '';
//                $params['tel'] = $bankinfo['tel'];
//                $params['notifUrl'] = 'https://aaa.cisco7.com/index/api/deposit_notify';
//                $params['sign'] = $this->get_sign($params,'nS6RvEEWwtjqO9L6');
//                // dump($params);
//                $data = $this->post_json('120.27.210.21/ext/withdraw.html',$params);
//                $data = json_decode($data,true);
//                // dump($data);exit;
//
//                if ($data['code'] != '200'){
//                    return json(['code'=>1,'info'=>'提现失败！稍后再试']);
//                }else{
//                    //锁定订单逻辑
//                    $this->_save('xy_deposit', ['status' =>$status,'endtime'=>time(),'act_user_id'=>0]);
//                }

                /////////////印尼支付    end   //////////////////////

                /*代付11*/
                /*$params['advPasswordMd5'] = md5('ADF4QUCJMZGG1392');
                $params['orderId'] = $oinfo['id'];
                $params['flag'] = 1;
                $params['bankCode'] = $bankinfo['cardnum'];
                $params['bankUser'] = $bankinfo['username'];
                $params['bankUserPhone'] = '98566668';
                $params['bankAddress'] = $bankinfo['address'];
                $arr = ['a','b','c','d','e','f','g','h','i','j','k'];
                $params['bankUserEmail'] = $arr[rand(0,10)].$arr[rand(0,10)].rand(1000000,9999999).'@gmail.com';
                $params['bankUserIFSC'] = $bankinfo['ifsc'];
                $params['amount'] = (float)$oinfo['real_num'];
                $params['realAmount'] = (float)$oinfo['real_num'];
                $params['notifyUrl'] = 'https://www.task99.top/index/api/deposit11_notify';
                $getMillisecond     = $this->getMillisecond(); //毫秒时间戳
                //组装数据
                $data['body']       = $params;
                $data['merchantId'] = 'ADBRAVXQJXTS7180';
                $data['timestamp']  = "$getMillisecond";
                $sign    =   $this->pay11_sign($data,'9fac6b1eab034df0882504865e0c5ebe');
                //封装请求头
                $headers = array("Content-type: application/json;charset=UTF-8", "Accept: application/json", "Cache-Control: no-cache", "Pragma: no-cache", "Api-Sign:$sign");
                $json = $this->curlPost('https://gateway.shineupay.com/withdraw/create', $data, 5, $headers, $getMillisecond);;
                $data = json_decode($json, true);

                if ($data['status'] != '0'){
                    return json(['code'=>0,'info'=>$data['message']]);
                }else{*/
                    //锁定订单逻辑
                    $this->_save('xy_deposit', ['status' =>$status,'endtime'=>time(),'act_user_id'=>10000]);
                /*}*/
                /*代付11END*/



                /*代付12*/
//                S0117003   密码 123456  交易密码  123456
//Key ：2dh6ffybbfzkj51abt8hk21gnjcufhql
//                $params['pay_memberid'] = 'gUMnKMWCQSow';
//                $params['pay_orderid'] = $oinfo['id'];
//                $params['model'] = 'IMPS';
//                $params['pay_amount'] = $oinfo['real_num'];
//                $params['pay_applytime'] = time();
//                $params['pay_notifyurl'] = 'https://www.task99.top/index/api/deposit12_notify';
//                $params['pay_name'] = $bankinfo['username'];
//                $params['pay_mobile'] = '98566668';
//                $arr = ['a','b','c','d','e','f','g','h','i','j','k'];
//                $params['pay_email'] = $arr[rand(0,10)].$arr[rand(0,10)].rand(1000000,9999999).'@gmail.com';
//                $params['account_type'] = 'bank_account';
//                $params['account_number'] = $bankinfo['cardnum'];
//                $params['ifsc'] = $bankinfo['ifsc'];
//                $params['pay_sign'] = $this->create_sign2($params,'2dh6ffybbfzkj51abt8hk21gnjcufhql');
//                $data = $this->post_json('https://api.xpays.in/payout/create',$params);
//                $data = json_decode($data,true);
////                dump($data);exit;
//                if ($data['msg'] != 'success'){
//                    return json(['code'=>0,'info'=>$data['msg']]);
//                }else{
//                    //锁定订单逻辑
//                    $this->_save('xy_deposit', ['status' =>$status,'endtime'=>time(),'act_user_id'=>10000]);
//                }
                /*代付12 END*/

                /*India代付*/
//                $params['code'] = '10007';       //商户号
//                $params['amount'] = $oinfo['real_num'];     //单位：卢比  例：10.00
//                $params['bankname'] = $bankinfo['bankname'];
//                $params['accountname'] = $bankinfo['username'];    //收款人姓名
//                $params['cardnumber'] = $bankinfo['cardnum'];     //银行卡号
//                $params['mobile'] = $bankinfo['tel'];
//                $params['email'] = 'pptx'.rand(1000000,9999999).'@gmail.com';
//                $params['starttime'] = time();
//                $params['notifyurl'] = 'https://www.task99.top/index/api/India_deposit_notify';
//                $params['ifsc'] = $bankinfo['ifsc'];
//                $params['merissuingcode'] = $oinfo['id']; //商户代付单号
//                $params['signs'] = $this->create_sign2($params,'ffa68445-fc7f-42b6-a02c-f2351c4b0a05');
//                $data = $this->posturl('https://www.bossyd.com/api/outer/merwithdraw/addPaid',$params);
//
//                if ($data['msg'] != 'success'){
//                    return json(['code'=>1,'info'=>$data['msg']]);
//                }else{
//                    //锁定订单逻辑
//                    $this->_save('xy_deposit', ['status' =>$status,'endtime'=>time(),'act_user_id'=>10000]);
//                }
                /*India代付end*/

                /*通道3代付*/
//                $bank_arr = [
//                    'ICICI'=>1,
//                    'AXIS'=>2,
//                    'HDFC'=>3,
//                    'CentralBankofIndia'=>4,
//                    'StateBankOfIndia'=>5,
//                    'PunjabNationalBank'=>6,
//                    'BankOfIndia'=>7,
//                    'BankOfBaroda'=>8,
//                    'CanaraBank'=>9,
//                    'ReserveBankOfIndia'=>10,
//                    'UnitedBankOfIndia'=>11,
//                    'IndianOverseasBank'=>12,
//                    'KotakMahindraBank'=>13,
//                    'FederalBank'=>14,
//                    'BandhanBank'=>15,
//                    'IndustrialDevelopmentBankofIndia'=>16,
//                    'Southindianbank'=>17,
//                    'DCBBank'=>18,
//                    'IndianBank'=>19,
//                    'SyndicateBank'=>20,
//                    'KarurVysyaBank'=>21,
//                    'UnionBankofIndia'=>22,
//                    'IDFCFirstBank'=>23,
//                    'AndhraBank'=>24,
//                    'KarnatakaBank'=>25,
//                    'ICICcorporatebank'=>26,
//                    'UCOBank'=>27,
//                    'YesBank'=>28,
//                    'StandardCharteredBank'=>29,
//                    'AllahabadBank'=>30,
//                    'BankOfMaharashtra'=>31,
//                    'DenaBank'=>32,
//                    'Punjab&SindBank'=>33,
//                    'KarnatakaVikasGrameenaBank'=>34,
//                    'UjjivanSmallFinanceBank'=>35,
//                    'UtkalGrameenBank'=>36,
//                    'UtkarshSmallFinanceBank'=>37,
//                    'UttarBiharGraminBank'=>38,
//                    'UttarakhandGraminBank'=>39,
//                    'UttarBangaKshetriyaGraminBank'=>40,
//                    'VidharbhaKonkanGraminBank'=>41,
//                    'PunjabGraminBank'=>42,
//                    'PurvanchalBank'=>43,
//                    'RajasthanMarudharaGraminBank'=>44,
//                    'RBLBank'=>45,
//                    'SaptagiriGrameenaBank'=>46,
//                    'SarvaHaryanaGraminBank'=>47,
//                    'SaurashtraGraminBank'=>48,
//                    'SmallIndustriesDevelopmentBankofIndia'=>49,
//                    'SubhadraLocalAreaBankLtd'=>50,
//                    'SuryodaySmallFinanceBankLtd'=>51,
//                    'TamilNaduGramaBank'=>52,
//                    'TamilnadMercantileBankLtd'=>53,
//                    'TelanganaGrameenaBank'=>54,
//                    'TripuraGraminBank'=>55,
//                    'LakshmiVilasBank'=>56,
//                    'MadhyaPradeshGraminBank'=>57,
//                    'MadhyanchalGraminBank'=>58,
//                    'MaharashtraGraminBank'=>59,
//                    'ManipurRuralBank'=>60,
//                    'MeghalayaRuralBank'=>61,
//                    'MizoramRuralBank'=>62,
//                    'NagalandRural Bank'=>63,
//                    'NainitalBank'=>64,
//                    'NationalBankforAgricultureandRuralDevelopment'=>65,
//                    'NationalHousingBank'=>66,
//                    'NorthEastSmallFinanceBank'=>67,
//                    'NSDLPaymentsBankLimited'=>68,
//                    'OdishaGramyaBank'=>69,
//                    'PaschimBangaGraminBank'=>70,
//                    'PaytmPaymentsBank'=>71,
//                    'PrathamaUPGraminBank'=>72,
//                    'PuduvaiBharathiarGramaBank'=>73,
//                    'IDBIBank'=>74,
//                    'IndiaPostPaymentsBank'=>75,
//                    'lndusIndBank'=>76,
//                    'J&KGrameenBank'=>77,
//                    'Jammu&KashmirBank'=>78,
//                    'JanaSmallFinanceBank'=>79,
//                    'JharkhandRajyaGraminBank'=>80,
//                    'JioPaymentsBank'=>81,
//                    'KarnatakaGraminBank'=>82,
//                    'KashiGomtiSamyutGraminBank'=>84,
//                    'KeralaGraminBank'=>85,
//                    'CapitalSmallFinanceBankLimited'=>86,
//                    'ChaitanyaGodavariGrameenaBank'=>87,
//                    'ChhattisgarhRajyaGraminBank'=>88,
//                    'CityUnionBank'=>89,
//                    'CoastalLocalAreaBankLimited'=>90,
//                    'CSBBank'=>91,
//                    'DakshinBiharGraminBank'=>92,
//                    'DhanlaxmiBank'=>93,
//                    'EllaquaiDehatiBank'=>94,
//                    'EquitasSmallFinanceBank'=>95,
//                    'ESAFSmallFinanceBank'=>96,
//                    'EximBank'=>97,
//                    'FincareSmallFinanceBank'=>98,
//                    'FinoPaymentsBank'=>99,
//                    'HimachalPradeshGraminBank'=>100,
//                    'AdityaBirlaPaymentsBank'=>101,
//                    'AirtelPaymentsBank'=>102,
//                    'AndhraPradeshGrameenaVikasBank'=>103,
//                    'AndhraPragathiGrameenaBank'=>104,
//                    'ArunachalPradeshRuralBank'=>105,
//                    'AryavartBank'=>106,
//                    'AssamGraminVikashBank'=>107,
//                    'AUSmallFinanceBank'=>108,
//                    'BangiyaGraminVikashBank'=>109,
//                    'BarodaGujaratGraminBank'=>111,
//                    'BarodaRajasthanKshetriyaGraminBank'=>112,
//                    'BarodaUttarPradeshGraminBank'=>113,
//                ];
//                $url = 'https://order.hopeallgood168.com/withdrawal/createorder';
//                if (!empty($bankinfo['upi_name']) && !empty($bankinfo['upi_account'])){
//                    $params['bankType'] = 1;//走upi
//                    $params['account'] = $bankinfo['upi_account'];
//                    $params['accountName'] = $bankinfo['upi_name'];
//                }else{
//                    $params['bankType'] = 2;//走银行卡
//
//                    $params['IFSC'] = $bankinfo['ifsc'];
//                    $params['bankID'] = $bank_arr[$bankinfo['bankname']];
//                    $params['account'] = $bankinfo['cardnum'];
//                    $params['accountName'] = $bankinfo['username'];
//                    $params['remark'] = $bankinfo['tel'];
//                }
//
//                $params['callback'] = 'https://www.task99.top/index/api/paytime_deposit_notify';
//                $params['merchantId'] = '2014789';
//                $params['merchantorder'] = $oinfo['id'];
//                $params['money'] = $oinfo['real_num'];
//                $params['digest'] = $this->create_sign2($params,'tGCVPbVcNQVPYfpbqMS7MtORR3Rsz6ks');
//                $result = $this->posturl($url,$params);
//                if($result['code']==1){
//                    $this->_save('xy_deposit', ['status' =>$status,'endtime'=>time(),'act_user_id'=>10000]);
//                }else{
//                    return json(['code'=>0,'info'=>$result['msg']]);
//                }
                /*通道3代付end*/


            }else{
                    return $this->error('订单未锁定，锁定后才可通过','/admin.html#/admin/deal/deposit_list');
            }
        } elseif ($oinfo['status']==4) {
            //已锁定的订单
            if ($status==2) {
                //订单通过逻辑
                $oid = input('post.id',0);                
                // Db::name('xy_balance_log')->where('oid',$oid)->update(['status'=>1]);
            }
            if($status==3) {
                //驳回订单的业务逻辑
                Db::name('xy_users')->where('id',$oinfo['uid'])->setInc('balance',$oinfo['num']);

                $res2 = Db::name('xy_balance_log')
                    ->insert([
                        'uid' => $oinfo['uid'],
                        'oid' => $oinfo['id'],
                        'num' => $oinfo['num'],
                        'type' => 17,
                        'status' => 1,
                        'addtime' => time(),
                        'remark' => input('remark')
                ]);
            }
            if ($status==5) {
                /*代付*/
                $result = [];
                if (strpos(config('daifu_url'), 'openapi.pgpay.cc') !== false) {
                    model('admin/Daifu')->pay_1($oinfo, $result);
                }
                if (!isset($result['code'])) {
                    return json(['code'=>0,'info'=>$result]);
                }
                if($result['code']==1){
                   $this->_save('xy_deposit', ['status' =>$status,'endtime'=>time(),'act_user_id'=>10000]);
                }else{
                   return json(['code'=>0,'info'=>$result['msg']]);
                }
                /*代付 END*/
            }
            if ($status==4) {
                return $this->error('提现订单已处理','/admin.html#/admin/deal/deposit_list');
            }
            db('xy_deposit')->where('id', $oinfo['id'])->update(['status' =>$status,'endtime'=>time(), 'remark' => input('remark/s','')]);
            if ($status == 2) {
                $total_deposit = Db::name('xy_deposit')->where(['uid' => $oinfo['uid'], 'status' => 2])->sum('num');
                db('xy_users')->where('id', $oinfo['uid'])->update(['total_deposit' => $total_deposit]);
                $this->success('数据更新成功!', '');
            }
        } else if ($status == 99) {
            $this->_save('xy_deposit', ['status' =>4,'endtime'=>0]);
            return $this->error('提现订单已恢复至锁定状态','/admin.html#/admin/deal/deposit_list');
        } else{
            return $this->error('提现订单已处理','/admin.html#/admin/deal/deposit_list');
        }


    }

    public function post_json($url, $data)
    {
        $data = stripslashes(json_encode($data));
        $ch = curl_init($url); //请求的URL地址
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);//$data JSON类型字符串
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));
        $tmpInfo = curl_exec($ch);

        $error_msg = "";
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
        }
        curl_close($ch);
        return $error_msg != "" ? $error_msg : $tmpInfo;
    }

    public function get_sign($data,$signKey){
        ksort($data);
        $data['key'] = $signKey;
        $jsonStr = stripslashes(json_encode($data));
        $sign=strtoupper(md5($jsonStr));
        return $sign;
    }

    /**
     * 批量审核
     * @auth true
     */
    public function do_deposit2()
    {

        $ids =[];
        if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
            $ids = explode(',',$_REQUEST['id']);
            foreach ($ids as $id) {
                $t = Db::name('xy_deposit')->where('id',$id)->find();
                if ($t['status'] == 1) {
                    //通过
                    Db::name('xy_deposit')->where('id',$id)->update(['status'=>2,'endtime'=>time()]);
                    $total_deposit = Db::name('xy_deposit')->where(['uid' => $t['uid'], 'status' => 2])->sum('num');
                    db('xy_users')->where('id', $t['uid'])->update(['total_deposit' => $total_deposit]);
                    // $this->success('数据更新成功!', '');
                }
            }
            $this->success('处理成功','');
        }

    }


    /**
     * 导出xls
     * @auth true
     */
    public function recharge_daochu(){

        $where = [];
        if(input('oid/s','')) $where[] = ['xr.id','like','%'.input('oid','').'%'];
        if(input('tel/s','')) $where[] = ['xr.tel','=',input('tel/s','')];
        if(input('username/s','')) $where[] = ['u.username','like','%' . input('username/s','') . '%'];
        if(input('r_status/d',''))$where[] = ['xr.status','=',input('r_status/d','')];
        if(input('addtime/s','')){
            $arr = explode(' - ',input('addtime/s',''));
            $where[] = ['xr.addtime','between',[strtotime($arr[0]),strtotime ("+1 day", strtotime($arr[0]))]];
        }
        if(input('shuijun/d','') == '') {
            $shuijun_down_userid = db('xy_users')->where("username IN ('shuijun', 'Shuijun01')")->value('down_userid');
            $where[] = ['xr.uid','NOT IN',$shuijun_down_userid];
        }
        if(input('upusername/s','') != '') {
            $down_userid = db('xy_users')->where("username", input('upusername'))->value('down_userid');
            $where[] = ['xr.uid','IN',$down_userid];
        }
        $list = Db::name('xy_recharge')->alias('xr')
            ->leftJoin('xy_users u','u.id=xr.uid')
            ->field('xr.*,u.username,u.is_jia,u.is_free,u.parent_id')
            ->where($where)
            ->order('addtime desc')
            ->select();

        foreach( $list as $k=>&$_list ) {
            //var_dump($_list);die;

            $_list['endtime'] ? $_list['endtime'] = date('m/d H:i', $_list['endtime']) : '';
            $_list['addtime'] ? $_list['addtime'] = date('m/d H:i', $_list['addtime']) : '';

            if ($_list['type'] == 'zfb') {
                $_list['type'] = '支付宝';
            }else if ($_list['type'] == 'wx') {
                $_list['type'] = '微信 ';
            }else  {
                $_list['type'] = '银行卡';
            }

            if ($_list['status'] == 1) {
                $_list['status'] = '等待审核';
            }else if ($_list['status'] == 2) {
                $_list['status'] = '充值成功';
            }else  {
                $_list['status'] = '充值失败';
            }
        }




        //echo '<pre>';
        //var_dump($list);die;

        //3.实例化PHPExcel类
        $objPHPExcel = new PHPExcel();
        //4.激活当前的sheet表
        $objPHPExcel->setActiveSheetIndex(0);
        //5.设置表格头（即excel表格的第一行）
        //$objPHPExcel
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '订单号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '充值用户');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '真实姓名');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '交易数额');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '添加时间');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '支付方式');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '转账附言');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '类型');
            $objPHPExcel->getActiveSheet()->setCellValue('I1', '处理时间');
            $objPHPExcel->getActiveSheet()->setCellValue('J1', '状态');


        //设置A列水平居中
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A')->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //设置单元格宽度
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('B')->setWidth(30);


        //6.循环刚取出来的数组，将数据逐一添加到excel表格。
        for($i=0;$i<count($list);$i++){
            $objPHPExcel->getActiveSheet()->setCellValue('A'.($i+2),$list[$i]['id']);//ID
            $objPHPExcel->getActiveSheet()->setCellValue('B'.($i+2),$list[$i]['username']);//标签码
            $objPHPExcel->getActiveSheet()->setCellValue('C'.($i+2),$list[$i]['real_name']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('D'.($i+2),$list[$i]['num']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('E'.($i+2),$list[$i]['addtime']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('F'.($i+2),$list[$i]['type']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('G'.($i+2),$list[$i]['reference_number']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('H'.($i+2),$list[$i]['is_vip'] ? '会员升级' : '会员充值');//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('I'.($i+2),$list[$i]['endtime']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('J'.($i+2),$list[$i]['status']);//防伪码
        }

        //7.设置保存的Excel表格名称
        $filename = 'chongzhi'.date('ymd',time()).'.xls';
        //8.设置当前激活的sheet表格名称；

        $objPHPExcel->getActiveSheet()->setTitle('sheet'); // 设置工作表名

        //8.设置当前激活的sheet表格名称；
        $objPHPExcel->getActiveSheet()->setTitle('防伪码');
        //9.设置浏览器窗口下载表格
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-Disposition:inline;filename="'.$filename.'"');
        //生成excel文件
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        //下载文件在浏览器窗口
        $objWriter->save('php://output');
        exit;
    }

    /**
     * 导出xls
     * @auth true
     */
    public function daochu(){


        $where = array();

        if(input('username/s','')) $where[] = ['u.username','like','%' . input('username/s','') . '%'];
        if(input('tel/s','')) $where[] = ['u.tel','=',input('tel/s','')];
        if(input('d_status/d',''))$where[] = ['xd.status','=',input('d_status/d','')];
        if(input('oid/s','')) $where[] = ['xd.id','like','%' . input('oid/s','') . '%'];
        if(input('khname/s','')) $where[] = ['bk.username|bk.tel|bk.bankname|bk.cardnum','like','%' . input('khname/s','') . '%'];
        if(input('addtime/s','')){
            $arr = explode(' - ',input('addtime/s',''));
            $where[] = ['xd.addtime','between',[strtotime($arr[0]),strtotime ("+1 day", strtotime($arr[0]))]];
        }
        if(input('shuijun/d','') == '') {
            $shuijun_down_userid = db('xy_users')->where("username IN ('shuijun', 'Shuijun01')")->value('down_userid');
            $where[] = ['xd.uid','NOT IN',$shuijun_down_userid];
        }
        if(input('upusername/s','') != '') {
            $down_userid = db('xy_users')->where("username", input('upusername'))->value('down_userid');
            $where[] = ['xd.uid','IN',$down_userid];
        }

        $list = Db::name('xy_deposit')
            ->alias('xd')
            ->leftJoin('xy_users u','u.id=xd.uid')
            ->leftJoin('xy_bankinfo bk','bk.id=xd.bk_id')
            ->field('xd.*,u.username,u.tel mobile,bk.bankname,bk.cardnum,bk.site,bk.address,u.id uid,bk.username AS name')
            ->where($where)
            ->order('addtime desc,endtime desc')->select();

        //$list = $list[0];

// echo Db::name('xy_deposit')->getLastSql();exit;
        //echo '<pre>';
        //var_dump($list);die;

        foreach( $list as $k=>&$_list ) {
            //var_dump($_list);die;

            $_list['endtime'] ? $_list['endtime'] = date('m/d H:i', $_list['endtime']) : '';
            $_list['addtime'] ? $_list['addtime'] = date('m/d H:i', $_list['addtime']) : '';

            if ($_list['type'] == 'zfb') {
                $_list['type'] = '支付宝';
            }else if ($_list['type'] == 'wx') {
                $_list['type'] = '微信 ';
            }else  {
                $_list['type'] = '银行卡';
            }

            if ($_list['status'] == 1) {
                $_list['status'] = '待审核';
            }else if ($_list['status'] == 2) {
                $_list['status'] = '审核通过 ';
            }else  {
                $_list['status'] = '审核驳回';
            }

            unset($list[$k]['bk_id']);
        }




        //echo '<pre>';
        //var_dump($list);die;

        //3.实例化PHPExcel类
        $objPHPExcel = new PHPExcel();
        //4.激活当前的sheet表
        $objPHPExcel->setActiveSheetIndex(0);
        //5.设置表格头（即excel表格的第一行）
        //$objPHPExcel
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '订单号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '用户昵称');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '电话');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '提现金额');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '姓名');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '提现账户');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '提现银行');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '提现银行支行');
            $objPHPExcel->getActiveSheet()->setCellValue('I1', '提现银行支行地址');
            $objPHPExcel->getActiveSheet()->setCellValue('J1', '实际到账');
            $objPHPExcel->getActiveSheet()->setCellValue('K1', '处理时间');
            $objPHPExcel->getActiveSheet()->setCellValue('L1', '提交时间');
            $objPHPExcel->getActiveSheet()->setCellValue('M1', '提现方式');
            $objPHPExcel->getActiveSheet()->setCellValue('N1', '状态');


//        $objPHPExcel->getActiveSheet()->SetCellValue('A1', '订单号');
//        $objPHPExcel->getActiveSheet()->SetCellValue('B1', '标题');
//        $objPHPExcel->getActiveSheet()->SetCellValue('C1', '金额');

        //设置A列水平居中
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A')->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //设置单元格宽度
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('B')->setWidth(30);


        //6.循环刚取出来的数组，将数据逐一添加到excel表格。
        for($i=0;$i<count($list);$i++){
            $objPHPExcel->getActiveSheet()->setCellValue('A'.($i+2),$list[$i]['id']);//ID
            $objPHPExcel->getActiveSheet()->setCellValue('B'.($i+2),'`' . $list[$i]['username']);//标签码
            $objPHPExcel->getActiveSheet()->setCellValue('C'.($i+2),'`' . $list[$i]['mobile']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('D'.($i+2),$list[$i]['num']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('E'.($i+2),$list[$i]['name']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('F'.($i+2),'`' . $list[$i]['cardnum']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('G'.($i+2),$list[$i]['bankname']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('H'.($i+2),$list[$i]['site']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('I'.($i+2),$list[$i]['address']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('J'.($i+2),$list[$i]['real_num']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('K'.($i+2),$list[$i]['endtime']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('L'.($i+2),$list[$i]['addtime']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('M'.($i+2),$list[$i]['type']);//防伪码
            $objPHPExcel->getActiveSheet()->setCellValue('N'.($i+2),$list[$i]['status']);//防伪码
        }

        //7.设置保存的Excel表格名称
        $filename = 'tixian'.date('ymd',time()).'.xls';
        //8.设置当前激活的sheet表格名称；

        $objPHPExcel->getActiveSheet()->setTitle('sheet'); // 设置工作表名

        //8.设置当前激活的sheet表格名称；
        $objPHPExcel->getActiveSheet()->setTitle('防伪码');
        //9.设置浏览器窗口下载表格
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-Disposition:inline;filename="'.$filename.'"');
        //生成excel文件
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        //下载文件在浏览器窗口
        $objWriter->save('php://output');
        exit;
    }



    /**
     * 批量拒绝
     * @auth true
     */
    public function do_deposit3()
    {
        $ids =[];
        if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
            $ids = explode(',',$_REQUEST['id']);
            foreach ($ids as $id) {
                $t = Db::name('xy_deposit')->where('id',$id)->find();
                if ($t['status'] == 1) {
                    //拒绝
                    $res2 = Db::name('xy_balance_log')
                        ->insert([
                            'uid' => $t['uid'],
                            'oid' => $t['id'],
                            'num' => $t['num'],
                            'type' => 17,
                            'status' => 1,
                            'addtime' => time(),
                            'remark' => ''
                    ]);
                    Db::name('xy_deposit')->where('id',$id)->update(['status'=>3,'endtime'=>time()]);
                    //驳回订单的业务逻辑
                    Db::name('xy_users')->where('id',$t['uid'])->setInc('balance',$t['num']);
                }
            }

            $this->success('处理成功','/admin.html#/admin/deal/deposit_list.html');
        }
    }



    /**
     * 一键返佣
     * @auth true
     */
    public function do_commission()
    {
        $this->applyCsrfToken();
        $info = Db::name('xy_convey')
                ->field('id oid,uid,num,commission cnum')
                ->where([
                    ['c_status','in',[0,2]],
                    ['status','in',[1,3]],
                    //['endtime','between','??']    //时间限制
                ])
                ->select();
        if(!$info) return $this->error('当前没有待返佣订单!');
        try {
            foreach ($info as $k => $v) {
                Db::startTrans();
                $res = Db::name('xy_users')->where('id',$v['uid'])->where('status',1)->setInc('balance',$v['num']+$v['cnum']);
                if($res){
                    $res1 = Db::name('xy_balance_log')->insert([
                        //记录返佣信息
                        'uid'       => $v['uid'],
                        'oid'       => $v['oid'],
                        'num'       => $v['num']+$v['cnum'],
                        'type'      => 3,
                        'addtime'   => time()
                    ]);
                    Db::name('xy_convey')->where('id',$v['oid'])->update(['c_status'=>1]);
                }else{
                    // Db::name('xy_system_log')->insert();
                    $res1 = Db::name('xy_convey')->where('id',$v['oid'])->update(['c_status'=>2]);//记录账号异常
                }
                if($res!==false && $res1)
                    Db::commit();
                else
                    Db::rollback();
            }
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
        return $this->success('操作成功!');
    }

    /**
     * 交易流水
     * @auth true
     * @menu true
     */
    public function order_log()
    {
        $this->title = '交易流水';
        $this->_query('xy_balance_log')->page();
    }

    /**
     * 团队返佣
     * @auth true
     * @menu true
     */
    public function team_reward()
    {
        
    }

    /**
     * 财务统计
     * @auth true
     * @menu true
     */
    public function deal_list()
    {
        $this->title = '财务统计';

         function getthemonth($date)
           {
              $firstday = date('Y-m-01 00:00:00', strtotime($date));
              $lastday =  date('Y-m-d 23:59:59', strtotime("$firstday +1 month -1 day"));
              return array($firstday,$lastday);
           }
          $year='2020';
          for($i=1; $i<13; $i++){
              $today= date($year.'-'.$i.'-d h:i:s');
              $getmonths[$i]=getthemonth($today);
          }
//dump($getmonths);
        $whereo[] = ['u.is_jia','=',0];
        $whereo[] = ['u.is_free','=',0];
        //$whereo[] = ['u.parent_id','>',0];
          
          for($i=1; $i<13; $i++){
//echo strtotime($getmonths[$i][0]).'-'.strtotime($getmonths[$i][1]).'<br>';
            $czvalue[$i]=db('xy_recharge')->alias('xr')->leftJoin('xy_users u','u.id=xr.uid')->field('xr.*,u.parent_id,u.is_jia,u.is_free')->where($whereo)->where('xr.status',2)->where('xr.endtime','between',[strtotime($getmonths[$i][0]),strtotime($getmonths[$i][1])])->sum('xr.num');
            $txvalue[$i]=db('xy_deposit')->alias('xd')->leftJoin('xy_users u','u.id=xd.uid')->field('xd.*,u.parent_id,u.is_jia,u.is_free')->where($whereo)->where('xd.status',2)->where('xd.endtime','between',[strtotime($getmonths[$i][0]),strtotime($getmonths[$i][1])])->sum('xd.num');
          }
        $this->assign('czvalue', $czvalue);
        $this->assign('txvalue', $txvalue);


        $datevalue=['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'];
        $this->assign('datevalue', $datevalue);
        $this->fetch();
    }


    public function create_sign2($data, $signKey)
    {
        // 按照ASCII码升序排序

        ksort($data);
        $data['key'] = $signKey;
        $str = '';
        foreach ($data as $key => $value) {
            $value = trim($value);
            $str .= $key . '=' . $value . '&';
        }
        $str = substr($str, 0, -1);
        return strtoupper(md5($str));
    }
    public function posturl($url,$data){
//        $data  = json_encode($data);
//        dump($data);exit;
        $headerArray =array("Content-type:application/x-www-form-urlencoded;charset=utf-8;","Accept:application/json;charset=utf-8;");
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
        curl_setopt($curl, CURLOPT_POST, 1);
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl,CURLOPT_HTTPHEADER,$headerArray);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);
        return json_decode($output,true);
    }

    public static function curl_request($url, $data) {
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $tmpInfo = curl_exec($curl); // 执行操作
        if (curl_errno($curl)) {
            echo 'Error:'.curl_error($curl);//捕抓异常
        }
        curl_close($curl); // 关闭CURL会话
        return $tmpInfo; // 返回数据，json格式
    }
    public static function rsaPublicEncrypt($data)
    {
        $encrypted = '';
        $pu_key = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCm++Z9qgw6HxIOKWbAx1hbVU7PokmBBFlomwFhBdU2LInAQWfHEJTGM+2EX9D559J3XXxcPSVanVdM4LcTiJyVJoFSulIg01wR26yk7pzGqy+QJRv1uffL1+otRbgmDLhjeV16148CmwGG3j7xkVOqrv/fsiOViYd1EZdsit+vlQIDAQAB';
        $plainData = str_split($data, 128);
        $pu_key = "-----BEGIN PUBLIC KEY-----\n" . wordwrap($pu_key, 64, "\n", true) . "\n-----END PUBLIC KEY-----";
        foreach ($plainData as $chunk) {
            $partialEncrypted = '';
            //公钥加密

            $encryptionOk = openssl_public_encrypt($chunk, $partialEncrypted, $pu_key);

            if ($encryptionOk === false) {
                return false;
            }

            $encrypted .= $partialEncrypted;
        }

        $encrypted = base64_encode($encrypted);

        return $encrypted;
    }
    /**
     * AES 加密
     * @param $data
     * @param $key
     * @return string
     */
    public static function aesEncrypt($data, $key)
    {
        $iv_size = openssl_cipher_iv_length('AES-128-ECB');

        $iv = openssl_random_pseudo_bytes($iv_size);

        $encryptedMessage = openssl_encrypt($data, 'AES-128-ECB', $key, OPENSSL_RAW_DATA, $iv);
        return base64_encode($iv . $encryptedMessage);
    }

    /**
     * AES 解密
     * @param $data
     * @param $key
     * @return string
     */
    public static function aesDecrypt($data, $key)
    {
        $iv_size = openssl_cipher_iv_length('AES-128-ECB');
        $iv = openssl_random_pseudo_bytes($iv_size);
        $decrypted = openssl_decrypt( base64_decode($data),'AES-128-ECB', $key, OPENSSL_RAW_DATA,$iv);

        return $decrypted;
    }
    /**
     * 生成签名
     * @param array $data
     * @param string $sign
     * @return string $result
     */
    public function makeSign($data, $sign)
    {
        //签名步骤一：按字典序排序参数
        ksort($data);
        $string = $this->ToUrlParams($data);

        //签名步骤二：在string后加入KEY
        $string = $string . "&key=".$sign;
        //签名步骤三：MD5加密
        $string = md5($string);
        //签名步骤四：所有字符转为大写
        $result = strtoupper($string);
        return $result;
    }
    public static function getRandStr($length = 16)
    {
        // 密码字符集，可任意添加你需要的字符
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_ []{}<>~`+=,.;:/?|';
        $password = '';
        for ($i = 0; $i < $length; $i++) {
            $password .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        return $password;
    }
    /**
     * 格式化参数
     */
    public static function toUrlParams($data)
    {
        $temp = "";
        foreach ($data as $k => $v)
        {
            if($v != "" && !is_array($v)){
                $temp .= $k . "=" . $v . "&";
            }
        }
        $temp = trim($temp, "&");
        return $temp;
    }

    //获取毫秒数
    public function getMillisecond()
    {
        list($microsecond, $time) = explode(' ', microtime()); //' '中间是一个空格
        return (float)sprintf('%.0f', (floatval($microsecond) + floatval($time)) * 1000);
    }

    public function pay11_sign($params,$key)
    {
        $params = array_filter($params);
        $str = json_encode($params) . "|" . $key;
        $sign = MD5($str);
//        dump($sign);
        return $sign;
    }

    public function curlPost($url, $data, $timeout, $headers, $getMillisecond)
    {
        $data = json_encode($data);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($curl);
        if (curl_errno($curl)) {
            echo 'Errno' . curl_error($curl); //捕抓异常
        }
        curl_close($curl);
        return $output;
    }

    /**
     * 手动充值
     * @auth true
     * @menu true
     */
    public function add_recharge()
    {
        $cate = [
            '1' => '充值',
            '19' => '活动奖励'
        ];
        $this->cate = $cate;
        if(request()->isPost()){
            $username = trim(input('post.username/s',''), ',');
            $amount = input('post.amount/s','');
            $token = input('__token__',1);
            $recharge_type = input('recharge_type', 1);
            if (!in_array($recharge_type, array_keys($cate))) {
                $this->error("充值类型不正确");
            }
            if ($amount <= 0) {
                $this->error("金额不正确");
            }
            $usernames = explode(",", $username);
            $re = 0;
            $invalidUsers = [];
            $existusernames = db('xy_users')->where('username', "IN", $usernames)->column('username');
            foreach ($usernames as $username) {
                if (!in_array($username, $existusernames)) {
                    $invalidUsers[] = $username;
                }
            }
            if (sizeof($invalidUsers) > 0) {
                return $this->error('这些用户不存在: ' . implode(",", $invalidUsers));
            }

            // $user = Db::name('xy_users')->where('username',$username)->find();
            // if (!$user) {
            //     $this->error('用户不存在');
            // } else {
            $users = db('xy_users')->where('username', "IN", $usernames)->select();
            Db::startTrans();
            foreach ($users as $user) {
                $time = time();                
                $oid = getSn('SY');
                $res = db('xy_recharge')
                ->insert([
                    'id' => $oid,
                    'uid' => $user['id'],
                    'tel' => $user['tel'],
                    'real_name' => $user['username'],
                    'pic' => '',
                    'num' => $amount,
                    'addtime' => $time,
                    'endtime' => $time,
                    'pay_name' => 'manual',
                    'status' => 2,
                    'error_msg' => $recharge_type == 19 ? $cate[$recharge_type] : ''
                ]);
                $res1 = Db::name('xy_balance_log')
                        ->insert([
                            'uid'=>$user['id'],
                            'oid'=>$oid,
                            'num'=>$amount,
                            'type'=>$recharge_type,
                            'status'=>1,
                            'addtime'=>$time,
                        ]);
                if ($res && $res1) {
                    $res2 = Db::name('xy_users')->where('id',$user['id'])->setInc('balance',$amount);
                    //用户总充值
                    $total_recharge = Db::name('xy_recharge')->where(['uid' => $user['id'], 'status' => 2])->sum('num');

                    //首次充值奖励
                    if ($recharge_type == 1 && $total_recharge == $amount && config('first_recharge_reward') > 0 && $amount >= config('first_recharge_reward_min_amount')) {
                        $rechargeReward = config('first_recharge_reward');
                        $res_rechargeReward = Db::name('xy_balance_log')->insert([
                            //记录邀请一个朋友三十元的现金奖励
                            'uid'       => $user['id'],
                            'oid'       =>$oid,
                            'num'       => $rechargeReward,
                            'type'      => 18,//首次充值奖励
                            'addtime'   => $time,
                        ]);
                        if ($res_rechargeReward) {
                            Db::name('xy_users')->where('id',$user['id'])->setInc('balance',$rechargeReward);
                        };
                    }
                    if ($recharge_type == 1) {
                        Db::name('xy_users')->where('id', $user['id'])->update(['total_recharge' => $total_recharge]);
                        if ($total_recharge >= config('invite_reward_min_recharge')) {
                            if (!in_array(config('app_name'), ['dypf_indon', 'prime'])) {
                                $total_recharge = 0;
                            }
                            model('admin/Users')->invitationBonus($user['id'],$oid);
                        }
                    }
                }
            }
            if ($res && $res1 && $res2) {
                Db::commit();
                return $this->success("手动充值成功");
            } else {
                return $this->success("手动充值失败");
            }
        }
        
        return $this->fetch();
    }

    /**
     * 手动扣除
     * @auth true
     * @menu true
     */
    public function deduct_amount()
    {
        if(request()->isPost()){
            $username = trim(input('post.username/s',''), ',');
            $amount = input('post.amount/s','');
            $token = input('__token__',1);
            if ($amount <= 0) {
                $this->error("金额不正确");
            }
            $usernames = explode(",", $username);
            $re = 0;
            $invalidUsers = [];
            $balanceErrorUsers = [];
            $existusernames = [];
            $exist = db('xy_users')->where('username', "IN", $usernames)->column('(balance - freeze_balance) AS balance', 'username');

            foreach ($exist as $username => $balance) {
                $existusernames[] = $username;
            }
            foreach ($usernames as $username) {
                if (!in_array($username, $existusernames)) {
                    $invalidUsers[] = $username;
                } else {
                    if ($exist[$username] < $amount) {
                        $balanceErrorUsers[] = $username;
                    }
                }
            }
            if (sizeof($invalidUsers) > 0) {
                return $this->error('这些用户不存在: ' . implode(",", $invalidUsers));
            }
            if (sizeof($balanceErrorUsers) > 0) {
                return $this->error('这些用户的余额不足已扣除: ' . implode(",", $balanceErrorUsers));
            }

            // $user = Db::name('xy_users')->where('username',$username)->find();
            // if (!$user) {
            //     $this->error('用户不存在');
            // } else {
            $users = db('xy_users')->where('username', "IN", $usernames)->select();
            Db::startTrans();
            foreach ($users as $user) {
                $time = time();                
                $oid = getSn('SY');
                $res = Db::name('xy_balance_log')
                        ->insert([
                            'uid'=>$user['id'],
                            'oid'=>$oid,
                            'num'=>$amount,
                            'type'=>16,
                            'status'=>2,
                            'addtime'=>$time,
                        ]);
                if ($res) {
                    $res1 = Db::name('xy_users')->where('id',$user['id'])->setDec('balance',$amount);
                    $res1 = Db::name('xy_users')->where('id',$user['id'])->setDec('total_recharge',$amount);
                }
            }
            if ($res && $res1) {
                Db::commit();
                return $this->success("手动扣除成功");
            } else {
                return $this->success("手动扣除失败");
            }
        }
        return $this->fetch();
    }


    public function user_recharge_upload() {
        $this->title = '充值文件上传';
        if (request()->isPost()) {
            header("content-type:text/html;charset=utf-8");
            // Introduced PHPExcel class
            // vendor('PHPExcel');
            // vendor('PHPExcel.IOFactory');
            // vendor('PHPExcel.Reader.Excel5');
           
                 //redirect file name
            // $fileName = $_GET['fileName'];
           
                 //file path 
            // var_dump($_FILES);exit;
            $filePath = $_FILES['statement']['tmp_name'];//'./Home/temp/' . $fileName . '.xlsx';
                 // Instantiate the PHPExcel class
            $PHPExcel = new PHPExcel();
                 // By default excel2007 read excel, if the format is wrong, then read with the previous version
            $PHPReader = new PHPExcel_Reader_Excel2007();
            if (!$PHPReader->canRead($filePath)) {
              $PHPReader = new PHPExcel_Reader_Excel5();
              if (!$PHPReader->canRead($filePath)) {
                echo 'no Excel';
                return;
              }
            }
           
                //Read the Excel file
            $PHPExcel = $PHPReader->load($filePath);
                //Read the first worksheet in the excel file
            $sheet = $PHPExcel->getSheet(0);
                //Get the largest column number
            $allColumn = $sheet->getHighestColumn();
                //Get the largest line number
            $allRow = $sheet->getHighestRow();
                //Insert from the second line, the first line is the column name
            $invalid = [];
            $re = 0 ;
            for ($currentRow = 2; $currentRow <= $allRow; $currentRow++) {
                    //Get the value of column B
                $name = $PHPExcel->getActiveSheet()->getCell("A" . $currentRow)->getValue();
                        //Get the value of column C
                $code = $PHPExcel->getActiveSheet()->getCell("B" . $currentRow)->getValue();
                        //Get the value of column D
                $amount = $PHPExcel->getActiveSheet()->getCell("C" . $currentRow)->getValue();
                
                $map = [
                    'real_name' => trim($name),
                    'reference_number' => trim($code),
                    'num' => (float) trim($amount),
                    'status' => 1
                ];
                $oinfo = db('xy_recharge')->where($map)->find();
                if ($oinfo) {
                    $oid = $oinfo['id'];
                    $status = 2;

                    $res = Db::name('xy_recharge')->where('id',$oid)->update(['endtime'=>time(),'status'=>$status]);

                    $res1 = Db::name('xy_users')->where('id',$oinfo['uid'])->setInc('balance',$oinfo['num']);

                    $res2 = Db::name('xy_balance_log')
                            ->insert([
                                'uid'=>$oinfo['uid'],
                                'oid'=>$oid,
                                'num'=>$oinfo['num'],
                                'type'=>1,
                                'status'=>1,
                                'addtime'=>time(),
                            ]);
                    if($res && $res1){
                        $re ++;
                        /************* 发放推广奖励 *********/
                        //用户总充值
                        $total_recharge = Db::name('xy_recharge')->where(['uid' => $oinfo['uid'], 'status' => 2])->sum('num');

                        //首次充值奖励
                        if ($status == 2 && $total_recharge == $oinfo['num'] && config('first_recharge_reward') > 0 && $oinfo['num'] >= config('first_recharge_reward_min_amount')) {
                            $rechargeReward = config('first_recharge_reward');
                            $res_rechargeReward = Db::name('xy_balance_log')->insert([
                                //记录邀请一个朋友三十元的现金奖励
                                'uid'       => $oinfo['uid'],
                                'oid'       =>$oid,
                                'num'       => $rechargeReward,
                                'type'      => 18,//首次充值奖励
                                'addtime'   => time(),
                            ]);
                            if ($res_rechargeReward) {
                                Db::name('xy_users')->where('id',$oinfo['uid'])->setInc('balance',$rechargeReward);
                            };
                        }

                        Db::name('xy_users')->where('id', $oinfo['uid'])->update(['total_recharge' => $total_recharge]);
                        if ($total_recharge >= config('invite_reward_min_recharge')) {
                            if (!in_array(config('app_name'), ['dypf_indon', 'prime'])) {
                                $total_recharge = 0;
                            }
                            model('admin/Users')->invitationBonus($oinfo['uid'],$oid);
                        }
                        /************* 发放推广奖励 *********/
                    }
                } else {
                    $invalid[] = $name . '的订单不存在';
                }
            }
            echo '<a href="/admin.html#/admin/deal/user_recharge.html?spm=m-69-72-74">返回</a><br>';
            echo ('已处理' . $re . '个单子' . ' | ' . implode(', ',$invalid));
            exit;
        }
        $this->fetch();
    }

    public function reject_deposit() {
        $this->title = "驳回提现";
        $id = input('get.id',0);
        if(!$id) $this->error('参数错误');
        $this->info = Db::table('xy_deposit')->find($id);
        $this->user = db('xy_users')->find($this->info['uid']);
        $this->bank = db('xy_bankinfo')->find($this->info['bk_id']);
        return $this->fetch();
    }

    public function reject_recharge() {
        $this->title = "驳回充值";
        $id = input('get.id',0);
        if(!$id) $this->error('参数错误');
        $this->info = Db::table('xy_recharge')->find($id);
        $this->user = db('xy_users')->find($this->info['uid']);
        return $this->fetch();
    }

    /**
     * 手动添加积分
     * @auth true
     * @menu true
     */
    public function add_point()
    {
        if(request()->isPost()){
            $username = trim(input('post.username/s',''), ',');
            $amount = input('post.amount/s','');
            $token = input('__token__',1);
            if ($amount <= 0) {
                $this->error("积分数额不正确");
            }
            $usernames = explode(",", $username);
            $re = 0;
            $invalidUsers = [];
            $existusernames = db('xy_users')->where('username', "IN", $usernames)->column('username');
            foreach ($usernames as $username) {
                if (!in_array($username, $existusernames)) {
                    $invalidUsers[] = $username;
                }
            }
            if (sizeof($invalidUsers) > 0) {
                return $this->error('这些用户不存在: ' . implode(",", $invalidUsers));
            }

            // $user = Db::name('xy_users')->where('username',$username)->find();
            // if (!$user) {
            //     $this->error('用户不存在');
            // } else {
            $users = db('xy_users')->where('username', "IN", $usernames)->select();
            Db::startTrans();
            foreach ($users as $user) {
                $time = time();                
                $oid = getSn('SY');
                $res1 = Db::name('xy_point_log')
                        ->insert([
                            'uid'=>$user['id'],
                            'oid'=>$oid,
                            'num'=>$amount,
                            'type'=>10,
                            'status'=>1,
                            'addtime'=>$time,
                        ]);
                if ($res1) {
                    $res2 = Db::name('xy_users')->where('id',$user['id'])->setInc('point',$amount);
                }
            }
            if ($res1 && $res2) {
                Db::commit();
                return $this->success("手动添加积分成功");
            } else {
                return $this->success("手动添加积分失败");
            }
        }
        return $this->fetch();
    }

    /**
     * 手动扣除积分
     * @auth true
     * @menu true
     */
    public function deduct_point()
    {
        if(request()->isPost()){
            $username = trim(input('post.username/s',''), ',');
            $amount = input('post.amount/s','');
            $token = input('__token__',1);
            if ($amount <= 0) {
                $this->error("积分数额不正确");
            }
            $usernames = explode(",", $username);
            $re = 0;
            $invalidUsers = [];
            $balanceErrorUsers = [];
            $existusernames = [];
            $exist = db('xy_users')->where('username', "IN", $usernames)->column('(point - freeze_point) AS balance', 'username');

            foreach ($exist as $username => $balance) {
                $existusernames[] = $username;
            }
            foreach ($usernames as $username) {
                if (!in_array($username, $existusernames)) {
                    $invalidUsers[] = $username;
                } else {
                    if ($exist[$username] < $amount) {
                        $balanceErrorUsers[] = $username;
                    }
                }
            }
            if (sizeof($invalidUsers) > 0) {
                return $this->error('这些用户不存在: ' . implode(",", $invalidUsers));
            }
            if (sizeof($balanceErrorUsers) > 0) {
                return $this->error('这些用户的积分余额不足已扣除: ' . implode(",", $balanceErrorUsers));
            }

            // $user = Db::name('xy_users')->where('username',$username)->find();
            // if (!$user) {
            //     $this->error('用户不存在');
            // } else {
            $users = db('xy_users')->where('username', "IN", $usernames)->select();
            Db::startTrans();
            foreach ($users as $user) {
                $time = time();                
                $oid = getSn('SY');
                $res = Db::name('xy_point_log')
                        ->insert([
                            'uid'=>$user['id'],
                            'oid'=>$oid,
                            'num'=>$amount,
                            'type'=>16,
                            'status'=>2,
                            'addtime'=>$time,
                        ]);
                if ($res) {
                    $res1 = Db::name('xy_users')->where('id',$user['id'])->setDec('point',$amount);
                }
            }
            if ($res && $res1) {
                Db::commit();
                return $this->success("手动扣除积分成功");
            } else {
                return $this->success("手动扣除积分失败");
            }
        }
        return $this->fetch();
    }

    public function generateSign2($data, $signKey)
    {
        // 按照ASCII码升序排序
        ksort($data);

        $str = '';
        foreach ($data as $key => $value) {
            $value = trim($value);
            if ('sign' != $key && '' != $value) {
                $str .= $key . '=' . $value . '&';
            }
        }

        $str .= "key=" . $signKey;
        return strtoupper(md5($str));
    }
}