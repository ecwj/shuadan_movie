<?php
namespace app\admin\controller;

use app\admin\service\NodeService;
use library\Controller;
use library\tools\Data;
use think\Db;
use think\facade\Cache;

/**
 * 帮助中心
 * Class Users
 * @package app\admin\controller
 */
class Help extends Controller
{

    /**
     * 公告管理
     * @auth true
     * @menu true
     */
    public function message_ctrl()
    {
        $this->title = '公告管理';
        //$this->_query('xy_message')->page();
		
        $this->_query('xy_message')->alias('m')
            ->leftJoin('xy_users u','u.id=m.uid')
            ->field('m.*,u.tel,u.username')
            ->where('m.type',3)
            ->order('addtime desc')
            ->page();
			

    }

    /**
     * 添加公告
     * @auth true
     * @menu true
     */
    public function add_message()
    {
        if(request()->isPost()){
            $this->applyCsrfToken();
            $title   = input('post.title/s', '');
            $content = $_POST['content'];//input('post.content/s', '');
            $addtime = input('post.addtime', date('Y-m-d H:i:s'));

            if(!$title)$this->error('标题为必填项');
            if(mb_strlen($title) > 120)$this->error('标题长度限制为120个字符');
            if(!$content)$this->error('公告内容为必填项');

            if ($addtime) {
                $time = strtotime($addtime);
            } else {
                $time = time();
            }
            $res = Db::table('xy_message')->insert(['addtime'=>$time,'sid'=>0,'type'=>3,'title'=>$title,'content'=>$content]);
            if($res){
                $this->success('发送公告成功','/admin.html#/admin/help/message_ctrl.html');
            }else
                $this->error('发送公告失败');
        }
        return $this->fetch();
    }

    /**
     * 编辑公告
     * @auth true
     * @menu true
     */
    public function edit_message($id)
    {
        $id = intval($id);
        if(request()->isPost()){
            $this->applyCsrfToken();
            $title   = input('post.title/s', '');
            $content = $_POST['content'];//input('post.content/s', '');
            $id      = input('post.id/d',0);
            $addtime = input('post.addtime', date('Y-m-d H:i:s'));

            if(!$title)$this->error('标题为必填项');
            if(mb_strlen($title) > 120)$this->error('标题长度限制为120个字符');
            if(!$content)$this->error('公告内容为必填项');

            if ($addtime) {
                $time = strtotime($addtime);
            } else {
                $time = time();
            }
            $res = Db::table('xy_message')->where('id',$id)->update(['addtime'=>$time,'type'=>3,'title'=>$title,'content'=>$content]);
            if($res){
                $this->success('编辑成功','/admin.html#/admin/help/message_ctrl.html');
            }else
                $this->error('编辑失败');
        }

        $info = Db::table('xy_message')->find($id);
        $this->assign('info',$info);
        $this->fetch();
    }

    /**
     * 删除公告
     * @auth true
     * @menu true
     */
    public function del_message()
    {
        $this->applyCsrfToken();
        $id = input('post.id/d',0);
        $res = Db::table('xy_message')->where('id',$id)->delete();
        if($res)
            $this->success('删除成功!');
        else
            $this->error('删除失败!');
    }

    /**
     * 前台首页文本
     * @auth true
     * @menu true
     */
    public function home_msg()
    {
        $this->_query('xy_index_msg')->page();
    }

    /**
     * 编辑前台首页文本
     * @auth true
     * @menu true
     */
    public function edit_home_msg($id)
    {
        $id = intval($id);
        if(request()->isPost()){
            $this->applyCsrfToken();
            $cn_content = $_POST['cn_content'];//input('post.cn_content/s', '');
            $en_content = $_POST['en_content'];//input('post.en_content/s', '');
            $yn_content = $_POST['yn_content'];//input('post.yn_content/s', '');
            $yd_content = $_POST['yd_content'];//input('post.yd_content/s', '');
            $id      = input('post.id/d',0);

            if(!$cn_content)$this->error('正文内容为必填项');

            $res = Db::table('xy_index_msg')->where('id',$id)->update(['addtime'=>time(),'cn_content'=>$cn_content,'en_content'=>$en_content,'yn_content'=>$yn_content,'yd_content'=>$yd_content]);
            if($res){
                $this->success('编辑成功','/admin.html#/admin/help/home_msg.html');
            }else
                $this->error('编辑失败');
        }

        $info = Db::table('xy_index_msg')->find($id);
        $this->assign('info',$info);
        $this->fetch();
    }

    /**
     * 首页轮播图
     * @auth true
     * @menu true
     */
    public function banner()
    {
//        if(request()->isPost()){
//            $image = input('post.image/s','');
//            if($image=='') $this->error('请上传图片');
//            $res = Db::name('xy_banner')->where('id',1)->update(['image'=>$image]);
//            if($res!==false)
//                $this->success('操作成功');
//            else
//                $this->error('操作失败');
//        }
//        $this->title = '轮播图设置';
//        $this->info = Db::name('xy_banner')->find(1);
//        $this->fetch();


        $this->_query('xy_banner')->page();
    }

    /**
     * 编辑前台首页文本
     * @auth true
     * @menu true
     */
    public function edit_banner($id)
    {
        $id = intval($id);
        if(request()->isPost()){
            $this->applyCsrfToken();
            $id      = input('post.id/d',0);
//            $url   = input('post.url/s', '');
            $image = input('post.image/s', '');

            if(!$image)$this->error('图片为必填项');

            $res = Db::table('xy_banner')->where('id',$id)->update(['image'=>$image]);
//            dump($res);exit;
            if($res!==false){
                Cache::set('banner', NULL);
                $this->success('编辑成功','/admin.html#/admin/help/banner.html');
            }else
                $this->error('编辑失败');
        }

        $info = Db::table('xy_banner')->find($id);
        $this->assign('info',$info);
        $this->fetch();
    }

    /**
     * 添加公告
     * @auth true
     * @menu true
     */
    public function add_banner()
    {
        if(request()->isPost()){

//            $this->applyCsrfToken();
//            $url   = input('post.url/s', '');
            $image = input('post.image/s', '');

            //if(!$title)$this->error('标题为必填项');
            //if(mb_strlen($title) > 50)$this->error('标题长度限制为50个字符');
//            if(!$url)$this->error('图片为必填项');

            $res = Db::table('xy_banner')->insert(['image'=>$image]);
            if (in_array(config('app_name'), ['dypf_indon'])) {
                $res = Db::table('xy_banner')->insert(['image'=>$image, 'type' => 'en']);
                $res = Db::table('xy_banner')->insert(['image'=>$image, 'type' => 'yn']);
            }
            if($res){
                Cache::set('banner', NULL);
                $this->success('提交成功','/admin.html#/admin/help/banner.html');
            }else
                $this->error('提交失败');
        }
        return $this->fetch();
    }


    public function del_banner()
    {
        $this->applyCsrfToken();
        $id = input('post.id/d',0);
        $res = Db::table('xy_banner')->where('id',$id)->delete();
        if($res) {
            Cache::set('banner', NULL);
            $this->success('删除成功!');
        } else {
            $this->error('删除失败!');
        }
    }

    public function edit_message_tanchuang()
    {
        $id = input('id/d',0);
        $status = input('status/d',0);
        if(!$id) return $this->error('参数错误');
        
        if (!in_array($status, ['0', '1'])) {
            $this->error('参数错误');    
        }
        $res = db('xy_message')->where('id', $id)->update(['is_tanchuang' => $status]);
        if(!$res){
            return $this->error('无改变');
        }
        $txt = $status == 1 ? '已设置弹窗' : '已禁用弹窗';
        sysoplog('系统管理', "编辑公告弹窗成功(id:$id|action:$txt)");
        return $this->success($txt);
    }
}