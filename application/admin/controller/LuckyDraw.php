<?php
namespace app\admin\controller;

use app\admin\service\NodeService;
use library\Controller;
use library\tools\Data;
use think\Db;
use PHPExcel;//tp5.1用法
use PHPExcel_IOFactory;
use think\facade\Cache;

/**
 * 交易中心
 * Class Users
 * @package app\admin\controller
 */
class LuckyDraw extends Controller
{

    /**
     * 幸运抽奖列表
     *@auth true
     *@menu true
     */
    public function lists()
    {
        $this->title = '幸运抽奖列表';
        $where = [];
        if(input('name/s','')) $where[] = ['xc.name','like','%'.input('name/s','').'%'];

        $user = session('admin_user');

        $this->_query('xy_lucky_draw')
            ->alias('xc')
            ->where($where)
            ->group('xc.id')
            ->order('xc.addtime desc')
            ->page();
    }

    /**
     * 添加幸运抽奖
     *@auth true
     *@menu true
     */
    public function add_lucky_draw()
    {
        if(\request()->isPost()){
            $this->applyCsrfToken();//验证令牌
            $name      = input('post.name/s','');
            $content     = input('post.content/s','');
            $data = [
                'name' => $name,
                'content' => $content,
                'addtime' => time(),
                'status' => 0
            ];
            $res = Db::table('xy_lucky_draw')->insert($data);
            if($res)
                return ['code'=>1,'info'=>'操作成功!'];
            else 
                return ['code'=>0,'info'=>'操作失败!'];
        } 
        return $this->fetch();
    }

    /**
     * 编辑幸运抽奖
     *@auth true
     *@menu true
     */
    public function edit_lucky_draw($id)
    {
        $id = (int)$id;
        if(\request()->isPost()){
            $this->applyCsrfToken();//验证令牌
            $id      = input('post.id/s','');
            $name      = input('post.name/s','');
            $content     = input('post.content/s','');
            $data = [
                'id' => $id,
                'name' => $name,
                'content' => $content,
            ];
            $res = Db::table('xy_lucky_draw')->update($data);
            if($res)
                return ['code'=>1,'info'=>'操作成功!'];
            else 
                return ['code'=>0,'info'=>'未更新任何记录!'];
        } 
        $this->info = db('xy_lucky_draw')->find($id);
        return $this->fetch();
    }

    /**
     * 启动幸运抽奖
     *@auth true
     *@menu true
     */
    public function activate_lucky_draw()
    {
        if(\request()->isPost()){
            $this->applyCsrfToken();
            $data = [
                'id' => input('post.id/d', 0),
                'status' => 1,
            ];
            $res = Db::table('xy_lucky_draw')->update($data);
            if($res)
                return ['code'=>1,'info'=>'操作成功!'];
            else 
                return ['code'=>0,'info'=>'未更新任何记录!'];
        }
    }

    /**
     * 关闭幸运抽奖
     *@auth true
     *@menu true
     */
    public function deactivate_lucky_draw()
    {
        if(\request()->isPost()){
            $this->applyCsrfToken();
            $data = [
                'id' => input('post.id/d', 0),
                'status' => 0,
            ];
            $res = Db::table('xy_lucky_draw')->update($data);
            if($res)
                return ['code'=>1,'info'=>'操作成功!'];
            else 
                return ['code'=>0,'info'=>'未更新任何记录!'];
        }
    }

    /**
     * 幸运抽奖用户报名列表
     *@auth true
     *@menu true
     */
    public function lucky_draw_users($id)
    {
        $id = (int) $id;
        $this->title = '幸运抽奖用户报名列表';
        $info = db('xy_lucky_draw')->find($id);
        if (!$info) {
            return $this->redirect('/admin/lucky_draw/list');
        }
        $this->info = $info;
        $where[] = ['lucky_draw_id','=',$id];
        if(input('username/s','')) $where[] = ['u.username','like','%'.input('username/s','').'%'];
        if(input('tel/s','')) $where[] = ['u.tel','like','%'.input('tel/s','').'%'];
        if(input('invite_code/s','')) $where[] = ['u.invite_code','like','%'.input('invite_code/s','').'%'];

        $user = session('admin_user');

        $this->_query('xy_lucky_draw_users')
            ->alias('xc')
            ->join('xy_users u','u.id = xc.uid')
            ->field('xc.id, u.username, u.tel, u.invite_code, u.addtime AS reg_time, xc.addtime, xc.image_url, xc.code')
            ->where($where)
            ->order('xc.addtime desc')
            ->page();
    }

    public function edit_winner($id)
    {
        $id = (int) $id;
        if (request()->isPost()) {
            $winner_content = input('post.winner_content/s', '');
            $data = [
                'id' => $id,
                'winner_content' => $winner_content
            ];
            $res = db('xy_lucky_draw')->update($data);
            if ($res) {
                return $this->success('操作成功');
            } else {
                return $this->success('未更新任何记录');
            }
        }
    }
}