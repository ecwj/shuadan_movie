<?php
namespace app\admin\controller;

use app\admin\service\NodeService;
use library\Controller;
use library\tools\Data;
use think\Db;

/**
 * 商城
 * Class Users
 * @package app\admin\controller
 */
class Mall extends Controller
{
	/**
     * 商城轮播图
     * @auth true
     * @menu true
     */
    public function banner()
    {
        $this->_query('xy_mall_banner')->page();
    }

    /**
     * 编辑商城轮播图
     * @auth true
     * @menu true
     */
    public function edit_banner($id)
    {
        $id = intval($id);
        if(request()->isPost()){
            $this->applyCsrfToken();
            $id      = input('post.id/d',0);
//            $url   = input('post.url/s', '');
            $image = input('post.image/s', '');

            if(!$image)$this->error('图片为必填项');

            $res = Db::table('xy_mall_banner')->where('id',$id)->update(['image'=>$image]);
//            dump($res);exit;
            if($res!==false){
                $this->success('编辑成功','/admin.html#/admin/mall/banner.html');
            }else
                $this->error('编辑失败');
        }

        $info = Db::table('xy_mall_banner')->find($id);
        $this->assign('info',$info);
        $this->fetch();
    }

    /**
     * 添加商城轮播图
     * @auth true
     * @menu true
     */
    public function add_banner()
    {
        if(request()->isPost()){

//            $this->applyCsrfToken();
//            $url   = input('post.url/s', '');
            $image = input('post.image/s', '');

            //if(!$title)$this->error('标题为必填项');
            //if(mb_strlen($title) > 50)$this->error('标题长度限制为50个字符');
//            if(!$url)$this->error('图片为必填项');

            $res = Db::table('xy_mall_banner')->insert(['image'=>$image]);
            if($res){
                $this->success('提交成功','/admin.html#/admin/mall/banner.html');
            }else
                $this->error('提交失败');
        }
        return $this->fetch();
    }


    public function del_banner()
    {
        $this->applyCsrfToken();
        $id = input('post.id/d',0);
        $res = Db::table('xy_mall_banner')->where('id',$id)->delete();
        if($res)
            $this->success('删除成功!');
        else
            $this->error('删除失败!');
    }
}