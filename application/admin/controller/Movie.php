<?php
namespace app\admin\controller;

use app\admin\service\NodeService;
use library\Controller;
use library\tools\Data;
use think\Db;
use PHPExcel;//tp5.1用法
use PHPExcel_IOFactory;
use think\facade\Cache;

/**
 * 交易中心
 * Class Users
 * @package app\admin\controller
 */
class Movie extends Controller
{

    /**
     * 订单列表
     *@auth true
     *@menu true
     */
    public function order_list()
    {
        $this->title = '订单列表';
        $where = [];
        if(input('oid/s','')) $where[] = ['xc.id','like','%'.input('oid','').'%'];
        if(input('username/s','')) $where[] = ['u.username','like','%' . input('username/s','') . '%'];
        if(input('tel/s',''))$where[] = ['u.tel','like','%' . input('tel/s','') . '%'];
        if(input('is_presale/d','')==-1){$is_presale=0;}else{$is_presale=input('is_presale/d','');}
        if(input('is_presale/d',''))$where[] = ['g.is_presale','=',$is_presale];
        if(input('addtime/s','')){
            $arr = explode(' - ',input('addtime/s',''));
            $where[] = ['xc.addtime','between',[strtotime($arr[0]),strtotime ("+1 day", strtotime($arr[0]))]];
        }

        $user = session('admin_user');
        if($user['authorize']==2  && !empty($user['nodes']) ){
            //获取直属下级
            $mobile = $user['phone'];
            $uid = db('xy_users')->where('tel', $mobile)->value('id');

            $ids1  = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');

            $ids1 ? $ids2  = db('xy_users')->where('parent_id','in', $ids1)->field('id')->column('id') : $ids2 = [];

            $ids2 ? $ids3  = db('xy_users')->where('parent_id','in', $ids2)->field('id')->column('id') : $ids3 = [];

            $ids3 ? $ids4  = db('xy_users')->where('parent_id','in', $ids3)->field('id')->column('id') : $ids4 = [];

            $idsAll = array_merge([$uid],$ids1,$ids2 ,$ids3 ,$ids4);  //所有ids
            $where[] = ['xc.uid','in',$idsAll];


            //echo '<pre>';
            //var_dump($where,$idsAll,$ids3,$ids4);die;
        }

        $this->_query('xy_convey_movie')
            ->alias('xc')
            ->leftJoin('xy_users u','u.id=xc.uid')
            ->leftJoin('xy_movie g','g.id=xc.goods_id')
            ->field('xc.*,u.username,u.tel,u.is_jia,u.is_free,u.parent_id,g.name,g.price,g.name AS goods_name')
            ->where($where)
            ->where('xc.uid != 38')
            ->order('addtime desc')
            ->page();
    }

    /**
     * 电影票管理
     *@auth true
     *@menu true
     */
    public function goods_list()
    {
        $this->title = '电影票管理';

        $where = "";
        //var_dump($this->cate);die;
        $query = $this->_query('xy_movie');
        if(input('title/s',''))$where ="name like '%" . input('title/s','') . "%' OR name_en LIKE '%" . input('title/s','') . "%'";

        $is_presale = input('is_presale/i','');
        if ($is_presale) {
            if ($is_presale == '-1') {
                $where = $where ? $where . " AND is_presale = 0" : 'is_presale = 0';
            } else if ($is_presale == '1') {
                $where = $where ? $where . " AND is_presale = 1" : 'is_presale = 1';
            }
        }
        // if(input('cid/d',''))$where[] = ['cid','=',input('cid/d','')];

        //var_dump($where);die;
        $query->where($where)->order('addtime desc')->page();
    }

    /**
     * 编辑电影信息
     * @auth true
     * @menu true
     */
    public function edit_goods($id)
    {
        $id = (int)$id;
        if(\request()->isPost()){
            $this->applyCsrfToken();//验证令牌
            $name_en      = input('post.name_en/s','');
            $price    = input('post.price/f',0);
            $stock    = input('post.stock/f',0);
            $description_en     = input('post.description_en/s','');
            $people_en     = input('post.people_en/s','');
            $id             = input('post.id/d',0);
            $image_upload = input('image_upload/s','');

            if(!$price)$this->error('电影票价为必填项');
            if(!$stock)$this->error('电影票数库存为必填项');

            $res = model('Movie')->submit_goods($name_en,$description_en,$price,$people_en,$stock,$image_upload,$id);
            if($res['code']===0) {
                Cache::set('movielist', NULL);
                Cache::set('movie_banner', NULL);
                return $this->success($res['info'],'/admin.html#/admin/movie/goods_list.html');
            } else {
                return $this->error($res['info']);
            }
        }
        $info = db('xy_movie')->find($id);
        $this->assign('info',$info);
        return $this->fetch();
    }

    public function order_list_report()
    {
        $this->title = '用户订单报告';
        $where = [];
        if(input('username/s','')) $where[] = ['u.username','like','%' . input('username/s','') . '%'];
        if(input('tel/s',''))$where[] = ['u.tel','like','%' . input('tel/s','') . '%'];
        if(input('addtime/s','')){
            $arr = explode(' - ',input('addtime/s',''));
            $where[] = ['xc.addtime','between',[strtotime($arr[0]),strtotime ("+1 day", strtotime($arr[0]))]];
        }

        $user = session('admin_user');

        $this->_query('xy_convey_movie')
            ->alias('xc')
            ->Join('xy_users u','u.id=xc.uid')
            ->leftJoin('xy_movie g','g.id=xc.goods_id')
            ->field('u.username,u.tel,u.is_jia,u.is_free,u.parent_id,SUM(xc.num) AS total_num, SUM(xc.goods_count) AS total_count ,g.name,g.price,g.name AS goods_name')
            ->where($where)
            ->where("u.username != 'supermovie'")
            ->group('xc.uid')
            ->order('u.id asc')
            ->page();
    }

    /**
     * 编辑电影状态
     * @auth false
     */
    public function edit_goods_status()
    {
        $id = input('id/d',0);
        $status = input('status/d',0);
        if(!$id || !$status) return $this->error('参数错误');
        
        $status = intval($status);
        $id = intval($id);

        if(!in_array($status,[1,2])) return ['code'=>1,'info'=>'参数错误'];

        $res = Db::table('xy_movie')->where('id',$id)->update(['status'=>$status]);

        if($res !== false){
            return $this->success('操作成功');
        }
        $this->error('操作失败');
    }

    /**
     * 添加预售电影
     *@auth true
     *@menu true
     */
    public function add_goods_presale()
    {
        if(\request()->isPost()){
            // try {
                $this->applyCsrfToken();//验证令牌
                $name      = input('post.name/s','');
                $price    = input('post.price/f',0);
                $stock    = input('post.stock/f',0);
                $description     = $_POST['description'];//input('post.description/s','');
                $people     = $_POST['people'];//input('post.people/s','');
                $image_upload = input('image_upload/s','');
                $release_date = input('release_time/s',0);
                $roi_percentage = input('roi_percentage/s', '');
                $min_purchase = input('min_purchase/s', '');
                $max_purchase = input('max_purchase/s', '');
                $playtime = input('playtime/s', '');
                $category = input('category/s', '');

                if(!$name) return $this->error('电影名称为必填项');
                if(!$people) return $this->error('电影演员列表为必填项');
                if(!$image_upload) return $this->error('电影图片为必填项');
                if(!$price) return $this->error('电影票价为必填项');
                if(!$stock) return $this->error('电影票数库存为必填项');
                if(!$release_date) return $this->error('奖励发放时间为必填项');
                if (!$roi_percentage) return $this->error('Roi 巴仙率 为必填项');
                if (!$min_purchase) return $this->error('最低购买票数为必填项');
                // if (!$max_purchase) return $this->error('每人限购票数为必填项');
                if(!$playtime) return $this->error('电影上映时间为必填项');
                if (!$category) return $this->error('电影类型为必填项');

                $levelCount = db('xy_level')->count('id');
                if (sizeof(explode(",", $roi_percentage)) != $levelCount) {
                    return $this->error('Roi 巴仙率 应该有' . $levelCount . '个值');
                }
                if (sizeof(explode(",", $min_purchase)) != $levelCount) {
                    return $this->error('最低购买票数应该有' . $levelCount . '个值');
                }
                if (sizeof(explode(",", $max_purchase)) != $levelCount) {
                    // return $this->error('每人限购票数应该有' . $levelCount . '个值');
                }
                // $data = [
                //     'name' => $name,
                //     'price' => $price,
                //     'stock' => $stock,
                //     'description' => $description,
                //     'people' => $people,
                //     'image_upload' => $image_upload,
                //     'is_presale' => 1,
                //     'release_date' => $release_date,
                //     'roi_percentage' => $roi_percentage,
                //     'addtime' => time(),
                //     'playtime' => $playtime,
                //     'playtime_date' => $playtime,
                //     'category' => $category
                // ];

                $time = time();
                $sql = "INSERT INTO xy_movie (name, playtime, category, description, image_upload, people, addtime, status, price, stock, playtime_date, is_presale, release_date, roi_percentage, min_purchase, max_purchase) VALUES ('$name', '$playtime', '$category', '$description', '$image_upload', '$people', '$time', 1, '$price', '$stock', '$playtime', 1, '$release_date', '$roi_percentage', '$min_purchase', '$max_purchase');";

                $res = Db::execute($sql);

                // $res = db('xy_movie')->insert($data);
                if($res) {
                    Cache::set('movielist', NULL);
                    Cache::set('movie_banner', NULL);
                    return $this->success('预售电影' . $name . '添加成功','/admin.html#/admin/movie/goods_list.html');
                } else {
                    return $this->error('添加失败');
                }
            // } catch (\Exception $ex) {
            //     dump($ex);
            //     exit;
            // }
        }
        return $this->fetch();
    }

    /**
     * 编辑预售电影信息
     * @auth true
     * @menu true
     */
    public function edit_goods_presale($id)
    {
        $id = (int)$id;
        if(\request()->isPost()){
            $this->applyCsrfToken();//验证令牌
            // try {
            $name      = input('post.name/s','');
            $price    = input('post.price/f',0);
            $stock    = input('post.stock/f',0);
            $description     = $_POST['description'];//input('post.description/s','');
            $people     = $_POST['people'];//input('post.people/s','');
            $image_upload = input('image_upload/s','');
            $release_date = input('release_time/s',0);
            $roi_percentage = input('roi_percentage/s', '');
            $min_purchase = input('min_purchase/s', '');
            $max_purchase = input('max_purchase/s', '');
            $playtime = input('playtime/s', '');
            $category = input('category/s', '');

            if(!$name) return $this->error('电影名称为必填项');
            if(!$people) return $this->error('电影演员列表为必填项');
            if(!$image_upload) return $this->error('电影图片为必填项');
            if(!$price) return $this->error('电影票价为必填项');
            if(!$stock) return $this->error('电影票数库存为必填项');
            if(!$release_date) return $this->error('奖励发放时间为必填项');
            if (!$roi_percentage) return $this->error('Roi 巴仙率 为必填项');
            if (!$min_purchase) return $this->error('最低购买票数为必填项');
            // if (!$max_purchase) return $this->error('每人限购票数为必填项');
            if(!$playtime) return $this->error('电影上映时间为必填项');
            if (!$category) return $this->error('电影类型为必填项');

            $levelCount = db('xy_level')->count('id');
            if (sizeof(explode(",", $roi_percentage)) != $levelCount) {
                return $this->error('Roi 巴仙率 应该有' . $levelCount . '个值');
            }
            if (sizeof(explode(",", $min_purchase)) != $levelCount) {
                return $this->error('最低购买票数应该有' . $levelCount . '个值');
            }
            if (sizeof(explode(",", $max_purchase)) != $levelCount) {
                // return $this->error('每人限购票数应该有' . $levelCount . '个值');
            }
            // $data = [
            //     'name' => $name,
            //     'price' => $price,
            //     'stock' => $stock,
            //     'description' => $description,
            //     'people' => $people,
            //     'image_upload' => $image_upload,
            //     'release_date' => $release_date,
            //     'is_presale' => 1,
            //     'roi_percentage' => $roi_percentage,
            //     'addtime' => time(),
            //     'playtime' => $playtime,
            //     'playtime_date' => $playtime,
            //     'category' => $category,
            //     'id' => $id
            // ];

            $time = time();
            $sql = "UPDATE xy_movie SET name = '$name', category = '$category', price = '$price', stock = '$stock', playtime = '$playtime', description = '$description', image_upload = '$image_upload', people = '$people', updatetime = $time, playtime_date = '$playtime', is_presale = '1', release_date = '$release_date', roi_percentage = '$roi_percentage', min_purchase = '$min_purchase', max_purchase = '$max_purchase' WHERE id = '$id';";

            // $res = Db::table('xy_movie')->where('id',$id)->update($data);
            $res = Db::execute($sql);
            if($res) {
                Cache::set('movielist', NULL);
                Cache::set('movie_banner', NULL);
                return $this->success('预售电影' . $name . '编辑成功','/admin.html#/admin/movie/goods_list.html');
            } else {
                return $this->error('编辑失败');
            }
            // } catch (\Exception $ex) {
            //     dump($ex);
            //     exit;
            // }
        }
        $info = db('xy_movie')->find($id);
        $this->assign('info',$info);
        return $this->fetch();
    }
}