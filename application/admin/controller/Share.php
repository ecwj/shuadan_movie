<?php
namespace app\admin\controller;

use app\admin\service\NodeService;
use library\Controller;
use library\tools\Data;
use think\Db;
use PHPExcel;//tp5.1用法
use PHPExcel_IOFactory;
use think\facade\Cache;

/**
 * 交易中心
 * Class Users
 * @package app\admin\controller
 */
class Share extends Controller
{

    /**
     * 分享活动列表
     *@auth true
     *@menu true
     */
    public function index()
    {
        $this->title = '分享活动列表';
        $where = [];
        if(input('tel/s',''))$where[] = ['u.tel','like','%' . input('tel/s','') . '%'];
        if(input('username/s',''))$where[] = ['u.username','like','%' . input('username/s','') . '%'];
        if(input('real_name/s',''))$where[] = ['u.real_name','like','%' . input('real_name/s','') . '%'];
        if(input('c_status/s',''))$where[] = ['xc.c_status','=',input('c_status/s','')];

        if(input('addtime/s','')){
            $arr = explode(' - ',input('addtime/s',''));

            $where[] = ['xc.addtime','between',[strtotime($arr[0]),strtotime ("+1 day", strtotime($arr[0]))]];
        }

        $user = session('admin_user');

        $this->_query('xy_share_log')
            ->alias('xc')
            ->join('xy_users u','u.id = xc.uid')
            ->where($where)
            ->field('xc.*, u.username, u.tel, u.real_name')
            ->group('xc.id')
            ->order('xc.addtime desc')
            ->page();
    }


    /**
     * 通过分享活动提交
     *@auth true
     *@menu true
     */
    public function approve_sharing()
    {
        if(\request()->isPost()){
            $this->applyCsrfToken();
            $id = input('post.id/d', 0);
            $data = [
                'id' => $id,
                'c_status' => 2,
                'updatetime' => time()
            ];
            $info = db('xy_share_log')->find($id);
            if (!$info) {
                return ['code'=>0,'info'=>'查无记录! ID:' . $id];
            }
            
            if($info) {
                $uid = $info['uid'];
                $map = [
                    'date' => date('Y-m-d', strtotime('-1 day')),
                    'uid' => $uid
                ];

                $shareId = $info['id'];

                $time = time();
                $amt = config('share_reward');
                $id = getSn('SY');
                $total = 0;
                $res2 = Db::name('xy_point_log')->insert([
                    //记录积分信息
                    'uid'       => $uid,
                    'sid'       => 0,
                    'oid'       => $id,
                    'num'       => $amt,
                    'type'      => 14,//分享奖励
                    'status'    => 1,
                    'f_lv'      => NULL,
                    'addtime'   => $time
                ]);
                if ($res2) {
                    $total += $amt;
                }
                $count = $info['day_count'];
                if ($count == config('share_reward_special_1_day_count')) {
                    $amt = config('share_reward_special_1');
                    $res2 = Db::name('xy_point_log')->insert([
                        //记录积分信息
                        'uid'       => $uid,
                        'sid'       => 0,
                        'oid'       => $id,
                        'num'       => $amt,
                        'type'      => 15,//满3天分享奖励
                        'status'    => 1,
                        'f_lv'      => NULL,
                        'addtime'   => $time
                    ]); 
                    if ($res2) {
                        $total += $amt;
                    }
                }
                if ($count == config('share_reward_special_2_day_count')) {
                    $amt = config('share_reward_special_2');
                    $res2 = Db::name('xy_point_log')->insert([
                        //记录积分信息
                        'uid'       => $uid,
                        'sid'       => 0,
                        'oid'       => $id,
                        'num'       => $amt,
                        'type'      => 17,//满3天分享奖励
                        'status'    => 1,
                        'f_lv'      => NULL,
                        'addtime'   => $time
                    ]); 
                    if ($res2) {
                        $total += $amt;
                    }
                }
                if ($count == config('share_reward_special_3_day_count')) {
                    $amt = config('share_reward_special_3');
                    $res2 = Db::name('xy_point_log')->insert([
                        //记录积分信息
                        'uid'       => $uid,
                        'sid'       => 0,
                        'oid'       => $id,
                        'num'       => $amt,
                        'type'      => 18,//满3天分享奖励
                        'status'    => 1,
                        'f_lv'      => NULL,
                        'addtime'   => $time
                    ]); 
                    if ($res2) {
                        $total += $amt;
                    }
                }
                //积分
                if ($total > 0) {
                    $res = Db::table('xy_share_log')->update($data);
                    $current_point = Db::name('xy_users')->where('id',$uid)->value('point - freeze_point');
                    Db::name('xy_users')->where('id',$uid)->setInc('point',$amt);
                    Db::name('xy_share_log')->where('id',$shareId)->update(['oid' => $id, 'total_point_collected' => $total, 'updateby' => session('admin_user')['username']]);
                }
                return ['code'=>1,'info'=>'操作成功!'];
            } else {
                return ['code'=>0,'info'=>'未更新任何记录!'];
            }
        }
    }

    /**
     * 驳回分享活动提交
     *@auth true
     *@menu true
     */
    public function reject_sharing()
    {
        if(\request()->isPost()){
            $this->applyCsrfToken();
            $data = [
                'id' => input('post.id/d', 0),
                'c_status' => 3,
                'remark' => input('post.remark/s', ''),
                'updatetime' => time(),
                'updateby' => session('admin_user')['username']
            ];
            $res = Db::table('xy_share_log')->update($data);
            if($res)
                return ['code'=>1,'info'=>'操作成功!'];
            else 
                return ['code'=>0,'info'=>'未更新任何记录!'];
        }
        $this->title = "驳回分享活动提交";
        $id = input('get.id',0);
        if(!$id) $this->error('参数错误');
        $this->info = Db::table('xy_share_log')->find($id);
        $this->user = db('xy_users')->find($this->info['uid']);
        return $this->fetch();
    }

}