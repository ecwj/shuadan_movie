<?php

namespace app\admin\model;

use think\Model;
use think\Db;

class ConveyMovie extends Model
{

    protected $table = 'xy_convey_movie';


    /**
     * 创建订单
     *
     * @param int $uid
     * @return array
     */
    public function create_order($uid,$goodsid=0,$goods_count=1)
    {
        $uinfo = Db::name('xy_users')->field('deal_status,balance,level')->find($uid);
        if($uinfo['deal_status']!=2) return ['code'=>1,'info'=>'Match terminated'];
        
        $goods = db('xy_movie')->find($goodsid);
        
        if (!$goods) {
            return ['code' => 1, 'info' => lang('The movie does not exist')];
        }
        if ($goods['price'] <= 0) {
            return ['code' => 1, 'info' => lang('Movie ticket price error')];
        }
        if ($goods['playtime_date'] < config('movie_start_date')) {
            return ['code' => 1, 'info' => lang('Only pre-sale movies can be purchased')];   
        }

        $usedStock = db('xy_convey_movie')->where('status NOT IN (2, 4) AND goods_id = ' . $goodsid)->sum('goods_count');
        if (($goods['stock'] - $usedStock) < $goods_count) {
            return ['code' => 1, 'info' => lang('Insufficient inventory of movie tickets')];
        }
        $id = getSn('DY');
         
       if ($this->where('id',$id)->field('id')->lock(true)->find()) {
            return ['code' => 1, 'info' => 'Repeat orders submit!'];
        }
        
        Db::startTrans();
        $res = Db::name('xy_users')->where('id',$uid)->update(['deal_time'=>time(),'deal_count'=>Db::raw('deal_count+1')]);//将账户状态改为交易中
        $total_num = $goods['price'] * $goods_count;
        if($total_num > $uinfo['balance']) return ['code'=>1,'info'=>lang('The available balance of the account is insufficient')];        

        /*$res1 = Db::name($this->table)
            ->insert([
                'id'            => $id,
                'uid'           => $uid,
                'num'           => $total_num,
                'addtime'       => time(),
                // 'endtime'       => time()+config('deal_timeout'),
//                    'add_id'        => $add_id,
                'status' => 1,
                'goods_id'      => $goods['id'],
                'goods_count'   => $goods_count,
                'commission'    => 0,  //交易佣金按照分类,
                'release_time' => (!is_null($goods['release_date']) && $goods['release_date'] != 0 ? strtotime($goods['release_date']) : 0)
            ]);*/
        $time = time();
        $release_time = (!is_null($goods['release_date']) && $goods['release_date'] != 0 ? strtotime($goods['release_date']) : 0);
        $sql = "INSERT INTO " . $this->table . " (id, uid, num, addtime, status, goods_id, goods_count, commission, release_time) VALUES ('$id', '$uid', '$total_num', '$time', 1, '" . $goods['id'] . "', '$goods_count', 0, '$release_time')";
        $res1 = Db::execute($sql);
        if($res && $res1){

            try {
                $res1 = Db::name('xy_users')
                ->where('id', $uid)
                ->dec('balance',$total_num)
                ->update(['deal_status' => 1,'status'=>1]);
            } catch (\Throwable $th) {
                Db::rollback();
                dump($th);
                return ['code'=>1,'info'=>lang('The available balance of the account is insufficient')];
            }            

            $res2 = Db::name('xy_balance_log')->insert([
                'uid'           => $uid,
                'oid'           => $id,
                'num'           => $total_num,
                'type'          => 2,
                'status'        => 2,
                'addtime'       => time()
            ]);

            Db::commit();

            $remark = "";
            $map = [
                'uid' => $uid,
                'type' => 20,//体验金
            ];
            $freeCredit = db('xy_balance_log')->where($map)->value('SUM(IF(status = 1, num, -num)) AS num');
            $map = [
                'uid' => $uid,
                'status' => 1
            ];
            $usedFreeCredit = db('xy_convey_movie')->where($map)->where("remark LIKE '%free_credit%'")->value('SUM(num) AS num');
            $balanceFreeCredit = $freeCredit - $usedFreeCredit;
            if ($balanceFreeCredit >= $total_num) {
                $remark = 'free_credit|' . $total_num;
            } else if ($total_num >= $balanceFreeCredit) {
                $remark = 'free_credit|' . ($balanceFreeCredit);
            }
            if ($remark != '') {
                Db::name('xy_convey_movie')->where('id', $id)->update(['remark' => $remark]);
            }

            //update userbalance
            $balance = db('xy_balance_log')->where('uid', $uid)->field('sum(if(status = 1, num,-num)) as total')->find();
            if ($balance) {
                $res1 = Db::name('xy_users')
                    ->where('id', $uid)
                    ->update(['balance' => $balance['total']]);
            }
            
            return ['code'=>0,'info'=>lang('Purchase successfully!'),'oid'=>$id];
        }else{
            Db::rollback();
            return ['code'=>1,'info'=>lang('Purchase failed!')];
        }
    }
}