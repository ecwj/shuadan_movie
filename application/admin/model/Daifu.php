<?php

namespace app\admin\model;

use think\Model;
use think\Db;

class Daifu extends Model
{
	public function pay_1($oinfo, &$result) {
		$bank_arr = [
            '中国银行' => 1,
            '招商银行' => 2,
            '民生银行' => 3,
            '建设银行' => 5,
            '农业银行' => 6,
            '中国邮政储蓄银行' => 7,
            '工商银行' => 8,
            '交通银行' => 9,
            '华夏银行' => 10,
            '中信银行' => 11,
            '平安银行' => 12,
            '浦发银行' => 13,
            '光大银行' => 14,
            '兴业银行' => 15,
            '北京银行' => 16,
            '广发银行' => 17,
            '温州银行' => 18,
            '宁波银行' => 19,
            '浙江稠州银行' => 20,
            '台州银行' => 21,
            '浙江民泰银行' => 22,
            '浙商銀行' => 23,
            '上海銀行' => 24,
            '苏州銀行' => 25,
            '江苏银行' => 26,
            '南京银行' => 27,
            '晋中银行' => 28,
            '厦门銀行' => 29,
            '浙江省农村信用社' => 30,
            '广州银行' => 31,
            '恒丰银行' => 32,
            '渤海银行' => 33,
            '农村信用合作社' => 34,
       ];
       $url = 'http://api.jfmg168.com/withdrawal/creatWithdrawal';
       $bankinfo = Db::name('xy_bankinfo')->where('id',$oinfo['bk_id'])->find();

        $protocol = 'http';
        if (isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] != '127.0.0.1') {
            $protocol = 'https';
        }

        $params = [
            'appid' => '1010674',
            'account' => $bankinfo['cardnum'],
            'money' => number_format($oinfo['num'], 2, '.', ''),
            'name' => $bankinfo['username'],
            'bank_type' => 2,
            'bank_id' => $bank_arr[$bankinfo['bankname']],
            'callback' => "$protocol://".$_SERVER['HTTP_HOST']."/index/api2/daifu_callback",
            'out_trade_no' => $oinfo['id'],
        ];
        $params['sign'] = $this->generateSign2($params,'YnOExRZkbwF5C61qyRgllUP1LCQUzfjQ');
        // dump(json_encode($params));
        $result = $this->post_json($url,$params);
	}
}
