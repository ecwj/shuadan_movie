<?php

namespace app\admin\model;

use think\Model;
use think\Db;

class GoodsList extends Model
{
    public function __construct()
    {
        // date_default_timezone_set("Asia/Calcutta");
    }

    protected $tabel = 'xy_goods_list';

    /**
     * 添加商品
     *
     * @param string $shop_name
     * @param string $goods_name
     * @param string $goods_price
     * @param string $goods_pic
     * @param string $goods_info
     * @param string $id 传参则更新数据,不传则写入数据
     * @return array
     */
    public function submit_goods($shop_name,$goods_name,$goods_price,$goods_pic,$goods_info,$cat_id,$id='')
    {
        if(!$goods_pic) return ['code'=>1,'info'=>('请上传商品图片')];
        // $goods_pic =ltrim(parse_url($goods_pic,PHP_URL_PATH),'');
        $goods_pic =ltrim($goods_pic,'');
        if(!$goods_name) return ['code'=>1,'info'=>('请输入商品名称')];
        if(!$shop_name) return ['code'=>1,'info'=>('请输入店铺名称')];
        if(!$goods_price) return ['code'=>1,'info'=>('请填写正确的商品价格')];
        $data = [
            'shop_name'     => $shop_name,
            'goods_name'    => $goods_name,
            'goods_price'   => $goods_price,
            'goods_pic'     => $goods_pic,
            'goods_info'    => $goods_info,
            // 'cid' => 1,
            'cat_id'    => $cat_id,
            'addtime'       => time()
        ];
        if ($id == '') {
            $data['cid'] = 1;
            $data['status'] = 1;
            $data['shop_status'] = 1;
            $data['price_status'] = 1;
        }
        if(!$id){
            $res = Db::table('xy_goods_list_cn')->insert($data);
        }else{
            $res = Db::table('xy_goods_list_cn')->where('id',$id)->update($data);
        }
        if($res)
            return ['code'=>0,'info'=>'操作成功!'];
        else 
            return ['code'=>1,'info'=>'操作失败!'];
    }
}