<?php

namespace app\admin\model;

use think\Model;
use think\Db;

class Movie extends Model
{
    public function __construct()
    {
        // date_default_timezone_set("Asia/Calcutta");
    }

    protected $tabel = 'xy_movie';

    /**
     * 编辑电影信息
     *
     * @param string $name_en
     * @param string $description_en
     * @param string $price
     * @param string $people_en
     * @param string $id 传参则更新数据,不传则写入数据
     * @return array
     */
    public function submit_goods($name_en,$description_en,$price,$people_en,$stock,$image_upload,$id='')
    {
        $data = [];
        if ($name_en) {
            $data['name_en'] = $name_en;
        }
        if ($description_en) {
            $data['description_en'] = $description_en;
        }
        if ($price) {
            $data['price'] = $price;
        }
        if ($people_en) {
            $data['people_en'] = $people_en;
        }

        $data['stock'] = $stock;

        if ($image_upload) {
            $data['image_upload'] = $image_upload;
        }
        
        $res = 0;
        if ($data != []) {
            $data['id'] = $id;
            $res = Db::table('xy_movie')->where('id',$id)->update($data);
        }
            
        if($res)
            return ['code'=>0,'info'=>'操作成功!'];
        else 
            return ['code'=>1,'info'=>'操作失败!'];
    }
}