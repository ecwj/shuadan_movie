<?php

namespace app\admin\model;

use think\Image;
use think\Model;
use think\Db;
use think\facade\Cache;

class Users extends Model
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Calcutta");
    }
    protected $table = 'xy_users';
    protected $rule = [
                    'tel'           => 'require|mobile',
                    'username'      => 'require|length:3,15',
                    'pwd'           => 'require|length:6,16',
                    '__token__'     => 'token',
                    ];
    protected $info = [
                    'tel.require'       => '手机号不能为空！',
//                    'tel.mobile'        => '手机号格式错误！',
                    'username.length'   => '用户名长度为3-10字符！',
                    'username.require'  => '用户名不能为空！',
                    'pwd.require'       => '密码不能为空！',
                    'pwd.length'        => '密码长度为6-16位字符！',
                    '__token__'         => '令牌已过期，请刷新页面再试！',
                    ];
    /**
     * 添加会员
     *
     * @param string $tel
     * @param string $user_name
     * @param string $pwd
     * @param int    $parent_id
     * @param string $token
     * @return array
     */
    public function add_users($tel,$user_name,$pwd,$parent_id,$token='',$pwd2='',$email='',$country_code='',$real_name='')
    {
        if(is_numeric($user_name)) {
            // return ['code'=>1,'info'=>lang('The user name cannot be a pure number')];
        }
//        if(mb_strlen($user_name)>6 || mb_strlen($user_name)<3) {
//            return ['code'=>1,'info'=>lang('combination of letters and numbers (3-6 characters)')];
//        }
        if (!ctype_alnum($user_name)) {
            return ['code'=>1,'info'=>lang('用户名仅允许使用字母数字')];
        }
        $tmp = Db::table($this->table)->where(['tel'=>$tel])->count();
        if($tmp){
            return ['code'=>1,'info'=>lang('The phone number is already registered')];
        }
        $tmp = Db::table($this->table)->where(['username'=>$user_name])->count();
        if($tmp){
            return ['code'=>1,'info'=>lang('Duplicate username')];
        }
        if(!$user_name) $user_name=get_username();
        $data = [
            'tel'           => $tel,
            'country_code' => $country_code,
            'username'      => $user_name,
            'pwd'           => $pwd,
            'parent_id'     => $parent_id,
            'headpic'       => '/static_new6/headimg/'.rand(0,184).'.png',
            'email' => $email,
            'real_name' => $real_name
        ];
        if($token) $data['__token__'] = $token;

        //验证表单
        $validate   = \Validate::make($this->rule,$this->info);
//        if (!$validate->check($data)) {
//            return ['code'=>1,'info'=>$validate->getError()];
//        }
        if($parent_id){
            $parent_id = Db::table($this->table)->where('id',$parent_id)->value('id');
            if(!$parent_id){
                return ['code'=>1,'info'=>'The parent ID does not exist'];
            }  
        }
        
        $salt = rand(0,99999);  //生成盐
        $invite_code = self::create_invite_code();//生成邀请码

        $data['pwd'] = sha1($pwd.$salt.config('pwd_str'));
        $data['salt'] = $salt;
        $data['addtime'] = time();
        $data['invite_code'] = $invite_code;
        $data['level'] = 1;
        if($pwd2){
            $salt2 = rand(0,99999);  //生成盐
            $data['pwd2'] = sha1($pwd2.$salt2.config('pwd_str'));
            $data['salt2'] = $salt2;
        }
        //开启事务
        unset($data['__token__']);
        Db::startTrans();
        $res = Db::table($this->table)->insertGetId($data);
        if($parent_id){
            $res2 = Db::table($this->table)->where('id',$data['parent_id'])->update(['childs'=>Db::raw('childs+1'),'deal_reward_count'=>Db::raw('deal_reward_count+'.config('deal_reward_count'))]);
        }else{
            $res2 = true;
        }
        //生成二维码
        // self::create_qrcode($invite_code,$res);

        if($res && $res2){
            // 提交事务            
            Db::commit();

            if($parent_id){
                $this->add_son_userdata($res,$parent_id);
            }
            db('xy_users')->where('id', $res)->update(['down_userid' => $res]);

            return ['code'=>0,'info'=>lang('Operation successful')];
        }else
            // 回滚事务
            Db::rollback();
            return ['code'=>1,'info'=>lang('Operation failed')];
    }
    /**
     * 添加免费试玩会员
     *
     * @param string $tel
     * @param string $user_name
     * @param string $pwd
     * @param int    $parent_id
     * @param string $token
     * @return array
     */
    public function add_freeusers($tel,$user_name,$pwd,$parent_id,$token='',$pwd2='',$session_id='')
    {
        $tmp = Db::table($this->table)->where(['tel'=>$tel])->count();
        if($tmp){
            return ['code'=>1,'info'=>'请重新申请'];
        }
        $tmp = Db::table($this->table)->where(['username'=>$user_name])->count();
        if($tmp){
            return ['code'=>1,'info'=>'请重新申请'];
        }
        if(!$user_name) $user_name=get_username();
        $data = [
            'tel'           => $tel,
            'username'      => $user_name,
            'pwd'           => $pwd,
            'parent_id'     => $parent_id,
            'is_jia'        => 1,
            'is_free'       => 1,
            'balance'       => 2000,
            'headpic'       => '/static_new6/headimg/'.rand(0,184).'.png',
            'session_id'    => $session_id,
        ];
        if($token) $data['__token__'] = $token;

        //验证表单
        $validate   = \Validate::make($this->rule,$this->info);
        if (!$validate->check($data)) {
            return ['code'=>1,'info'=>$validate->getError()];
        }
        if($parent_id){
            $parent_id = Db::table($this->table)->where('id',$parent_id)->value('id');
            if(!$parent_id){
                return ['code'=>1,'info'=>'上级ID不存在'];
            }  
        }
        
        $salt = rand(0,99999);  //生成盐
        $invite_code = self::create_invite_code();//生成邀请码

        $data['pwd'] = sha1($pwd.$salt.config('pwd_str'));
        $data['salt'] = $salt;
        $data['addtime'] = time();
        $data['invite_code'] = $invite_code;
        if($pwd2){
            $salt2 = rand(0,99999);  //生成盐
            $data['pwd2'] = sha1($pwd2.$salt2.config('pwd_str'));
            $data['salt2'] = $salt2;
        }
        //开启事务
        unset($data['__token__']);
        Db::startTrans();
        $res = Db::table($this->table)->insertGetId($data);
        if($parent_id){
            $res2 = Db::table($this->table)->where('id',$data['parent_id'])->update(['childs'=>Db::raw('childs+1'),'deal_reward_count'=>Db::raw('deal_reward_count+'.config('deal_reward_count'))]);
        }else{
            $res2 = true;
        }
        //生成二维码
        self::create_qrcode($invite_code,$res);

        if($res && $res2){
            // 提交事务
            Db::commit();
            return ['code'=>0,'info'=>'操作成功'];
        }else
            // 回滚事务
            Db::rollback();
            return ['code'=>1,'info'=>'操作失败'];
    }
    /**
     * 编辑用户
     *
     * @param int       $id
     * @param string    $tel
     * @param string    $user_name
     * @param string    $pwd
     * @param int       $parent_id
     * @param string    $token
     * @return array
     */
    public function edit_users($id,$tel,$user_name,$pwd,$parent_id,$balance,$freeze_balance,$token,$real_name,$nickname,$pwd2=''){
        $tmp = Db::table($this->table)->where(['tel'=>$tel])->where('id','<>',$id)->count();
        if($tmp){
            return ['code'=>1,'info'=>'手机号码已注册'];
        }
        $data = [
            'tel'               => $tel,
            'balance'           => $balance,
            'freeze_balance'    => $freeze_balance,
            'username'          => $user_name,
            // 'parent_id'         => $parent_id,
            '__token__'         => $token,
            'real_name' => $real_name,
            'nickname' => $nickname
        ];
        if($pwd){
            //不提交密码则不改密码
            $data['pwd'] = $pwd;
        }else{
            $this->rule['pwd'] = '';
        }
        if($parent_id){
            $parent_id = Db::table($this->table)->where('id',$parent_id)->value('id');
            if(!$parent_id){
                return ['code'=>1,'info'=>'上级ID不存在'];
            }  
            $data['parent_id'] = $parent_id;
        }

//        $validate   = \Validate::make($this->rule,$this->info);//验证表单
//        if (!$validate->check($data)) return ['code'=>1,'info'=>$validate->getError()];

        if($pwd){
            $salt = rand(0,99999); //生成盐
            $data['pwd']    = sha1($pwd.$salt.config('pwd_str'));
            $data['salt']   = $salt;
        }
        if($pwd2){
            $salt2 = rand(0,99999); //生成盐
            $data['pwd2']    = sha1($pwd2.$salt2.config('pwd_str'));
            $data['salt2']   = $salt2;
        }


        unset($data['__token__']);
        $res = Db::table($this->table)->where('id',$id)->update($data);
        if($res)
            return ['code'=>0,'info'=>'编辑成功'];
        else
            return ['code'=>1,'info'=>'操作失败'];
    }

    public function edit_users_status($id,$status)
    {
        $status = intval($status);
        $id = intval($id);

        if(!in_array($status,[1,2])) return ['code'=>1,'info'=>'参数错误'];

        if($status == 2){
            //查看有无未完成的订单
            // if($num > 0)$this->error('该用户尚有未完成的支付订单！');
        }

        $res = Db::table($this->table)->where('id',$id)->update(['status'=>$status]);
        if($res !== false)
            return ['code'=>0,'info'=>'操作成功'];
        else
            return ['code'=>1,'info'=>'操作失败'];
    }

    //生成邀请码
    public static function create_invite_code(){
        $str = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
        $str = '1234567890';
        $rand_str = substr(str_shuffle($str),0,6);
        $num = Db::table('xy_users')->where('invite_code',$rand_str)->count();
        if($num)
            // return $this->create_invite_code();
            return self::create_invite_code();
        else
            return $rand_str;
    }

    //生成用户二维码
    public static function create_qrcode($invite_code,$user_id){ 
        $n = ($user_id%20);

        $dir = './upload/qrcode/user/'.$n . '/' . $user_id . '.png';
//        if(file_exists($dir)) {
//            return;
////            unlink($dir);
//        }
        $share_url = config('share_url');

//SITE_URL
        $qrCode = new \Endroid\QrCode\QrCode($share_url . url('@index/user/register/invite_code/'.$invite_code));
        //设置前景色
        $qrCode->setForegroundColor(['r' => 0, 'g' => 0, 'b' =>0, 'a' => 0]);
        //设置背景色
        $qrCode->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
        //设置二维码大小x
        $qrCode->setSize(230);
        $qrCode->setPadding(5);
        $qrCode->setLogoSize(40);
        $qrCode->setLabelFontSize(14);
        $qrCode->setLabelHalign(100);

        $dir = './upload/qrcode/user/'.$n;
        if(!file_exists($dir)) {
            mkdir($dir, 0777,true);
        }
        $time = time();
        $qrCode->save($dir . '/' . $user_id.$time . '.png');

        $qr = \Env::get('root_path').'public/upload/qrcode/user/' . $n . '/' . $user_id.$time. '.png';
        $bgimg1 = \Env::get('root_path').'/public/public/img/userqr1.png';
//        $qr = '/www/wwwroot/www.ruifengdeyong.com/public/upload/qrcode/user/' . $n . '/' . $user_id.$time. '.png';
//        $bgimg1 ='/www/wwwroot/www.ruifengdeyong.com/public/public/img/userqr1.png';
//        dump($bgimg1);exit;
        $image = \think\Image::open($bgimg1);
        $image->water($qr,[255,743])->text($invite_code,\Env::get('root_path').'public/public/fz.TTF',22,'#ffffff',[(678-(24*strlen($user_id)))/2,685])->save(\Env::get('root_path').'public/upload/qrcode/user/'.$n.'/'.$user_id.'-1.png');

       return $time;
    }

    /**
     * 重置密码
     */
    public function reset_pwd($tel,$pwd,$type=1)
    {
        $data = [
            'tel'   => $tel,
            'pwd'   => $pwd,
        ];
        unset($this->rule['username']);
//        $validate   = \Validate::make($this->rule,$this->info);//验证表单
//        if (!$validate->check($data)) return ['code'=>1,'info'=>$validate->getError()];

        $user_id = Db::table($this->table)->where(['tel'=>$tel])->value('id');
        if(!$user_id){
            return ['code'=>1,'info'=>'用户不存在'];
        }
        
        $salt = mt_rand(0,99999);  
        if($type == 1){
            $data = [
                'pwd'       => sha1($pwd.$salt.config('pwd_str')),
                'salt'      => $salt,
            ];
        }elseif($type == 2){
            $data = [
                'pwd2'       => sha1($pwd.$salt.config('pwd_str')),
                'salt2'      => $salt,
            ];
        }

        $res = Db::table($this->table)->where('id',$user_id)->data($data)->update();

        if($res)
            return ['code'=>0,'info'=>lang('Operation successful')];
        else
            return ['code'=>1,'info'=>'error'];

    }

    //获取上级会员
    public function parent_user($uid,$num=1,$lv=1)
    {
        $pid = db($this->table)->where('id',$uid)->value('parent_id');
        $uinfo = db($this->table)->where('id',$pid)->find();
        if($uinfo){
            if($uinfo['parent_id']&&$num>1) $data = self::parent_user($uinfo['id'],$num-1,$lv+1);
            $data[] = ['id'=>$uinfo['id'],'pid'=>$uinfo['parent_id'],'lv'=>$lv,'status'=>$uinfo['status']];
            return $data;
        }
        return false;
    }


    //获取下级会员
    public function child_user($uid,$num=1,$lv=1)
    {

        $data=[];
        $where = [];
        if ($num ==1) {
            $where[] = ['parent_id','=',$uid];
            $data = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');

        }else if ($num == 2) {
            //二代
            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $where[] = ['parent_id','in',$ids1] : $where[] = ['parent_id','in',[-1]];
            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$data) : $data;
        }else if ($num == 3) {
            //三代

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where[] = ['parent_id','in',$ids2] : $where[] = ['parent_id','in',[-1]];
            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2, $data) : $data;
        }else if ($num == 4) {
            //四带
            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where[] = ['parent_id','in',$ids3] : $where[] = ['parent_id','in',[-1]];
            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3, $data): $data;

        }else if ($num == 5) {
            //五带
            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where[] = ['parent_id','in',$ids4] : $where[] = ['parent_id','in',[-1]];
            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4, $data) :$data;
        }else if ($num == 6) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where[] = ['parent_id','in',$ids5] : $where[] = ['parent_id','in',[-1]];

            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5, $data) :$data;
        }else if ($num == 7) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where5[] = ['parent_id','in',$ids5] : $where5[] = ['parent_id','in',[-1]];
            $ids6 = db('xy_users')->where($where5)->field('id')->column('id');
            $ids6 ? $where[] = ['parent_id','in',$ids6] : $where[] = ['parent_id','in',[-1]];
            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5,$ids6, $data) :$data;
        }else if ($num == 8) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where5[] = ['parent_id','in',$ids5] : $where5[] = ['parent_id','in',[-1]];
            $ids6 = db('xy_users')->where($where5)->field('id')->column('id');
            $ids6 ? $where6[] = ['parent_id','in',$ids6] : $where6[] = ['parent_id','in',[-1]];
            $ids7 = db('xy_users')->where($where6)->field('id')->column('id');
            $ids7 ? $where[] = ['parent_id','in',$ids7] : $where[] = ['parent_id','in',[-1]];

            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5,$ids6,$ids7, $data) :$data;
        }else if ($num == 9) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where5[] = ['parent_id','in',$ids5] : $where5[] = ['parent_id','in',[-1]];
            $ids6 = db('xy_users')->where($where5)->field('id')->column('id');
            $ids6 ? $where6[] = ['parent_id','in',$ids6] : $where6[] = ['parent_id','in',[-1]];
            $ids7 = db('xy_users')->where($where6)->field('id')->column('id');
            $ids7 ? $where7[] = ['parent_id','in',$ids7] : $where7[] = ['parent_id','in',[-1]];
            $ids8 = db('xy_users')->where($where7)->field('id')->column('id');
            $ids8 ? $where[] = ['parent_id','in',$ids8] : $where[] = ['parent_id','in',[-1]];

            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5,$ids6,$ids7,$ids8, $data) :$data;
        }else if ($num == 10) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where5[] = ['parent_id','in',$ids5] : $where5[] = ['parent_id','in',[-1]];
            $ids6 = db('xy_users')->where($where5)->field('id')->column('id');
            $ids6 ? $where6[] = ['parent_id','in',$ids6] : $where6[] = ['parent_id','in',[-1]];
            $ids7 = db('xy_users')->where($where6)->field('id')->column('id');
            $ids7 ? $where7[] = ['parent_id','in',$ids7] : $where7[] = ['parent_id','in',[-1]];
            $ids8 = db('xy_users')->where($where7)->field('id')->column('id');
            $ids8 ? $where8[] = ['parent_id','in',$ids8] : $where8[] = ['parent_id','in',[-1]];
            $ids9 = db('xy_users')->where($where8)->field('id')->column('id');
            $ids9 ? $where[] = ['parent_id','in',$ids9] : $where[] = ['parent_id','in',[-1]];

            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5,$ids6,$ids7,$ids8,$ids9, $data) :$data;
        }else if ($num == 11) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where5[] = ['parent_id','in',$ids5] : $where5[] = ['parent_id','in',[-1]];
            $ids6 = db('xy_users')->where($where5)->field('id')->column('id');
            $ids6 ? $where6[] = ['parent_id','in',$ids6] : $where6[] = ['parent_id','in',[-1]];
            $ids7 = db('xy_users')->where($where6)->field('id')->column('id');
            $ids7 ? $where7[] = ['parent_id','in',$ids7] : $where7[] = ['parent_id','in',[-1]];
            $ids8 = db('xy_users')->where($where7)->field('id')->column('id');
            $ids8 ? $where8[] = ['parent_id','in',$ids8] : $where8[] = ['parent_id','in',[-1]];
            $ids9 = db('xy_users')->where($where8)->field('id')->column('id');
            $ids9 ? $where9[] = ['parent_id','in',$ids9] : $where9[] = ['parent_id','in',[-1]];
            $ids10 = db('xy_users')->where($where9)->field('id')->column('id');
            $ids10 ? $where[] = ['parent_id','in',$ids10] : $where[] = ['parent_id','in',[-1]];

            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5,$ids6,$ids7,$ids8,$ids9,$ids10, $data) :$data;
        }else if ($num == 12) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where5[] = ['parent_id','in',$ids5] : $where5[] = ['parent_id','in',[-1]];
            $ids6 = db('xy_users')->where($where5)->field('id')->column('id');
            $ids6 ? $where6[] = ['parent_id','in',$ids6] : $where6[] = ['parent_id','in',[-1]];
            $ids7 = db('xy_users')->where($where6)->field('id')->column('id');
            $ids7 ? $where7[] = ['parent_id','in',$ids7] : $where7[] = ['parent_id','in',[-1]];
            $ids8 = db('xy_users')->where($where7)->field('id')->column('id');
            $ids8 ? $where8[] = ['parent_id','in',$ids8] : $where8[] = ['parent_id','in',[-1]];
            $ids9 = db('xy_users')->where($where8)->field('id')->column('id');
            $ids9 ? $where9[] = ['parent_id','in',$ids9] : $where9[] = ['parent_id','in',[-1]];
            $ids10 = db('xy_users')->where($where9)->field('id')->column('id');
            $ids10 ? $where10[] = ['parent_id','in',$ids10] : $where10[] = ['parent_id','in',[-1]];
            $ids11 = db('xy_users')->where($where10)->field('id')->column('id');
            $ids11 ? $where[] = ['parent_id','in',$ids11] : $where[] = ['parent_id','in',[-1]];

            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5,$ids6,$ids7,$ids8,$ids9,$ids10,$ids11, $data) :$data;
        }else if ($num == 12) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where5[] = ['parent_id','in',$ids5] : $where5[] = ['parent_id','in',[-1]];
            $ids6 = db('xy_users')->where($where5)->field('id')->column('id');
            $ids6 ? $where6[] = ['parent_id','in',$ids6] : $where6[] = ['parent_id','in',[-1]];
            $ids7 = db('xy_users')->where($where6)->field('id')->column('id');
            $ids7 ? $where7[] = ['parent_id','in',$ids7] : $where7[] = ['parent_id','in',[-1]];
            $ids8 = db('xy_users')->where($where7)->field('id')->column('id');
            $ids8 ? $where8[] = ['parent_id','in',$ids8] : $where8[] = ['parent_id','in',[-1]];
            $ids9 = db('xy_users')->where($where8)->field('id')->column('id');
            $ids9 ? $where9[] = ['parent_id','in',$ids9] : $where9[] = ['parent_id','in',[-1]];
            $ids10 = db('xy_users')->where($where9)->field('id')->column('id');
            $ids10 ? $where10[] = ['parent_id','in',$ids10] : $where10[] = ['parent_id','in',[-1]];
            $ids11 = db('xy_users')->where($where10)->field('id')->column('id');
            $ids11 ? $where11[] = ['parent_id','in',$ids11] : $where11[] = ['parent_id','in',[-1]];
            $ids12 = db('xy_users')->where($where11)->field('id')->column('id');
            $ids12 ? $where[] = ['parent_id','in',$ids12] : $where[] = ['parent_id','in',[-1]];

            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5,$ids6,$ids7,$ids8,$ids9,$ids10,$ids11,$ids12, $data) :$data;
        }else if ($num == 13) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where5[] = ['parent_id','in',$ids5] : $where5[] = ['parent_id','in',[-1]];
            $ids6 = db('xy_users')->where($where5)->field('id')->column('id');
            $ids6 ? $where6[] = ['parent_id','in',$ids6] : $where6[] = ['parent_id','in',[-1]];
            $ids7 = db('xy_users')->where($where6)->field('id')->column('id');
            $ids7 ? $where7[] = ['parent_id','in',$ids7] : $where7[] = ['parent_id','in',[-1]];
            $ids8 = db('xy_users')->where($where7)->field('id')->column('id');
            $ids8 ? $where8[] = ['parent_id','in',$ids8] : $where8[] = ['parent_id','in',[-1]];
            $ids9 = db('xy_users')->where($where8)->field('id')->column('id');
            $ids9 ? $where9[] = ['parent_id','in',$ids9] : $where9[] = ['parent_id','in',[-1]];
            $ids10 = db('xy_users')->where($where9)->field('id')->column('id');
            $ids10 ? $where10[] = ['parent_id','in',$ids10] : $where10[] = ['parent_id','in',[-1]];
            $ids11 = db('xy_users')->where($where10)->field('id')->column('id');
            $ids11 ? $where11[] = ['parent_id','in',$ids11] : $where11[] = ['parent_id','in',[-1]];
            $ids12 = db('xy_users')->where($where11)->field('id')->column('id');
            $ids12 ? $where12[] = ['parent_id','in',$ids12] : $where12[] = ['parent_id','in',[-1]];
            $ids13 = db('xy_users')->where($where12)->field('id')->column('id');
            $ids13 ? $where[] = ['parent_id','in',$ids13] : $where[] = ['parent_id','in',[-1]];

            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5,$ids6,$ids7,$ids8,$ids9,$ids10,$ids11,$ids12,$ids13, $data) :$data;
        } else if ($num == 14) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where5[] = ['parent_id','in',$ids5] : $where5[] = ['parent_id','in',[-1]];
            $ids6 = db('xy_users')->where($where5)->field('id')->column('id');
            $ids6 ? $where6[] = ['parent_id','in',$ids6] : $where6[] = ['parent_id','in',[-1]];
            $ids7 = db('xy_users')->where($where6)->field('id')->column('id');
            $ids7 ? $where7[] = ['parent_id','in',$ids7] : $where7[] = ['parent_id','in',[-1]];
            $ids8 = db('xy_users')->where($where7)->field('id')->column('id');
            $ids8 ? $where8[] = ['parent_id','in',$ids8] : $where8[] = ['parent_id','in',[-1]];
            $ids9 = db('xy_users')->where($where8)->field('id')->column('id');
            $ids9 ? $where9[] = ['parent_id','in',$ids9] : $where9[] = ['parent_id','in',[-1]];
            $ids10 = db('xy_users')->where($where9)->field('id')->column('id');
            $ids10 ? $where10[] = ['parent_id','in',$ids10] : $where10[] = ['parent_id','in',[-1]];
            $ids11 = db('xy_users')->where($where10)->field('id')->column('id');
            $ids11 ? $where11[] = ['parent_id','in',$ids11] : $where11[] = ['parent_id','in',[-1]];
            $ids12 = db('xy_users')->where($where11)->field('id')->column('id');
            $ids12 ? $where12[] = ['parent_id','in',$ids12] : $where12[] = ['parent_id','in',[-1]];
            $ids13 = db('xy_users')->where($where12)->field('id')->column('id');
            $ids13 ? $where13[] = ['parent_id','in',$ids13] : $where13[] = ['parent_id','in',[-1]];
            $ids14 = db('xy_users')->where($where13)->field('id')->column('id');
            $ids14 ? $where[] = ['parent_id','in',$ids1] : $where[] = ['parent_id','in',[-1]];

            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5,$ids6,$ids7,$ids8,$ids9,$ids10,$ids11,$ids12,$ids13,$ids14, $data) :$data;
        }else if ($num == 15) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where5[] = ['parent_id','in',$ids5] : $where5[] = ['parent_id','in',[-1]];
            $ids6 = db('xy_users')->where($where5)->field('id')->column('id');
            $ids6 ? $where6[] = ['parent_id','in',$ids6] : $where6[] = ['parent_id','in',[-1]];
            $ids7 = db('xy_users')->where($where6)->field('id')->column('id');
            $ids7 ? $where7[] = ['parent_id','in',$ids7] : $where7[] = ['parent_id','in',[-1]];
            $ids8 = db('xy_users')->where($where7)->field('id')->column('id');
            $ids8 ? $where8[] = ['parent_id','in',$ids8] : $where8[] = ['parent_id','in',[-1]];
            $ids9 = db('xy_users')->where($where8)->field('id')->column('id');
            $ids9 ? $where9[] = ['parent_id','in',$ids9] : $where9[] = ['parent_id','in',[-1]];
            $ids10 = db('xy_users')->where($where9)->field('id')->column('id');
            $ids10 ? $where10[] = ['parent_id','in',$ids10] : $where10[] = ['parent_id','in',[-1]];
            $ids11 = db('xy_users')->where($where10)->field('id')->column('id');
            $ids11 ? $where11[] = ['parent_id','in',$ids11] : $where11[] = ['parent_id','in',[-1]];
            $ids12 = db('xy_users')->where($where11)->field('id')->column('id');
            $ids12 ? $where12[] = ['parent_id','in',$ids12] : $where12[] = ['parent_id','in',[-1]];
            $ids13 = db('xy_users')->where($where12)->field('id')->column('id');
            $ids13 ? $where13[] = ['parent_id','in',$ids13] : $where13[] = ['parent_id','in',[-1]];
            $ids14 = db('xy_users')->where($where13)->field('id')->column('id');
            $ids14 ? $where14[] = ['parent_id','in',$ids14] : $where14[] = ['parent_id','in',[-1]];
            $ids15 = db('xy_users')->where($where14)->field('id')->column('id');
            $ids15 ? $where[] = ['parent_id','in',$ids15] : $where[] = ['parent_id','in',[-1]];

            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5,$ids6,$ids7,$ids8,$ids9,$ids10,$ids11,$ids12,$ids13,$ids14,$ids15, $data) :$data;
        }else if ($num == 16) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where5[] = ['parent_id','in',$ids5] : $where5[] = ['parent_id','in',[-1]];
            $ids6 = db('xy_users')->where($where5)->field('id')->column('id');
            $ids6 ? $where6[] = ['parent_id','in',$ids6] : $where6[] = ['parent_id','in',[-1]];
            $ids7 = db('xy_users')->where($where6)->field('id')->column('id');
            $ids7 ? $where7[] = ['parent_id','in',$ids7] : $where7[] = ['parent_id','in',[-1]];
            $ids8 = db('xy_users')->where($where7)->field('id')->column('id');
            $ids8 ? $where8[] = ['parent_id','in',$ids8] : $where8[] = ['parent_id','in',[-1]];
            $ids9 = db('xy_users')->where($where8)->field('id')->column('id');
            $ids9 ? $where9[] = ['parent_id','in',$ids9] : $where9[] = ['parent_id','in',[-1]];
            $ids10 = db('xy_users')->where($where9)->field('id')->column('id');
            $ids10 ? $where10[] = ['parent_id','in',$ids10] : $where10[] = ['parent_id','in',[-1]];
            $ids11 = db('xy_users')->where($where10)->field('id')->column('id');
            $ids11 ? $where11[] = ['parent_id','in',$ids11] : $where11[] = ['parent_id','in',[-1]];
            $ids12 = db('xy_users')->where($where11)->field('id')->column('id');
            $ids12 ? $where12[] = ['parent_id','in',$ids12] : $where12[] = ['parent_id','in',[-1]];
            $ids13 = db('xy_users')->where($where12)->field('id')->column('id');
            $ids13 ? $where13[] = ['parent_id','in',$ids13] : $where13[] = ['parent_id','in',[-1]];
            $ids14 = db('xy_users')->where($where13)->field('id')->column('id');
            $ids14 ? $where14[] = ['parent_id','in',$ids14] : $where14[] = ['parent_id','in',[-1]];
            $ids15 = db('xy_users')->where($where14)->field('id')->column('id');
            $ids15 ? $where15[] = ['parent_id','in',$ids15] : $where15[] = ['parent_id','in',[-1]];
            $ids16 = db('xy_users')->where($where15)->field('id')->column('id');
            $ids16 ? $where[] = ['parent_id','in',$ids16] : $where[] = ['parent_id','in',[-1]];

            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5,$ids6,$ids7,$ids8,$ids9,$ids10,$ids11,$ids12,$ids13,$ids14,$ids15,$ids16, $data) :$data;
        }else if ($num == 17) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where5[] = ['parent_id','in',$ids5] : $where5[] = ['parent_id','in',[-1]];
            $ids6 = db('xy_users')->where($where5)->field('id')->column('id');
            $ids6 ? $where6[] = ['parent_id','in',$ids6] : $where6[] = ['parent_id','in',[-1]];
            $ids7 = db('xy_users')->where($where6)->field('id')->column('id');
            $ids7 ? $where7[] = ['parent_id','in',$ids7] : $where7[] = ['parent_id','in',[-1]];
            $ids8 = db('xy_users')->where($where7)->field('id')->column('id');
            $ids8 ? $where8[] = ['parent_id','in',$ids8] : $where8[] = ['parent_id','in',[-1]];
            $ids9 = db('xy_users')->where($where8)->field('id')->column('id');
            $ids9 ? $where9[] = ['parent_id','in',$ids9] : $where9[] = ['parent_id','in',[-1]];
            $ids10 = db('xy_users')->where($where9)->field('id')->column('id');
            $ids10 ? $where10[] = ['parent_id','in',$ids10] : $where10[] = ['parent_id','in',[-1]];
            $ids11 = db('xy_users')->where($where10)->field('id')->column('id');
            $ids11 ? $where11[] = ['parent_id','in',$ids11] : $where11[] = ['parent_id','in',[-1]];
            $ids12 = db('xy_users')->where($where11)->field('id')->column('id');
            $ids12 ? $where12[] = ['parent_id','in',$ids12] : $where12[] = ['parent_id','in',[-1]];
            $ids13 = db('xy_users')->where($where12)->field('id')->column('id');
            $ids13 ? $where13[] = ['parent_id','in',$ids13] : $where13[] = ['parent_id','in',[-1]];
            $ids14 = db('xy_users')->where($where13)->field('id')->column('id');
            $ids14 ? $where14[] = ['parent_id','in',$ids14] : $where14[] = ['parent_id','in',[-1]];
            $ids15 = db('xy_users')->where($where14)->field('id')->column('id');
            $ids15 ? $where15[] = ['parent_id','in',$ids15] : $where15[] = ['parent_id','in',[-1]];
            $ids16 = db('xy_users')->where($where15)->field('id')->column('id');
            $ids16 ? $where16[] = ['parent_id','in',$ids16] : $where16[] = ['parent_id','in',[-1]];
            $ids17 = db('xy_users')->where($where16)->field('id')->column('id');
            $ids17 ? $where[] = ['parent_id','in',$ids17] : $where[] = ['parent_id','in',[-1]];

            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5,$ids6,$ids7,$ids8,$ids9,$ids10,$ids11,$ids12,$ids13,$ids14,$ids15,$ids16,$ids17, $data) :$data;
        } else if ($num == 18) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where5[] = ['parent_id','in',$ids5] : $where5[] = ['parent_id','in',[-1]];
            $ids6 = db('xy_users')->where($where5)->field('id')->column('id');
            $ids6 ? $where6[] = ['parent_id','in',$ids6] : $where6[] = ['parent_id','in',[-1]];
            $ids7 = db('xy_users')->where($where6)->field('id')->column('id');
            $ids7 ? $where7[] = ['parent_id','in',$ids7] : $where7[] = ['parent_id','in',[-1]];
            $ids8 = db('xy_users')->where($where7)->field('id')->column('id');
            $ids8 ? $where8[] = ['parent_id','in',$ids8] : $where8[] = ['parent_id','in',[-1]];
            $ids9 = db('xy_users')->where($where8)->field('id')->column('id');
            $ids9 ? $where9[] = ['parent_id','in',$ids9] : $where9[] = ['parent_id','in',[-1]];
            $ids10 = db('xy_users')->where($where9)->field('id')->column('id');
            $ids10 ? $where10[] = ['parent_id','in',$ids10] : $where10[] = ['parent_id','in',[-1]];
            $ids11 = db('xy_users')->where($where10)->field('id')->column('id');
            $ids11 ? $where11[] = ['parent_id','in',$ids11] : $where11[] = ['parent_id','in',[-1]];
            $ids12 = db('xy_users')->where($where11)->field('id')->column('id');
            $ids12 ? $where12[] = ['parent_id','in',$ids12] : $where12[] = ['parent_id','in',[-1]];
            $ids13 = db('xy_users')->where($where12)->field('id')->column('id');
            $ids13 ? $where13[] = ['parent_id','in',$ids13] : $where13[] = ['parent_id','in',[-1]];
            $ids14 = db('xy_users')->where($where13)->field('id')->column('id');
            $ids14 ? $where14[] = ['parent_id','in',$ids14] : $where14[] = ['parent_id','in',[-1]];
            $ids15 = db('xy_users')->where($where14)->field('id')->column('id');
            $ids15 ? $where15[] = ['parent_id','in',$ids15] : $where15[] = ['parent_id','in',[-1]];
            $ids16 = db('xy_users')->where($where15)->field('id')->column('id');
            $ids16 ? $where16[] = ['parent_id','in',$ids16] : $where16[] = ['parent_id','in',[-1]];
            $ids17 = db('xy_users')->where($where16)->field('id')->column('id');
            $ids17 ? $where17[] = ['parent_id','in',$ids17] : $where17[] = ['parent_id','in',[-1]];
            $ids18 = db('xy_users')->where($where17)->field('id')->column('id');
            $ids18 ? $where[] = ['parent_id','in',$ids18] : $where[] = ['parent_id','in',[-1]];

            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5,$ids6,$ids7,$ids8,$ids9,$ids10,$ids11,$ids12,$ids13,$ids14,$ids15,$ids16,$ids17,$ids18, $data) :$data;
        }else if ($num == 19) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where5[] = ['parent_id','in',$ids5] : $where5[] = ['parent_id','in',[-1]];
            $ids6 = db('xy_users')->where($where5)->field('id')->column('id');
            $ids6 ? $where6[] = ['parent_id','in',$ids6] : $where6[] = ['parent_id','in',[-1]];
            $ids7 = db('xy_users')->where($where6)->field('id')->column('id');
            $ids7 ? $where7[] = ['parent_id','in',$ids7] : $where7[] = ['parent_id','in',[-1]];
            $ids8 = db('xy_users')->where($where7)->field('id')->column('id');
            $ids8 ? $where8[] = ['parent_id','in',$ids8] : $where8[] = ['parent_id','in',[-1]];
            $ids9 = db('xy_users')->where($where8)->field('id')->column('id');
            $ids9 ? $where9[] = ['parent_id','in',$ids9] : $where9[] = ['parent_id','in',[-1]];
            $ids10 = db('xy_users')->where($where9)->field('id')->column('id');
            $ids10 ? $where10[] = ['parent_id','in',$ids10] : $where10[] = ['parent_id','in',[-1]];
            $ids11 = db('xy_users')->where($where10)->field('id')->column('id');
            $ids11 ? $where11[] = ['parent_id','in',$ids11] : $where11[] = ['parent_id','in',[-1]];
            $ids12 = db('xy_users')->where($where11)->field('id')->column('id');
            $ids12 ? $where12[] = ['parent_id','in',$ids12] : $where12[] = ['parent_id','in',[-1]];
            $ids13 = db('xy_users')->where($where12)->field('id')->column('id');
            $ids13 ? $where13[] = ['parent_id','in',$ids13] : $where13[] = ['parent_id','in',[-1]];
            $ids14 = db('xy_users')->where($where13)->field('id')->column('id');
            $ids14 ? $where14[] = ['parent_id','in',$ids14] : $where14[] = ['parent_id','in',[-1]];
            $ids15 = db('xy_users')->where($where14)->field('id')->column('id');
            $ids15 ? $where15[] = ['parent_id','in',$ids15] : $where15[] = ['parent_id','in',[-1]];
            $ids16 = db('xy_users')->where($where15)->field('id')->column('id');
            $ids16 ? $where16[] = ['parent_id','in',$ids16] : $where16[] = ['parent_id','in',[-1]];
            $ids17 = db('xy_users')->where($where16)->field('id')->column('id');
            $ids17 ? $where17[] = ['parent_id','in',$ids17] : $where17[] = ['parent_id','in',[-1]];
            $ids18 = db('xy_users')->where($where17)->field('id')->column('id');
            $ids18 ? $where18[] = ['parent_id','in',$ids18] : $where18[] = ['parent_id','in',[-1]];
            $ids19 = db('xy_users')->where($where18)->field('id')->column('id');
            $ids19 ? $where[] = ['parent_id','in',$ids19] : $where[] = ['parent_id','in',[-1]];

            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5,$ids6,$ids7,$ids8,$ids9,$ids10,$ids11,$ids12,$ids13,$ids14,$ids15,$ids16,$ids17,$ids18,$ids19, $data) :$data;
        }else if ($num == 20) {

            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $ids1 ? $wher[] =  ['parent_id','in',$ids1] : $wher[] =  ['parent_id','in',[-1]];
            $ids2 = db('xy_users')->where($wher)->field('id')->column('id');
            $ids2 ? $where2[] = ['parent_id','in',$ids2] : $where2[] = ['parent_id','in',[-1]];
            $ids3 = db('xy_users')->where($where2)->field('id')->column('id');
            $ids3 ? $where3[] = ['parent_id','in',$ids3] : $where3[] = ['parent_id','in',[-1]];
            $ids4 = db('xy_users')->where($where3)->field('id')->column('id');
            $ids4 ? $where4[] = ['parent_id','in',$ids4] : $where4[] = ['parent_id','in',[-1]];
            $ids5 = db('xy_users')->where($where4)->field('id')->column('id');
            $ids5 ? $where5[] = ['parent_id','in',$ids5] : $where5[] = ['parent_id','in',[-1]];
            $ids6 = db('xy_users')->where($where5)->field('id')->column('id');
            $ids6 ? $where6[] = ['parent_id','in',$ids6] : $where6[] = ['parent_id','in',[-1]];
            $ids7 = db('xy_users')->where($where6)->field('id')->column('id');
            $ids7 ? $where7[] = ['parent_id','in',$ids7] : $where7[] = ['parent_id','in',[-1]];
            $ids8 = db('xy_users')->where($where7)->field('id')->column('id');
            $ids8 ? $where8[] = ['parent_id','in',$ids8] : $where8[] = ['parent_id','in',[-1]];
            $ids9 = db('xy_users')->where($where8)->field('id')->column('id');
            $ids9 ? $where9[] = ['parent_id','in',$ids9] : $where9[] = ['parent_id','in',[-1]];
            $ids10 = db('xy_users')->where($where9)->field('id')->column('id');
            $ids10 ? $where10[] = ['parent_id','in',$ids10] : $where10[] = ['parent_id','in',[-1]];
            $ids11 = db('xy_users')->where($where10)->field('id')->column('id');
            $ids11 ? $where11[] = ['parent_id','in',$ids11] : $where11[] = ['parent_id','in',[-1]];
            $ids12 = db('xy_users')->where($where11)->field('id')->column('id');
            $ids12 ? $where12[] = ['parent_id','in',$ids12] : $where12[] = ['parent_id','in',[-1]];
            $ids13 = db('xy_users')->where($where12)->field('id')->column('id');
            $ids13 ? $where13[] = ['parent_id','in',$ids13] : $where13[] = ['parent_id','in',[-1]];
            $ids14 = db('xy_users')->where($where13)->field('id')->column('id');
            $ids14 ? $where14[] = ['parent_id','in',$ids14] : $where14[] = ['parent_id','in',[-1]];
            $ids15 = db('xy_users')->where($where14)->field('id')->column('id');
            $ids15 ? $where15[] = ['parent_id','in',$ids15] : $where15[] = ['parent_id','in',[-1]];
            $ids16 = db('xy_users')->where($where15)->field('id')->column('id');
            $ids16 ? $where16[] = ['parent_id','in',$ids16] : $where16[] = ['parent_id','in',[-1]];
            $ids17 = db('xy_users')->where($where16)->field('id')->column('id');
            $ids17 ? $where17[] = ['parent_id','in',$ids17] : $where17[] = ['parent_id','in',[-1]];
            $ids18 = db('xy_users')->where($where17)->field('id')->column('id');
            $ids18 ? $where18[] = ['parent_id','in',$ids18] : $where18[] = ['parent_id','in',[-1]];
            $ids19 = db('xy_users')->where($where18)->field('id')->column('id');
            $ids19 ? $where19[] = ['parent_id','in',$ids19] : $where19[] = ['parent_id','in',[-1]];
            $ids20 = db('xy_users')->where($where19)->field('id')->column('id');
            $ids20 ? $where[] = ['parent_id','in',$ids20] : $where[] = ['parent_id','in',[-1]];

            $data = db('xy_users')->where($where)->field('id')->column('id');
            $data = $lv ? array_merge($ids1 ,$ids2,$ids3,$ids4,$ids5,$ids6,$ids7,$ids8,$ids9,$ids10,$ids11,$ids12,$ids13,$ids14,$ids15,$ids16,$ids17,$ids18,$ids19,$ids20, $data) :$data;
        }

        return $data;
    }


    /**
     * @param $uid  当前用户id
     * 获取当前用户的全部上级数据
     */
    function get_parent_users($uid){

        //TODO  待优化查询方式
        //$map['id']  = array('exp',' and FIND_IN_SET( ' .$uid . ' , [down_userid]) ');
        //$where   = "charindex(','+'".$uid ."'+',' , ','+cast([down_userid] as varchar(5000))+',') > 0 ";
        $up_userid = $uid;
        $up_userid_arr[] = $up_userid;
        while ($up_userid)
        {
            //查询上级的上级id ;
            $up_userid = $this->where("id =  '{$up_userid}'")->value('parent_id');
            if($up_userid)
            {
                $up_userid_arr[] =  $up_userid;
            }
        }


        return $up_userid_arr;

    }

    /**
     * @param $data  要提交的数组
     * @param $uid  直接上级ID
     * 增加会员 并添加id到 上级中
     */
    function add_son_userdata($newuid, $uid){

        $reid =  $newuid;
        if( $reid )  //提交成功
        {
            $lastid = $reid;
            //更新down_userid默认为自己的id
            db('xy_users')->where('id', $lastid)->update(['down_userid'=>$lastid]);
            //查询所有上级
            $parent = $this->get_parent_users($uid);

            //$parentid = '';
            $parentid = implode(',',$parent);

            $parentid = rtrim($parentid,",");


            if($parentid){
                $parentid = $parentid .','. $uid;
            }
            else{
                $parentid = $uid;
            }

            $a = explode(",", $parentid);
            foreach ($a as $pid) {
                Cache::set('contact_list_' . $pid, NULL);
                Cache::set('api_contact_list_' . $pid, NULL);
            }

            //将当前新用户的ID加入他们的down_userid字段
            $sql = "UPDATE xy_users SET down_userid= CONCAT(down_userid,',{$lastid}') WHERE ( id in 
            ({$parentid}) )";
            if ($parentid) {
                $this->execute($sql);
            }


            return $reid;
        }

    }

    public function invitationBonus($uid,$oid,$total_recharge = 0)
    {
        $uinfo = Db::name('xy_users')->field('id,active')->find($uid);
        if($uinfo['active']===0){
            Db::name('xy_users')->where('id',$uinfo['id'])->update(['active'=>1]);
            //将账号状态改为已发放推广奖励
            $userList = model('admin/Users')->parent_user($uinfo['id'],3);
            if($userList){
                foreach($userList as $v){
                    if($v['status']===1){
                        if ($v['lv'] == 1) {
                            $bonus = config('invite_reward');
                            $invite_reward_open = sysconf('invite_reward_open');
                            if ($bonus > 0 && $invite_reward_open == 1) {
                                //记录邀请一个朋友三十元的现金奖励
                                if ($total_recharge > 0) {
                                    $bonus = $total_recharge * 0.1;
                                }
                                $resInv = Db::name('xy_balance_log')->insert([
                                    'uid'       => $v['id'],
                                    'sid'       =>$uinfo['id'],
                                    'oid'       =>$oid,
                                    'num'       => $bonus,
                                    'type'      => 11,//邀请奖励
                                    'addtime'   => time(),
                                    'f_lv'=>$v['lv'],
                                ]);
                                if ($resInv) {
                                    Db::name('xy_users')->where('id',$v['id'])->setInc('balance',$bonus);
                                }
                            }

                            //邀请满人奖励
                            $bonus2 = config('invite_target_reward');
                            //至少300元
                            $bonus2_min_recharge = config('invite_target_reward_min_recharge');
                            $invite_target_reward_open = sysconf('invite_target_reward_open');
                            if ($bonus2 > 0 && $invite_target_reward_open == 1) {
                                //读取之前从哪个用户得到过满人邀请奖励
                                $down_userids = DB::name('xy_balance_log')->where(['type'=>12, 'uid'=>$v['id']])->column('down_userid');
                                $down_userids = implode(",", $down_userids);
                                $down_userids_where = $down_userids ? "id NOT IN ($down_userids)" : "";

                                $userids = Db::name('xy_users')->where(['parent_id'=>$v['id'],'active'=>1])->where("total_recharge >= $bonus2_min_recharge")->where($down_userids_where)->column('id');
                                $childCount = sizeof($userids);
                                if ($childCount >= config('invite_target_reward_count')) {
                                    //查询用户是否有得过此奖励
                                    $re = Db::table('xy_balance_log')->where(['uid' => $v['id'], 'type' => 12])->count();
                                    if ($re == 0) {
                                        $resInv2 = Db::name('xy_balance_log')->insert([
                                            //记录邀请一个朋友三十元的现金奖励
                                            'uid'       => $v['id'],
                                            'sid'       =>$uinfo['id'],
                                            'oid'       =>$oid,
                                            'num'       => $bonus2,
                                            'type'      => 12,//邀请满人奖励
                                            'addtime'   => time(),
                                            'f_lv'=>$v['lv'],
                                            'down_userid' => implode(",", $userids)
                                        ]);
                                        if ($resInv2) {
                                            Db::name('xy_users')->where('id',$v['id'])->setInc('balance',$bonus2);
                                        }
                                    }
                                }
                            }
                        }

                        // Db::name('xy_reward_log')
                        //     ->insert([
                        //         'uid'=>$v['id'],
                        //         'sid'=>$uinfo['id'],
                        //         'oid'=>$oid,
                        //         'num'=>$bonus,
                        //         'lv'=>$v['lv'],
                        //         'type'=>1,
                        //         'status'=>1,
                        //         'addtime'=>time(),
                        //     ]);

                        $amt = config($v['lv'].'_invite_reward');
                        $res2 = Db::name('xy_point_log')->insert([
                            //记录积分信息
                            'uid'       => $v['id'],
                            'sid'       =>$uinfo['id'],
                            'oid'       =>$oid,
                            'num'       => $amt,
                            'type'      => 11,//邀请奖励
                            'status'    => 1,
                            'f_lv'      => $v['lv'],
                            'addtime'   => time()
                        ]);
                        //积分
                        if ($res2) {
                            Db::name('xy_users')->where('id',$v['id'])->setInc('point',$amt);
                        }
                    }
                }
            }
        }
    }
}
