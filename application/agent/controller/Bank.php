<?php
namespace app\agent\controller;

use app\agent\controller\Common;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Bank extends Common{
    /**
     * 空操作处理
     */
    public function _empty(){
        echo "none";
    }
    
    /**
     * 充值记录
     */
    public function recharge_record(){
        
        // $teamlists = implode(',',$this->teamlists);
        
        if (request()->isAjax()) {
            $param = input('param.');
            
            //查询条件组装
            $where = array();
            
            $where[] = array('xy_recharge.type', '<>', 0);
            //用户名搜索
            if (isset($param['user_type']) && $param['user_type']) {
                // $where[] = array('users.user_type', '=', $param['user_type']);
            }
            
            if(isset($param['username']) && $param['username']){
                // $uid = model('manage/Users')->where('username', $param['username'])->value('id');
                $where[] = array('xy_users.username','LIKE',$param['username']);
            }
            //订单号搜索
            if(isset($param['order_number']) && $param['order_number']){
                $where[] = array('xy_recharge.id','=',$param['order_number']);
            }
            //状态搜索
            if(isset($param['state']) && $param['state']){
                $where[] = array('xy_recharge.status','=',$param['state']);
            }
            // 时间
            if(isset($param['datetime_range']) && $param['datetime_range']){
                $dateTime = explode(' - ', $param['datetime_range']);
                $where[] = array('xy_recharge.addtime','>=',strtotime($dateTime[0]));
                $where[] = array('xy_recharge.addtime','<=',strtotime($dateTime[1]));
            }else{
                $todayStart = mktime(0,0,0,date('m'),date('d'),date('Y'));
                $where[] = array('xy_recharge.addtime','>=',$todayStart);
                $todayEnd = mktime(23,59,59,date('m'),date('d'),date('Y'));
                $where[] = array('xy_recharge.addtime','<=',$todayEnd);
            }
            
            $down_userids_arr = [];
            // $this->getDownline($this->userid, $down_userids_arr);
            // $down_userids_arr[] = $this->userid;
            $down_userids = db('xy_users')->where('id', $this->userid)->value('down_userid');//implode(",", $down_userids_arr);
            
            // $where[] = array('xy_recharge.uid','in',$teamlists);
            $where[] = array('xy_recharge.uid','IN',$down_userids);
            
            $recharge_where2 = "xy_recharge.id NOT IN ('SY2107251151259242','SY2107251151002279','SY2107251150449815','SY2107251148427238','SY2107251148114087','SY2107251147578332','SY2107251147427923','SY2107251147287011','SY2107251147144023','SY2107251146569183','SY2107251145473196','SY2107251145252488','SY2107251144517484','SY2107251144243426','SY2107251143591316','SY2107251143368936','SY2107251143173945','SY2107251142539171','SY2107251142265092','SY2107251142076757','SY2107251141421445','SY2107251140389732','SY2107251140184928','SY2107251140024973','SY2107251139252880','SY2107251139062084','SY2107251138217953','SY2107251052198794','SY2107251051251526','SY2107251051251583','SY2107251051251704','SY2107251051251790','SY2107251051252006','SY2107251051252270','SY2107251051252546','SY2107251051253724','SY2107251051253847','SY2107251051254697','SY2107251051254865','SY2107251051255476','SY2107251051255771','SY2107251051255823','SY2107251051255870','SY2107251051256456','SY2107251051256539','SY2107251051256584','SY2107251051257184','SY2107251051257850','SY2107251051257984','SY2107251051258232','SY2107251051258793','SY2107251051259486','SY2107251051259733','SY2107251051259752','SY2107251051259901','SY2107251039421436','SY2107251039425007','SY2107251039425452','SY2107251039426377','SY2107251039427043','SY2107251039427068','SY2107251039427553','SY2107251039427719','SY2107251039427858','SY2107251039429470')";
            
            $count              = db('xy_recharge')->join('xy_users','xy_recharge.uid = xy_users.id')
            ->where($where)->where($recharge_where2)->count(); // 总记录数
            
            $param['limit']     = (isset($param['limit']) and $param['limit']) ? $param['limit'] : 15; // 每页记录数
            $param['page']      = (isset($param['page']) and $param['page']) ? $param['page'] : 1; // 当前页
            $limitOffset        = ($param['page'] - 1) * $param['limit']; // 偏移量
            $param['sortField'] = (isset($param['sortField']) && $param['sortField']) ? $param['sortField'] : 'xy_recharge.addtime';
            $param['sortType']  = (isset($param['sortType']) && $param['sortType']) ? $param['sortType'] : 'desc';
            
            //查询符合条件的数据
            
            $data = db('xy_recharge')->field('xy_recharge.*,xy_users.username,xy_recharge.id AS order_number,xy_recharge.num AS money,xy_recharge.real_name AS name,xy_recharge.endtime AS dispose_time')
            ->join('xy_users','xy_recharge.uid = xy_users.id')
            ->where($where)
            ->where($recharge_where2)
            ->order($param['sortField'], $param['sortType'])->limit($limitOffset, $param['limit'])->select();
            foreach ($data as $key => &$value) {
                switch ($value['status']) {
                    case '1':
                        $value['statusStr'] = '下单成功';
                        break;
                    case '2':
                        $value['statusStr'] = '充值成功';
                        break;
                    default:
                        $value['statusStr'] = '充值失败';
                        break;
                }
                $value['add_time'] = date('Y-m-d H:i:s', $value['addtime']);
                $value['dispose_time']   = $value['dispose_time'] > 0 ? date('Y-m-d H:i:s', $value['dispose_time']) : '-';
                // if ($value['daozhang_money'] <= 0) {
                $value['daozhang_money'] = $value['num'];
                // }
                }
                
                return json([
                    'code'  => 0,
                    'msg'   => '',
                    'count' => $count,
                    'data'  => $data
                ]);
            }
            
            return view();
        }
        
        /**
         * 提现记录
         */
        public function present_record(){
            
            // $teamlists = implode(',',$this->teamlists);
            
            if (request()->isAjax()) {
                $param = input('param.');
                //查询条件组装
                $where = array();
                if (isset($param['user_type']) && $param['user_type']) {
                    $where[] = array('users.user_type', '=', $param['user_type']);
                }
                // 状态搜索
                if (isset($param['isUser']) && $param['isUser'] == 1) $pageParam['isUser'] = $param['isUser'];
                //搜索类型
                if(isset($param['search_t']) && $param['search_t'] && isset($param['search_c']) && $param['search_c']){
                    switch ($param['search_t']) {
                        case 'username':
                            // $userId = model('Users')->where('username',$param['search_c'])->value('id');
                            $where[] = array('xy_users.username','LIKE',$param['search_c']);
                            break;
                        case 'order_number':
                            $where[] = array('xy_deposit.id','=',$param['search_c']);
                            break;
                        case 'card_name':
                            $where[] = array('xy_bankinfo.username','=',$param['search_c']);
                            break;
                        case 'card_number':
                            $where[] = array('xy_bankinfo.cardnum','=',$param['search_c']);
                            break;
                    }
                }
                
                //状态搜索
                if(isset($param['state']) && $param['state']){
                    $where[] = array('xy_deposit.status','=',$param['state']);
                }
                // 时间
                if(isset($param['datetime_range']) && $param['datetime_range']){
                    $dateTime = explode(' - ', $param['datetime_range']);
                    $where[] = array('xy_deposit.addtime','>=',strtotime($dateTime[0]));
                    $where[] = array('xy_deposit.addtime','<=',strtotime($dateTime[1]));
                }else{
                    $todayStart = mktime(0,0,0,date('m'),date('d'),date('Y'));
                    $where[] = array('xy_deposit.addtime','>=',$todayStart);
                    $todayEnd = mktime(23,59,59,date('m'),date('d'),date('Y'));
                    $where[] = array('xy_deposit.addtime','<=',$todayEnd);
                }
                $down_userids_arr = [];
                // $this->getDownline($this->userid, $down_userids_arr);
                // $down_userids_arr[] = $this->userid;
                $down_userids = db('xy_users')->where('id', $this->userid)->value('down_userid');//implode(",", $down_userids_arr);
                
                // $where[] = array('xy_recharge.uid','in',$teamlists);
                $where[] = array('xy_deposit.uid','IN',$down_userids);
                
                $count              = db('xy_deposit')
                ->join('xy_users','xy_deposit.uid = xy_users.id')
                ->join('xy_bankinfo','xy_deposit.bk_id = xy_bankinfo.id','left')
                ->where($where)->count(); // 总记录数
                $param['limit']     = (isset($param['limit']) and $param['limit']) ? $param['limit'] : 15; // 每页记录数
                $param['page']      = (isset($param['page']) and $param['page']) ? $param['page'] : 1; // 当前页
                $limitOffset        = ($param['page'] - 1) * $param['limit']; // 偏移量
                $param['sortField'] = (isset($param['sortField']) && $param['sortField']) ? $param['sortField'] : 'addtime';
                $param['sortType']  = (isset($param['sortType']) && $param['sortType']) ? $param['sortType'] : 'desc';
                
                //查询符合条件的数据
                $data = db('xy_deposit')->field('xy_deposit.*,xy_deposit.id AS order_number,xy_users.username as aname,xy_users.username,xy_bankinfo.bankname AS bank_name,xy_bankinfo.cardnum AS card_number,xy_bankinfo.username AS card_name,xy_deposit.num AS price')
                ->join('xy_users','xy_deposit.uid = xy_users.id')
                ->join('xy_bankinfo','xy_deposit.bk_id = xy_bankinfo.id','left')
                ->where($where)->order($param['sortField'], $param['sortType'])->limit($limitOffset, $param['limit'])->select();
                
                foreach ($data as $key => &$value) {
                    switch ($value['status']) {
                        case '1':
                            $value['statusStr'] = '待处理';
                            break;
                        case '2':
                            $value['statusStr'] = '审核通过';
                            break;
                        case '3':
                            $value['statusStr'] = '审核不通过';
                            break;
                        default:
                            $value['statusStr'] = '提现锁定';
                            break;
                    }
                    $value['time'] = date('Y-m-d H:i:s', $value['addtime']);
                    $value['set_time']   = date('Y-m-d H:i:s', $value['addtime']);
                }
                
                //权限查询
                // if ($count) $data['power'] = model('ManageUserRole')->getUserPower(['uid'=>session('manage_userid')]);
                
                return json([
                    'code'  => 0,
                    'msg'   => '',
                    'count' => $count,
                    'data'  => $data
                ]);
            }
            
            $state = [
                1 => '待处理',
                2 => '审核通过',
                3 => '审核不通过',
                4 => '提现锁定',
            ];
            
            $this->assign('state', $state);
            
            return $this->fetch();
        }
        
        
    }