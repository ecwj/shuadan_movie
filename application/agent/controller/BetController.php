<?php
namespace app\agent\controller;

use app\agent\controller\Common;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

class BetController extends Common
{
    /**
     * 空操作处理
     */
    public function _empty()
    {
        return $this->financial();
    }
     
    /**
     * 用户资金流水
     */
    public function financial()
    {
        if (request()->isAjax()) {
            $param = input('post.');

            //查询条件组装
            $where = array();
            $where[] = array('types','=',1);

            if (isset($param['isUser'])) {
                $where[] = array('types','=',$param['isUser']);
                $pageParam['isUser'] = $param['isUser'];
            }
            //搜索类型
            if (isset($param['search_type']) && $param['search_type'] && isset($param['search_content']) && $param['search_content']) {
                switch ($param['search_type']) {
                    case 'remarks':
                        $where[] = array('remarks','like','%'.$param['search_content'].'%');
                        break;
                    default:
                        $where[] = array($param['search_type'],'=',$param['search_content']);
                        break;
                }
            }
            //交易类型
            if (isset($param['trade_type']) && $param['trade_type']) {
                $where[] = array('trade_type','=',$param['trade_type']);
            }
            //交易金额
            if (isset($param['price1']) && $param['price1']) {
                $where[] = array('trade_amount','>=',$param['price1']);
            }
            //交易金额
            if (isset($param['price2']) && $param['price2']) {
                $where[] = array('trade_amount','<=',$param['price2']);
            }
            //时间
            if (isset($param['datetime_range']) && $param['datetime_range']) {
                $dateTime = explode(' - ', $param['datetime_range']);
                $where[] = array('trade_time','>=',strtotime($dateTime[0]));
                $where[] = array('trade_time','<=',strtotime($dateTime[1]));
            } else {
                $todayStart = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                $where[] = array('trade_time','>=',$todayStart);
                $todayEnd = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
                $where[] = array('trade_time','<=',$todayEnd);
            }
 
            // $teamlists = implode(',',$this->teamlists); 
			// $where[] = array('uid','in',$teamlists);
			$where[] = array('user_team.uid','=',$this->userid);

            $count              = model('TradeDetails')
            ->where($where)
			->join('user_team','user_team.team = ly_trade_details.uid')
            ->count(); // 总记录数
     
            $param['limit']     = (isset($param['limit']) and $param['limit']) ? $param['limit'] : 15; // 每页记录数
            $param['page']      = (isset($param['page']) and $param['page']) ? $param['page'] : 1; // 当前页
            $limitOffset        = ($param['page'] - 1) * $param['limit']; // 偏移量
            $param['sortField'] = (isset($param['sortField']) && $param['sortField']) ? $param['sortField'] : 'trade_time';
            $param['sortType']  = (isset($param['sortType']) && $param['sortType']) ? $param['sortType'] : 'desc';

            //查询符合条件的数据
            $data = model('TradeDetails')->where($where)
			->join('user_team','user_team.team = ly_trade_details.uid')
            ->order($param['sortField'], $param['sortType'])
            ->limit($limitOffset, $param['limit'])->select()->toArray();
            
            //部分元素重新赋值
            $tradeType   = config('custom.transactionType');//交易类型
            $orderColor  = config('manage.color');
            $adminColor  = config('manage.adminColor');
            foreach ($data as $key => &$value) {
                $value['trade_time']     = date('Y-m-d H:i:s', $value['trade_time']);
                $value['tradeType']      = $tradeType[$value['trade_type']];
                $value['tradeTypeColor'] = $adminColor[$value['trade_type']];
                $value['statusStr']      = config('custom.tradedetailsStatus')[$value['state']];
                $value['statusColor']    = $orderColor[$value['state']];
                $value['front_type_str'] = config('custom.front_type')[$value['front_type']];
                $value['payway_str']     = config('custom.payway')[$value['payway']];
            }

            return json([
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $data
            ]);
        }

        return view();
    }
   
    /**
     * 任务记录
     * @return [type] [description]
     */
    public function userTaskList()
    {
        if (request()->isAjax()) {
            $param = input('param.');
            //查询条件初始化
            $where = array();

            // 用户名
            if (isset($param['username']) && $param['username']) {
                $where[] = array(['ly_user_task.username','=',$param['username']]);
            }
            
            // 状态
            if (isset($param['status']) && $param['status']) {
                $where[] = array(['ly_user_task.status','=',$param['status']]);
            }

            // 时间
            if (isset($param['datetime_range']) && $param['datetime_range']) {
                $dateTime = explode(' - ', $param['datetime_range']);
                $where[] = ['ly_user_task.add_time', 'between time', [$dateTime[0], $dateTime[1]]];
            } else {
                $todayStart = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                $todayEnd = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
                $where[] = ['ly_user_task.add_time', 'between time', [$todayStart, $todayEnd]];
            }

 
            // $teamlists = implode(',',$this->teamlists); 
			// $where[] = array('ly_user_task.uid','in',$teamlists);
			$where[] = array('user_team.uid','=',$this->userid);

            $count = model('UserTask')->join('ly_task', 'ly_task.id=ly_user_task.task_id')
			->join('user_team','user_team.team = ly_user_task.uid')
            ->where($where)->count(); // 总记录数
            
            
            $param['limit']     = (isset($param['limit']) and $param['limit']) ? $param['limit'] : 10; // 每页记录数
            $param['page']      = (isset($param['page']) and $param['page']) ? $param['page'] : 1; // 当前页
            $limitOffset        = ($param['page'] - 1) * $param['limit']; // 偏移量
            $param['sortField'] = (isset($param['sortField']) && $param['sortField']) ? $param['sortField'] : 'trial_time';
            $param['sortType']  = (isset($param['sortType']) && $param['sortType']) ? $param['sortType'] : 'desc';

            //查询符合条件的数据
            $data = model('UserTask')->field('ly_task.title,ly_user_task.*')
            ->join('ly_task', 'ly_task.id=ly_user_task.task_id')
			->join('user_team','user_team.team = ly_user_task.uid')
            ->where($where)->order($param['sortField'], $param['sortType'])
            ->limit($limitOffset, $param['limit'])->select()->toArray();

            foreach ($data as $key => &$value) {
                $value['statusStr']      = config('custom.cntaskOrderStatus')[$value['status']];
                $value['add_time'] 		 = ($value['add_time']) ? date('Y-m-d H:i:s', $value['add_time']) : '';//接单时间
                $value['o_id'] 		 = $value['id'];//接单时间
            }

            return json([
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $data
            ]);
        }
        
        return view('');
    }
     
}
