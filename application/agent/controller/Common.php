<?php
namespace app\agent\controller;

use library\Controller;

class Common extends Controller
{
    protected $USER = null;
    protected $userid = null;
    
    function __construct()
    {
        //判断是否登陆
        $is_agent_login = session('agent');
        
        if (empty($is_agent_login) || !isset($is_agent_login['id'])) {
            if (request()->isAjax()) {
                $this->error('未登录！', '/agent/login');
            } else {
                $this->redirect('/agent/login');
            }
        }
        
        $this->userid = $is_agent_login['id'];
        
        $this->USER = db('xy_users')->where('id', $this->userid)->find();

        // $this->teamlists = model('user_team')->where('uid', $this->userid)->column("team");
 
        $this->assign('USER', $this->USER);
        if (!$this->USER) {
            if (request()->isAjax()) {
                $this->error('未登录！', '/agent/login');
            } else {
                $this->redirect('/agent/login');
            }
        }
    }

    function getDownline($uid, &$dl) {
        $users = db('xy_users')->where(['parent_id' => $uid])->select();
        if ($users) {
            foreach ($users as $user) {
                $dl[] = $user['id'];
                $this->getDownline($user['id'], $dl);
            }
        }

        return 1;
    }
}
