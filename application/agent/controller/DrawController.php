<?php
namespace app\agent\controller;

use app\agent\controller\Common;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class DrawController extends Common
{ 
    public function lists(){
		 
		// $teamlists = implode(',',$this->teamlists); 

		if (request()->isAjax()) {
			$param = input('param.');
			//查询条件组装
			$where = array();
			if (isset($param['user_type']) && $param['user_type']) {
			    $where[] = array('users.user_type', '=', $param['user_type']);
			}
			
			//搜索类型
			if(isset($param['search_t']) && $param['search_t'] && isset($param['search_c']) && $param['search_c']){
				switch ($param['search_t']) {
					case 'username':
						$userId 	= model('Users')->where('username',$param['search_c'])->value('id');
						$where[] 	= array('ly_user_luckydraw.uid','=',$userId);
						break;
					case 'order_number':
						$where[] 	= array('ly_user_luckydraw.order_number','=',$param['search_c']);
						break;
				 
				}
			}

			//状态搜索
			if(isset($param['state']) && $param['state']){
				$where[] 	= array('ly_user_luckydraw.state','=',$param['state']);
			}
			// 时间
			if(isset($param['datetime_range']) && $param['datetime_range']){
				$dateTime 	= explode(' - ', $param['datetime_range']);
				$where[] 	= array('ly_user_luckydraw.add_time','>=',strtotime($dateTime[0]));
				$where[] 	= array('ly_user_luckydraw.add_time','<=',strtotime($dateTime[1]));
			}else{
				$todayStart = strtotime(date("Y-m")."-01 00:00:00"); 
				$where[] 	= array('ly_user_luckydraw.add_time','>=',$todayStart);
				$todayEnd 	= mktime(23,59,59,date('m'),date('d'),date('Y'));
				$where[] 	= array('ly_user_luckydraw.add_time','<=',$todayEnd);
			} 
			// $where[] = array('ly_user_luckydraw.uid','in',$teamlists);
			$where[] = array('user_team.uid','=',$this->userid);

			$count              = model('api/UserDraw')
			->join('users','ly_user_luckydraw.uid = users.id') 
			->join('user_team','user_team.team = users.id')
			->where($where)->count(); // 总记录数

			$param['limit']     = (isset($param['limit']) and $param['limit']) ? $param['limit'] : 15; // 每页记录数
			$param['page']      = (isset($param['page']) and $param['page']) ? $param['page'] : 1; // 当前页
			$limitOffset        = ($param['page'] - 1) * $param['limit']; // 偏移量
			$param['sortField'] = (isset($param['sortField']) && $param['sortField']) ? $param['sortField'] : 'add_time';
			$param['sortType']  = (isset($param['sortType']) && $param['sortType']) ? $param['sortType'] : 'desc';

			//查询符合条件的数据
			$data = model('api/UserDraw')->field('ly_user_luckydraw.*,users.username')
			->join('users','ly_user_luckydraw.uid = users.id')
			->join('user_team','user_team.team = users.id')
			->where($where)
			->order($param['sortField'], $param['sortType'])->limit($limitOffset, $param['limit'])->select()->toArray();
			foreach ($data as $key => &$value) {
				switch ($value['status']) {
					case '1':
						$value['statusStr'] = '中奖';
						break;
					case '0':
						$value['statusStr'] = '没中奖';
						break; 
				}
				$value['add_time']   = date('Y-m-d H:i:s', $value['add_time']);
			}

			//权限查询
			// if ($count) $data['power'] = model('ManageUserRole')->getUserPower(['uid'=>session('manage_userid')]);

			return json([
				'code'  => 0,
				'msg'   => '',
				'count' => $count,
				'data'  => $data
			]);
		}

		return view();
	}
      
}
