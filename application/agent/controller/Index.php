<?php
namespace app\agent\controller;

use library\Controller;
use think\Db;

class Index extends Common
{
    public function index()
    {
        error_reporting(0);
        // $do = model('api/UserTeam');
        $today = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        
        //开始时间
        $startDate = (isset($param['startdate']) && $param['startdate']) ? strtotime($param['startdate']) : $today - 86400 * 0;
        //结束时间
        $endDate = (isset($param['enddate']) && $param['enddate']) ? strtotime($param['enddate'].' 23:59:59') : $today + 86400;
        
        $data['date_range'] = "";
        
        $param = input('param.');
        // 时间
        if (isset($param['date_range']) && $param['date_range']) {
            $dateTime  = explode(' - ', $param['date_range']);
            $startDate = strtotime($dateTime[0]);
            $endDate   = strtotime($dateTime[1]);
            // dump($param);exit;
            $data['date_range'] = date('Y-m-d', $startDate) . ' - ' . date('Y-m-d', $endDate);//$param['datetime_range'];
        }
        else
        {
            $data['date_range'] = date('Y-m-d', $startDate).' - '.date('Y-m-d', $endDate);
        }
        
        $down_userids_arr = [];
        // $this->getDownline($this->userid, $down_userids_arr);
        // $down_userids_arr[] = $this->userid;
        $down_userids = db('xy_users')->where('id', $this->userid)->value('down_userid');//implode(",", $down_userids_arr);
        /**
         * 团队报表
         */
        
        $recharge_where2 = "id NOT IN ('SY2107251151259242','SY2107251151002279','SY2107251150449815','SY2107251148427238','SY2107251148114087','SY2107251147578332','SY2107251147427923','SY2107251147287011','SY2107251147144023','SY2107251146569183','SY2107251145473196','SY2107251145252488','SY2107251144517484','SY2107251144243426','SY2107251143591316','SY2107251143368936','SY2107251143173945','SY2107251142539171','SY2107251142265092','SY2107251142076757','SY2107251141421445','SY2107251140389732','SY2107251140184928','SY2107251140024973','SY2107251139252880','SY2107251139062084','SY2107251138217953','SY2107251052198794','SY2107251051251526','SY2107251051251583','SY2107251051251704','SY2107251051251790','SY2107251051252006','SY2107251051252270','SY2107251051252546','SY2107251051253724','SY2107251051253847','SY2107251051254697','SY2107251051254865','SY2107251051255476','SY2107251051255771','SY2107251051255823','SY2107251051255870','SY2107251051256456','SY2107251051256539','SY2107251051256584','SY2107251051257184','SY2107251051257850','SY2107251051257984','SY2107251051258232','SY2107251051258793','SY2107251051259486','SY2107251051259733','SY2107251051259752','SY2107251051259901','SY2107251039421436','SY2107251039425007','SY2107251039425452','SY2107251039426377','SY2107251039427043','SY2107251039427068','SY2107251039427553','SY2107251039427719','SY2107251039427858','SY2107251039429470')";
        // 团队余额
        $data['teamBalance']        = round(db('xy_users')
            ->where('id', "IN", $down_userids)
            ->sum(Db::raw('balance - freeze_balance')), 2);
        
        // $param['trade_number']      = 'L'.trading_number();
        // 团队收益
        $data['teamProfit']         = round(db('xy_reward_log')
            ->where('uid', "IN", $down_userids)
            ->whereTime('addtime', 'between', [$startDate, $endDate])
            ->sum('num'), 2);
        
        // 团队总充值
        $data['teamRecharge']       = round(db('xy_recharge')
            ->where('uid', "IN", $down_userids)
            ->where('status', '=', 2)
            ->where($recharge_where2)
            ->whereTime('addtime', 'between', [$startDate, $endDate])
            ->sum('num'), 2);
        
        $user_manual_deduct = round(db('xy_balance_log')->alias('xr')
            ->where('type = 16')
            ->where('uid', "IN", $down_userids)
            ->where('xr.addtime','between',[$startDate, $endDate])
            ->where($recharge_where2)
            ->sum('xr.num'), 2);
        $data['teamRecharge'] = $data['teamRecharge'];//$user_manual_deduct > $data['teamRecharge'] ? 0 : $data['teamRecharge'] - $user_manual_deduct;
        $data['user_manual_deduct'] = $user_manual_deduct;
        // 团队总提现
        $data['teamWithdrawal']     = round(db('xy_deposit')
            ->where('uid', "IN", $down_userids)
            ->where('status', '=', 2)
            ->whereTime('addtime', 'between', [$startDate, $endDate])
            ->sum('num'), 2);
        
        $param['trade_number']      = 'L'.trading_number();
        // 直推人数
        $data['directlyUnder']      = db('xy_users')
        ->where('parent_id', $this->userid)
        ->whereTime('addtime', 'between', [$startDate, $endDate])
        ->count();
        
        // 今日首冲
        $data['firstRechargeToday'] = db('xy_recharge')
        ->where('uid', "IN", $down_userids)
        ->where('status', '=', 2)
        ->where($recharge_where2)
        ->where("pay_name != 'manual'")
        ->whereTime('addtime', 'between', [$startDate, $endDate])
        ->group('uid')
        ->count();
        
        //团队总人数
        $data['teamNumber']         = sizeof(explode(",",$down_userids));
        
        // 新增人数
        $data['newReg']             = db('xy_users')
        ->where(['id' => ["IN", $down_userids]])
        ->whereTime('addtime', 'between', [$startDate, $endDate])
        ->count();
        
        // 计算日期间隔
        $dateSpace = ($endDate - $startDate) / 86400;
        if ($dateSpace > 31) {
            $dateSpace = 31;
        }
        if ($dateSpace < 1) {
            $dateSpace = 1;
        }
        
        
        $data['team1']['teamRechargeCount'] = 0;
        $data['team1']['teamRechargeNumber'] = 0;
        $data['team1']['teamSpreadSum']  = 0;
        
        $data['team2']['teamRechargeCount'] = 0;
        $data['team2']['teamRechargeNumber'] = 0;
        $data['team2']['teamSpreadSum']  = 0;
        
        $data['team3']['teamRechargeCount'] = 0;
        $data['team3']['teamRechargeNumber'] = 0;
        $data['team3']['teamSpreadSum']  = 0;
        
        $uid      = $user['id'];
        // $lvl1     = model('Users')->where('sid', '=', $uid)->column('id,sid');
        // $key_lvl1 = array_keys($lvl1);
        
        // if(count( $key_lvl1) > 0)
        // {
        //     $data['team1']['teamRechargeCount']     = model('UserDaily')->where([['uid', 'in', $key_lvl1]])->sum('recharge');//充值金额(QWE)
        //     $data['team1']['teamRechargeNumber']    = model('UserDaily')->where([['uid', 'in', $key_lvl1],['recharge', '>', '0']])->distinct(true)->field('uid')->count();//充值人数(个)
        //     $data['team1']['teamSpreadSum']         = model('UserDaily')->where([['uid', 'in', $key_lvl1]])->sum('rebate');//充值返佣(QWE)
        
        //     $lvl2     = model('Users')->where('sid', 'in', $key_lvl1)->column('id,sid');
        //     $key_lvl2 = array_keys($lvl2);
        
        //     if(count( $key_lvl2) > 0)
        //     {
        
        //         $data['team2']['teamRechargeCount']     = model('UserDaily')->where([['uid', 'in', $key_lvl2]])->sum('recharge');//充值金额(QWE)
        //         $data['team2']['teamRechargeNumber']    = model('UserDaily')->where([['uid', 'in', $key_lvl2],['recharge', '>', '0']])->distinct(true)->field('uid')->count();//充值人数(个)
        //         $data['team2']['teamSpreadSum']         = model('UserDaily')->where([['uid', 'in', $key_lvl1]])->sum('rebate');//充值返佣(QWE)
        
        //         $lvl3     = model('Users')->where('sid', 'in', $key_lvl2)->column('id,sid');
        //         $key_lvl3 = array_keys($lvl3);
        
        //         if(count( $key_lvl3) > 0)
        //         {
        
        //             $data['team3']['teamRechargeCount']     = model('UserDaily')->where([['uid', 'in', $key_lvl3]])->sum('recharge');//充值金额(QWE)
        //             $data['team3']['teamRechargeNumber']    = model('UserDaily')->where([['uid', 'in', $key_lvl3],['recharge', '>', '0']])->distinct(true)->field('uid')->group('uid')->count();//充值人数(个)
        //             $data['team3']['teamSpreadSum']         = model('UserDaily')->where([['uid', 'in', $key_lvl3]])->sum('rebate');//充值返佣(QWE)
        //         }
        
        //     }
        // }
        
        return view('', [
            'data' => $data,
        ]);
    }
    
    public function userlist()
    {
        if (request()->isAjax()) {
            //获取参数
            $param = input('post.');
            //查询条件组装
            $where = [];
            //查询符合条件的数据
            
            $count              = model('Users')->field('id,sid,username,state')->where('sid', $this->userid)->count(); // 总记录数
            $param['limit']     = (isset($param['limit']) and $param['limit']) ? $param['limit'] : 15; // 每页记录数
            $param['page']      = (isset($param['page']) and $param['page']) ? $param['page'] : 1; // 当前页
            $limitOffset        = ($param['page'] - 1) * $param['limit']; // 偏移量
            
            $touserid =  isset($param['touserid'])?intval($param['touserid']):$this->userid;
            $userList = model('Users')->field('id,sid,username,state')->where('sid', $touserid)->limit($limitOffset, $param['limit'])->select()->toArray();
            $data = model('manage/UserDaily')->teamStatistic($userList, 0, 9999999999, $touserid);
            return json([
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $userList
            ]);
            unset($data['totalAll']);
            return json([
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $data
            ]);
        }
        return view('', [
        ]);
    }
    
    public function _empty($name)
    {
        //echo $name;
        return view($name);
    }
    
}
