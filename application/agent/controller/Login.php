<?php
namespace app\agent\controller;

use library\Controller;

class Login extends Controller
{
    public function initialize()
    {
        header('Access-Control-Allow-Origin:*');
    }
    
    public function index()
    {
        //是否是isAjax提交
        if ($this->request->isAjax()) {
            $username = input('username/s');
            $password = input('password/s');
            $verifyCode = input('code/d');
            //if (session('code') != $verifyCode) return 'code';
            //获取用户信息
            $userinfo = db('xy_users')->where(array(['username','=',$username],['status','=',1],['is_agent','=',1]))->find();

            if ($verifyCode != session('code')) {
                return 'code';
            }
            //用户名不存在
            if (!$userinfo) {
                return 0;
            }

            if($userinfo['pwd'] != sha1($password.$userinfo['salt'].config('pwd_str')))
            { 
                //检查密码
                return 'pwd';
            }

            // // 添加登陆日志
            // $loginlog = array(
            //     'uid'			=> $userinfo['id'],
            //     'username'		=> $userinfo['username'],
            //     'os'			=> get_os(),
            //     'browser'		=> get_broswer(),
            //     'ip'			=> request()->ip(),
            //     'time'			=> time(),
            //     'address'		=> $address,
            //     'type'			=> $type
            // );
            // $is_insert_user_loginlog = model('Loginlog')->insert($loginlog);
            // //更新用户登录状态
            // $is_user_update = model('Users')->where('id', $userinfo['id'])->data(array('last_ip'=>request()->ip(),'last_login'=>time(),'login_error'=>0))->setInc('login_number', 1);
            session('agent', $userinfo);

            return 1;
        }
        return $this->fetch();
    }
    
    public function code()
    {
        ob_clean();
        $image = imagecreatetruecolor(100, 34);
        $bgcolor = imagecolorallocate($image, 255, 255, 255);
        imagefill($image, 0, 0, $bgcolor);
      
        $captch_code = '';
        for ($i=0;$i<4;$i++) {
            $fontsize = 6;
            $fontcolor = imagecolorallocate($image, rand(0, 120), rand(0, 120), rand(0, 120));
      
            $data = '0123456789';
            $fontcontent = substr($data, rand(0, strlen($data)-1), 1);
            $captch_code .= $fontcontent;
      
            $x = ($i*100/4) + rand(5, 10);
            $y = rand(5, 10);
      
            imagestring($image, $fontsize, $x, $y, $fontcontent, $fontcolor);
        }
        session('code', $captch_code);
      
        //增加点干扰元素
        for ($i=0; $i<200;$i++) {
            $pointcolor = imagecolorallocate($image, rand(50, 200), rand(50, 200), rand(50, 200));
            imagesetpixel($image, rand(1, 99), rand(1, 29), $pointcolor);
        }
      
        //增加线干扰元素
        for ($i=0;$i<3;$i++) {
            $linecolor = imagecolorallocate($image, rand(80, 220), rand(80, 220), rand(80, 220));
            imageline($image, rand(1, 99), rand(1, 29), rand(1, 99), rand(1, 29), $linecolor);
        }
      
      
        header('content-type:image/png');
        imagepng($image);
      
        imagedestroy($image);
    }
    
    //退出
    public function logout()
    { 
        session('agent', null);
        // $this->success("成功退出", '/agent');
        return 1;
    }
}
