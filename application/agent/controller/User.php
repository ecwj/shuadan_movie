<?php
namespace app\agent\controller;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

use app\agent\controller\Common;

use think\Db;

class User extends Common{
	/**
	 * 空操作处理
	 */
	public function _empty(){
		return $this->UserList();
	}
	public function index(){
		return $this->redirect('userList');	
	}
	/**
	 * 用户列表
	 */
	public function userList(){
  
		if (request()->isAjax()) {

			
			// $teamlists = model('user_team')->where('uid', $this->userid)->column("team");

			$down_userids_arr = [];
	        // $this->getDownline($this->userid, $down_userids_arr);
	        $down_userids = db('xy_users')->where('id', $this->userid)->value('down_userid');
	        // $down_userids_arr[] = $this->userid;
	        // $down_userids = implode(",", $down_userids_arr);

			//获取参数
			$param = input('post.');
			//查询条件组装
			$where = [];
			// $where[] = ['ly_users.id','<>',$this->userid]; 
			// $where[] = ['ly_users.id','in', implode(',', $teamlists)]; 
			$where[] = array('xy_users.id','IN',$down_userids);
		  
			//用户名
			if(isset($param['username']) && $param['username']){
				$where[] = ['xy_users.username','like','%'.$param['username'].'%'];
			}
			//用户名
			if(isset($param['uid']) && $param['uid']){
				$where[] = ['xy_users.uid','=',$param['uid']];
			}
			
			if (isset($param['user_type']) && $param['user_type']) {
			    $where[] = ['xy_users.user_type', '=', $param['user_type']];
			}

			//邀请码 推荐人
			if(isset($param['idcode']) && $param['idcode']){
				$where[] = ['xy_users.invite_code','=',$param['idcode']];
			}

			//用户名
			if(isset($param['balance1']) && $param['balance1']){
				$where[] = [Db::raw('xy_users.balance - xy_users.freeze_balance'),'>=',$param['balance1']];
			}
			//用户名
			if(isset($param['balance2']) && $param['balance2']){
				$where[] = ['balance','<=',$param['balance2']];
			}
			//用户名
			if(isset($param['state']) && $param['state']){
				$where[] = ['xy_users.status','=',$param['state']];
			}
			//用户名
			if(isset($param['is_automatic']) && $param['is_automatic']){
				$where[] = ['is_automatic','=',$param['is_automatic']];
			}
			// 时间
			if(isset($param['datetime_range']) && $param['datetime_range']){
				$dateTime = explode(' - ', $param['datetime_range']);
				$dateTime[0] = date('Y-m-d 00:00:00', strtotime($dateTime[0]));
				$dateTime[1] = date('Y-m-d 23:59:59', strtotime($dateTime[1]));
				$param['datetime_range'] = $dateTime[0] . ' - ' . $dateTime[1];
				$where[]  = ['xy_users.addtime','>=',strtotime($dateTime[0])];
				$where[]  = ['xy_users.addtime','<=',strtotime($dateTime[1])];
			}

			$count              = model('admin/Users')
			->where($where)->count(); // 总记录数

			$param['limit']     = (isset($param['limit']) and $param['limit']) ? $param['limit'] : 15; // 每页记录数
			$param['page']      = (isset($param['page']) and $param['page']) ? $param['page'] : 1; // 当前页
			$limitOffset        = ($param['page'] - 1) * $param['limit']; // 偏移量
			$param['sortField'] = (isset($param['sortField']) && $param['sortField']) ? $param['sortField'] : 'reg_time';
			$param['sortType']  = (isset($param['sortType']) && $param['sortType']) ? $param['sortType'] : 'desc';

			//查询符合条件的数据
			$data = db('xy_users')->field(['xy_users.*',Db::raw('xy_users.addtime AS reg_time'),'xy_users.balance','xy_users.freeze_balance', 'xy_level.name as level_name'])
			->join('xy_level','xy_users.level = xy_level.level') 
			->where($where)->order($param['sortField'], $param['sortType'])->limit($limitOffset, $param['limit'])->select();
 
			// $userState = config('custom.userState');//账号状态
			$userState = [
				1 => '正常',
				2 => '冻结'
			];
			
			foreach ($data as $key => &$value) {
				$value['reg_time'] = date('Y-m-d H:i:s', $value['reg_time']);
				$value['stateStr']    = $userState[$value['status']];
				$value['isOnline'] = (cache('C_token_'.$value['id'])) ? '在线' : '离线';
				$value['level_name'] = ($value['level_name'] != '') ? $value['level_name'] : 'Trial Fan';
			}
 
			return json([
				'code'  => 0,
				'msg'   => '',
				'count' => $count,
				'data'  => $data
			]);
		}

		return $this->fetch();
	}
 
	/**
	 * 添加账号
	 */
	public function add(){
		if(request()->isAjax()){ 
			$tel = input('post.tel/s','');
            $user_name = input('post.username/s','');
            $pwd = input('post.pwd/s','');
            $parent_username = input('post.parent_username/s','');
            $shuijun = input('post.shuijun/d',0);
            $parent_id = 0;

            if ($shuijun == 1) {
            	$info = db('xy_users')->where('username', 'shuijun')->value('id');
            	$parent_id = $info;
            } else {
	            if ($parent_username != '') {
	                $info = db('xy_users')->where('username', $parent_username)->value('id');
	                if ($info) {
	                	$down_userid = db('xy_users')->where('id', $this->userid)->value('down_userid');
	                	$info = db('xy_users')->where('username', $parent_username)->where("id IN ($down_userid)")->value('id');
	                	if (!$info) {
	                		return '上级用户只能是自己的团队';	
	                	}
	                    $parent_id = $info;
	                } else {
	                    return '上级用户名不存在';
	                }
	            } else {
	            	return '上级用户名不能为空';
	            }
	        }
            $user_names = explode(",", $user_name);
            $re = 0;
            $invalidUsers = [];
            $existusernames = db('xy_users')->where('username', "IN", $user_name)->column('username');
            if (sizeof($existusernames) > 0) {
            	if (sizeof($user_names) == 1) {
            		return '用户名已被使用';
            	} else {
            		return '这些用户名已被使用: ' . implode(",", $existusernames);
            	}
            }
            $token = '';
            foreach ($user_names as $username) {
                // if (sizeof($user_names) > 0) {
                    $tel = $username;
                // }
                $res = model('admin/Users')->add_users($tel,$username,$pwd,$parent_id,$token);
                if($res['code']!==0){
                    $invalidUsers[] = $username;
                } else {
                    $re ++;
                }
            }
            $msg = "";
            if (sizeof($invalidUsers) > 0) {
                $msg = "这些用户名已被使用： " . implode(",", $invalidUsers);
            }
            
            if($re == 0){
                return $res['info'] . $msg;
            }
            return 1;
		}

		return view('', [

		]);
	}
	/**
	 * 用户编辑
	 */
	public function edit(){
		if(request()->isAjax()){
			$id = input('post.id/d',0);
			$pwd = input('post.pwd/s','');
            $pwd2 = input('post.pwd2/s','');
            $data = [];
            if($pwd){
	            $salt = rand(0,99999); //生成盐
	            $data['pwd']    = sha1($pwd.$salt.config('pwd_str'));
	            $data['salt']   = $salt;
	        }
	        if($pwd2){
	            $salt2 = rand(0,99999); //生成盐
	            $data['pwd2']    = sha1($pwd2.$salt2.config('pwd_str'));
	            $data['salt2']   = $salt2;
	        }
	        if ($data) {
	        	$res = db('xy_users')->where('id',$id)->update($data);
	        	if ($res) {
	        		return 1;
	        	} else {
	        		return 0;
	        	}
	        }
		}
		
		$data = db('xy_users')->find(input('id'));
		$upusername = db('xy_users')->where('id', $data['parent_id'])->value('username');

		$this->assign('userInfo',$data);
		$this->assign('upusername',$upusername);
		return $this->fetch();
	}
 
	/**
	 * 删除操作
	 */
	public function del(){
		return db('xy_bankinfo')->whzhihangere('id', input('id'))->delete();
	}
 
 
	/**
	 * 代理迁移
	 */
	public function team_move(){
		if(request()->isAjax()){
			return model('manage/Users')->agentTeamMove();
		}

		$data = model('manage/TeammoveLog')->agentTeamMoveView();

		$this->assign('teammoveLog',$data['teammoveLog']);
		$this->assign('page',$data['page']);

		return $this->fetch();
	}

	/**
	 * 用户银行
	 */
	public function bank(){
		if (request()->isAjax()) {
			
			$agentID = session("agent")['id'];
			$this->assign('agentID', $agentID);

			$param = input('post.');//获取参数
			//查询条件组装
			$where = array();
			//用户名搜索
			if(isset($param['username']) && $param['username']){
				$where[] = array('users.username','=',$param['username']);
			}
			//账户名搜索
			if(isset($param['name']) && $param['name']){
				$where[] = array('name','=',$param['name']);
			}
			//账号搜索
			if(isset($param['card_no']) && $param['card_no']){
				$where[] = array('cardnum','=',$param['card_no']);
			}
			//绑定时间搜索
			// if(isset($param['datetime_range']) && $param['datetime_range']){
			// 	$dateTime = explode(' - ', $param['datetime_range']);
			// 	$where[] = array('ly_user_bank.add_time','>=',strtotime($dateTime[0]));
			// 	$where[] = array('ly_user_bank.add_time','<=',strtotime($dateTime[1]));
			// }

			$down_userids_arr = [];
	        // $this->getDownline($this->userid, $down_userids_arr);
	        // $down_userids_arr[] = $this->userid;
	        // $down_userids = implode(",", $down_userids_arr);
	        $down_userids = db('xy_users')->where('id', $agentID)->value('down_userid');
	        $where[] = array('xy_users.id','IN',$down_userids);
			 
			// $teamlists = implode(',',$this->teamlists); 
			// $where[] = array('users.id','in',$teamlists);
			// $where[] = array('user_team.uid','=',$this->userid);
		 
			$count              = db('xy_bankinfo')
			->join('xy_users','xy_bankinfo.uid = xy_users.id')
			->where($where)->count(); // 总记录数
 
			$param['limit']     = (isset($param['limit']) and $param['limit']) ? $param['limit'] : 15; // 每页记录数
			$param['page']      = (isset($param['page']) and $param['page']) ? $param['page'] : 1; // 当前页
			$limitOffset        = ($param['page'] - 1) * $param['limit']; // 偏移量
			$param['sortField'] = (isset($param['sortField']) && $param['sortField']) ? $param['sortField'] : 'xy_bankinfo.id';
			$param['sortType']  = (isset($param['sortType']) && $param['sortType']) ? $param['sortType'] : 'desc';

			//查询符合条件的数据
			$data = db('xy_bankinfo')->field('xy_bankinfo.*,xy_users.username,xy_bankinfo.username AS name, xy_bankinfo.cardnum AS card_no,xy_bankinfo.bankname AS bank_name,xy_bankinfo.site AS bank_branch_name')
			->join('xy_users','xy_bankinfo.uid = xy_users.id')
			->where($where)->order($param['sortField'], $param['sortType'])->limit($limitOffset, $param['limit'])->select();

			$adminColor = config('manage.adminColor');
			//部分元素重新赋值
			foreach ($data as $key => &$value) {
				$value['statusColor'] = $adminColor[$value['status']];
				switch ($value['status']) {
					case '2':
						$value['status'] = '锁定';
						break;
					case '3':
						$value['status'] = '删除';
						break;
					default:
						$value['status'] = '正常';
						break;
				}
			}

			return json([
				'code'  => 0,
				'msg'   => '',
				'count' => $count,
				'data'  => $data
			]);
		}

		return view();
	}

	/**
	 * 关系树
	 */
	public function relation(){
		if (request()->isAjax()) {
			$param = input('param.');

			$newUser = model('manage/Users')->alias('u')->field('u.id,username as title,sid as field');

			$where = [];

			$parentID = $this->userid;
			if (isset($param['username']) && $param['username']) {
				// $where[] = ['username', 'like', '%'.$param['username'].'%'];
				// $newUser->join('user_team','u.id=user_team.team');
				$user = model('manage/Users')->alias('u')->field('u.id,username as title,sid as field');
				$findUser = $user->where('username', 'like', '%'.$param['username'].'%')->find();
				$parentID = $findUser['id'];

			}

				
			// $teamlists = implode(',',$this->teamlists);  
			$where1 = [['user_team.uid','=',$this->userid]]; 
			$array = $newUser
			->join('user_team','user_team.team = ly_users.id')
			->where($where1)->select()->toArray();
			
			if (!$array) return json(['code'=>0, 'data'=>[], 'msg'=>'查无此人']);
 
			$res  = [];
			$tree = [];

			//整理数组
			foreach($array as $key=>$value){
				$res[$value['id']] = $value;
				$res[$value['id']]['children'] = [];
			}
			unset($array);
			//查询子孙
			foreach($res as $key=>$value){
				if($value['field'] != 0){
					$res[$value['field']]['children'][] = &$res[$key];
				}
			} 

			//去除杂质  
			foreach($res as $key=>$value){
				if (!isset($value['title'])) {
					unset($res[$key]);
					continue;
				}
				if($value['field'] == $parentID){
					$tree[] = $value;
				}
			}
			unset($res);

			return json(['code'=>1, 'data'=>$tree, 'msg'=>'ok']);
		}

		return view();
	}

	
    /**
     * 团队报表
     */
    public function team_statistic()
    { 
        $agentID = session("agent")['id'];
        $this->assign('agentID', $agentID);

        $param = input('get.');
        $username = input('username');
        $id = input('id');
        $date_range = input('date_range');
        // $data  = model('manage/Users')->agentTeamStatistic();
        $uid = $id ? $id : $agentID;

        // $down_userids_arr = [];
        // $this->getDownline($agentID, $down_userids_arr);
        // $down_userids_arr[] = $agentID;
        // $down_userids = implode(",", $down_userids_arr);
        $down_userids = db('xy_users')->where('id', $agentID)->value('down_userid');

        // $where['id'] = ['IN', $down_userids];
        $where['parent_id'] = $uid;
        $whereOr['id'] = $uid;

        $upusername_list = "";
        $level = 1;
        $ids = team_get_ids($uid, $level, $idsStr);
        if ($uid != $agentID) {
        	$parents = model('admin/Users')->get_parent_users($uid);
            $newparents = [];
            foreach ($parents as $key => $p) {
                if ($p == $agentID) {
                    $newparents[] = $p;
                    break;
                } else {
                    $newparents[] = $p;
                }
            }
            $parents = $newparents;
            krsort($parents);
            // $mapupusername['id'] = ["IN", implode(",", $parents)];
            $upusernames = db('xy_users')->where('id IN (' . implode(",", $parents) . ')')->column("id, username");
            $size = sizeof($upusernames);
            $upusername_list = "";
            $i = 1;
            foreach ($parents as $uid) {
            	$upusername_list .= "<a href=\"/agent/user/team_statistic?date_range=$date_range&isUser=&id={$uid}\">{$upusernames[$uid]}</a>";
                if ($i != $size) {
                    $upusername_list .= ' > ';
                }
                $i ++;
            }
        }

        $whereInDownline = '';
        if ($username) {
        	unset($where['parent_id']);
        	unset($whereOr['id']);
        	$where['username'] = $username;
        	$whereInDownline = 'id IN (' . $down_userids . ')';
        }

        $where_date_range = [];
        if ($date_range) {
        	$dates = explode(" - ", $date_range);
        	$date1 = strtotime(date('Y-m-d 00:00:00', strtotime($dates[0])));
        	$date2 = strtotime(date('Y-m-d 23:59:59', strtotime($dates[1])));
        	$where_date_range[] = ['addtime', 'BETWEEN', [$date1, $date2]];
        }

        $querydata = array(
            'date_range'=>$date_range,
            'username'=>$username,
            'id' => $id
        );

        $down_users_list = db('xy_users')->where($where)->where($whereInDownline)->whereOr($whereOr)->paginate(10, false, ['query' => $querydata]);

        $lists = $down_users_list->all();
		$page = $down_users_list->render();

		$total['totalPage'] = [
			'recharge' => 0,
			'withdrawal' => 0,
			'team_recharge' => '-',
			'team_withdrawal' => '-',
			'spread' => 0,
			'commission' => 0,
		];
		$total['totalAll'] = [
			'recharge' => 0,
			'withdrawal' => 0,
			'team_recharge' => 0,
			'team_withdrawal' => 0,
			'spread' => 0,
			'commission' => 0,
		];
		
		$recharge_where2 = "id NOT IN ('SY2107251151259242','SY2107251151002279','SY2107251150449815','SY2107251148427238','SY2107251148114087','SY2107251147578332','SY2107251147427923','SY2107251147287011','SY2107251147144023','SY2107251146569183','SY2107251145473196','SY2107251145252488','SY2107251144517484','SY2107251144243426','SY2107251143591316','SY2107251143368936','SY2107251143173945','SY2107251142539171','SY2107251142265092','SY2107251142076757','SY2107251141421445','SY2107251140389732','SY2107251140184928','SY2107251140024973','SY2107251139252880','SY2107251139062084','SY2107251138217953','SY2107251052198794','SY2107251051251526','SY2107251051251583','SY2107251051251704','SY2107251051251790','SY2107251051252006','SY2107251051252270','SY2107251051252546','SY2107251051253724','SY2107251051253847','SY2107251051254697','SY2107251051254865','SY2107251051255476','SY2107251051255771','SY2107251051255823','SY2107251051255870','SY2107251051256456','SY2107251051256539','SY2107251051256584','SY2107251051257184','SY2107251051257850','SY2107251051257984','SY2107251051258232','SY2107251051258793','SY2107251051259486','SY2107251051259733','SY2107251051259752','SY2107251051259901','SY2107251039421436','SY2107251039425007','SY2107251039425452','SY2107251039426377','SY2107251039427043','SY2107251039427068','SY2107251039427553','SY2107251039427719','SY2107251039427858','SY2107251039429470')";

		$total['totalAll']['recharge'] = db('xy_recharge')->where('uid IN (' . $down_userids . ')')->where($where_date_range)->where('status', 2)->where($recharge_where2)->sum('num');
		$total['totalAll']['withdrawal'] = db('xy_deposit')->where('uid IN (' . $down_userids . ')')->where($where_date_range)->where('status', 2)->sum('num');
		$total['totalAll']['team_recharge'] = $total['totalAll']['recharge'];
		$total['totalAll']['team_withdrawal'] = $total['totalAll']['withdrawal'];
		$total['totalAll']['spread'] = db('xy_balance_log')->where('uid IN (' . $down_userids . ')')->where($where_date_range)->where('type IN (11, 12)')->where('status', 1)->sum('num');
		$total['totalAll']['commission'] = db('xy_balance_log')->where('uid IN (' . $down_userids . ')')->where($where_date_range)->where('type IN (13)')->sum('num');

		foreach ($lists as $key => $list) {
			$lists[$key]['teamCount'] = sizeof(explode(",", $list['down_userid'])) - 1;
			$lists[$key]['recharge'] = db('xy_recharge')->where('uid', $list['id'])->where($where_date_range)->where('status', 2)->where($recharge_where2)->sum('num');
			$lists[$key]['withdrawal'] = db('xy_deposit')->where('uid', $list['id'])->where($where_date_range)->where('status', 2)->sum('num');
			$lists[$key]['spread'] = db('xy_balance_log')->where('uid', $list['id'])->where($where_date_range)->where('type IN (11, 12)')->where('status', 1)->sum('num');
			$lists[$key]['commission'] = db('xy_balance_log')->where('uid', $list['id'])->where($where_date_range)->where('type IN (13)')->sum('num');

			$lists[$key]['team_recharge'] = db('xy_recharge')->where('uid', 'IN', $list['down_userid'])->where($where_date_range)->where('status', 2)->where($recharge_where2)->sum('num');
			$lists[$key]['team_withdrawal'] = db('xy_deposit')->where('uid', 'IN', $list['down_userid'])->where($where_date_range)->where('status', 2)->sum('num');

			$lists[$key]['sid'] = $list['parent_id'];

			$total['totalPage']['recharge'] += $lists[$key]['recharge'];
			$total['totalPage']['withdrawal'] += $lists[$key]['withdrawal'];
			$total['totalPage']['spread'] += $lists[$key]['spread'];
			$total['totalPage']['commission'] += $lists[$key]['commission'];

			$parentId = $list['parent_id'];
			$lists[$key]['upusername'] = $parentId ? db('xy_users')->where('id', $parentId)->value('username') : '';
		}

		if ($lists) {
            $lists = team_level_dc($lists, $ids);
        }

		$this->assign('page', $page);
		$this->assign('data', $lists);
		$this->assign('upusername_list', $upusername_list);
		$this->assign('where', $where);
		$this->assign('date_range', $date_range);
		$this->assign('total', $total);
		$this->assign('uid', $uid);
        return $this->fetch();
    }

    /**
	 * 用户列表
	 */
	public function shuijunList(){
  
		if (request()->isAjax()) {

			
			// $teamlists = model('user_team')->where('uid', $this->userid)->column("team");

			// $down_userids_arr = [];
	  //       $this->getDownline($this->userid, $down_userids_arr);
	  //       $down_userids_arr[] = $this->userid;
	  //       $down_userids = implode(",", $down_userids_arr);

	        $down_userids = db('xy_users')->where('username', 'shuijun')->value('down_userid');
			//获取参数
			$param = input('post.');
			//查询条件组装
			$where = [];
			// $where[] = ['ly_users.id','<>',$this->userid]; 
			// $where[] = ['ly_users.id','in', implode(',', $teamlists)]; 
			$where[] = array('xy_users.id','IN',$down_userids);
		  
			//用户名
			if(isset($param['username']) && $param['username']){
				$where[] = ['xy_users.username','like','%'.$param['username'].'%'];
			}
			//用户名
			if(isset($param['uid']) && $param['uid']){
				$where[] = ['xy_users.uid','=',$param['uid']];
			}
			
			if (isset($param['user_type']) && $param['user_type']) {
			    $where[] = ['xy_users.user_type', '=', $param['user_type']];
			}

			//邀请码 推荐人
			if(isset($param['idcode']) && $param['idcode']){
				$where[] = ['xy_users.invite_code','=',$param['idcode']];
			}

			//用户名
			if(isset($param['balance1']) && $param['balance1']){
				$where[] = [Db::raw('xy_users.balance - xy_users.freeze_balance'),'>=',$param['balance1']];
			}
			//用户名
			if(isset($param['balance2']) && $param['balance2']){
				$where[] = ['balance','<=',$param['balance2']];
			}
			//用户名
			if(isset($param['state']) && $param['state']){
				$where[] = ['xy_users.status','=',$param['state']];
			}
			//用户名
			if(isset($param['is_automatic']) && $param['is_automatic']){
				$where[] = ['is_automatic','=',$param['is_automatic']];
			}
			// 时间
			if(isset($param['datetime_range']) && $param['datetime_range']){
				$dateTime = explode(' - ', $param['datetime_range']);
				$where[]  = ['xy_users.addtime','>=',strtotime($dateTime[0])];
				$where[]  = ['xy_users.addtime','<=',strtotime($dateTime[1])];
			}

			$count              = model('admin/Users')
			->where($where)->count(); // 总记录数

			$param['limit']     = (isset($param['limit']) and $param['limit']) ? $param['limit'] : 15; // 每页记录数
			$param['page']      = (isset($param['page']) and $param['page']) ? $param['page'] : 1; // 当前页
			$limitOffset        = ($param['page'] - 1) * $param['limit']; // 偏移量
			$param['sortField'] = (isset($param['sortField']) && $param['sortField']) ? $param['sortField'] : 'reg_time';
			$param['sortType']  = (isset($param['sortType']) && $param['sortType']) ? $param['sortType'] : 'desc';

			//查询符合条件的数据
			$data = db('xy_users')->field(['xy_users.*',Db::raw('xy_users.addtime AS reg_time'),'xy_users.balance','xy_users.freeze_balance', 'xy_level.name as level_name'])
			->join('xy_level','xy_users.level = xy_level.level') 
			->where($where)->order($param['sortField'], $param['sortType'])->limit($limitOffset, $param['limit'])->select();
 
			// $userState = config('custom.userState');//账号状态
			$userState = [
				1 => '正常',
				2 => '冻结'
			];
			
			foreach ($data as $key => &$value) {
				$value['reg_time'] = date('Y-m-d H:i:s', $value['reg_time']);
				$value['stateStr']    = $userState[$value['status']];
				$value['isOnline'] = (cache('C_token_'.$value['id'])) ? '在线' : '离线';
				$value['level_name'] = ($value['level_name'] != '') ? $value['level_name'] : 'Trial Fan';
			}
 
			return json([
				'code'  => 0,
				'msg'   => '',
				'count' => $count,
				'data'  => $data
			]);
		}

		return $this->fetch();
	}

	/**
	 * 用户充值
	 */
	public function recharge(){
		if(request()->isAjax()) {
			$id = input('post.id/d',0);

			$user = db('xy_users')->find($id);
			if (!$user) {
				return '用户不存在';
			}
			$amount = input('post.amount/s','');
            $time = time();
            Db::startTrans();
            $oid = getSn('SY');
            $res = db('xy_recharge')
            ->insert([
                'id' => $oid,
                'uid' => $user['id'],
                'tel' => $user['tel'],
                'real_name' => $user['username'],
                'pic' => '',
                'num' => $amount,
                'addtime' => $time,
                'endtime' => $time,
                'pay_name' => 'manual',
                'status' => 2
            ]);
            $res1 = Db::name('xy_balance_log')
                    ->insert([
                        'uid'=>$user['id'],
                        'oid'=>$oid,
                        'num'=>$amount,
                        'type'=>1,
                        'status'=>1,
                        'addtime'=>$time,
                    ]);
            if ($res && $res1) {
                $res2 = Db::name('xy_users')->where('id',$user['id'])->setInc('balance',$amount);
                //用户总充值
                $total_recharge = Db::name('xy_recharge')->where(['uid' => $user['id'], 'status' => 2])->sum('num');
                Db::name('xy_users')->where('id', $user['id'])->update(['total_recharge' => $total_recharge]);
                if ($total_recharge >= config('invite_reward_min_recharge')) {
                    model('admin/Users')->invitationBonus($user['id'],$oid);
                }
            }

            if ($res && $res1 && $res2) {
	            Db::commit();
	            return 1;
	        } else {
	            return "手动充值失败";
	        }
        }
		
		$data = db('xy_users')->find(input('id'));

		$this->assign('userInfo',$data);
		return $this->fetch();
	}

	/**
	 * 用户影票订单报告
	 */
	public function userOrderList(){
  
		if (request()->isAjax()) {

			

			$down_userids_arr = [];
	        $down_userids = db('xy_users')->where('id', $this->userid)->value('down_userid');

			//获取参数
			$param = input('post.');
			//查询条件组装
			$where = [];
			if(input('username/s','')) $where[] = ['u.username','like','%' . input('username/s','') . '%'];
	        if(input('tel/s',''))$where[] = ['u.tel','like','%' . input('tel/s','') . '%'];
	        if(isset($param['datetime_range']) && $param['datetime_range']){
				$dateTime = explode(' - ', $param['datetime_range']);
				$dateTime[0] = date('Y-m-d 00:00:00', strtotime($dateTime[0]));
				$dateTime[1] = date('Y-m-d 23:59:59', strtotime($dateTime[1]));
				$param['datetime_range'] = $dateTime[0] . ' - ' . $dateTime[1];
				$where[]  = ['xc.addtime','>=',strtotime($dateTime[0])];
				$where[]  = ['xc.addtime','<=',strtotime($dateTime[1])];
			}
	        $where[] = ['u.id', 'IN', $down_userids];

			$count = db('xy_convey_movie')
            ->alias('xc')
            ->Join('xy_users u','u.id=xc.uid')            
            ->where($where)
            ->where("u.username != 'supermovie'")
            ->count('xc.id'); // 总记录数

			$param['limit']     = (isset($param['limit']) and $param['limit']) ? $param['limit'] : 15; // 每页记录数
			$param['page']      = (isset($param['page']) and $param['page']) ? $param['page'] : 1; // 当前页
			$limitOffset        = ($param['page'] - 1) * $param['limit']; // 偏移量
			$param['sortField'] = (isset($param['sortField']) && $param['sortField']) ? $param['sortField'] : 'u.id';
			$param['sortType']  = (isset($param['sortType']) && $param['sortType']) ? $param['sortType'] : 'desc';

			//查询符合条件的数据
			$data = db('xy_convey_movie')
            ->alias('xc')
            ->Join('xy_users u','u.id=xc.uid')
            ->leftJoin('xy_movie g','g.id=xc.goods_id')
            ->field('xc.*,u.real_name,u.nickname,u.username,u.tel,u.is_jia,u.is_free,u.parent_id,g.name,g.price,g.name AS goods_name')
            ->where($where)
            ->where("u.username != 'supermovie'")
            ->order($param['sortField'], $param['sortType'])
            ->limit($limitOffset, $param['limit'])->select();
 			
 			foreach ($data as $key => &$value) {
				$value['addtime'] = date('Y-m-d H:i:s', $value['addtime']);
				$value['status']    = '完成付款';
				$value['commission'] = $value['commission'];
				$value['price'] = $value['price'];
				$value['num'] = $value['num'];
				$value['real_name'] = ($value['real_name'] ? $value['real_name'] : '-') . '/' . ($value['nickname'] ? $value['nickname'] : '-');
			}

			return json([
				'code'  => 0,
				'msg'   => '',
				'count' => $count,
				'data'  => $data
			]);
		}

		return $this->fetch();
	}
}