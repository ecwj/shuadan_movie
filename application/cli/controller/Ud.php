<?php
namespace app\cli\Controller;

use library\Controller;
use think\Db;

class Ud extends Controller
{

    public function index()
    {
        $map = [];
        $users = db('xy_users')->where($map)->order('id desc')->select();
        $re = 0;
        Db::startTrans();
        foreach ($users as $user) {
            $up_userid = $user['parent_id'];
            $dl = [];
            dump($user['username'] . ' | ' . $up_userid);
            $this->getDownline($user['id'], $dl);
            $dlStr = implode(",", $dl);
            if ($dlStr) {
                $dlStr = $user['id'] . "," . $dlStr;
            } else {
                $dlStr = $user['id'];
            }
            $user['down_userid'] = $dlStr;
            $re = db('xy_users')->where('id', $user['id'])->update(['down_userid' => $dlStr]);
        }
        Db::commit();
        return $re;
    }

    function getDownline($uid, &$dl) {
        $users = db('xy_users')->where(['parent_id' => $uid])->select();
        if ($users) {
            foreach ($users as $user) {
                $dl[] = $user['id'];
                $this->getDownline($user['id'], $dl);
            }
        }

        return 1;
    }
}