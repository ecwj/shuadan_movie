<?php

$post = array_merge($_POST, $_GET, $_REQUEST);
foreach ($post as $k => $v) {
    $v = strtolower($v);
    if (strpos($v, 'select') !== false 
        || strpos($v, 'insert') !== false 
        || strpos($v, 'delete') !== false 
        || strpos($v, 'from') !== false 
        || strpos($v, 'xy_') !== false 
        || strpos($v, 'OR') !== false 
        || strpos($v, 'exp') !== false 
        || strpos($v, 'sql') !== false 
        || strpos($v, 'sqli') !== false) {
        // var_dump($post);
        // echo 'HELLO';exit;
    }
}

if (strpos(getcwd(), 'OIAJDHASD') === false) {

    require '../extend/Hashids/Hashids.php';

    require '../extend/PHPMailer/PHPMailer.php';
    require '../extend/PHPMailer/SMTP.php';
    require '../extend/PHPMailer/Exception.php';
}

/*
 * 会员等级获取当前允许单量额度
 * 计算过期
 */
function vip_num_order($uid, $level, &$upgradeFee = 0)
{

    $Levels = \think\Db::table('xy_level')->select();
    $count = 0;
    foreach ($Levels as $key => $item) {
        $count = $count + $item['out_day'] * $item['order_num'];
        if ($item['level'] == $level) break;
    }

    //二次匹配下一个等级的所需额度

    foreach ($Levels as $key => $item) {
        if ($item['level'] == $level+1) {
            $upgradeFee  =  $item['num'];
            break;
        }
    }
    return $count;

}


/*
 *
 * vip等级翻译 css
 *
 */

function vip_color_level($val = 1)
{
    return 'vip' . $val;
}


/**
 *  等级对称类型
 */

function team_level_dc($data, $levelData)
{

    foreach ($data as $key => $item) {

        $level = 0;
        foreach ($levelData as $a => $k) {
            if ($item['id'] == $k['id']) {
                $level = $k['level'];
                break;
            }
        }

        switch ($level) {
            case 3:
            case 1:
                $color = '#1E9FFF';
                break;
            case 2:
                $color = '#2b9aec';
                break;
            case 4:
                $color = '#76c0f7';
                break;
            default:
                $color = '#92c7ef';
                break;

        }

        $data[$key]['jb'] = '<span class="layui-btn layui-btn-xs layui-btn-danger" style="background: ' . $color . '">' . $level . '级' . '</span>';
        $data[$key]['level'] = $level;

    }

    return $data;
}


/**
 *
 * 获取当前用户名下所有用户id以及等级
 * @param int $uid
 * @param int $level
 * @param string $str_ids
 * @return array
 */
function team_get_ids($uid = 0, $level = -1, &$str_ids = "")
{
    $allData = [];
    $allIds = Db::table('xy_users')->field('id,parent_id,childs')->select();
    $newData = team_parent($allIds, $uid, 1, $allData);
    foreach ($allData as $key => $item) {
        foreach ($item as $a => $s) {
            $newData = array_merge($newData, $s);
        }
    }

    if ($level > 0) { //判断是否有等级要求
        $arr = [];
        foreach ($newData as $key => $item) {
            if ($item['level'] != $level) continue;
            $arr[] = $item;
        }
        $newData = $arr;
    }
    foreach ($newData as $key => $item) {
        $str_ids .= ',' . $item['id'];
    }
    $str_ids = substr($str_ids, 1);
    $newData = arraySort($newData, 'level', SORT_ASC);
    return $newData;
}


/**
 *  团队递归数组
 * @param $data
 * @param $uid
 * @param int $level
 * @param array $allData
 * @return array
 */
function team_parent($data, $uid, $level = 1, &$allData = [])
{

    $newData = [];
    foreach ($data as $key => $item) {
        if ($uid == $item['parent_id']) {
            $item['level'] = $level;
            unset($data[$key]);
            if ($item['childs'] > 0) {
                $newKey = $level + 1;
                $allData["lv_$newKey"][] = team_parent($data, $item['id'], $newKey, $allData);
            }
            $newData[] = $item;
        }
    }
    return $newData;
}


/**
 * 二维数组根据某个字段排序
 * @param array $array 要排序的数组
 * @param string $keys 要排序的键字段
 * @param string $sort 排序类型  SORT_ASC  SORT_DESC
 * @return array 排序后的数组
 */

function arraySort($array, $keys, $sort = SORT_DESC)
{
    $keysValue = [];
    foreach ($array as $k => $v) {
        $keysValue[$k] = $v[$keys];
    }
    array_multisort($keysValue, $sort, $array);
    return $array;
}


/*** 更新部分数据 ***/


function is_mobile($tel)
{
    if (preg_match("/^[6-9][0-9]{9}$/", $tel)) {
//    if(preg_match("/^1[345789]{1}\d{9}$/",$tel)){
        return true;
    } else {
        return false;
    }
}

/*
 * 检查图片是不是bases64编码的
 */
function is_image_base64($base64)
{
    if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64, $result)) {
        return true;
    } else {
        return false;
    }
}

function check_pic($dir, $type_img)
{
    $new_files = $dir . date("YmdHis") . '-' . rand(0, 9999999) . "{$type_img}";
    if (!file_exists($new_files))
        return $new_files;
    else
        return check_pic($dir, $type_img);
}

/**
 * 获取数组中的某一列
 * @param array $arr 数组
 * @param string $key_name 列名
 * @return array  返回那一列的数组
 */
function get_arr_column($arr, $key_name)
{
    $arr2 = array();
    foreach ($arr as $key => $val) {
        $arr2[] = $val[$key_name];
    }
    return $arr2;
}

//保留两位小数
function tow_float($number)
{
    return (floor($number * 100) / 100);
}

//生成订单号
function getSn($head = '')
{
    @date_default_timezone_set("PRC");
    $order_id_main = date('YmdHis') . mt_rand(1000, 9999);
    //唯一订单号码（YYMMDDHHIISSNNN）
    $osn = $head . substr($order_id_main, 2); //生成订单号
    return $osn;
}

/**
 * 修改本地配置文件
 *
 * @param array $name ['配置名']
 * @param array $value ['参数']
 * @return void
 */
function setconfig($name, $value)
{
    if (is_array($name) and is_array($value)) {
        for ($i = 0; $i < count($name); $i++) {
            $names[$i] = '/\'' . $name[$i] . '\'(.*?),/';
            $values[$i] = "'" . $name[$i] . "'" . "=>" . "'" . $value[$i] . "',";
        }
        $fileurl = APP_PATH . "../config/app.php";
        $string = file_get_contents($fileurl); //加载配置文件
        $string = preg_replace($names, $values, $string); // 正则查找然后替换
        file_put_contents($fileurl, $string); // 写入配置文件
        return true;
    } else {
        return false;
    }
}

//生成随机用户名
function get_username()
{
    $chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $chars2 = "abcdefghijklmnopqrstuvwxyz0123456789";
    $username = "";
    for ($i = 0; $i < mt_rand(2, 3); $i++) {
        $username .= $chars1[mt_rand(0, 25)];
    }
    $username .= '_';

    for ($i = 0; $i < mt_rand(4, 6); $i++) {
        $username .= $chars2[mt_rand(0, 35)];
    }
    return $username;
}

//生成随机密码
function get_password()
{
    $code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $rand = $code[rand(0, 25)]
        . strtoupper(dechex(date('m')))
        . date('d') . substr(time(), -5)
        . substr(microtime(), 2, 5)
        . sprintf('%02d', rand(0, 99));
    for (
        $a = md5($rand, true),
        $s = '0123456789ABCDEFGHIJKLMNOPQRSTUV',
        $d = '',
        $f = 0;
        $f < 8;
        $g = ord($a[$f]),
        $d .= $s[($g ^ ord($a[$f + 8])) - $g & 0x1F],
        $f++
    ) ;
    return $d;
}

/**
 * 判断当前时间是否在指定时间段之内
 * @param integer $a 起始时间
 * @param integer $b 结束时间
 * @return boolean
 */
function check_time($a, $b)
{
    $nowtime = time();
    $start = strtotime($a . ':00:00');
    $end = strtotime($b . ':00:00');

    if ($nowtime >= $end || $nowtime <= $start) {
        return true;
    } else {
        return false;
    }
}

//加密
function authcode($string, $key, $operation, $expiry = 0)
{
    $ckey_length = 4;
    $keya = md5(substr($key, 0, 16));
    $keyb = md5(substr($key, 16, 16));
    $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length) :
        substr(md5(microtime()), -$ckey_length)) : '';
    $cryptkey = $keya . md5($keya . $keyc);
    $key_length = strlen($cryptkey);
    $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) :
        sprintf('%010d', $expiry ? $expiry + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
    $string_length = strlen($string);
    $result = '';
    $box = range(0, 255);
    $rndkey = array();
    for ($i = 0; $i <= 255; $i++) {
        $rndkey[$i] = ord($cryptkey[$i % $key_length]);
    }
    for ($j = $i = 0; $i < 256; $i++) {
        $j = ($j + $box[$i] + $rndkey[$i]) % 256;
        $tmp = $box[$i];
        $box[$i] = $box[$j];
        $box[$j] = $tmp;
    }
    for ($a = $j = $i = 0; $i < $string_length; $i++) {
        $a = ($a + 1) % 256;
        $j = ($j + $box[$a]) % 256;
        $tmp = $box[$a];
        $box[$a] = $box[$j];
        $box[$j] = $tmp;
        $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
    }
    if ($operation == 'DECODE') {
        if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) &&
            substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16)) {
            return substr($result, 26);
        } else {
            return '';
        }
    } else {
        return $keyc . str_replace('=', '', base64_encode($result));
    }
}

function encrypt($string, $key, $expire = 0)
{
    return base64_encode(authcode($string, $key, 'ENCODE', $expire));
}

function decrypt($string, $key)
{
    return authcode(base64_decode($string), $key, 'DECODE');
}

function random_alphanumeric($len = 6) {
    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
    // Output: 54esmdr0qf
   return substr(str_shuffle($permitted_chars), 0, $len);
}

function get_latest_level($userinfo, $level) {
    $levels = \think\Db::table('xy_level')->where('level', $level + 1)->find();
    if (!$levels) {
        return $level;
    }

    $minRecharge = config('invite_reward_min_recharge');
    $uid = $userinfo['id'];
    $total_recharge = $userinfo['total_recharge'];
    $down_userid = $userinfo['down_userid'] == NULL ? '' : $userinfo['down_userid'];
    $down_userid_arr = $down_userid != '' ? explode(",", $userinfo['down_userid']) : [];
    $downcount = sizeof($down_userid_arr);
    $index = array_search($uid, $down_userid_arr);
    if ($index !== false) {
        unset($down_userid_arr[0]);
        $downcount = sizeof($down_userid_arr);
    }

    //充值最低300的团队人数
    $new_down_userid = $downcount == 0 ? "''" : implode(",", $down_userid_arr);
    $count = \think\Db::table('xy_users')->where("id IN ($new_down_userid) AND total_recharge >= $minRecharge")->count('id');

    $newlevel = $level;
    if ($count >= $levels['auto_vip_xu_num'] && $total_recharge >= $levels['num']) {
        $newlevel += 1;
        return get_latest_level($userinfo, $newlevel);
    }

    return $newlevel;
}

function http_curl($url, $data = []){
    if (strpos($url, 'haojingke') !== false) {
        $data['apikey'] = config('apikey_haojingke');
    }
    $header = [];
    if (isset($data['header'])) {
        $header = $data['header'];
        unset($data['header']);
    }
    $timeout = 0;
    if (isset($data['timeout'])) {
        $timeout = $data['timeout'];
    }
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if ($timeout > 0) {
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    }
    // post数据
    curl_setopt($ch, CURLOPT_POST, 1);
    // post的变量
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    if ($header != []) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    }

    $output = curl_exec($ch);
    // dump(curl_getinfo($ch));
    // dump(curl_error($ch));
    curl_close($ch);
    return $output;
}

function myCurl($url){
    $ch = curl_init();     // Curl 初始化  
    $timeout = 30;     // 超时时间：30s  
    $ua='Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36';    // 伪造抓取 UA  
        $ip = mt_rand(11, 191) . "." . mt_rand(0, 240) . "." . mt_rand(1, 240) . "." . mt_rand(1, 240);
    curl_setopt($ch, CURLOPT_URL, $url);              // 设置 Curl 目标  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);      // Curl 请求有返回的值  
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);     // 设置抓取超时时间  
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);        // 跟踪重定向  
    curl_setopt($ch, CURLOPT_REFERER, $url);   // 伪造来源网址  
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:'.$ip, 'CLIENT-IP:'.$ip));  //伪造IP  
    curl_setopt($ch, CURLOPT_USERAGENT, $ua);   // 伪造ua   
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts  
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0); //强制协议为1.0
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); //强制使用IPV4协议解析域名
    $content = curl_exec($ch);   

    curl_close($ch);    // 结束 Curl  
    return $content;    // 函数返回内容  
}

//参数1：访问的URL，参数2：post数据(不填则为GET)，参数3：提交的$cookies,参数4：是否返回$cookies
function curl_request($url,$ip='',$post='',$cookie='', $returnCookie=0){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)');
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
    curl_setopt($curl, CURLOPT_REFERER, "http://axweb.my.gov.cn/dzdmjs/Vote_Show.asp?InfoId=227&ClassId=32&Topid=0");

    $header = array('CLIENT-IP:'.$ip,'X-FORWARDED-FOR:'.$ip,);

    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);



    if($post) {
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post));
    }
    if($cookie) {
        curl_setopt($curl, CURLOPT_COOKIE, $cookie);
    }
    curl_setopt($curl, CURLOPT_HEADER, $returnCookie);
    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($curl);
    if (curl_errno($curl)) {
        return curl_error($curl);
    }
    curl_close($curl);
    if($returnCookie){
        list($header, $body) = explode("\r\n\r\n", $data, 2);
        preg_match_all("/Set\-Cookie:([^;]*);/", $header, $matches);
        $info['cookie']  = substr($matches[1][0], 1);
        $info['content'] = $body;
        return $info;
    }else{
        return $data;
    }
}

function CURL_Get($url,$data=array(),$ssl='1')
{   
        
        $ch = curl_init();
        $pageUrl=$url;
        // if ($ssl=="1")
        // {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);    
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        // }
        $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
        $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[0] .= ",application/signed-exchange;v=b3;q=0.9";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[] = "Accept-Language: en-us,en;q=0.5";
        $header[] = "Pragma: "; // browsers keep this blank.
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36" );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL,$pageUrl );
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,TRUE );
        curl_setopt($ch, CURLOPT_MAXREDIRS,10 );
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, ($data));
        curl_setopt( $ch, CURLOPT_COOKIESESSION, false );
        // curl_setopt( $ch, CURLOPT_COOKIEJAR, 'c:/xampp/htdocs/cookie1.txt' );
        // curl_setopt( $ch, CURLOPT_COOKIEFILE, 'c:/xampp/htdocs/cookie1.txt' );
        


        $result = curl_exec($ch);
        $info = curl_getinfo($ch); 
        
        return $result;
}

function cg($url)
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Cookie: theme=moviepro'
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function encrypt_hashids($value, $length = 6)
{
    $hashids = new Hashids\Hashids(config('app_name'), $length);

    $hashed = $hashids->encode($value);

    return $hashed;
}

function decrypt_hashids($value, $length = 6)
{
    $hashids = new Hashids\Hashids(config('app_name'), $length);

    $decrypted = $hashids->decode($value);

    return $decrypted;
}

function sendEmail( $subject, $toemail, $desc_content)
{
    // 实力化类
    $mail = new PHPMailer\PHPMailer();
    // 使用SMTP服务
    $mail->isSMTP();
    $mail->Mailer = "smtp";
    $mail->SMTPDebug = 0;
    // 编码格式为utf8，不设置编码的话，中文会出现乱码
    $mail->CharSet = "utf8";
    // 发送方的SMTP服务器地址
    $mail->Host = "smtp.gmail.com";
    // 是否使用身份验证
    $mail->SMTPAuth = true;
    // 发送方的163邮箱用户名，就是你申请163的SMTP服务使用的163邮箱
    $mail->Username = "ecwjwork@gmail.com";
    // 发送方的邮箱密码，注意用163邮箱这里填写的是“客户端授权密码”而不是邮箱的登录密码
    $mail->Password = "S3cr3t0619";
    // 使用ssl协议方式
    $mail->SMTPSecure = "tls";//"ssl";
    // 163邮箱的ssl协议方式端口号是465/994
    $mail->Port = 587;//994;
    // 设置发件人信息，如邮件格式说明中的发件人，这里会显示为Mailer(xxxx@163.com），Mailer是当做名字显示
    $mail->setFrom("ecwjwork@gmail.com", sysconf('site_name'));
    // 设置收件人信息，如邮件格式说明中的收件人，这里会显示为Liang(yyyy@163.com)
    $mail->addAddress($toemail, '');
    // 设置回复人信息，指的是收件人收到邮件后，如果要回复，回复邮件将发送到的邮箱地址
    $mail->addReplyTo("ericwenjun@gmail.com", "Reply");
    // 设置邮件抄送人，可以只写地址，上述的设置也可以只写地址(这个人也能收到邮件)
    //$mail->addCC("xxx@163.com");
    // 设置秘密抄送人(这个人也能收到邮件)
    //$mail->addBCC("xxx@163.com");
    // 添加附件
    //$mail->addAttachment("bug0.jpg");
    // 邮件标题
    $mail->Subject = $subject;
    // 邮件正文
    $mail->Body = $desc_content;
    $mail->IsHTML(true);
    // 这个是设置纯文本方式显示的正文内容，如果不支持Html方式，就会用到这个，基本无用
    //$mail->AltBody = "This is the plain text纯文本";
    if (!$mail->send()) { // 发送邮件
        return $mail->ErrorInfo;
        // echo "Message could not be sent.";
        // echo "Mailer Error: ".$mail->ErrorInfo;// 输出错误信息
    } else {
        return 1;
    }
}