<?php
namespace app\index\controller;

use library\Controller;
use think\Db;

/**
 * 支付控制器
 */
class Api extends Controller
{

    public $BASE_URL = "https://bapi.app";
    public $appKey = '';
    public $appSecret = '';

    const POST_URL = "https://pay.bbbapi.com/";


    public function __construct()
    {
        $this->appKey = config('app.bipay.appKey');
        $this->appSecret = config('app.bipay.appSecret');
    }

    public function bipay()
    {

        $oid = isset($_REQUEST['oid']) ? $_REQUEST['oid']: '';
        if ($oid) {
            $r = db('xy_recharge')->where('id',$oid)->find();
            if ($r) {
                $server_url = $_SERVER['SERVER_NAME']?"http://".$_SERVER['SERVER_NAME']:"http://".$_SERVER['HTTP_HOST'];
                $notifyUrl = $server_url.url('/index/api/bipay_notify');
                $returnUrl = $server_url.url('/index/api/bipay_return');
                $price = $r['num'] * 100;
                $res = $this->create_order($oid,$price,'用户充值',$notifyUrl, $returnUrl);

                if ($res && $res['code']==200) {
                    $url = $res['data']['pay_url'];
                    $this->redirect($url);
                }
            }
        }
    }

    public function bipay_return()
    {
        return $this->fetch();
    }


    public function bipay_notify()
    {

        $content = file_get_contents('php://input');
        $post    = (array)json_decode($content, true);
        file_put_contents("bipay_notify.log",$content."\r\n",FILE_APPEND);

        if (!$post['order_id']) {
            die('fail');
        }
        $oid = $post['order_id'];
        $r = db('xy_recharge')->where('id',$oid)->find();
        if (!$r) {
            die('fail');
        }
        if ($post['order_state']!=1) {
            die('fail');
        }

        if ($r['status'] == 2){
            die('SUCCESS');
        }

        if ($post['order_state']) {
            $res = Db::name('xy_recharge')->where('id',$oid)->update(['endtime'=>time(),'status'=>2]);
            $oinfo = $r;
            $res1 = Db::name('xy_users')->where('id',$oinfo['uid'])->setInc('balance',$oinfo['num']);
            $res2 = Db::name('xy_balance_log')
                ->insert([
                    'uid'=>$oinfo['uid'],
                    'oid'=>$oid,
                    'num'=>$oinfo['num'],
                    'type'=>1,
                    'status'=>1,
                    'addtime'=>time(),
                ]);
            /************* 发放推广奖励 *********/
            $uinfo = Db::name('xy_users')->field('id,active')->find($oinfo['uid']);
            if($uinfo['active']===0){
                Db::name('xy_users')->where('id',$uinfo['id'])->update(['active'=>1]);
                //将账号状态改为已发放推广奖励
                $userList = model('Users')->parent_user($uinfo['id'],3);
                if($userList){
                    foreach($userList as $v){
                        if($v['status']===1 && ($oinfo['num'] * config($v['lv'].'_reward') != 0)){
                            Db::name('xy_reward_log')
                                ->insert([
                                    'uid'=>$v['id'],
                                    'sid'=>$uinfo['id'],
                                    'oid'=>$oid,
                                    'num'=>$oinfo['num'] * config($v['lv'].'_reward'),
                                    'lv'=>$v['lv'],
                                    'type'=>1,
                                    'status'=>1,
                                    'addtime'=>time(),
                                ]);
                        }
                    }
                }
            }
            /************* 发放推广奖励 *********/
            die('SUCCESS');
        }
    }


    public function create_order(
        $orderId, $amount, $body, $notifyUrl, $returnUrl, $extra = '', $orderIp = '', $amountType = 'CNY', $lang = 'zh_CN')
    {
        $reqParam = [
            'order_id' => $orderId,
            'amount' => $amount,
            'body' => $body,
            'notify_url' => $notifyUrl,
            'return_url' => $returnUrl,
            'extra' => $extra,
            'order_ip' => $orderIp,
            'amount_type' => $amountType,
            'time' => time() * 1000,
            'app_key' => $this->appKey,
            'lang' => $lang
        ];
        $reqParam['sign'] = $this->create_sign($reqParam, $this->appSecret);
        $url = $this->BASE_URL . '/api/v2/pay';

        return $this->http_request($url, 'POST', $reqParam);
    }

    /**
     * @return {
     * bapp_id: "2019081308272299266f",
     * order_id: "1565684838",
     * order_state: 0,
     * body: "php-sdk sample",
     * notify_url: "https://sdk.b.app/api/test/notify/test",
     * order_ip: "",
     * amount: 1,
     * amount_type: "CNY",
     * amount_btc: 0,
     * pay_time: 0,
     * create_time: 1565684842076,
     * order_type: 2,
     * app_key: "your_app_key",
     * extra: ""
     * }
     */
    public function get_order($orderId)
    {
        $reqParam = [
            'order_id' => $orderId,
            'time' => time() * 1000,
            'app_key' => $this->appKey
        ];
        $reqParam['sign'] = $this->create_sign($reqParam, $this->appSecret);
        $url = $this->BASE_URL . '/api/v2/order';
        return $this->http_request($url, 'GET', $reqParam);
    }

    public function is_sign_ok($params)
    {
        $sign = $this->create_sign($params, $this->appSecret);
        return $params['sign'] == $sign;
    }

    public function create_sign($params, $appSecret)
    {
        $signOriginStr = '';
        ksort($params);
        foreach ($params as $key => $value) {
            if (empty($key) || $key == 'sign') {
                continue;
            }
            $signOriginStr = "$signOriginStr$key=$value&";
        }
        return strtolower(md5($signOriginStr . "app_secret=$appSecret"));
    }

    private function http_request($url, $method = 'GET', $params = [])
    {
        $curl = curl_init();

        if ($method == 'POST') {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
            $jsonStr = json_encode($params);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonStr);
        } else if ($method == 'GET') {
            $url = $url . "?" . http_build_query($params, '', '&');
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);


        $output = curl_exec($curl);

        if (curl_errno($curl) > 0) {
            return [];
        }
        curl_close($curl);
        $json = json_decode($output, true);

        //var_dump($output,curl_errno($curl));die;

        return $json;
    }


    //----------------------------------------------------------------
    //  paysapi
    //----------------------------------------------------------------

    public function pay1(){

        $uid = session('user_id');
        $bank_info = db('xy_bankinfo')->where('uid',$uid)->find();
        if (!$bank_info){
            $this->redirect('my/bind_bank');
            json(['code'=>1,'info'=>'未提交银行卡信息']);

        }
        $amount = input('get.num/s'); //金额

        $params['partner_id'] = '109449';
        // 支付类型：0001=网银支付
        $params['pay_type'] = '0001';
        // 用户姓名；
        $params['bank_code'] = $bank_info['username'];
        // 接口版本: API协议版本，当前值：V1.0 (大写)
        $params['version'] = 'V1.0';
        // 商户订单号: 商家网站生成的订单号，由商户保证其唯一性，由字 母、数字、下划线组成，字符长度不超过32位.
        $params['order_no'] = getSn('SY');
        // 订单金额: 以元为单位，精确到小数点后两位.例如:12.00、14.01等
        $params['amount'] = $amount;

        // 页面跳转同步通知地址: 支付成功后，通过页面跳转的方式跳转到商家网站
        $params['notify_url'] = 'https://www.primeamazon.live/index/api/pay_notify';

        $params['sign'] = $this->generateSign($params, '30ba1bde5842e258687c57ae5dab05cf');

        $data = $this->post('https://gateway.linxi.shop/payment/gateway',$params);

        if ($data['platRespCode']== 'SUCCESS'){
            $uid = session('user_id');
            $tel = db('xy_users')->where('id',$uid)->value('tel');
            db('xy_recharge')->insert([
                'id'    =>  $params['order_no'],
                'uid'   =>  $uid,
                'tel'   =>  $tel,
                'num'   =>  $amount,
                'type'  =>  4,
                'pic'   =>  '',
                'addtime'   =>  time(),
                'pay_name'  =>  'online_pay',
                'status'    =>  1
            ]);
            $this->data = $data;
            return $this->fetch('index/ctrl/pay');
        }else{
            return json(['code'=>2,'info'=>'Kelebihan pesanan,Silakan coba lagi nanti']);
            //Kelebihan pesanan,Silakan coba lagi nanti
            //充值订单过多，请稍后再试
        }


    }


    /**
     * notify_url接收页面
     */
    public function pay1_notify(){

        $params['code'] =   $_POST['code'];
        $params['message']  =   $_POST['message'];
        $params['order_no']  =   $_POST['order_no'];
        $params['trade_no']  =   $_POST['trade_no'];
        $params['amount']  =   $_POST['amount'];
        $params['partner_id']  =   $_POST['partner_id'];
        $sign  =   $_POST['sign'];
        $o_sign = $this->generateSign($params,'30ba1bde5842e258687c57ae5dab05cf');

        //验证通过
        if ($sign===$o_sign) {
            $o_info = Db::name('xy_recharge')->where('id', $params['order_no'])->find();
            if ($o_info['status'] == 1) {
                if ($params['code'] == 00) {

                    Db::name('xy_recharge')->where('id', $params['order_no'])->update(['endtime' => time(), 'status' => 2]);
                    Db::name('xy_balance_log')
                        ->insert([
                            'uid' => $o_info['uid'],
                            'oid' => $params['order_no'],
                            'num' => $params['amount'],
                            'type' => 1,
                            'status' => 1,
                            'addtime' => time(),
                        ]);
                    Db::name('xy_users')->where('id', $o_info['uid'])->setInc('balance', $params['amount']);
                    die('ok');
                } else {
                    Db::name('xy_recharge')->where('id', $params['order_no'])->update(['endtime' => time(), 'error_msg' => $params['message'], 'status' => 3]);
                    die('ok');
                }
            }
            if ($o_info['status']==2 || $o_info['status']==3){
                die('ok');
            }
        }
    }

    public function get_sign($data,$signKey){
        ksort($data);
        $data['key'] = $signKey;
        $jsonStr = stripslashes(json_encode($data));
        $sign=strtoupper(md5($jsonStr));
        return $sign;
    }

    public function generateSign($data, $signKey)
    {
        // 按照ASCII码升序排序
        ksort($data);

        $str = '';
        foreach ($data as $key => $value) {
            $value = trim($value);
            if ('sign' != $key && '' != $value) {
                $str .= $key . '=' . $value . '&';
            }
        }

        $str .= $signKey;
        return strtolower(md5($str));
    }
    public function create_sign1($data, $signKey)
    {
        // 按照ASCII码升序排序

        ksort($data);
        $data['key'] = $signKey;
        $str = '';
        foreach ($data as $key => $value) {
            $value = trim($value);
            $str .= $key . '=' . $value . '&';
        }
        $str = substr($str, 0, -1);
//        $str .= $signKey;
        //amount=10000&appId=2200909000907628&currencyType=INR&goods=充值&key=a99dfca582c9411e97da6082b40d3c83&notifyUrl=http://www.task99.top/index/api/upi_pay_notify&orderId=SY2012091445289908&orderTime=20201209144528&"
//        dump($str);
//        dump(md5($str));exit;
        return (md5($str));
    }

    public function post1($url, $requestData)
    {
//        $requestData = json_encode($requestData);
//        dump($requestData);exit;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($curl,CURLOPT_HTTPHEADER,$this_header);
        //普通数据
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($requestData));
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $requestData);
        $res = curl_exec($curl);

        //$info = curl_getinfo($ch);
        curl_close($curl);
        return json_decode($res,true);
    }
    public function curl_post($data, $url) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $tmpInfo = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Errno' . curl_error($ch);
        }
        curl_close($ch);
        return $tmpInfo;
    }

    public function post_json($url, $data)
    {
        $data = stripslashes(json_encode($data));
        $ch = curl_init($url); //请求的URL地址
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);//$data JSON类型字符串
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));
        $tmpInfo = curl_exec($ch);
        curl_close($ch);
        return $tmpInfo;
    }

    public function curlPost($url, $data, $timeout, $headers, $getMillisecond)
    {
        $data = json_encode($data);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($curl);
        if (curl_errno($curl)) {
            echo 'Errno' . curl_error($curl); //捕抓异常
        }
        curl_close($curl);
        return $output;
    }
    public function pay(){

        $uid = session('user_id');
//        $bank_info = db('xy_bankinfo')->where('uid',$uid)->find();
//        if (!$bank_info){
//            $this->redirect('my/bind_bank');
//            json(['code'=>1,'info'=>'未提交银行卡信息']);
//
//        }
        $amount = input('get.num/s'); //金额

        $params['goods'] = 'BNI.VA';

        $params['merchantCode'] = 't10027';
        // 支付类型：0001=网银支付
        $params['payType'] = '301';
        // 商户订单号:
        $params['merchantOrderId'] = getSn('SY');
        // 订单金额: 以元为单位，精确到小数点后两位.例如:12.00、14.01等
        $params['tradeAmount'] = $amount;

        $params['tradeTime'] = date("Y-m-d H:i:s");

        // 页面跳转同步通知地址: 支付成功后，通过页面跳转的方式跳转到商家网站
        $params['notifUrl'] = 'https://aaa.cisco7.com/index/api/pay_notify';
        $params['returnUrl'] = 'https://aaa.cisco7.com/index/api/pay_notify';

        $params['sign'] = $this->get_sign($params, 'nS6RvEEWwtjqO9L6');
//        $params = '{"goods":"BNI.VA Bank Negara Indonesia","merchantCode":"t10027","payType":"301","merchantOrderId":"SY2011121749268591","tradeAmount":"100000","tradeTime":1605174566,"notifUrl":"https://aaa.cisco.com/index/api/pay_notify","returnUrl":"https://aaa.cisco.com/index/api/pay_notify","sign":"ed293ea2014221f140a6cce4e99403f1"}';

//        $params = http_build_query($params);
//dump($params);exit;
        $data = $this->post_json('120.27.210.21/ext/pay.html',$params);
dump('正在跳转支付');
        $data = json_decode($data,true);
        if ($data['code'] !== 200){
//            dump(111);exit;
            return json(['code'=>2,'info'=>'Kelebihan pesanan,Silakan coba lagi nanti 1']);
            //Kelebihan pesanan,Silakan coba lagi nanti
            //充值订单过多，请稍后再试

//            exit;

        } else{
            $uid = session('user_id');
            $tel = db('xy_users')->where('id',$uid)->value('tel');
            db('xy_recharge')->insert([
                'id'    =>  $params['merchantOrderId'],
                'uid'   =>  $uid,
                'tel'   =>  $tel,
                'num'   =>  $amount,
                'type'  =>  4,
                'pic'   =>  '',
                'addtime'   =>  time(),
                'pay_name'  =>  'online_pay',
                'status'    =>  1
            ]);


//            $this->data = $data;
//            return $this->fetch('index/ctrl/pay');
    return $data['obj'];

        }


    }


    /**
     * notify_url接收页面
     */
    public function pay_notify()
    {

//        $post_param = json_decode(json_encode($_POST),true);
//        file_put_contents('/www/wwwroot/shuadan/runtime/log/log.php',$post_param,FILE_APPEND);
        $post_param = json_decode(file_get_contents("php://input"),true);

        if ($post_param['code'] == 200) {

            $params['resultCode'] = $post_param['obj']['resultCode'];
            $params['merchantCode'] = $post_param['obj']['merchantCode'];
            $params['amount'] = $post_param['obj']['amount'];
            $params['orderId'] = $post_param['obj']['orderId'];
            $params['time'] = $post_param['obj']['time'];

            $sign = $post_param['obj']['sign'];
            $o_sign = $this->get_sign($params, 'nS6RvEEWwtjqO9L6');
//            dump($sign);
//            dump($o_sign);exit;
//            file_put_contents('/www/wwwroot/shuadan/runtime/log/log.php',$params.PHP_EOL,FILE_APPEND);
//            file_put_contents('/www/wwwroot/shuadan/runtime/log/log.php',$sign.'[[[[[[[[[[][]]]]]]'.$o_sign,FILE_APPEND);
            //验证通过
            if ($sign === $o_sign) {
                $o_info = Db::name('xy_recharge')->where('id', $params['orderId'])->find();
                if ($o_info['status'] == 1) {
                    file_put_contents('/www/wwwroot/shuadan/runtime/log/log.php',$params['resultCode'],FILE_APPEND);
                    if ($params['resultCode'] == 0) {

                        Db::name('xy_recharge')->where('id', $params['orderId'])->update(['endtime' => time(), 'status' => 2]);
                        Db::name('xy_balance_log')
                            ->insert([
                                'uid' => $o_info['uid'],
                                'oid' => $params['orderId'],
                                'num' => $params['amount'],
                                'type' => 1,
                                'status' => 1,
                                'addtime' => time(),
                            ]);
                        Db::name('xy_users')->where('id', $o_info['uid'])->setInc('balance', $params['amount']);
                        die('OK');
                    } else {
                        Db::name('xy_recharge')->where('id', $params['orderId'])->update(['endtime' => time(), 'error_msg' => $post_param['message'], 'status' => 3]);
                        die('OK');
                    }
                }
                if ($o_info['status'] == 2 || $o_info['status'] == 3) {
                    die('OK');
                }
            }
        }

    }

    //提现
    public function deposit_notify(){
        $post_param = json_decode(file_get_contents("php://input"),true);
        if ($post_param['code'] == 200) {
            $params['resultCode'] = $post_param['obj']['resultCode'];
            $params['merchantCode'] = $post_param['obj']['merchantCode'];
            $params['amount'] = $post_param['obj']['amount'];
            $params['orderId'] = $post_param['obj']['orderId'];
            $params['time'] = $post_param['obj']['time'];
            $sign = $post_param['obj']['sign'];
            $o_sign = $this->get_sign($params, 'nS6RvEEWwtjqO9L6');
            //验证通过
            if ($sign === $o_sign) {
                $depositinfo = Db::name('xy_deposit')->where('id',$params['orderId'])->find();
                if ($depositinfo['status']==4) {
                    Db::name('xy_deposit')->where('id', $params['orderId'])->update(['status' => 2]);

//                    Db::name('xy_users')->where('id', $depositinfo['uid'])->setDec('balance', $params['amount']);
                }
            }
            die("OK");
        }

    }

    //upi
    public function upi_pay(){
        $amount = input('get.num/f') * 100; //金额 单位分

        //测试
//        $params['orderId'] = getSn('SY');
//        $params['appId'] = '2200909000907628';
//        $params['orderTime'] = date("YmdHis");
//        $params['amount'] = $amount;
//        $params['currencyType'] = 'INR';
//        $params['goods'] = 'pay';
//        $params['notifyUrl'] = 'http://www.task99.top/index/api/upi_pay_notify';
//        //签名
//        $params['sign'] = $this->create_sign1($params,'a99dfca582c9411e97da6082b40d3c83');
//        $params['mchtId'] = '2001102000104894';//不参与签名
//        $params['version'] = '20';//不参与签名
//        $params['biz'] = 'ca001';//不参与签名

        //正式
        $params['orderId'] = getSn('SY');
        $params['appId'] = '2201111000382187';
        $params['orderTime'] = date("YmdHis");
        $params['amount'] = $amount;
        $params['currencyType'] = 'INR';
        $params['goods'] = 'pay';
        $params['notifyUrl'] = 'http://www.task99.top/index/api/upi_pay_notify';
        //签名
        $params['sign'] = $this->create_sign1($params,'75f612f2eda1482e88dae265ee40a79a');
        $params['mchtId'] = '2001210000192650';//不参与签名
        $params['version'] = '20';//不参与签名
        $params['biz'] = 'ca001';//不参与签名

        $this->assign('params',$params);
        return $this->fetch();
    }

    public function create_upi_pay(){
        if (request()->isPost()) {
            $amount = input('post.num/f')/100;
            $orderId = input('post.orderId/s');
            $uid = session('user_id');
            $tel = db('xy_users')->where('id', $uid)->value('tel');
            db('xy_recharge')->insert([
                'id' => $orderId,
                'uid' => $uid,
                'tel' => $tel,
                'num' => $amount,
                'type' => 4,
                'pic' => '',
                'addtime' => time(),
                'pay_name' => 'upi_pay',
                'status' => 1
            ]);
        }
    }



    public function upi_pay_notify(){
        $post_param = file_get_contents("php://input");
        file_put_contents('/home/wwwroot/202.181.136.38/runtime/log/log.php',$post_param,FILE_APPEND);
//            $post_param = '{"body":{"amount":"10000","biz":"ca001","chargeTime":"20201210194500","mchtId":"2001210000192650","orderId":"SY2012101923426136","payType":"pu","seq":"1d68fc5f60b3487cb8160adf6fde4208","status":"PROCESSING","tradeId":"W2012101923445669820"},"head":{"respCode":"0000","respMsg":"Request success"},"sign":"c294e16f171ef50eaec3f81cbacc3be2"}';
        $post_param = json_decode($post_param,true);
        if ($post_param['head']['respCode'] == 0000) {
            $params = $post_param['body'];
            $o_info = Db::name('xy_recharge')->where('id', $params['orderId'])->find();
            if ($o_info['status'] == 1) {
                if ($params['status']=='SUCCESS') {
                    Db::name('xy_recharge')->where('id', $params['orderId'])->update(['endtime' => time(), 'status' => 2]);
                    Db::name('xy_balance_log')
                        ->insert([
                            'uid' => $o_info['uid'],
                            'oid' => $params['orderId'],
                            'num' => $params['amount'],
                            'type' => 1,
                            'status' => 1,
                            'addtime' => time(),
                        ]);
                    Db::name('xy_users')->where('id', $o_info['uid'])->setInc('balance', $params['amount']);

                }

                if($params['status']=='FAIL'){
                    Db::name('xy_recharge')->where('id', $params['orderId'])->update(['endtime' => time(), 'error_msg' => $post_param['message'], 'status' => 3]);
                }
            }
        }

        die('SUCCESS');
    }

    public function new_pay_upi_notify(){
        $post_param = file_get_contents("php://input");
        file_put_contents('/home/wwwroot/202.181.136.38/runtime/log/pay_nine_log.php',$post_param,FILE_APPEND);
//        $post_param = 'status=PAY_SUCCESS&merchantId=2014789&merchantorder=SY2101021411314916&money=200.00&paytype=upi&successurl=https%3A%2F%2Fwww.task99.top&money_true=200.00&uid=&digest=723DB17CF572B128D1A68FA892A8E740';
        $post_param = urldecode($post_param);
        $post_param = $this->str2arr($post_param);
//        dump($post_param);
//        exit;
        if ($post_param['status'] == 'PAY_SUCCESS') {

            $params['status'] = $post_param['status'];
            $params['merchantId'] = $post_param['merchantId'];
            $params['merchantorder'] = $post_param['merchantorder'];
            $params['money'] = $post_param['money'];
            $params['paytype'] = $post_param['paytype'];
            $params['successurl'] = $post_param['successurl'];
            $params['money_true'] = $post_param['money_true'];
            $params['uid'] = $post_param['uid'];


            $sign = $post_param['digest'];
            $params = array_filter($params);
            $o_sign = $this->create_sign2($params,'tGCVPbVcNQVPYfpbqMS7MtORR3Rsz6ks');
            //验证通过
            if ($sign === $o_sign) {
                $o_info = Db::name('xy_recharge')->where('id', $params['merchantorder'])->find();
                if ($o_info['status'] == 1) {
                    if ($post_param['status'] == 'PAY_SUCCESS') {

                        Db::name('xy_recharge')->where('id', $params['merchantorder'])->update(['endtime' => time(), 'status' => 2]);
                        Db::name('xy_balance_log')
                            ->insert([
                                'uid' => $o_info['uid'],
                                'oid' => $params['merchantorder'],
                                'num' => $params['money'],
                                'type' => 1,
                                'status' => 1,
                                'addtime' => time(),
                            ]);
                        Db::name('xy_users')->where('id', $o_info['uid'])->setInc('balance', $params['money']);
                        die('ok');
                    } else {
                        Db::name('xy_recharge')->where('id', $params['merchantorder'])->update(['endtime' => time(), 'error_msg' => 'pay_error', 'status' => 3]);
                        die('ok');
                    }
                }
                die('ok');
            }
        }

        die('SUCCESS');
    }

    function strToArr($str,$sp="&",$kv="="){
        $arr = str_replace(array($kv,$sp),array('"=>"','","'),'array("'.$str.'")');
//        dump($arr);exit;
        eval("$arr"." = $arr;");   // 把字符串作为PHP代码执行
        return $arr;
    }
    function str2arr ($str,$sp="&",$kv="=")
    {
        $arr = str_replace(array($kv,$sp),array('"=>"','","'),'array("'.$str.'")');
//        dump($arr);exit;
        eval("\$arr"." = $arr;");   // 把字符串作为PHP代码执行
        return $arr;
    }

    /*pay11*/

    //获取毫秒数
    public function getMillisecond()
    {
        list($microsecond, $time) = explode(' ', microtime()); //' '中间是一个空格
        return (float)sprintf('%.0f', (floatval($microsecond) + floatval($time)) * 1000);
    }
    public function posturl($url,$data){
//        $data  = json_encode($data);
//        dump($data);exit;
        $headerArray =array("Content-type:application/x-www-form-urlencoded;charset=utf-8;","Accept:application/json;charset=utf-8;");
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
        curl_setopt($curl, CURLOPT_POST, 1);
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl,CURLOPT_HTTPHEADER,$headerArray);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);
        return json_decode($output,true);
    }
    public function pay11_sign($params,$key)
    {
        $params = array_filter($params);
        $str = json_encode($params) . "|" . $key;
        $sign = MD5($str);
//        dump($sign);
        return $sign;
    }

    public function pay7(){
        $uid = session('user_id');
//        return json(['code'=>1,'info'=>'The channel is under maintenance']);
//        if($uid !== 34){
//            return json(['code'=>1,'info'=>'The channel is under maintenance']);
//        }
        $uinfo = Db::name('xy_users')->where('id',$uid)->find();
        $amount = input('get.num/s'); //金额
        $params['code'] = '10007';
        $params['notifyurl'] = 'https://www.task99.top/index/my/index.html';
        $params['amount'] = $amount;
        $params['callbackurl'] = 'https://www.task99.top/index/api/India_pay_notify';
        $params['merordercode'] = getSn('SY');
        $params['signs']    =   $this->create_sign3($params,'ffa68445-fc7f-42b6-a02c-f2351c4b0a05');
        $params['starttime'] = time().'000';
        $params['name'] = $uinfo['username'];
        $params['mobile'] = '969483'.rand(1000,9999);
//        $params['mobile'] = $uinfo['tel'];
        $arr = ['a','b','c','d','e','f','g','h','i','j','k'];
        $params['email'] = $arr[rand(0,10)].$arr[rand(0,10)].rand(1000000,9999999).'@gmail.com';
        $params['paycode'] = '904';
        $data = $this->posturl('https://www.bossyd.com/api/outer/collections/addOrderByLndia',$params);
//        dump($data);exit;
        if($data['success']===true) {
           $res =  db('xy_recharge')->insert([
                'id'    =>  $params['merordercode'],
                'uid'   =>  $uid,
                'tel'   =>  $uinfo['tel'],
                'num'   =>  $amount,
                'type'  =>  4,
                'pic'   =>  '',
                'addtime'   =>  time(),
                'pay_name'  =>  'pay',
                'status'    =>  1
            ]);
           dump('paying');
           if ($res) {
               return $this->redirect($data['data']['checkstand']);
           }else{
               return json(['code'=>1,'info'=>'pay error']);
           }
        }

    }

    public function India_pay_notify()
    {
        $post_param =file_get_contents("php://input");
        file_put_contents('/home/wwwroot/202.181.136.38/runtime/log/pay_log.php',$post_param,FILE_APPEND);

        if (empty($post_param)){
            die('error');
        }
//$post_param = 'amount=100.0000&chnltrxid=112233&code=10128&createtime=1608178785000&merordercode=SY2012171219454652&returncode=00&sign=9351E8D6B54FC9EF64604F5CC32AF2B8&successtime=-1&terraceordercode=112233';
        $post_param = $this->str2arr($post_param);
        $o_sign = strtoupper(md5('code='.$post_param['code'].'&key=ffa68445-fc7f-42b6-a02c-f2351c4b0a05'.'&terraceordercode='.$post_param['terraceordercode'].'&merordercode='.$post_param['merordercode'].'&createtime='.$post_param['createtime'].'&chnltrxid='.$post_param['chnltrxid']));

        if ($o_sign == $post_param['sign']) {
            $params = $post_param;
            $o_info = Db::name('xy_recharge')->where('id', $params['merordercode'])->find();
            if ($o_info['status'] == 1) {
                if ($params['returncode']==00) {
                    Db::name('xy_recharge')->where('id', $params['merordercode'])->update(['endtime' => time(), 'status' => 2]);
                    Db::name('xy_balance_log')
                        ->insert([
                            'uid' => $o_info['uid'],
                            'oid' => $params['merordercode'],
                            'num' => $params['amount'],
                            'type' => 1,
                            'status' => 1,
                            'addtime' => time(),
                        ]);
                    Db::name('xy_users')->where('id', $o_info['uid'])->setInc('balance', $params['amount']);

                }else{
                    Db::name('xy_recharge')->where('id', $params['merordercode'])->update(['endtime' => time(),'status' => 3]);
                }
            }
        }
        die("OK");
    }
    public function create_sign2($data, $signKey)
    {
        // 按照ASCII码升序排序

        ksort($data);
        $data['key'] = $signKey;
        $str = '';
        foreach ($data as $key => $value) {
            $value = trim($value);
            $str .= $key . '=' . $value . '&';
        }
        $str = substr($str, 0, -1);
//        dump($str);
        return strtoupper(md5($str));
    }
    /*India代付回调*/
    public function India_deposit_notify(){
        $post_param =file_get_contents("php://input");
        file_put_contents('/home/wwwroot/202.181.136.38/runtime/log/deposit_log.php',$post_param,FILE_APPEND);
//        $post_param = 'amount=475.00&code=10007&issuingcode=CO2101231515294096&signs=164E04B930D76C3D8F68A41C0D63BC3B&accountname=Suhasd&merissuingcode=CO2101231515294096&returncode=SUCCESS&bankname=Karnataka+Bank&cardnumber=1672500102355501&starttime=1611386166708&ifsc=KARB0000167';
//        $post_param = str_replace('+','%20',$post_param);

        if (empty($post_param)){
            die('error');
        }
        $post_param = $this->str2arr($post_param);
//        dump($post_param);
        $bank_name =  str_replace('+',' ',$post_param['bankname']);
        $accountname =  str_replace('+',' ',$post_param['accountname']);
//        dump($post_param);exit;
            $params['code'] = $post_param['code'];
            $params['amount'] = $post_param['amount'];
            $params['ifsc'] = $post_param['ifsc'];
            $params['bankname'] = $bank_name;
            $params['accountname'] = $accountname;
            $params['cardnumber'] = $post_param['cardnumber'];
            $params['starttime'] = $post_param['starttime'];
            $params['merissuingcode'] = $post_param['merissuingcode'];
            $params['issuingcode'] = $post_param['issuingcode'];
            $params['returncode'] = $post_param['returncode'];
            $sign = $post_param['signs'];
            $o_sign = $this->create_sign2($params, 'ffa68445-fc7f-42b6-a02c-f2351c4b0a05');
//        dump($params);
//        dump($sign);
//        dump($o_sign);
//        exit;
            //验证通过
            if ($sign === $o_sign) {
//                dump(111);
                $depositinfo = Db::name('xy_deposit')->where('id', $params['merissuingcode'])->find();
                if ($post_param['returncode'] == 'SUCCESS') {
                    $depositinfo = Db::name('xy_deposit')->where('id', $params['merissuingcode'])->find();
                    if ($depositinfo['status'] == 4) {
                        Db::name('xy_deposit')->where('id', $params['merissuingcode'])->update(['status' => 2]);

//                    Db::name('xy_users')->where('id', $depositinfo['uid'])->setDec('balance', $params['amount']);

                    }
                }
                if ($post_param['returncode'] == 'FAIL'){

                    //驳回订单的业务逻辑
                    Db::name('xy_users')->where('id',$depositinfo['uid'])->setInc('balance',$depositinfo['num']);
                    Db::name('xy_deposit')->where('id', $params['merissuingcode'])->update(['status' => 3]);
                }
                die("OK");
            }


    }

    /**/
    public function paytime_deposit_notify()
    {
        $post_param = file_get_contents("php://input");
        file_put_contents('/home/wwwroot/202.181.136.38/runtime/log/deposit_log.php', $post_param, FILE_APPEND);
//        $post_param ='code=0&msg=The+platform+rejected+the+withdrawal+order.&data%5BmerchantId%5D=2014789&data%5Bsystemorder%5D=2002754317004968E1&data%5Bmerchantorder%5D=CO2101021616328732&data%5Baccount%5D=6074310109499310&data%5BbankType%5D=2&data%5Bmoney%5D=1164.00&data%5Bdigest%5D=7FD3F58F5B6EEE5A4D4EBBE8F30CF907';
        $post_param = urldecode($post_param);

        $post_param = $this->str2arr($post_param);
//        dump($post_param);exit;
//        $bank_name =  str_replace('+',' ',$post_param['bankname']);
//        $accountname =  str_replace('+',' ',$post_param['accountname']);

        $params['merchantId'] = $post_param['data[merchantId]'];
        $params['systemorder'] = $post_param['data[systemorder]'];
        $params['merchantorder'] = $post_param['data[merchantorder]'];
        $params['account'] = $post_param['data[account]'];
        $params['bankType'] = $post_param['data[bankType]'];
        $params['money'] = $post_param['data[money]'];
        $sign = $post_param['data[digest]'];

        $o_sign = $this->create_sign2($params,'tGCVPbVcNQVPYfpbqMS7MtORR3Rsz6ks');
        $params['code'] = $post_param['code'];
//        dump($params);
//        dump($sign);
//        dump($o_sign);exit;
//        exit;
        //验证通过
        if ($sign === $o_sign) {
//                dump($params['code']);exit;
            $depositinfo = Db::name('xy_deposit')->where('id', $params['merchantorder'])->find();
            if ($params['code'] == '1') {
                $depositinfo = Db::name('xy_deposit')->where('id', $params['merchantorder'])->find();
                if ($depositinfo['status'] == 4) {
                    Db::name('xy_deposit')->where('id', $params['merchantorder'])->update(['status' => 2]);

//                    Db::name('xy_users')->where('id', $depositinfo['uid'])->setDec('balance', $params['amount']);

                }
            }
            if ($params['code'] == '0'){

                //驳回订单的业务逻辑
                Db::name('xy_users')->where('id',$depositinfo['uid'])->setInc('balance',$depositinfo['num']);
                Db::name('xy_deposit')->where('id', $params['merchantorder'])->update(['status' => 3]);
            }
            die("ok");
        }


    }

    public function create_sign3($data, $signKey)
    {
        // 按照ASCII码升序排序
//code=test1231&merordercode=test13245&notifyurl=qweqwsdafsadf001&callbackurl=www.baidu.com&amount=100&key=sjdfsgdsfgs
      $str = 'code='.$data['code'].'&merordercode='.$data['merordercode'].'&notifyurl='.$data['notifyurl'].'&callbackurl='.$data['callbackurl'].'&amount='.$data['amount'].'&key='.$signKey;
        return strtoupper(md5($str));
    }

    public function pay8(){
        $uid = session('user_id');
//        return json(['code'=>1,'info'=>'The channel is under maintenance']);
//        if($uid !== 34){
//            return json(['code'=>1,'info'=>'The channel is under maintenance']);
//        }

        $uinfo = Db::name('xy_users')->where('id',$uid)->find();
        $amount = input('get.num/s'); //金额
        $params['code'] = '10128';
        $params['notifyurl'] = 'https://www.task99.top/index/my/index.html';
        $params['amount'] = $amount;
        $params['callbackurl'] = 'https://www.task99.top/index/api/new_pay_notify';
        $params['merordercode'] = getSn('SY');
        $params['signs']    =   $this->create_sign3($params,'70135d2f-1286-4d55-995e-a66614750529');
        $params['starttime'] = time().'000';
        $params['name'] = $uinfo['username'];
        $params['mobile'] = '969483'.rand(1000,9999);
//        $params['mobile'] = $uinfo['tel'];
        $arr = ['a','b','c','d','e','f','g','h','i','j','k'];
        $params['email'] = $arr[rand(0,10)].$arr[rand(0,10)].rand(1000000,9999999).'@gmail.com';
        $params['paycode'] = '905';
        $data = $this->posturl('https://www.mixyd.com/api/outer/icic/createOrder',$params);
//        dump($data);exit;
        if($data['success']===true) {
            $res =  db('xy_recharge')->insert([
                'id'    =>  $params['merordercode'],
                'uid'   =>  $uid,
                'tel'   =>  $uinfo['tel'],
                'num'   =>  $amount,
                'type'  =>  4,
                'pic'   =>  '',
                'addtime'   =>  time(),
                'pay_name'  =>  'new_pay',
                'status'    =>  1
            ]);
            dump('paying');
            if ($res) {
                return $this->redirect($data['data']['checkstand']);
            }else{
                return json(['code'=>1,'info'=>'pay error']);
            }
        }

    }


    public function pay9(){
        $uid = session('user_id');

        $uinfo = Db::name('xy_users')->where('id',$uid)->find();
        $amount = input('get.num/s'); //金额
        $url = 'https://order.hopeallgood168.com/index/payOrderV2?request=json';
//        $url = 'https://order.hopeallgood168.com/index/payOrderV2';
        $params['merchantId'] = '2014789';
        $params['merchantorder'] = getSn('SY');
        $params['money'] = $amount;
        $params['paytype'] = 'upi';
        $params['verifyurl'] = 'https://www.task99.top/index/api/new_pay_upi_notify';
        $params['successurl'] = 'https://www.task99.top';
        $params['versions'] = 'v2.0';
        $params['digest'] = $this->create_sign2($params,'tGCVPbVcNQVPYfpbqMS7MtORR3Rsz6ks');

        $data = $this->posturl($url,$params);
//        dump($data);exit;
        if($data['code']===2000) {
            $res =  db('xy_recharge')->insert([
                'id'    =>  $params['merchantorder'],
                'uid'   =>  $uid,
                'tel'   =>  $uinfo['tel'],
                'num'   =>  $amount,
                'type'  =>  4,
                'pic'   =>  '',
                'addtime'   =>  time(),
                'pay_name'  =>  'new_pay_upi',
                'status'    =>  1
            ]);
            dump('paying');
            if ($res) {
                return $this->redirect($data['data']['url']);
            }else{
                return json(['code'=>1,'info'=>'pay error']);
            }
        }

    }

    public function new_pay_notify()
    {
        $post_param =file_get_contents("php://input");
        file_put_contents('/home/wwwroot/202.181.136.38/runtime/log/new_pay_log.php',$post_param,FILE_APPEND);
//exit;
        if (empty($post_param)){
            die('error');
        }
//        $post_param = 'amount=200&chnltrxid=&code=10128&createtime=1608530501000&merordercode=p17_000000001865162&returncode=00&sign=407C0B43F880768A2397E1BF2DCC4420&successtime=-1&terraceordercode=SY2012211401415013';
        $post_param = $this->str2arr($post_param);
//        dump($post_param);exit;
        $o_sign = strtoupper(md5('code='.$post_param['code'].'&key=70135d2f-1286-4d55-995e-a66614750529'.'&terraceordercode='.$post_param['terraceordercode'].'&merordercode='.$post_param['merordercode'].'&createtime='.$post_param['createtime'].'&chnltrxid='.$post_param['chnltrxid']));

        if ($o_sign == $post_param['sign']) {
            $params = $post_param;
            $o_info = Db::name('xy_recharge')->where('id', $params['terraceordercode'])->find();
            if ($o_info['status'] == 1) {
                if ($params['returncode']==00) {
                    Db::name('xy_recharge')->where('id', $params['terraceordercode'])->update(['endtime' => time(), 'status' => 2]);
                    Db::name('xy_balance_log')
                        ->insert([
                            'uid' => $o_info['uid'],
                            'oid' => $params['terraceordercode'],
                            'num' => $params['amount'],
                            'type' => 1,
                            'status' => 1,
                            'addtime' => time(),
                        ]);
                    Db::name('xy_users')->where('id', $o_info['uid'])->setInc('balance', $params['amount']);

                }else{
                    Db::name('xy_recharge')->where('id', $params['terraceordercode'])->update(['endtime' => time(),'status' => 3]);
                }
            }
        }
        die("OK");
    }

    public function pay10(){
        $uid = session('user_id');
//        return json(['code'=>1,'info'=>'The channel is under maintenance']);
//        if($uid !== 34){
//            return json(['code'=>1,'info'=>'The channel is under maintenance']);
//        }

//        WEBSTAGING
        $uinfo = Db::name('xy_users')->where('id',$uid)->find();
        $amount = input('get.num/s'); //金额
        $params['pay_memberid'] = 'gUMnKMWCQSow';
        $params['pay_type'] = 'WEBSTAGING';
        $params['pay_returnurl'] = 'https://www.part-time88.com/index/my/index.html';
        $params['pay_amount'] = $amount;
        $params['pay_notifyurl'] = 'https://www.part-time88.com/index/api/pay10_notify';
        $params['pay_orderid'] = getSn('SY');
        $params['pay_applytime'] = time();
        $params['pay_name'] = $uinfo['username'];
        $params['pay_mobile'] = '969483'.rand(1000,9999);
//        $params['pay_mobile'] = $uinfo['tel'];
        $arr = ['a','b','c','d','e','f','g','h','i','j','k'];
        $params['pay_email'] = $arr[rand(0,10)].$arr[rand(0,10)].rand(1000000,9999999).'@gmail.com';
        $params['pay_sign']    =   $this->create_sign2($params,'2dh6ffybbfzkj51abt8hk21gnjcufhql');
        $data = $this->post_json('https://api.xpays.in/pay',$params);
        $res =  db('xy_recharge')->insert([
            'id'    =>  $params['pay_orderid'],
            'uid'   =>  $uid,
            'tel'   =>  $uinfo['tel'],
            'num'   =>  $amount,
            'type'  =>  4,
            'pic'   =>  '',
            'addtime'   =>  time(),
            'pay_name'  =>  'pay10',
            'status'    =>  1
        ]);
        echo $data;

    }

    public function pay10_notify()
    {
        $post_param =file_get_contents("php://input");
        file_put_contents('/home/wwwroot/202.181.136.38/runtime/log/new_pay_log.php',$post_param,FILE_APPEND);
//exit;
//        $post_param = '{"memberId":"gUMnKMWCQSow","orderId":"SY2101172312072333","amount":"200.0000","payTime":1610896378,"tradeId":"DD698087604161089632262204","orderStatus":"success","attach":"","resCode":1000,"resMsg":"success","sign":"2D3631AB4BA727AABAFD3BBF6C870C82"}';

        if (empty($post_param)){
            die('error');
        }
        $post_param = json_decode($post_param,true);
        $param_sign = $post_param['sign'];
        unset($post_param['sign']);
        $post_param = array_filter($post_param);
        $o_sign = $this->create_sign2($post_param,'2dh6ffybbfzkj51abt8hk21gnjcufhql');
        if ($o_sign == $param_sign) {
            $params = $post_param;
            $o_info = Db::name('xy_recharge')->where('id', $params['orderId'])->find();
            if ($o_info['status'] == 1) {
                if ($params['orderStatus']=='success') {
                    Db::startTrans();
                    try {
                        Db::name('xy_recharge')->where('id', $params['orderId'])->update(['endtime' => time(), 'status' => 2]);
                        Db::name('xy_balance_log')
                            ->insert([
                                'uid' => $o_info['uid'],
                                'oid' => $params['orderId'],
                                'num' => $params['amount'],
                                'type' => 1,
                                'status' => 1,
                                'addtime' => time(),
                            ]);
                        Db::name('xy_users')->where('id', $o_info['uid'])->setInc('balance', $params['amount']);
                        Db::commit();
                    }catch (\Exception $e){
                        Db::rollback();
                    }
                }else{
                    Db::name('xy_recharge')->where('id', $params['orderId'])->update(['endtime' => time(),'status' => 3]);
                }
            }
        }
        die("ok");
    }


    public function pay11(){
//        task9944@yahoo.com
//        ADBRAVXQJXTS7180
//        96083b99cdc74088bc206dc8c765fa2e
        $uid = session('user_id');
//        return json(['code'=>1,'info'=>'The channel is under maintenance']);
//        if($uid !== 24){
//            return json(['code'=>1,'info'=>'The channel is under maintenance']);
//        }

//        WEBSTAGING
        $uinfo = Db::name('xy_users')->where('id',$uid)->find();
        $amount = input('get.num/f'); //金额
        $params['userId'] = 'test417'.rand(1,9);
        $params['payTypeId'] = '';
        $params['redirectUrl'] = 'https://www.part-time88.com/index/my/index.html';
        $params['amount'] = $amount;
        $params['notifyUrl'] = 'https://www.part-time88.com/index/api/pay11_notify';
        $params['orderId'] = getSn('SY');
        $params['details'] = 'pay11';

        $getMillisecond     = $this->getMillisecond(); //毫秒时间戳
        //组装数据
        $data['body']       = $params;
        $data['merchantId'] = 'AAIOM76BJAPS9094';
        $data['timestamp']  = "$getMillisecond";
        $sign    =   $this->pay11_sign($data,'596bbf8593164c13807fd68e7573f1f0');
        //封装请求头
        $headers = array("Content-type: application/json;charset=UTF-8", "Accept: application/json", "Cache-Control: no-cache", "Pragma: no-cache", "Api-Sign:$sign");
        $json = $this->curlPost('https://gateway.shineupay.com/pay/create', $data, 5, $headers, $getMillisecond);;
        $result = json_decode($json, true);
        
        var_dump($result);
        if ($result['status']==0) {


            $res = db('xy_recharge')->insert([
                'id' => $params['orderId'],
                'uid' => $uid,
                'tel' => $uinfo['tel'],
                'num' => $amount,
                'type' => 4,
                'pic' => '',
                'addtime' => time(),
                'pay_name' => 'pay11',
                'status' => 1
            ]);
            if ($res){
                $this->redirect($result['body']['content']);
            }
        }
        
        var_dump($data);
        // echo $data;

    }

    public function pay11_notify()
    {
        $post_param =file_get_contents("php://input");
        file_put_contents('/home/wwwroot/202.181.136.38/runtime/log/pay_log.php','||||'.$post_param,FILE_APPEND);
//exit;
//        $post_param = '{"body":{"platformOrderId":"20210121A9TXG2G0EXA83325","orderId":"SY2101211552118280","status":1,"amount":1000.0000,"payType":"ip_h5_d0_v1"},"status":0,"merchantId":"A9IWFL0TGIRK1348","timestamp":"1611220115567"}';

        if (empty($post_param)){
            die('error');
        }
        $post_param = json_decode($post_param,true);


        if ($post_param['merchantId']=='ADBRAVXQJXTS7180'){
                $params = $post_param['body'];
                $o_info = Db::name('xy_recharge')->where('id', $params['orderId'])->find();
                if ($o_info['status'] == 1) {
                    if ($params['status']=='1') {
                        Db::startTrans();
                        try {
                            Db::name('xy_recharge')->where('id', $params['orderId'])->update(['endtime' => time(), 'status' => 2]);
                            Db::name('xy_balance_log')
                                ->insert([
                                    'uid' => $o_info['uid'],
                                    'oid' => $params['orderId'],
                                    'num' => $params['amount'],
                                    'type' => 1,
                                    'status' => 1,
                                    'addtime' => time(),
                                ]);
                            Db::name('xy_users')->where('id', $o_info['uid'])->setInc('balance', $params['amount']);
                            Db::commit();
                        }catch (\Exception $e){
                            Db::rollback();
                        }
                    }else{
                        Db::name('xy_recharge')->where('id', $params['orderId'])->update(['endtime' => time(),'status' => 3]);
                    }
                }

        }
        die("success");
    }

    public function deposit12_notify(){
        $post_param =file_get_contents("php://input");
        file_put_contents('/home/wwwroot/202.181.136.38/runtime/log/deposit_log.php','||||'.$post_param,FILE_APPEND);
//        exit;
//        $post_param = '{"resCode":1000,"resMsg":"success","memberId":"gUMnKMWCQSow","orderId":"CO2101222154285255","amount":"100.0000","tradeId":"DD698087604161132434807611","orderStatus":"success","payTime":1611325268,"attach":"","sign":"9BC124406B86B33C9B196D6DD9F9D18E"}';

        if (empty($post_param)){
            die('error');
        }
        $post_param = json_decode($post_param,true);
        $params = array_filter($post_param);
        $sign = $params['sign'];
        unset($params['sign']);
        $o_sign = $this->create_sign2($params,'2dh6ffybbfzkj51abt8hk21gnjcufhql');
        //验证通过
        if ($sign === $o_sign) {
            $depositinfo = Db::name('xy_deposit')->where('id', $params['orderId'])->find();
            if ($depositinfo['status'] == 4) {
                if ($params['orderStatus'] == 'success') {
                    $depositinfo = Db::name('xy_deposit')->where('id', $params['orderId'])->find();
                    Db::name('xy_deposit')->where('id', $params['orderId'])->update(['status' => 2]);
                }
                if ($params['status'] == 'failed') {

                    //驳回订单的业务逻辑
                    Db::name('xy_users')->where('id', $depositinfo['uid'])->setInc('balance', $depositinfo['num']);
                    Db::name('xy_deposit')->where('id', $params['orderId'])->update(['status' => 3]);
                }
            }
            die("ok");
        }
    }
    public function deposit11_notify()
    {
        $post_param = file_get_contents("php://input");
        file_put_contents('/home/wwwroot/202.181.136.38/runtime/log/deposit_log.php', '||||' . $post_param, FILE_APPEND);
//        exit;
//        $post_param = '{"resCode":1000,"resMsg":"success","memberId":"gUMnKMWCQSow","orderId":"CO2101222154285255","amount":"100.0000","tradeId":"DD698087604161132434807611","orderStatus":"success","payTime":1611325268,"attach":"","sign":"9BC124406B86B33C9B196D6DD9F9D18E"}';

        if (empty($post_param)) {
            die('error');
        }
        $post_param = json_decode($post_param, true);
        $params = $post_param['body'];
        //验证通过
//        if ($sign === $o_sign) {
            $depositinfo = Db::name('xy_deposit')->where('id', $params['orderId'])->find();
            if ($depositinfo['status'] == 4) {
                if ($params['status'] == '1') {
                    Db::name('xy_deposit')->where('id', $params['orderId'])->update(['status' => 2]);
                }
                if ($params['status'] == '2') {

                    //驳回订单的业务逻辑
                    Db::name('xy_users')->where('id', $depositinfo['uid'])->setInc('balance', $depositinfo['num']);
                    Db::name('xy_deposit')->where('id', $params['orderId'])->update(['status' => 3]);
                }
//            }
                die("ok");
            }
//        }
    }
}