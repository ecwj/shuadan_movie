<?php
namespace app\index\controller;

use library\Controller;
use think\Db;

/**
 * 支付控制器
 */
class Api2 extends Controller
{

    public $BASE_URL = "http://api.jfmg168.com/index";
    public $appid = '';
    public $appKey = '';
    public $appSecret = '';
    public $appname = '';

    public $post_url = "https://openapi.toppay.asia/gateway/prepaidOrder";


    public function __construct()
    {
        if (config('app.toppay.status') == 1) {
            $this->appid = config('app.toppay.appid');
            $this->appKey = config('app.toppay.appKey');
        } else if (config('app.ftpay.status') == 1) {
            $this->appid = config('app.ftpay.appid');
            $this->appKey = config('app.ftpay.appKey');
        }
        $this->appname = config('app_name');
    }

    public function create_order(
        $orderId, $amount, $body, $notifyUrl, $returnUrl, $extra = '', $orderIp = '', $amountType = 'CNY', $lang = 'zh_CN')
    {
        $reqParam = [
            'order_id' => $orderId,
            'amount' => $amount,
            'body' => $body,
            'notify_url' => $notifyUrl,
            'return_url' => $returnUrl,
            'extra' => $extra,
            'order_ip' => $orderIp,
            'amount_type' => $amountType,
            'time' => time() * 1000,
            'app_key' => $this->appKey,
            'lang' => $lang
        ];
        $reqParam['sign'] = $this->create_sign($reqParam, $this->appSecret);
        $url = $this->BASE_URL . '/api/v2/pay';

        return $this->http_request($url, 'POST', $reqParam);
    }

    /**
     * @return {
     * bapp_id: "2019081308272299266f",
     * order_id: "1565684838",
     * order_state: 0,
     * body: "php-sdk sample",
     * notify_url: "https://sdk.b.app/api/test/notify/test",
     * order_ip: "",
     * amount: 1,
     * amount_type: "CNY",
     * amount_btc: 0,
     * pay_time: 0,
     * create_time: 1565684842076,
     * order_type: 2,
     * app_key: "your_app_key",
     * extra: ""
     * }
     */
    public function get_order($orderId)
    {
        $reqParam = [
            'order_id' => $orderId,
            'time' => time() * 1000,
            'app_key' => $this->appKey
        ];
        $reqParam['sign'] = $this->create_sign($reqParam, $this->appSecret);
        $url = $this->BASE_URL . '/api/v2/order';
        return $this->http_request($url, 'GET', $reqParam);
    }

    public function is_sign_ok($params)
    {
        $sign = $this->create_sign($params, $this->appSecret);
        return $params['sign'] == $sign;
    }

    public function generateSign($data, $signKey)
    {
        // 按照ASCII码升序排序
        ksort($data);

        $str = '';
        foreach ($data as $key => $value) {
            $value = trim($value);
            if ('sign' != $key && '' != $value) {
                $str .= $key . '=' . $value . '&';
            }
        }

        $str .= "key=" . $signKey;
        return strtoupper(md5($str));
    }

    public function generateSign2($data, $signKey)
    {
        // 按照ASCII码升序排序
        ksort($data);

        $str = '';
        foreach ($data as $key => $value) {
            $value = trim($value);
            if ('sign' != $key && '' != $value) {
                $str .= $value;
            }
        }

        $pivate_key = '-----BEGIN PRIVATE KEY-----'."\n".$signKey."\n".'-----END PRIVATE KEY-----';
        $pi_key = openssl_pkey_get_private($pivate_key);
        $crypto = '';
        foreach (str_split($str, 117) as $chunk) {
            openssl_private_encrypt($chunk, $encryptData, $pi_key);
            $crypto .= $encryptData;
        }

        return base64_encode($crypto);
    }

    public function generateSign3($data, $signKey)
    {
        $str = '';
        foreach ($data as $value) {
            $value = trim($value);
            if ('' != $value) {
                $str .= $value;
            }
        }

        $str .= $signKey;
        // dump('sign str: ' . $str);
        return md5($str);
    }

    private function http_request($url, $method = 'GET', $params = [])
    {
        $curl = curl_init();

        if ($method == 'POST') {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
            $jsonStr = json_encode($params);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonStr);
        } else if ($method == 'GET') {
            $url = $url . "?" . http_build_query($params, '', '&');
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);


        $output = curl_exec($curl);

        if (curl_errno($curl) > 0) {
            return [];
        }
        curl_close($curl);
        $json = json_decode($output, true);

        //var_dump($output,curl_errno($curl));die;

        return $json;
    }


    public function create_sign1($data, $signKey)
    {
        // 按照ASCII码升序排序

        ksort($data);
        $data['key'] = $signKey;
        $str = '';
        foreach ($data as $key => $value) {
            $value = trim($value);
            $str .= $key . '=' . $value . '&';
        }
        $str = substr($str, 0, -1);
//        $str .= $signKey;
        //amount=10000&appId=2200909000907628&currencyType=INR&goods=充值&key=a99dfca582c9411e97da6082b40d3c83&notifyUrl=http://www.task99.top/index/api/upi_pay_notify&orderId=SY2012091445289908&orderTime=20201209144528&"
//        dump($str);
//        dump(md5($str));exit;
        return (md5($str));
    }

    public function post1($url, $requestData)
    {
//        $requestData = json_encode($requestData);
//        dump($requestData);exit;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($curl,CURLOPT_HTTPHEADER,$this_header);
        //普通数据
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($requestData));
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $requestData);
        $res = curl_exec($curl);

        //$info = curl_getinfo($ch);
        curl_close($curl);
        return $res;
        return json_decode($res,true);
    }
    public function curl_post($data, $url) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $tmpInfo = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Errno' . curl_error($ch);
        }
        curl_close($ch);
        return $tmpInfo;
    }

    public function post_json($url, $data, &$error_msg = '')
    {
        $data = json_encode($data);
        $ch = curl_init($url); //请求的URL地址
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);//$data JSON类型字符串
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));
        $tmpInfo = curl_exec($ch);

        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
        }
        curl_close($ch);
        return $tmpInfo;
    }

    public function curlPost($url, $data, $timeout, $headers, $getMillisecond)
    {
        $data = json_encode($data);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($curl);
        if (curl_errno($curl)) {
            echo 'Errno' . curl_error($curl); //捕抓异常
        }
        curl_close($curl);
        return $output;
    }

    public function pay_16(){

        $uid = session('user_id');
        $uinfo = db('xy_users')->find($uid);

        $amount = input('num/s'); //金额

        $protocol = 'http';
        if (isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] != '127.0.0.1') {
            $protocol = 'https';
        }

        $bankaccname = db('xy_bankinfo')->where('uid', $uid)->value('username');
        $params = [
            'merchantCode' => $this->appid,
            'orderNum' => getSn('SY'),
            'payMoney' => round($amount),
            'productDetail' => 'recharge',
            'name' => $bankaccname ? $bankaccname : ($uinfo['real_name'] ? $uinfo['real_name'] : $uinfo['tel']),
            'email' => $uinfo['username'] . '@mail.com',
            'phone' => $uinfo['tel'],
            'notifyUrl' => "$protocol://".$_SERVER['HTTP_HOST']."/index/api2/pay_callback_16",
            'redirectUrl' => "$protocol://".$_SERVER['HTTP_HOST'] . "/index/ctrl/recharge",
            'dateTime' => date('YmdHis')
        ];

        $params['sign'] = $this->generateSign2($params, $this->appKey);
// dump($params);
        $error_msg = '';
        $res = $this->post_json($this->post_url,$params, $error_msg);
        
        if (!$res) {
            return json(['code'=>2,'info'=>'充值订单过多，请稍后再试' . $error_msg]);
        }
// dump($res);exit;
        $data = json_decode($res,true);
        if (!isset($data['platRespCode']) || $data['platRespCode'] != 'SUCCESS'){
            return json(['code'=>2,'info'=>'充值订单过多，请稍后再试' . $res]);
        } else{
            $uid = session('user_id');
            $tel = db('xy_users')->where('id',$uid)->value('tel');
            db('xy_recharge_pay')->insert([
                'id'    =>  $params['orderNum'],
                'uid'   =>  $uid,
                'real_name' => $uinfo['real_name'],
                'tel'   =>  $tel,
                'num'   =>  $amount,
                'type'  =>  4,
                'pic'   =>  '',
                'addtime'   =>  time(),
                'pay_name'  =>  'online_pay',
                'status'    =>  1,
                'pay_id' => 16,
                'api' => $this->post_url,
                'request' => json_encode($params),
                'response' => $res
            ]);
            return json(['code'=>3,'info'=>$data['url']]);
        }

    }


    /**
     * notify_url接收页面
     */
    public function pay_callback_16()
    {

       $params = json_decode(json_encode($_POST),true);
//        file_put_contents('/www/wwwroot/shuadan/runtime/log/log.php',$post_param,FILE_APPEND);
        // $params = json_decode(file_get_contents("php://input"),true);

        if ($params['code'] == '00') {

            $sign = $params['platSign'];
            $o_sign = $this->generateSign2($params, $this->appKey);
//            dump($sign);
//            dump($o_sign);exit;
//            file_put_contents('/www/wwwroot/shuadan/runtime/log/log.php',$params.PHP_EOL,FILE_APPEND);
//            file_put_contents('/www/wwwroot/shuadan/runtime/log/log.php',$sign.'[[[[[[[[[[][]]]]]]'.$o_sign,FILE_APPEND);
            //验证通过
            // if ($sign === $o_sign) {
                $o_info = Db::name('xy_recharge_pay')->where('id', $params['orderNum'])->find();
                if ($o_info['status'] == 1) {
                    // file_put_contents('/www/wwwroot/shuadan/runtime/log/log.php',$params['resultCode'],FILE_APPEND);
                    if ($params['msg'] == 'SUCCESS') {

                        // Db::name('xy_recharge')->where('id', $params['orderNum'])->update(['endtime' => time(), 'status' => 2]);
                        Db::name('xy_recharge')->insert([
                            'id'    =>  $o_info['id'],
                            'uid'   =>  $o_info['uid'],
                            'real_name' => $o_info['real_name'],
                            'tel'   =>  $o_info['tel'],
                            'num'   =>  $o_info['num'],
                            'type'  =>  4,
                            'pic'   =>  '',
                            'addtime'   =>  $o_info['addtime'],
                            'endtime'   =>  time(),
                            'pay_name'  =>  'online_pay',
                            'status'    =>  2,
                            'pay_id' => 16,
                        ]);
                        Db::name('xy_recharge_pay')->where('id', $params['orderNum'])->update(['endtime' => time(), 'status' => 2, 'order_status_response' => json_encode($params)]);
                        Db::name('xy_balance_log')
                            ->insert([
                                'uid' => $o_info['uid'],
                                'oid' => $params['orderNum'],
                                'num' => $params['payMoney'],
                                'type' => 1,
                                'status' => 1,
                                'addtime' => time(),
                            ]);
                        Db::name('xy_users')->where('id', $o_info['uid'])->setInc('balance', $params['payMoney']);

                        /************* 发放推广奖励 *********/
                        //用户总充值
                        $total_recharge = Db::name('xy_recharge')->where(['uid' => $o_info['uid'], 'status' => 2])->sum('num');

                        //首次充值奖励
                        if ($total_recharge == $params['payMoney'] && config('first_recharge_reward') > 0 && $payMoney['payMoney'] >= config('first_recharge_reward_min_amount')) {
                            $rechargeReward = config('first_recharge_reward');
                            $res_rechargeReward = Db::name('xy_balance_log')->insert([
                                //记录邀请一个朋友三十元的现金奖励
                                'uid'       => $o_info['uid'],
                                'oid'       =>$params['orderNum'],
                                'num'       => $rechargeReward,
                                'type'      => 18,//首次充值奖励
                                'addtime'   => time(),
                            ]);
                            if ($res_rechargeReward) {
                                Db::name('xy_users')->where('id',$o_info['uid'])->setInc('balance',$rechargeReward);
                            };
                        }

                        Db::name('xy_users')->where('id', $o_info['uid'])->update(['total_recharge' => $total_recharge]);
                        if ($total_recharge >= config('invite_reward_min_recharge')) {
                            if (!in_array(config('app_name'), ['dypf_indon', 'prime'])) {
                                $total_recharge = 0;
                            }
                            model('admin/Users')->invitationBonus($o_info['uid'],$params['orderNum']);
                        }
                        /************* 发放推广奖励 *********/

                        die('SUCCESS');
                    }
                }
            // }
        }
        die('SUCCESS');
    }

    public function pay_32(){

        $this->post_url = 'https://zaplati.cc/Apipay';

        $uid = session('user_id');
        $uinfo = db('xy_users')->find($uid);

        $amount = input('num/d'); //金额

        // dump(input());
        // dump($amount);

        $protocol = 'http';
        if (isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] != '127.0.0.1') {
            $protocol = 'https';
        }

        $bankaccname = db('xy_bankinfo')->where('uid', $uid)->value('username');
        $params = [
            'userid' => $this->appid,
            'orderno' => getSn('SY'),
            'desc' => 'recharge',
            'amount' => round($amount),            
            'notifyurl' => "$protocol://".$_SERVER['HTTP_HOST']."/index/api2/pay_callback_32",
            'backurl' => "$protocol://".$_SERVER['HTTP_HOST'] . "/index/ctrl/recharge",
            'paytype' => 'bank_auto',
            'acname' => $bankaccname ? $bankaccname : ($uinfo['real_name'] ? $uinfo['real_name'] : $uinfo['tel']),
            'userip' => request()->ip(),
            'currency' => 'CNY'
        ];
        // dump($params);
        $signparam = [
            $params['userid'],
            $params['orderno'],
            $params['amount'],
            $params['notifyurl'],
        ];
        $params['sign'] = $this->generateSign3($signparam, $this->appKey);
        
        $error_msg = '';
        $res = $this->post1($this->post_url,$params, $error_msg);
            
        // dump($res);exit;
        if (!$res) {
            return json(['code'=>2,'info'=>'充值订单过多，请稍后再试' . $error_msg]);
        }        
        $data = json_decode($res,true);
        if (!isset($data['status']) || $data['status'] != 1){
            if (isset($data['Message'])) {
                $msg = $data['Message'];
            } else {
                $msg = $res;
            }
            return json(['code'=>2,'info'=>'充值订单过多，请稍后再试。' . $msg]);
        } else{
            $uid = session('user_id');
            $tel = db('xy_users')->where('id',$uid)->value('tel');
            db('xy_recharge_pay')->insert([
                'id'    =>  $params['orderno'],
                'uid'   =>  $uid,
                'real_name' => $params['acname'],
                'tel'   =>  $tel,
                'num'   =>  $amount,
                'type'  =>  4,
                'pic'   =>  '',
                'addtime'   =>  time(),
                'pay_name'  =>  'online_pay',
                'status'    =>  1,
                'pay_id' => 32,
                'api' => $this->post_url,
                'request' => json_encode($params),
                'response' => $res
            ]);
            return json(['code'=>3,'info'=>$data['payurl']]);
        }

    }


    /**
     * notify_url接收页面
     */
    public function pay_callback_32()
    {
        if ($this->appname == 'prime') {
            die('success');
        }
       $params = json_decode(json_encode($_POST),true);
//        file_put_contents('/www/wwwroot/shuadan/runtime/log/log.php',$post_param,FILE_APPEND);
        // $params = json_decode(file_get_contents("php://input"),true);

        if (isset($params['status']) && $params['status'] == '1') {

            $sign = $params['sign'];
            $signparam = [
                $params['currency'],
                $params['status'],
                $params['userid'],
                $params['orderno'],
                $params['amount'],
            ];
            $o_sign = $this->generateSign3($signparam, $this->appKey);
//            dump($sign);
//            dump($o_sign);exit;
//            file_put_contents('/www/wwwroot/shuadan/runtime/log/log.php',$params.PHP_EOL,FILE_APPEND);
//            file_put_contents('/www/wwwroot/shuadan/runtime/log/log.php',$sign.'[[[[[[[[[[][]]]]]]'.$o_sign,FILE_APPEND);
            //验证通过
            if ($sign === $o_sign) {
                $orderno = $params['orderno'];
                $amount = $params['amount'];
                $o_info = Db::name('xy_recharge_pay')->where('id', $orderno)->find();
                if ($o_info['status'] == 1) {
                    // file_put_contents('/www/wwwroot/shuadan/runtime/log/log.php',$params['resultCode'],FILE_APPEND);
                    if ($params['status'] == '1') {

                        // Db::name('xy_recharge')->where('id', $params['orderNum'])->update(['endtime' => time(), 'status' => 2]);
                        Db::name('xy_recharge')->insert([
                            'id'    =>  $o_info['id'],
                            'uid'   =>  $o_info['uid'],
                            'real_name' => $o_info['real_name'],
                            'tel'   =>  $o_info['tel'],
                            'num'   =>  $o_info['num'],
                            'type'  =>  4,
                            'pic'   =>  '',
                            'addtime'   =>  $o_info['addtime'],
                            'endtime'   =>  time(),
                            'pay_name'  =>  'online_pay',
                            'status'    =>  2,
                            'pay_id' => 32,
                        ]);
                        $params['o_sign'] = $o_sign;
                        Db::name('xy_recharge_pay')->where('id', $orderno)->update(['endtime' => time(), 'status' => 2, 'order_status_response' => json_encode($params)]);
                        Db::name('xy_balance_log')
                            ->insert([
                                'uid' => $o_info['uid'],
                                'oid' => $orderno,
                                'num' => $amount,
                                'type' => 1,
                                'status' => 1,
                                'addtime' => time(),
                            ]);
                        Db::name('xy_users')->where('id', $o_info['uid'])->setInc('balance', $amount);

                        /************* 发放推广奖励 *********/
                        //用户总充值
                        $total_recharge = Db::name('xy_recharge')->where(['uid' => $o_info['uid'], 'status' => 2])->sum('num');

                        //首次充值奖励
                        if ($total_recharge == $amount && config('first_recharge_reward') > 0 && $amount >= config('first_recharge_reward_min_amount')) {
                            $rechargeReward = config('first_recharge_reward');
                            $res_rechargeReward = Db::name('xy_balance_log')->insert([
                                //记录邀请一个朋友三十元的现金奖励
                                'uid'       => $o_info['uid'],
                                'oid'       =>$orderno,
                                'num'       => $rechargeReward,
                                'type'      => 18,//首次充值奖励
                                'addtime'   => time(),
                            ]);
                            if ($res_rechargeReward) {
                                Db::name('xy_users')->where('id',$o_info['uid'])->setInc('balance',$rechargeReward);
                            };
                        }

                        Db::name('xy_users')->where('id', $o_info['uid'])->update(['total_recharge' => $total_recharge]);
                        if ($total_recharge >= config('invite_reward_min_recharge')) {
                            if (!in_array(config('app_name'), ['dypf_indon', 'prime'])) {
                                $total_recharge = 0;
                            }
                            model('admin/Users')->invitationBonus($o_info['uid'],$orderno);
                        }
                        /************* 发放推广奖励 *********/

                        die('success');
                    }
                }
            }
        }
        die('success');
    }

    //提现
    public function daifu_callback(){
        $post_param = json_decode(file_get_contents("php://input"),true);
        if ($post_param['code'] == 200) {
            $params['resultCode'] = $post_param['obj']['resultCode'];
            $params['merchantCode'] = $post_param['obj']['merchantCode'];
            $params['amount'] = $post_param['obj']['amount'];
            $params['orderId'] = $post_param['obj']['orderId'];
            $params['time'] = $post_param['obj']['time'];
            $sign = $post_param['obj']['sign'];
            $o_sign = $this->get_sign($params, 'nS6RvEEWwtjqO9L6');
            //验证通过
            if ($sign === $o_sign) {
                $depositinfo = Db::name('xy_deposit')->where('id',$params['orderId'])->find();
                if ($depositinfo['status']==4) {
                    Db::name('xy_deposit')->where('id', $params['orderId'])->update(['status' => 2]);

//                    Db::name('xy_users')->where('id', $depositinfo['uid'])->setDec('balance', $params['amount']);
                }
            }
            die("OK");
        }

    }

    function strToArr($str,$sp="&",$kv="="){
        $arr = str_replace(array($kv,$sp),array('"=>"','","'),'array("'.$str.'")');
//        dump($arr);exit;
        eval("$arr"." = $arr;");   // 把字符串作为PHP代码执行
        return $arr;
    }
    function str2arr ($str,$sp="&",$kv="=")
    {
        $arr = str_replace(array($kv,$sp),array('"=>"','","'),'array("'.$str.'")');
//        dump($arr);exit;
        eval("\$arr"." = $arr;");   // 把字符串作为PHP代码执行
        return $arr;
    }

    /*pay11*/

    //获取毫秒数
    public function getMillisecond()
    {
        list($microsecond, $time) = explode(' ', microtime()); //' '中间是一个空格
        return (float)sprintf('%.0f', (floatval($microsecond) + floatval($time)) * 1000);
    }
    public function posturl($url,$data){
//        $data  = json_encode($data);
//        dump($data);exit;
        $headerArray =array("Content-type:application/x-www-form-urlencoded;charset=utf-8;","Accept:application/json;charset=utf-8;");
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
        curl_setopt($curl, CURLOPT_POST, 1);
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl,CURLOPT_HTTPHEADER,$headerArray);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);
        return json_decode($output,true);
    }

    public function upload() {
        $pic = input('post.pic/s', '');
        $filename = input('post.filename/s', '');
        if (is_image_base64($pic)) {
            $data = [
                'is_sync' => true,
                'filename' => $filename
            ];
            if (input('post.is_admin')) {
                $data['dir'] = input('post.dir');
                $data['is_admin'] = input('post.is_admin');
            }
            $pic = '/' . \app\index\controller\Base::upload_base64('xy', $pic, $data);  //调用图片上传的方法
        }
        return $pic;
    }
}