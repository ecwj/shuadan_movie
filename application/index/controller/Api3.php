<?php
namespace app\index\controller;

use library\Controller;
use think\Db;
use think\facade\Cache;

/**
 * 支付控制器
 */
class Api3 extends Controller
{
    public $isApi = false;
    public $uid = null;
    public $uinfo = [];
    public $logId = null;

    function __construct() {
        $headers = request()->header();
        $this->logId = self::insertlog();
        if (isset($headers['api-key']) && $headers['api-key'] == config('api-key')) {
            $this->isApi = true;
            if (isset($headers['token'])) {
                $this->uid = decrypt($headers['token'], config('api-key'));
                $this->uinfo = db('xy_users')->field('username, down_userid, tel, id')->find($this->uid);
                if (!$this->uinfo) {
                    $this->error(lang('Please log in'));
                }
            } else {
                $this->error(lang('Please log in'));
            }
            if (isset($headers['locale']) && in_array($headers['locale'], ['cn', 'en', 'yn'])) {
                $locale = $headers['locale'];
            } else {
                $locale = 'cn';
            }
            cookie('think_var', $locale);
        } else {
            $this->error(lang('Please log in'));
        }
    }

    public function chatrooms()
    {
        $uid = $this->uid;
        
        if (in_array(strtolower($this->uinfo['username']), $tester)) {
            $sql = db('xy_inbox_group')
            ->alias('ing')
            ->where("receiver_uid LIKE '$uid' OR receiver_uid LIKE '$uid" . ",%' OR receiver_uid LIKE '%,$uid,%' OR receiver_uid LIKE '%,$uid'")
            ->field("ing.id, ing.updatetime, ing.sender_uid, ing.receiver_uid, 'group' AS type, name AS sender_username, name AS receiver_username")
            ->buildSql();
        } else {
            $sql = "";
        }
        $list = db('xy_inbox')
        ->alias('in')
        ->join('xy_users sender', 'sender.id = in.sender_uid')
        ->join('xy_users receiver', 'receiver.id = in.receiver_uid')
        ->where("(sender_uid = $uid OR receiver_uid = $uid) AND in.status = 1")
        ->field("in.id, in.updatetime, in.sender_uid, in.receiver_uid, 'one_to_one' AS type, COALESCE(sender.weixin, sender.username) AS sender_username, COALESCE(receiver.weixin, receiver.username) AS receiver_username")
        ->union($sql)
        ->order('updatetime desc')
        ->select();
        
        $time = time();
        foreach ($list as $k => $v) {
            $inboxId = $v['id'];
            $updatetime = $v['updatetime'];
            
            $table = 'xy_inbox_message';
            if ($v['type'] == 'group') {
                $table = 'xy_inbox_message_group';
            }
            $message = db($table)->where("inbox_id = '$inboxId' AND status = 1")->order('addtime DESC')->find();
            if ($message) {
                // $list[$k]['message'] = ($message['uid'] == $uid ? lang('You') . ': ' : '') . $message['content'];
                $list[$k]['last_message'] = $message['content'];
            } else {
                $list[$k]['last_message'] = '';
            }
            
            $list[$k]['title'] = $v['sender_uid'] == $uid ? $v['receiver_username'] : $v['sender_username'];
            // $diff_s = $time - $updatetime;
            // $time_text = strftime('%b %d',$updatetime);
            // if ($diff_s < 30) {
            //     $time_text = lang('Just now');
            // } else if ($diff_s < 60) {
            //     $time_text = $diff_s . lang('Sec ago');
            // } else if ($diff_s < 3600) {
            //     $time_text = floor($diff_s / 60) . lang('Min ago');
            // } else if (date('Y', $updatetime) < date('Y')) {
            //     $time_text = date('d/m/y', $updatetime);
            // } else {
            //     $time_text = date('H:i', $updatetime);
            // }
            
            // $list[$k]['time_text'] = $time_text;
            $list[$k]['isread'] = isset($message) && $message['uid'] != $uid ? $message['isread'] : 1;
            $list[$k]['inbox_id'] = $v['id'];
            // $list[$k]['sender_uid'] = encrypt_hashids($v['sender_uid']);
            unset($list[$k]['id']);
            unset($list[$k]['sender_uid']);
            unset($list[$k]['receiver_uid']);
        }

        return self::output_json(['code' => 0, 'info' => '', 'data' => $list]);
    }

    public function chat()
    {
        if (!request()->isPost()) {
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        $id = input('post.inbox_id/i', 0);
        $uid = $this->uid;
        $type = input('post.type/s','one_to_one');
        $limit = input('post.limit', 50);
        $page = input('post.page', 0);
        $offset = $page * $limit;
        
        $table = $type == 'group' ? 'xy_inbox_message_group' : 'xy_inbox_message';
        
        $messageId = input('page/i', 0);
        $map = [
            'inbox_id' => $id,
            'msg.status' => 1
        ];
        
        $list = db($table)->alias('msg')
        ->join('xy_users u', 'u.id = msg.uid')
        ->where($map)
        ->field('msg.content AS message, msg.id AS message_id, COALESCE(u.weixin, u.username) AS sender_username, uid')
        ->order('msg.addtime desc')
        ->limit($offset, $limit)
        ->select();
        
        if ($list) {
            krsort($list);
            $list = array_values($list);
        } else {
            $list = [];
        }
        
        foreach ($list as $k => $v) {
            $list[$k]['uid'] = encrypt_hashids($v['uid']);
        }
        return self::output_json(['code' => 0, 'info' => '', 'data' => $list]);
    }

    public function contact_list()
    {
        $uid = $this->uid;
        Cache::set('api_contact_list_' . $uid, NULL);
        if (!$contact_list = Cache::get('api_contact_list_' . $uid)) {
            $userids = db('xy_users')->where('id', $uid)->value('down_userid');
            $userids = explode(",", $userids);
            $parent = model('admin/Users')->get_parent_users($uid);
            $userids = array_merge($userids, $parent);

            $exclude = ['company'];
            $contact_list = db('xy_users')->where('id', 'IN', $userids)->where('username', 'NOT IN', $exclude)->where('id != ' . $uid)->field("tel, COALESCE(weixin, '') AS weixin, username, id AS uid, headpic AS profile_image")->select();

            foreach ($contact_list as $k => $c) {
                $contact_list[$k]['uid'] = encrypt_hashids($c['uid']);
                $contact_list[$k]['profile_image'] = config('image_domain') . $c['profile_image'];
            }
            Cache::set('api_contact_list_' . $uid, $contact_list);
        }

        return self::output_json(['data' => $contact_list]);
    }

    public function chat_checkuserexist()
    {
        if (!request()->isPost()) {
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        $uid = $this->uid;
        $uinfo = db('xy_users')->field('username, down_userid')->find($uid);
        $down_userid = $uinfo['down_userid'];
        $down_userid_arr = explode(",", $down_userid);
        $up_userid_arr = array_filter(model('admin/Users')->get_parent_users($uid));
        
        $arr = array_unique(array_merge($down_userid_arr, $up_userid_arr));
        $uids = $arr ? implode(",", $arr) : "''";
        
        $val = input('post.val/s', '');
        
        if (in_array(strtolower($uinfo['username']), ['cs001', 'test0001'])) {
            $receiver_uid = db('xy_users')->where("username = '$val' OR tel = '$val' OR weixin = '$val'")->where("id != '$uid' AND username NOT IN ('company', 'wmy001')")->value('id');
        } else {
            $receiver_uid = db('xy_users')->where("username = '$val' OR tel = '$val' OR weixin = '$val'")->where("id IN ($uids)")->where("id != '$uid' AND username NOT IN ('company', 'wmy001')")->value('id');
        }
        if (!$receiver_uid) {
            return self::output_json(['code' => 1, 'info' => lang('Recipient does not exist')]);
        } else {
            return self::output_json(['code' => 0, 'info' => '', 'data' => ['uid' => encrypt_hashids($receiver_uid)]]);
        }
    }

    public function checkgroupnameexist()
    {
        if (!request()->isPost()) {
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        $uid = $this->uid;
        $name = input("post.name/s", '');
        if (!$name) {
            return self::output_json(['code' => 1, 'info' => lang('Please enter group name')]);
        }
        
        $exist = db('xy_inbox_group')->where(['sender_uid' => $uid, 'name' => $name])->field('id')->find();
        
        if ($exist) {
            return self::output_json(['code' => 1, 'info' => lang('You have created the same group name')]);
        } else {
            return self::output_json(['code' => 0, 'info' => 'SUCCESS']);
        }
    }

    public function send()
    {
        $uid = $this->uid;
        if (request()->post()) {
            $receiver_uid = input('receiver_uid/i', 0);
            $message = input('message/s', '');
            
            $receiver_uid = decrypt_hashids($receiver_uid);
            $receiver_uid = isset($receiver_uid[0]) ? $receiver_uid[0] : 0;
            if (!db('xy_users')->find($receiver_uid)) {
                return self::output_json(['code' => 1, 'info' => lang('Recipient does not exist')]);
            }
            
            $map = [
                'sender_uid' => $uid,
                'receiver_uid' => $receiver_uid,
                'status' => 1,
            ];
            $inbox = db('xy_inbox')->where($map)->find();
            $time = time();
            if ($inbox) {
                $data_message = [
                    'inbox_id' => $inbox['id'],
                    'uid' => $uid,
                    'content' => $message,
                    'addtime' => $time,
                ];
                $re = db('xy_inbox_message')->insert($data_message);
                if ($re) {
                    db('xy_inbox')->where('id', $inbox['id'])->update(['action' => 1, 'updatetime' => $time]);
                    return self::output_json(['code' => 0, 'info' => lang('Sent successfully')]);
                } else {
                    return self::output_json(['code' => 1, 'info' => lang('Failed to send')]);
                }
            }
            
            $data = [
                'sender_uid' => $uid,
                'receiver_uid' => $receiver_uid,
                'addtime' => $time,
                'updatetime' => $time,
                'action' => 1,//1等待回复，2对方已回复
            ];
            
            $inboxId = db('xy_inbox')->insertGetId($data);
            
            if (!$inboxId) {
                return self::output_json(['code' => 1, 'info' => lang('Failed to send')]);
            }
            $data_message = [
                'inbox_id' => $inboxId,
                'uid' => $uid,
                'content' => $message,
                'addtime' => $time,
            ];
            $re = db('xy_inbox_message')->insert($data_message);
            if ($re) {
                return self::output_json(['code' => 0, 'info' => lang('Sent successfully')]);
            } else {
                return self::output_json(['code' => 1, 'info' => lang('Failed to send')]);
            }
        }
        if (!request()->isPost()) {
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
    }
    
    public function reply()
    {
        $uid = $this->uid;
        if (request()->post()) {
            $id = input('post.inbox_id/i', 0);
            $message = input('post.message/s', '');
            $type = input('post.type/s', 'one_to_one');
            $type = !in_array($type, ['one_to_one', 'group']) ? 'one_to_one' : $type;
            $table = $type == 'group' ? '_group' : '';
            $map = [
                'id' => $id,
                'status' => 1,
            ];
            $map2 = "";
            if ($type == 'one_to_one') {
                $map2 = "sender_uid = $uid OR receiver_uid = $uid";
            } else {
                $map2 = "receiver_uid LIKE '$uid' OR receiver_uid LIKE '$uid" . ",%' OR receiver_uid LIKE '%,$uid,%' OR receiver_uid LIKE '%,$uid'";
            }
            $inbox = db('xy_inbox' . $table)->where($map)->where($map2)->find();
            $time = time();
            if ($inbox) {
                $data_message = [
                    'inbox_id' => $inbox['id'],
                    'uid' => $uid,
                    'content' => $message,
                    'addtime' => $time,
                ];
                $re = db('xy_inbox_message' . $table)->insert($data_message);
                if ($re) {
                    db('xy_inbox' . $table)->where('id', $inbox['id'])->update(['action' => 1, 'updatetime' => $time]);
                    return self::output_json(['code' => 0, 'info' => lang('Sent successfully')]);
                } else {
                    return self::output_json(['code' => 1, 'info' => lang('Failed to send')]);
                }
            }
            
            return self::output_json(['code' => 1, 'info' => lang('Failed to send')]);
        }
        if (!request()->isPost()) {
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
    }

    public function sendgroup()
    {
        if (!request()->isPost()) {
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        $uid = $this->uid;
        if (request()->post()) {
            $receiver_uid = input('post.receiver_uid/s', '') ? input('post.receiver_uid/s', '') : [];

            $receiver_uid = trim($receiver_uid);
            $receiver_uid = rtrim($receiver_uid,',');

            $receiver_username = input('receiver_username/s', '');

            $receiver_usernames = [];
            if ($receiver_username) {
                $receiver_username = trim($receiver_username);
                $receiver_username = rtrim($receiver_username,',');

                $receiver_usernames = explode(",", $receiver_username);
            }

            $message = input('post.message/s', '');
            $groupname = input('post.groupname/s', '');

            if (!$groupname) {
                return self::output_json(['code' => 1, 'info' => lang('Please enter group name'), 'data' => ['msg' => 'Please include groupname']]);
            }
            if (!$receiver_usernames) {
                return self::output_json(['code' => 1, 'info' => lang('Please enter receiver'), 'data' => ['msg' => 'Please include receiver\'s username']]);
            }
            if (!$message) {
                return self::output_json(['code' => 1, 'info' => lang('Please enter message')]);
            }
            
            $userids = db('xy_users')->where('id', $uid)->value('down_userid');
            $userids = explode(",", $userids);
            $parent = model('admin/Users')->get_parent_users($uid);
            $userids = array_merge($userids, $parent);

            $exclude = ['company'];
            $valid_contact_userids = db('xy_users')->where('id', 'IN', $userids)->where('username', 'NOT IN', $exclude)->where('id != ' . $uid)->column("id");

            $receiver_uids = explode(",", $receiver_uid);
            $receiver_uid_ = [];
            foreach ($receiver_uids as $k => $receiver_uid) {
                $receiver_uid = decrypt_hashids($receiver_uid);
                $receiver_uid = isset($receiver_uid[0]) ? $receiver_uid[0] : 0;
                if (!db('xy_users')->find((int) $receiver_uid) || !in_array($receiver_uid, $valid_contact_userids)) {
                    return self::output_json(['code' => 1, 'info' => lang('The recipient {:username} does not exist', ['username' => $receiver_usernames[$k]])]);
                }
                $receiver_uid_[] = $receiver_uid;
            }
            $receiver_uids = $receiver_uid_;
            $receiver_uids[] = $uid;
            sort($receiver_uids);
            $map = [
                'name' => $groupname,
                'sender_uid' => $uid,
                'receiver_uid' => implode(",", $receiver_uids),
                'status' => 1,
            ];
            $inbox = db('xy_inbox_group')->where($map)->find();
            $time = time();
            if ($inbox) {
                $data_message = [
                    'inbox_id' => $inbox['id'],
                    'uid' => $uid,
                    'content' => $message,
                    'addtime' => $time,
                ];
                $re = db('xy_inbox_message_group')->insert($data_message);
                if ($re) {
                    db('xy_inbox_group')->where('id', $inbox['id'])->update(['action' => 1, 'updatetime' => $time]);
                    return self::output_json(['code' => 0, 'info' => lang('Sent successfully')]);
                } else {
                    return self::output_json(['code' => 1, 'info' => lang('Failed to send')]);
                }
            }
            
            $data = [
                'name' => $groupname,
                'sender_uid' => $uid,
                'receiver_uid' => implode(",", $receiver_uids),
                'owner_uids' => $uid,
                'addtime' => $time,
                'updatetime' => $time,
                'action' => 1,//1等待回复，2对方已回复
            ];
            
            $inboxId = db('xy_inbox_group')->insertGetId($data);
            
            if (!$inboxId) {
                return self::output_json(['code' => 1, 'info' => lang('Failed to send')]);
            }
            $data_message = [
                'inbox_id' => $inboxId,
                'uid' => $uid,
                'content' => $message,
                'addtime' => $time,
            ];
            $re = db('xy_inbox_message_group')->insert($data_message);
            if ($re) {
                return self::output_json(['code' => 0, 'info' => lang('Sent successfully')]);
            } else {
                return self::output_json(['code' => 1, 'info' => lang('Failed to send')]);
            }
        }
    }

    public function recall_message()
    {
        if (request()->post()) {
            $messageId = input('post.message_id/i', 0);
            $type = input('post.type/s', '');
            $table = $type == 'group' ? 'xy_inbox_message_group' : 'xy_inbox_message';
            $uid = $this->uid;
            $message = db($table)->where(['id' => $messageId, 'status' => 1, 'uid' => $uid])->find();
            if (!$message) {
                return self::output_json(['code' => 1, 'info' => lang('Unable to recall the recalled message')]);
            }
            
            $re = db($table)->where('id', $messageId)->update(['status' => 2, 'recall_time' => time()]);
            if ($re) {
                return self::output_json(['code' => 0, 'info' => lang('Recalled successfully')]);
            } else {
                return self::output_json(['code' => 1, 'info' => lang('No records updated')]);
            }
        }
        if (!request()->isPost()) {
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
    }

    //To create chat room for one-to-one chat
    public function one_to_one_chat()
    {
        $uid = $this->uid;
        if (request()->post()) {
            $chat_room_id = input('post.chat_room_id/s', '');
            $users = input('post.users');
            if (!is_array($users)) {
                $users = explode(",", $users);
            }
            $type = input('post.type/s', '');
            if ($type != 'one_to_one') {
                return self::output_json(['code' => 1, 'info' => lang('Invalid type')]);
            }
            if (sizeof($users) != 2) {
                return self::output_json(['code' => 1, 'info' => lang('Users size can only be 2')]);
            }
            if (!$chat_room_id) {
                return self::output_json(['code' => 1, 'info' => lang('chat_room_id is required.')]);   
            }
            $receiver_uid = $users[1];
            $sender_uid = $users[0];
            
            $receiver_uid = decrypt_hashids($receiver_uid);
            $receiver_uid = isset($receiver_uid[0]) ? $receiver_uid[0] : 0;

            $sender_uid = decrypt_hashids($sender_uid);
            $sender_uid = isset($sender_uid[0]) ? $sender_uid[0] : 0;

            if ($sender_uid != $uid) {
                return self::output_json(['code' => 1, 'info' => lang('Parameter error')]);
            }

            if ($sender_uid == $receiver_uid) {
                return self::output_json(['code' => 1, 'info' => lang('Sender and receiver cannot be the same')]);
            }

            if (!db('xy_users')->find($receiver_uid)) {
                return self::output_json(['code' => 1, 'info' => lang('Recipient does not exist')]);
            }
            
            $map = [
                'sender_uid' => $uid,
                'receiver_uid' => $receiver_uid,
                'status' => 1,
                'chat_room_id' => $chat_room_id
            ];
            $inbox = db('xy_inbox')->where($map)->find();
            $time = time();
            if ($inbox) {
                return self::output_json(['code' => 1, 'info' => lang("You've created a chat room with the receiver.")]);
            }
            
            $data = [
                'sender_uid' => $uid,
                'receiver_uid' => $receiver_uid,
                'addtime' => $time,
                'updatetime' => $time,
                'action' => 1,//1等待回复，2对方已回复,
                'chat_room_id' => $chat_room_id
            ];
            
            $inboxId = db('xy_inbox')->insertGetId($data);
            
            if (!$inboxId) {
                return self::output_json(['code' => 1, 'info' => lang('Failed to open a chat room')]);
            } else {
                return self::output_json(['code' => 0, 'info' => lang('Open chat room successfully'), 'data' => ['inbox_id' => $inboxId]]);
            }
            
        }
        if (!request()->isPost()) {
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
    }

    //To create chat room for group chat
    public function group_chat()
    {
        if (!request()->isPost()) {
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }

        $uid = $this->uid;
        if (request()->post()) {
            $chat_room_id = input('post.chat_room_id/s', '');
            $users = input('post.users');
            if (!is_array($users)) {
                $users = explode(",", $users);
            }
            $type = input('post.type/s', '');
            $chat_image = input('post.chat_image/s', '');

            $chat_image_url = $chat_image;
            // if (!$chat_image) {
            //     $chat_image_url = config('image_domain') . '/public/img/new/' . config('app_name') . '/community.png';
            // }

            if ($type != 'group') {
                return self::output_json(['code' => 1, 'info' => lang('Invalid type')]);
            }
            if (sizeof($users) < 3) {
                return self::output_json(['code' => 1, 'info' => lang('Users size should not be less than 3')]);
            }
            if (!$chat_room_id) {
                return self::output_json(['code' => 1, 'info' => lang('chat_room_id is required.')]);   
            }

            $sender_uid = $users[0];

            $sender_uid = decrypt_hashids($sender_uid);
            $sender_uid = isset($sender_uid[0]) ? $sender_uid[0] : 0;

            if ($sender_uid != $uid) {
                return self::output_json(['code' => 1, 'info' => lang('Parameter error')]);
            }

            $receiver_uids = $users;

            $groupname = input('post.chat_room_name/s', '');

            if (!$groupname) {
                return self::output_json(['code' => 1, 'info' => lang('Please enter group name'), 'data' => ['msg' => 'Please include groupname']]);
            }
            if (!$receiver_uids) {
                return self::output_json(['code' => 1, 'info' => lang('Please enter receiver'), 'data' => ['msg' => 'Please include receiver\'s username']]);
            }
            
            $userids = db('xy_users')->where('id', $uid)->value('down_userid');
            $userids = explode(",", $userids);
            $parent = model('admin/Users')->get_parent_users($uid);
            $userids = array_merge($userids, $parent);

            $exclude = ['company'];
            $valid_contact_userids = db('xy_users')->where('id', 'IN', $userids)->where('username', 'NOT IN', $exclude)->where('id != ' . $uid)->column("id");

            $receiver_uid_ = [];
            foreach ($receiver_uids as $k => $receiver_uid) {
                $receiver_uid = decrypt_hashids($receiver_uid);
                $receiver_uid = isset($receiver_uid[0]) ? $receiver_uid[0] : 0;
                if ($receiver_uid == $uid) {
                    continue;
                }
                if (!db('xy_users')->find((int) $receiver_uid) || !in_array($receiver_uid, $valid_contact_userids)) {
                    return self::output_json(['code' => 1, 'info' => lang('The recipient {:username} does not exist', ['username' => $receiver_uids[$k]])]);
                }
                $receiver_uid_[] = $receiver_uid;
            }
            $receiver_uids = $receiver_uid_;
            $receiver_uids[] = $uid;
            if (sizeof($receiver_uids) < 3) {
                return self::output_json(['code' => 1, 'info' => lang('Users size should not be less than 3')]);
            }
            sort($receiver_uids);
            $map = [
                'name' => $groupname,
                'sender_uid' => $uid,
                'receiver_uid' => implode(",", $receiver_uids),
                'status' => 1,
                'chat_room_id' => $chat_room_id
            ];
            $inbox = db('xy_inbox_group')->where($map)->find();
            $time = time();
            if ($inbox) {
                return self::output_json(['code' => 1, 'info' => lang("You've created a chat room with the receiver.")]);
            }
            
            // if ($chat_image) {
            //     if (is_image_base64($chat_image)) {
            //         $chat_image_url = '/' . \app\index\controller\Base::upload_base64('chat', $chat_image);  //调用图片上传的方法
            //     }
            // }

            $data = [
                'name' => $groupname,
                'sender_uid' => $uid,
                'receiver_uid' => implode(",", $receiver_uids),
                'owner_uids' => $uid,
                'addtime' => $time,
                'updatetime' => $time,
                'action' => 1,//1等待回复，2对方已回复,
                'chat_room_id' => $chat_room_id,
                'chat_image_url' => $chat_image_url
            ];
            
            $inboxId = db('xy_inbox_group')->insertGetId($data);
            
            if (!$inboxId) {
                return self::output_json(['code' => 1, 'info' => lang('Failed to open a chat room')]);
            } else {
                return self::output_json(['code' => 0, 'info' => lang('Open chat room successfully'), 'data' => ['inbox_id' => $inboxId]]);
            }
        }
    }

    //Get chat room list data one to one
    public function getchat()
    {
        if (!request()->isGet()) {
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        $uid = $this->uid;
        $current_user_id = input('get.current_user_id/s', '');
        $current_user_id = decrypt_hashids($current_user_id);
        $current_user_id = isset($current_user_id[0]) ? $current_user_id[0] : 0;

        if ($current_user_id != $uid) {
            return self::output_json(['code' => 1, 'info' => lang('Invalid user id')]);
        }

        $type = input('get.type/s', 'one_to_one');
     
        $sql = db('xy_inbox_group')
        ->alias('ing')
        ->where("receiver_uid LIKE '$uid' OR receiver_uid LIKE '$uid" . ",%' OR receiver_uid LIKE '%,$uid,%' OR receiver_uid LIKE '%,$uid'")
        ->field("ing.id, ing.updatetime AS last_message_time, ing.sender_uid, ing.receiver_uid, 'group' AS type, name AS sender_username, name AS receiver_username, COALESCE(chat_room_id, '') AS chat_room_id, COALESCE(chat_image_url, '') AS chat_image, ing.name AS groupname, '' AS s_headpic, '' AS r_headpic");

        if ($type == 'all') {
            $sql = $sql->buildSql();
        } else if ($type == 'group') {
            $list = $sql->order('last_message_time desc')->select();
            $sql = "";
        } else {
            $sql = "";
        }

        if ($type == 'all' || $type == 'one_to_one') {
            $list = db('xy_inbox')
            ->alias('in')
            ->join('xy_users sender', 'sender.id = in.sender_uid')
            ->join('xy_users receiver', 'receiver.id = in.receiver_uid')
            ->where("(sender_uid = $uid OR receiver_uid = $uid) AND in.status = 1")
            ->field("in.id, in.updatetime AS last_message_time, in.sender_uid, in.receiver_uid, 'one_to_one' AS type, COALESCE(sender.weixin, sender.username) AS sender_username, COALESCE(receiver.weixin, receiver.username) AS receiver_username, COALESCE(chat_room_id, '') AS chat_room_id, COALESCE(chat_image_url, '') AS chat_image,'' AS groupname, sender.headpic AS s_headpic, receiver.headpic AS r_headpic")
            ->union($sql)
            ->order('last_message_time desc')
            ->select();
        }
        
        $time = time();
        foreach ($list as $k => $v) {
            $inboxId = $v['id'];
            
            $table = 'xy_inbox_message';
            if ($v['type'] == 'group') {
                $table = 'xy_inbox_message_group';
            }
            $message = db($table)->where("inbox_id = '$inboxId' AND status = 1")->order('addtime DESC')->find();
            if ($message) {
                // $list[$k]['message'] = ($message['uid'] == $uid ? lang('You') . ': ' : '') . $message['content'];
                $list[$k]['last_message'] = $message['content'];
            } else {
                $list[$k]['last_message'] = '';
            }
            
            $list[$k]['chat_room_name'] = $v['sender_uid'] == $uid ? $v['receiver_username'] : $v['sender_username'];
            if ($v['type'] == 'group') {
                $list[$k]['chat_room_name'] = $v['groupname'];
            } else {
                if ($v['sender_uid'] == $uid) {
                    $chat_room_name = $v['receiver_username'];
                    $chat_image = $v['r_headpic'];
                } else {
                    $chat_room_name = $v['sender_username'];
                    $chat_image = $v['s_headpic'];
                }
                $list[$k]['chat_room_name'] = $chat_room_name;
                $list[$k]['chat_image'] = config('image_domain') . $chat_image;
            }
            // $list[$k]['time_text'] = $time_text;
            // $list[$k]['isread'] = isset($message) && $message['uid'] != $uid ? $message['isread'] : 1;
            // $list[$k]['inbox_id'] = $v['id'];
            // $list[$k]['sender_uid'] = encrypt_hashids($v['sender_uid']);
            unset($list[$k]['id']);
            unset($list[$k]['sender_uid']);
            unset($list[$k]['receiver_uid']);
            unset($list[$k]['groupname']);
            unset($list[$k]['s_headpic']);
            unset($list[$k]['r_headpic']);
        }

        return self::output_json(['code' => 0, 'info' => '', 'data' => $list]);
    }

    //Get chat data of spcifice chat room
    public function getchatroomtext()
    {
        if (!request()->isGet()) {
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        $chat_room_id = input('get.chat_room_id/s', '');
        $uid = $this->uid;
        $type = input('get.type/s','one_to_one');
        $limit = input('get.limit', 100);
        $page = input('get.page', 0);
        $offset = $page * $limit;
        
        $table = $type == 'group' ? 'xy_inbox_group' : 'xy_inbox';
        $inboxId = db($table)->where('chat_room_id', $chat_room_id)->value('id');
        if (!$inboxId) {
            return self::output_json(['code' => 1, 'info' => lang('Chat room not found')]);
        }
        $table = $type == 'group' ? 'xy_inbox_message_group' : 'xy_inbox_message';
        
        $messageId = input('page/i', 0);
        $map = [
            'inbox_id' => $inboxId,
            'msg.status' => 1
        ];
        
        $list = db($table)->alias('msg')
        ->join('xy_users u', 'u.id = msg.uid')
        ->where($map)
        ->field('msg.content AS message, msg.id AS message_id, COALESCE(u.weixin, u.username) AS sendBy, uid AS sendBy_uid, msg.addtime AS time, IF(file_url IS NOT NULL AND file_url != "", CONCAT("' . config('chat_file_domain') . '", file_url), "") AS file_url')
        ->order('msg.id desc')
        ->limit($offset, $limit)
        ->select();
        
        if ($list) {
            // krsort($list);
            // $list = array_values($list);
        } else {
            $list = [];
        }
        
        foreach ($list as $k => $v) {
            $list[$k]['profile_image'] = config('image_domain') . db('xy_users')->where('id', $v['sendBy_uid'])->value('headpic');
            $list[$k]['sendBy_uid'] = encrypt_hashids($v['sendBy_uid']);            
        }
        return self::output_json(['code' => 0, 'info' => '', 'data' => $list]);
    }

    //to send messages 
    public function send_message()
    {
        $uid = $this->uid;
        if (request()->post()) {
            $chat_room_id = input('post.chat_room_id/s', '');
            $sendBy = input('post.sendBy');
            $time = input('post.time');
            if (!$time) {
                $time = time();
            } else {
                if (strlen($time > 10)) {
                    $time = $time / 1000;
                }
            }
            $message = input('post.message/s', '');
            $type = input('post.type/s','one_to_one');
            $type = !in_array($type, ['one_to_one', 'group']) ? 'one_to_one' : $type;
            if (!$chat_room_id) {
                return self::output_json(['code' => 1, 'info' => lang('chat_room_id is required.')]);   
            }
            if (!$message) {
                return self::output_json(['code' => 1, 'info' => lang('Please enter message.')]);   
            }
            $sender_uid = $sendBy;
            
            $sender_uid = decrypt_hashids($sender_uid);
            $sender_uid = isset($sender_uid[0]) ? $sender_uid[0] : 0;

            if ($sender_uid != $uid) {
                return self::output_json(['code' => 1, 'info' => lang('Parameter error')]);
            }

            
            $table = $type == 'group' ? '_group' : '';
            $map = [
                'status' => 1,
                'chat_room_id' => $chat_room_id
            ];
            if ($type == 'one_to_one') {
                $inbox = db('xy_inbox' . $table)->where($map)->where("sender_uid = $uid OR receiver_uid = $uid")->find();
            } else {
                $inbox = db('xy_inbox' . $table)->where($map)->where("receiver_uid LIKE '$uid' OR receiver_uid LIKE '$uid" . ",%' OR receiver_uid LIKE '%,$uid,%' OR receiver_uid LIKE '%,$uid'")->find();
            }
            // $time = time();
            $url = '';
            if ($inbox) {
                $upload_file = (isset($_FILES['upload_file']) && $_FILES['upload_file']['name'] != '') ? $_FILES['upload_file'] : null;
                
                if ($upload_file) {
                    $size = $upload_file['size'];
                    $ext = strtolower(pathinfo($upload_file['name'], PATHINFO_EXTENSION));
                    $allowed = ['png', 'jpg', 'jpeg', 'pdf', 'xls', 'xlsx', 'doc', 'docx'];
                    if (!in_array($ext, $allowed)) {
                        $msg = lang('Invalid file format, only these format is allowed: _x_');
                        $msg = str_replace('_x_', implode(",", $allowed), $msg);
                        return self::output_json(['code' => 1, 'info' => $msg]);
                    }
                    $max_size = 10000000;
                    if ($size > $max_size) {
                        return self::output_json(['code' => 1, 'info' => lang('File size cannot exceed ') . ($max_size / 1000000) . 'mb']);
                    }
                    $error = '';
                    $url = self::upload($upload_file['tmp_name'], $ext, $error);
                    if (!$url && $error != '') {
                        return self::output_json(['code' => 1, 'info' => $error]);
                    }
                }
                $data_message = [
                    'inbox_id' => $inbox['id'],
                    'uid' => $uid,
                    'content' => $message,
                    'addtime' => $time,
                    'file_url' => $url
                ];
                $re = db('xy_inbox_message' . $table)->insert($data_message);
                if ($re) {
                    db('xy_inbox' . $table)->where('id', $inbox['id'])->update(['action' => 1, 'updatetime' => $time]);
                    return self::output_json(['code' => 0, 'info' => lang('Sent successfully')]);
                } else {
                    return self::output_json(['code' => 1, 'info' => lang('Failed to send')]);
                }
            } else {

                return self::output_json(['code' => 1, 'info' => lang('Chat room not found')]);
            }
            
        }
        if (!request()->isPost()) {
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
    }

    //图片上传为base64为的图片
    public function upload($tmp_name,$type_img,&$error){
        //上传 的文件目录

        $is_sync = isset($data['is_sync']) ? $data['is_sync'] : false;
        $filename = isset($data['filename']) ? $data['filename'] : null;
        $is_admin = isset($data['is_admin']) ? $data['is_admin'] : false;

        $App = new \think\App();
        $dir = '../../upload'. DIRECTORY_SEPARATOR . 'chat' . DIRECTORY_SEPARATOR . date('Y') . DIRECTORY_SEPARATOR . date('m-d') . DIRECTORY_SEPARATOR;

        $new_files = $App->getRootPath() . $dir;
        if(!file_exists($new_files)) {
            //检查是否有该文件夹，如果没有就创建，并给予最高权限
            //服务器给文件夹权限
            mkdir($new_files, 0777,true);
        }

        $new_files = check_pic($new_files,".{$type_img}");

        $result = move_uploaded_file($tmp_name, $new_files);
        if(!$result){
            $error = lang('Upload failed');
            return false;
        }

        $filenames=str_replace('\\', '/', $new_files);
        $file_name=substr($filenames,strripos($filenames,"/../../upload"));
        return str_replace("../../", "", $file_name);
    }

    public function insertlog() {
        $url = request()->url();
        $post = json_encode($_REQUEST);
        $header = json_encode(request()->header());
        $ip = request()->ip();

        $data = [
            'api' => $url,
            'request' => $post,
            'header' => $header,
            'addtime' => time(),
            'ip' => $ip
        ];
        $id = db ('xy_api_log')->insertGetId($data);

        return $id;
    }

    public function updatelog($response) {
        db('xy_api_log')->where('id', $this->logId)->update([
            'updatetime' => time(),
            'response' => json_encode($response)
        ]);

        return;
    }

    public function output_json($response) {
        self::updatelog($response);
        return json($response);
    }
}