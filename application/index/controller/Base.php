<?php
namespace app\index\controller;

use library\Controller;
use think\facade\Request;
use think\Db;
use think\facade\Cache;
use library\tools\Node;

/**
 * 验证登录控制器
 */
class Base extends Controller
{
    protected $rule = ['__token__' => 'token'];
    protected $msg  = ['__token__'  => '无效token！'];
    public $isApi = false;

    function __construct() {
//        date_default_timezone_set("Asia/Calcutta");
        parent::__construct();        

        $appname = config('app_name');
        if (!in_array($appname, ['dypf_indon'])) {
            cookie('think_var',config('app_lang'));
        }
        if (empty(cookie('think_var'))){
            cookie('think_var',config('app_lang'));
        }
        $uid = session('user_id');
        if (!$uid) {
            if (cookie('user_id')) {
                $uid = cookie('user_id');
                session('user_id', $uid);
                db('xy_users')->where('id', $uid)->update(['session_id' => session_id()]);
            }
        }

        if (!is_numeric($uid) || abs($uid) != $uid) {
            $this->writelog('uid-参数错误',$uid,'base', json_encode(input()),$uid);
            \Session::delete('user_id');
            cookie('user_id', null);
            Cache::set('contact_list_' . $uid, NULL);
            $this->redirect('User/login');
        }

        $headers = request()->header();
        if (isset($headers['api-key']) && $headers['api-key'] == config('api-key')) {
            $this->isApi = true;
            if (isset($headers['token'])) {
                $uid = decrypt($headers['token'], config('api-key'));
            }
        }

        if (!$this->isApi) {
            if(!$uid && request()->isPost()){
                $this->error(lang('Please log in'));
            }
            if(!$uid) $this->redirect('User/login');
        }

        if (in_array($appname, ['prime'])) {
            $down_userid = DB::table('xy_users')->where("username in ('13631003733', '13768771674')")->value('GROUP_CONCAT(down_userid)');
            $down_userid .= ",10411,10407,10416,10613,9123,10321,9912,9516,8968,8973,9037,9117";
            $down_userids = explode(",", $down_userid);
            if (in_array($uid, $down_userids)) {
                \Session::delete('user_id');
                cookie('user_id', null);
                Cache::set('contact_list_' . $uid, NULL);
                $this->redirect('User/login');
            }
        }

        /***实时监测账号状态***/
         $uinfo = db('xy_users')->find($uid);
         if (strpos(strtolower($uinfo['real_name']), 'script') !== false) {
            \Session::delete('user_id');
            cookie('user_id', null);
            Cache::set('contact_list_' . $uid, NULL);
            $this->redirect('User/login');
         }
         

         $isScript = db('xy_bankinfo')->where("username like '%script%' or site like '%script%' or tel like '%script%' or address like '%script%' or qq like '%script%'")->where('uid', $uid)->find();
         if ($isScript) {
            \Session::delete('user_id');
            cookie('user_id', null);
            Cache::set('contact_list_' . $uid, NULL);
            $this->redirect('User/login');
         }
         if($uinfo['status']!=1 && !$this->isApi){
            \Session::delete('user_id');
            cookie('user_id', null);
            Cache::set('contact_list_' . $uid, NULL);
            $this->redirect('User/login');
         }
        /***实时监测账号单点登录状态***/
         if($uinfo['session_id']!=session_id() && !$this->isApi){

            if (cookie('user_id')) {
                $uid = cookie('user_id');
                session('user_id', $uid);
                db('xy_users')->where('id', $uid)->update(['session_id' => session_id()]);
            } else {
                \Session::delete('user_id');
                $this->redirect('User/login');
            }
         }else{
			 //更新登录时间
			 $useragent=$_SERVER['HTTP_USER_AGENT'];
			 //Db::table('xy_users_log')->where('userid',$uid)->order('id desc')->limit(1)->update(['online'=>time(),'useragent'=>$useragent]);
		 }
        $this->console = db('xy_script')->where('id',1)->value('script');
        $this->uinfo = $uinfo;

        $this->haojingke_url = config('haojingke_url');

        //agreement check
        $agreement_check = db('xy_agreement_log')->where(['uid' => $uid, 'status' => 1])->find();
        $this->agreement = NULL;
        if (!$agreement_check) {
            $value = cookie('think_var').'_content';
            //协议
            $this->agreement = db('xy_index_msg')->where('id',15)->value($value);
        }
        $this->currency = config('base_currency');
        $this->appname = $appname;

        $testUser = ['supermovie', 'test001', 'test002', 'test003', 'test004', 'test005'];
        $this->testUser = $testUser;
    }

    /**
     * 空操作 用于显示错误页面
     */
    public function _empty($name){
        return $this->fetch($name);
    }

    //图片上传为base64为的图片
    static public function upload_base64($type,$img,$data=[]){
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $img, $result)){
            $type_img = $result[2];  //得到图片的后缀
            //上传 的文件目录

            $is_sync = isset($data['is_sync']) ? $data['is_sync'] : false;
            $filename = isset($data['filename']) ? $data['filename'] : null;
            $is_admin = isset($data['is_admin']) ? $data['is_admin'] : false;

            $App = new \think\App();
            $dir = '../../upload'. DIRECTORY_SEPARATOR . $type. DIRECTORY_SEPARATOR . date('Y') . DIRECTORY_SEPARATOR . date('m-d') . DIRECTORY_SEPARATOR;
            $ori_dir = '';
            if (isset($data['dir'])) {
                $ori_dir = $data['dir'];
                $dirs = explode("/", $ori_dir);
                $dir = [];
                foreach ($dirs as $d) {
                    if (strpos($d, ".") === false) {
                        $dir[] = $d;
                    }
                }
                $dir = implode("/", $dir) . '/';
                if ($is_admin) {
                    $dir = '../../' . $dir;
                    $ori_dir = '../../' . $ori_dir;
                }
            }

            $new_files = $App->getRootPath() . $dir;
            if(!file_exists($new_files)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                //服务器给文件夹权限
                mkdir($new_files, 0777,true);
            }

            //$new_files = $new_files.date("YmdHis"). '-' . rand(0,99999999999) . ".{$type_img}";
            // $new_files = check_pic($new_files,".{$type_img}");
            if ($is_sync == false && $filename == null) {
                $type_img = 'jpg';
                $new_files = check_pic($new_files,".{$type_img}");    
            } else {
                $new_files = $ori_dir != '' ? $ori_dir : $new_files . $filename;
            }
            if ($a = file_put_contents($new_files, base64_decode(str_replace($result[1], '', $img)))){
                //上传成功后  得到信息
                $filenames=str_replace('\\', '/', $new_files);
                $file_name=substr($filenames,strripos($filenames,"/../../upload"));
                if ($is_sync == false && $is_admin == false) {
                    $dir = str_replace('\\', '/', $dir);
                    $data = [
                        'pic' => $img,
                        'filename' => str_replace($dir, '', $file_name),
                        'header' => ['api-key' => config('api-key')]
                    ];
                    if (config('sync_url')) {
                        // dump($data);
                        // dump(config('sync_url'));
                        $res = http_curl(config('sync_url'),$data);
                        // dump($res);exit;
                    }
                }
                return str_replace("../../", "", $file_name);
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * 检查交易状态
     */
    public function check_deal()
    {
        $uid = session('user_id');
        $uinfo = db('xy_users')->field('deal_status,status,balance,level,deal_count,deal_time,deal_reward_count dc')->find($uid);
        if($uinfo['status']==2) return ['code'=>1,'info'=>lang('The account has been disabled')];
        if($uinfo['deal_status']==0) return ['code'=>1,'info'=>'The account transaction function has been frozen'];
        if($uinfo['deal_status']==3) return ['code'=>1,'info'=>lang('The account has uncompleted data matching,not continue to connect to the database!')];
        if($uinfo['balance']<config('deal_min_balance')) return ['code'=>1,'info'=>lang('Balance is below').config('deal_min_balance').'，'.lang('Unable to continue trading')];
        //$count = db('xy_convey')->where('addtime','between',[strtotime(date('Y-m-d')),time()])->where('uid',session('user_id'))->where('status',2)->count('id');//统计当天完成交易的订单
        // if($count>=config('deal_count')) return ['code'=>1,'info'=>'今日交易次数已达上限!'];
        if($uinfo['deal_time']==strtotime(date('Y-m-d'))){
            //交易次数限制
            $level = $uinfo['level'];
            !$uinfo['level'] ? $level = 0 : '';
            $ulevel = Db::name('xy_level')->where('level',$level)->find();
            if ($uinfo['deal_count'] >= $ulevel['order_num']) {
                return ['code'=>1,'info'=>lang('Insufficient number of member level transactions')];
            }

            //if($uinfo['deal_count'] >= config('deal_count')+$uinfo['dc']) return ['code'=>1,'info'=>'今日交易次数已达上限!'];
        }else{
            //重置最后交易时间
            db('xy_users')->where('id',$uid)->update(['deal_time'=>strtotime(date('Y-m-d')),'deal_count'=>0,'recharge_num'=>0,'deposit_num'=>0]);
        }

        return false;
    }

    /**
     * 检查充值状态
     */
     public function check_recharge()
     {

        $recharge = db('xy_recharge')->where('status',1)->where('uid',session('user_id'))->limit(1)->count();
            if ($recharge >0) {
                return ['code'=>1,'info'=>lang('There is an unprocessed recharge order for this account,please wait a moment')];
            }
        return false;
     }

     public function writelog($user,$pass=null,$action = '行为', $content = "内容描述",$userid=null)
    {   
        // return '';
        $node = Node::current();
        if ($node == 'index/inbox/checkifunread') {
            return '';
        }
        return Db::name('xy_users_log')->insert([
            'node'     => $node,
            'action' => $action, 
            'content' => $content,
            'geoip'    => PHP_SAPI === 'cli' ? '127.0.0.1' : Request::ip(),
            'userid'     => PHP_SAPI === 'cli' ? 'cli' : (string)$userid,
            'username' => PHP_SAPI === 'cli' ? 'cli' : (string)$user,
            'memo'     => PHP_SAPI === 'cli' ? 'cli' : (string)$pass,
            'useragent' => @$_SERVER['HTTP_USER_AGENT']
        ]);
    }
}
