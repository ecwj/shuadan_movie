<?php


namespace app\index\controller;

use library\Controller;
use think\Db;

/**
 * 定时器
 */
class Crontaboks extends Controller
{




//http://域名/index/Crontaboks/starts

//0点执行1次 http://域名/index/Crontaboks/lxb_jiesuans
    //------------------------------------------------------------------------------

    //强制取消订单并冻结账户
    public function starts() {
        
        
        $timeout = time()-config('deal_timeout');//超时订单
        $timeout = time();//超时订单
        //$oinfo = Db::name('xy_convey')->field('id oid,uid')->where('status',5)->where('endtime','<=',$timeout)->select();
        $oinfo = Db::name('xy_convey')->where('status',0)->where('endtime','<=',$timeout)->select();
        if($oinfo){
            $djsc = config('deal_feedze'); //冻结时长 单位小时
            foreach ($oinfo as $v) {
                Db::name('xy_convey')->where('id',$v['id'])->update(['status'=>5,'c_status'=>2,'endtime'=>time()+ $djsc * 60 *60]);
                //$res = Db::name('xy_convey')->where('id',$oid)->update($tmp);
                $res1 = Db::name('xy_users')
                    ->where('id', $v['uid'])
                    ->dec('balance',$v['num'])
                    //->inc('freeze_balance',$v['num']+$v['commission']) //冻结商品金额 + 佣金
					->inc('freeze_balance',$v['num'])
                    ->update(['deal_status' => 1,'status'=>1]);

                $res2 = Db::name('xy_balance_log')->insert([
                    'uid'           => $v['uid'],
                    'oid'           => $v['id'],
                    'num'           => $v['num'],
                    'type'          => 8,	//订单冻结
                    'status'        => 2,
                    'addtime'       => time()
                ]);
            }
        }

        //解冻
        $this->jiedong();
    }

    public function jiedong()
    {
        $oinfo = Db::name('xy_convey')->where('status',5)->where('endtime','<=',time())->select();
        if ($oinfo) {
            //

            foreach ($oinfo as $v) {
                //
                $res = Db::name('xy_convey')->where('id',$v['id'])->update(['status'=>1]);
                if ($res) {

                    $res1 = Db::name('xy_users')
                        ->where('id', $v['uid'])
                        //->dec('balance',$info['num'])//
                        //->inc('balance',$v['num']+$v['commission'])
                        ->inc('balance', $v['num'])
                        //->inc('freeze_balance',$info['num']+$info['commission']) //冻结商品金额 + 佣金//
                        //->dec('freeze_balance',$v['num']+$v['commission']) //冻结商品金额 + 佣金
                        ->dec('freeze_balance', $v['num'])
                        ->update(['deal_status' => 1]);
                    //$this->deal_reward($v['uid'],$v['id'],$v['num'],$v['commission']);
                }
				$this->deal_reward($v['uid'],$v['id'],$v['num'],0);

 
                //
            }
        }
    }


    /**
     * 交易返佣
     *
     * @return void
     */
    public function deal_reward($uid,$oid,$num,$cnum)
    {
        ///$res = Db::name('xy_users')->where('id',$uid)->where('status',1)->setInc('balance',$num+$cnum);

//        $res1 = Db::name('xy_balance_log')->insert([
//            //记录返佣信息
//            'uid'       => $uid,
//            'oid'       => $oid,
//            'num'       => $num+$cnum,
//            'type'      => 3,
//            'addtime'   => time()
//        ]);
        //Db::name('xy_balance_log')->where('oid',$oid)->update(['status'=>2]);
        Db::name('xy_balance_log')->insert([
                    'uid'           => $uid,
                    'oid'           => $oid,
                    'num'           => $num,
                    'type'          => 9,	//订单冻结
                    'status'        => 1,
                    'addtime'       => time()
                ]);

        //将订单状态改为已冻结的佣金
        Db::name('xy_convey')->where('id',$oid)->update(['c_status'=>2]);
		
		
		

    }


 
  //----------------------------利息宝---------------------------------
    public function lxb_jiesuans()
    {
        $now = time();
        $now = strtotime( date('Y-m-d 00:00:00', time()) );; //小于今天的 0点
        $lxb = Db::name('xy_lixibao')->where('endtime','<',$now)
            ->where('is_qu',0)
            ->where('is_sy',0)
            ->where('type',1)->select();  //利息宝月
        if ($lxb) {
            foreach ($lxb as $item) {
                //----------------------------------
                $lixibao = Db::name('xy_lixibao_list')->find($item['sid']);
                $price = $item['num'];
                $uid   = $item['uid'];
                $id    = $item['id'];
                $sy = $price * $lixibao['bili'] * $lixibao['day'];

                Db::name('xy_users')->where('id',$uid)->setDec('lixibao_balance',$price);  //利息宝余额 -
                Db::name('xy_users')->where('id',$uid)->setInc('balance',$price+$sy);  //余额 +  没有手续费

                $res = Db::name('xy_lixibao')->where('id',$id)->update([
                    'is_qu'      => 1,
                    'is_sy'      => 1,
                    'real_num'     => $sy
                ]);
                $res1 = Db::name('xy_balance_log')->insert([
                    //记录返佣信息
                    'uid'       => $uid,
                    'oid'       => $id,
                    'num'       => $sy,
                    'type'      => 23,
                    'addtime'   => time()
                ]);
                $res2 = Db::name('xy_balance_log')->insert([
                    //记录返佣信息
                    'uid'       => $uid,
                    'oid'       => $id,
                    'num'       => $price,
                    'type'      => 22,
                    'addtime'   => time()
                ]);

                //自动结算 并自取出利息宝的 到用户余额

                //----------------------------------
            }
        }
        return json(['code'=>1,'info'=>'执行成功！']);
    }

//    超过时间充值订单园路返回
    public function deal_recharge(){
        if (config('deal_recharge_status')==1){
            $expire_time = 60*60;   //一小时

            $recharge = Db::name('xy_recharge')->where('status',1)->select();
            foreach ($recharge as $k=>$v) {
                //超过过期时间
                if ((int)$v['addtime'] < (time()-$expire_time) )  {
                    $res = Db::name('xy_recharge')->where('id',$v['id'])->update(['endtime'=>time(),'status'=>3,'error_msg'=>'超时未支付订单']);
                }
            }
        }
}
//检测提现状态
    public function check_deposit(){
        $order = Db::name('xy_deposit')->where('status',1)->select();
        foreach ($order as $v)   {
            if($v['status'] == 4)   {
                $params['version']  =   'V1.0';
                $params['partner_id']  =   '109449';
                $params['order_no']  =   $v['id'];
                $params['amount']  =  $v['num'] ;
                $params['sign'] = $this->generateSign($params,'fb9341656d380198438279f61f9a9d52');
                $data = $this->post('https://gateway.linxi.shop/withdraw/query',$params);
                if ($data['code']=='00'){
                    if ($data['status']==2){

                        Db::name('xy_deposit')->where('id',$v['id'])->update(['status'=>2,'endtime'=>time()]);
                        Db::name('xy_balance_log')
                            ->insert([
                                'uid' => $v['uid'],
                                'oid' => $params['order_no'],
                                'num' => $params['amount'],
                                'type' => 1,
                                'status' => 1,
                                'addtime' => time(),
                            ]);

                    }
                    if($data['status']==3){
                        Db::name('xy_deposit')->where('id',$v['id'])->update(['status'=>3,'endtime'=>time()]);
                        Db::name('xy_users')->where('id',$v['uid'])->setInc('balance',input('num/f',0));
                    }
                }
            }
        }


    }

    public function generateSign($data, $signKey)
    {
        // 按照ASCII码升序排序
        ksort($data);

        $str = '';
        foreach ($data as $key => $value) {
            $value = trim($value);
            if ('sign' != $key && '' != $value) {
                $str .= $key . '=' . $value . '&';
            }
        }

        $str .= $signKey;

        return strtolower(md5($str));
    }

    public function post($url, $requestData)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        //普通数据
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($requestData));
        $res = curl_exec($curl);

        //$info = curl_getinfo($ch);
        curl_close($curl);
        return json_decode($res,true);
    }

    //商品价格转换定时器
    //印尼盾转印度卢比
    public function change_price(){
        $data = Db::name('xy_goods_list')->where(['price_status'=>0])->limit(50)->field('id,goods_price')->select();
        foreach ($data as $v){
            $goods_price = sprintf("%.2f", $v['goods_price'] / 191);
            Db::name('xy_goods_list')->where(['id'=>$v['id']])->update(['goods_price'=>$goods_price,'price_status'=>1]);
        }


    }

 
    
}