<?php

namespace app\index\controller;

use think\Controller;
use think\Request;
use think\Db;
use think\facade\Cache;

class Ctrl extends Base
{
    //钱包页面
    public function wallet()
    {
        $balance = db('xy_users')->where('id', session('user_id'))->value('balance');
        $this->assign('balance', $balance);
        $balanceT = db('xy_convey')->where('uid', session('user_id'))->where('status', 1)->sum('commission');
        $this->assign('balance_shouru', $balanceT);

        //收益
        $startDay = strtotime(date('Y-m-d 00:00:00', time()));
        $shouyi = db('xy_convey')->where('uid', session('user_id'))->where('addtime', '>', $startDay)->where('status', 1)->select();

        //充值
        $chongzhi = db('xy_recharge')->where('uid', session('user_id'))->where('addtime', '>', $startDay)->where('status', 2)->select();

        //提现
        $tixian = db('xy_deposit')->where('uid', session('user_id'))->where('addtime', '>', $startDay)->where('status', 1)->select();

        $this->assign('shouyi', $shouyi);
        $this->assign('chongzhi', $chongzhi);
        $this->assign('tixian', $tixian);
        return $this->fetch();
    }


    public function recharge_before()
    {
        $pay = db('xy_pay')->where('status', 1)->select();

        $this->assign('pay', $pay);
        return $this->fetch();
    }


    public function vip()
    {
        $pay = db('xy_pay')->where('status', 1)->select();
        $this->member_level = db('xy_level')->order('level asc')->select();;
        $this->info = db('xy_users')->where('id', session('user_id'))->find();
        $this->member = $this->info;

        //var_dump($this->info['level']);die;

        $level_name = $this->member_level[0]['name'];
        $order_num = $this->member_level[0]['order_num'];
        if (!empty($this->info['level'])) {
            $level_name = db('xy_level')->where('level', $this->info['level'])->value('name');;
        }
        if (!empty($this->info['level'])) {
            $order_num = db('xy_level')->where('level', $this->info['level'])->value('order_num');;
        }

        $this->level_name = $level_name;
        $this->order_num = $order_num;
        $this->list = $pay;
        return $this->fetch();
    }

    /**
     * @地址      recharge_dovip
     * @说明      利息宝
     * @参数       @参数 @参数
     * @返回      \think\response\Json
     */
    public function lixibao()
    {
        $this->assign('title', '利息宝');
        $uinfo = db('xy_users')->field('username,tel,level,id,headpic,balance,freeze_balance,lixibao_balance,lixibao_dj_balance')->find(session('user_id'));
        $startDay = strtotime(date('Y-m-d 00:00:00', time()));
        $this->assign('ubalance', $uinfo['balance']);
        $this->assign('balance', $uinfo['lixibao_balance']);
        $this->assign('balance_total', $uinfo['lixibao_balance'] + $uinfo['lixibao_dj_balance']);
        $balanceT = db('xy_lixibao')->where('uid', session('user_id'))->where('status', 1)->where('type', 3)->sum('num');

        $balanceT = db('xy_balance_log')->where('uid', session('user_id'))->where('status', 1)->where('type', 23)->sum('num');

        $yes1 = strtotime(date("Y-m-d 00:00:00", strtotime("-1 day")));
        $yes2 = strtotime(date("Y-m-d 23:59:59", strtotime("-1 day")));
//        $this->yes_shouyi = db('xy_balance_log')->where('uid',session('user_id'))->where('status',1)->where('type',23)->where('addtime','between',[$yes1,$yes2])->sum('num');

        $list = db('xy_lixibao')->where('uid', session('user_id'))->where('endtime', '>', $startDay)->where('addtime', '<', $yes1)->select();
        $yes_shouyi = 0;
        foreach ($list as $v) {
            $lixibao = Db::name('xy_lixibao_list')->find($v['sid']);
            $yes_shouyi = $yes_shouyi + $lixibao['bili'] * $v['num'];
        }
        $this->yes_shouyi = $yes_shouyi;
        $this->assign('balance_shouru', $balanceT);


        //收益

        $shouyi = db('xy_lixibao')->where('uid', session('user_id'))->select();

        foreach ($shouyi as &$item) {
            $type = '';
            if ($item['type'] == 1) {
                $type = '<font color="green">转入利息宝</font>';
            } elseif ($item['type'] == 2) {
                $n = $item['status'] ? lang('has arrived') : lang('Not arrived');
                $type = '<font color="red" >利息宝转出(' . $n . ')</font>';
            } elseif ($item['type'] == 3) {
                $type = '<font color="orange" >每日收益</font>';
            } else {

            }

            $lixbao = Db::name('xy_lixibao_list')->find($item['sid']);

            $name = $lixbao['name'] . '(' . $lixbao['day'] . '天)' . $lixbao['bili'] * 100 . '% ';

            $item['num'] = number_format($item['num'], 2);
            $item['name'] = $type . '　　' . $name;
            $item['shouxu'] = $lixbao['shouxu'] * 100 . '%';
            $item['addtime'] = date('Y/m/d H:i', $item['addtime']);

            if ($item['is_sy'] == 1) {
                $notice = '正常收益,实际收益' . $item['real_num'];
            } else if ($item['is_sy'] == -1) {
                $notice = '未到期提前提取,未收益,手续费为:' . $item['shouxu'];
            } else {
                $notice = '理财中...';
            }
            $item['notice'] = $notice;
        }

        $this->rililv = config('lxb_bili') * 100 . '%';
        $this->shouyi = $shouyi;
        if (request()->isPost()) {
            return json(['code' => 0, 'info' => '操作', 'data' => $shouyi]);
        }

        $lixibao = Db::name('xy_lixibao_list')->field('id,name,bili,day,min_num')->order('day asc')->select();
        $this->lixibao = $lixibao;
        return $this->fetch();
    }

    public function lixibao_ru()
    {
        $uid = session('user_id');
        $uinfo = Db::name('xy_users')->field('recharge_num,deal_time,balance,level')->find($uid);//获取用户今日已充值金额

        if (request()->isPost()) {
            $price = input('post.price/d', 0);
            $id = input('post.lcid/d', 0);
            $yuji = 0;
            if ($id) {
                $lixibao = Db::name('xy_lixibao_list')->find($id);
                if ($price < $lixibao['min_num']) {
                    return json(['code' => 1, 'info' => lang('The minimum investment amount for this product') . $lixibao['min_num']]);
                }
                if ($price > $lixibao['max_num']) {
                    return json(['code' => 1, 'info' => lang('The maximum investment amount of the product') . $lixibao['max_num']]);
                }
                $yuji = $price * $lixibao['bili'] * $lixibao['day'];
            } else {
                return json(['code' => 1, 'info' => lang('Data abnormal')]);
            }


            if ($price <= 0) {
                return json(['code' => 1, 'info' => 'you are sb']); //直接充值漏洞
            }
            if ($uinfo['balance'] < $price) {
                return json(['code' => 1, 'info' => lang('Balance valid is not enough')]);
            }
            Db::name('xy_users')->where('id', $uid)->setInc('lixibao_balance', $price);  //利息宝月 +
            Db::name('xy_users')->where('id', $uid)->setDec('balance', $price);  //余额 -

            $endtime = time() + $lixibao['day'] * 24 * 60 * 60;

            $res = Db::name('xy_lixibao')->insert([
                'uid' => $uid,
                'num' => $price,
                'addtime' => time(),
                'endtime' => $endtime,
                'sid' => $id,
                'yuji_num' => $yuji,
                'type' => 1,
                'status' => 0,
            ]);
            $oid = Db::name('xy_lixibao')->getLastInsID();
            $res1 = Db::name('xy_balance_log')->insert([
                //记录返佣信息
                'uid' => $uid,
                'oid' => $oid,
                'num' => $price,
                'type' => 21,
                'status' => 2,
                'addtime' => time()
            ]);
            if ($res) {
                return json(['code' => 0, 'info' => lang('Operation successful')]);
            } else {
                return json(['code' => 1, 'info' => lang('Operation failed') . '!' . lang('Please chech the account balance')]);
            }
        }

        $this->rililv = config('lxb_bili') * 100 . '%';
        $this->yue = $uinfo['balance'];
        $isajax = input('get.isajax/d', 0);

        if ($isajax) {
            $lixibao = Db::name('xy_lixibao_list')->field('id,name,bili,day,min_num')->select();
            $data2 = [];
            $str = $lixibao[0]['name'] . '(' . $lixibao[0]['day'] . '天)' . $lixibao[0]['bili'] * 100 . '% (' . $lixibao[0]['min_num'] . '起投)';
            foreach ($lixibao as $item) {
                $data2[] = array(
                    'id' => $item['id'],
                    'value' => $item['name'] . '(' . $item['day'] . '天)' . $item['bili'] * 100 . '% (' . $item['min_num'] . '起投)',
                );
            }
            return json(['code' => 0, 'info' => lang('Operation'), 'data' => $data2, 'data0' => $str]);
        }

        $this->libi = 1;

        $this->assign('title', lang('Interest treasure balance transfer in'));
        return $this->fetch();
    }


    public function deposityj()
    {
        $num = input('post.price/f', 0);
        $id = input('post.lcid/d', 0);
        if ($id) {
            $lixibao = Db::name('xy_lixibao_list')->find($id);

            $res = $num * $lixibao['day'] * $lixibao['bili'];
            return json(['code' => 0, 'info' => lang('Operation'), 'data' => $res]);
        }
    }

    public function lixibao_chu()
    {
        $uid = session('user_id');
        $uinfo = Db::name('xy_users')->field('recharge_num,deal_time,balance,level,lixibao_balance')->find($uid);//获取用户今日已充值金额

        if (request()->isPost()) {
            $id = input('post.id/d', 0);
            $lixibao = Db::name('xy_lixibao')->find($id);
            if (!$lixibao) {
                return json(['code' => 1, 'info' => lang('Data abnormal')]);
            }
            if ($lixibao['is_qu']) {
                return json(['code' => 1, 'info' => lang('Repeat operation')]);
            }
            $price = $lixibao['num'];

            if ($uinfo['lixibao_balance'] < $price) {
                // return json(['code' => 1, 'info' => lang('Lixibao balance is not enough')]);
            }
            //利息宝参数
            $lxbParam = Db::name('xy_lixibao_list')->find($lixibao['sid']);

            //
            $issy = 0;
            $time = time();
            if ($time > $lixibao['endtime']) {
                //到期
                $issy = 1;
            } else {
                $issy = -1;
            }

            Db::name('xy_users')->where('id', $uid)->setDec('lixibao_balance', $price);  //余额 -

            $oldprice = $price;
            $shouxu = $lxbParam['shouxu'];
            if ($shouxu) {
                if ($issy == -1) {
                    $price = $price - $price * $shouxu;
                }
            }
            // dump($oldprice . ' | ' . $price);exit;

            $date1 = date_create(date('Y-m-d H:i:s', $lixibao['addtime']));
            $date2 = date_create(date('Y-m-d H:i:s', $time));    

            $diff = date_diff($date1,$date2);
            $sign = $diff->format("%R");
            $dayDiff = $diff->format("%a");
            $bonus = 0;
            if ($sign == "+" && $dayDiff > 0) {
                $bili = $lxbParam['bili'];
                if ($dayDiff > $lxbParam['day']) {
                    $bonus = $lixibao['yuji_num'];
                } else {
                    $bonus = $bili * $oldprice * $dayDiff;
                }
                Db::name('xy_balance_log')->insert([
                    //记录返佣信息
                    'uid' => $uid,
                    'oid' => $id,
                    'num' => $bonus,
                    'type' => 23,
                    'addtime' => time()
                ]);
            }

            $res = Db::name('xy_lixibao')->where('id', $id)->update([
                'endtime' => $time,
                'is_qu' => 1,
                'is_sy' => $issy,
                'shouxu' => $oldprice * $shouxu,
                'real_num' => $bonus
            ]);


            Db::name('xy_users')->where('id', $uid)->setInc('balance', ($price + $bonus));  //余额 +
            $res1 = Db::name('xy_balance_log')->insert([
                //记录返佣信息
                'uid' => $uid,
                'oid' => $id,
                'num' => $price,
                'type' => 22,
                'addtime' => time()
            ]);

            //利息宝记录转出


            if ($res) {
                return json(['code' => 0, 'info' => lang('Operation successful')]);
            } else {
                return json(['code' => 1, 'info' => lang('Operation failed') . '!' . lang('Please chech the account balance')]);
            }

        }

        $this->assign('title', lang('Profit balance transfer out'));
        $this->rililv = config('lxb_bili') * 100 . '%';
        $this->yue = $uinfo['lixibao_balance'];

        $log = $this->_query('xy_lixibao')->join('xy_lixibao_list', 'xy_lixibao.sid = xy_lixibao_list.id')->field('xy_lixibao.*, xy_lixibao_list.shouxu')->where('uid', session('user_id'))->order('addtime desc')->page();


        return $this->fetch();
    }


    //升级vip
    public function recharge_dovip()
    {
        if (request()->isPost()) {
            $level = input('post.level/d', 1);
            $type = input('post.type/s', '');

            $uid = session('user_id');
            $uinfo = db('xy_users')->field('pwd,salt,tel,username,balance')->find($uid);
            if (!$level) return json(['code' => 1, 'info' => lang('Parameter wrong')]);

            //
            $pay = db('xy_pay')->where('id', $type)->find();
            $num = db('xy_level')->where('level', $level)->value('num');;

            if ($num > $uinfo['balance']) {
                return json(['code' => 1, 'info' => lang('Balance valid is not enough')]);
            }


            $id = getSn('SY');
            $res = db('xy_recharge')
                ->insert([
                    'id' => $id,
                    'uid' => $uid,
                    'tel' => $uinfo['tel'],
                    'real_name' => $uinfo['username'],
                    'pic' => '',
                    'num' => $num,
                    'addtime' => time(),
                    'pay_name' => $type,
                    'is_vip' => 1,
                    'level' => $level
                ]);
            if ($res) {
                if ($type == 999) {
                    $res1 = Db::name('xy_users')->where('id', $uid)->update(['level' => $level]);
                    $res1 = Db::name('xy_users')->where('id', $uid)->setDec('balance', $num);
                    $res = Db::name('xy_recharge')->where('id', $id)->update(['endtime' => time(), 'status' => 2]);


                    $res2 = Db::name('xy_balance_log')
                        ->insert([
                            'uid' => $uid,
                            'oid' => $id,
                            'num' => $num,
                            'type' => 1,
                            'status' => 1,
                            'addtime' => time(),
                        ]);
                    return json(['code' => 0, 'info' => lang('Upgrade successfully')]);
                }


                $pay['id'] = $id;
                $pay['num'] = $num;
                if ($pay['name2'] == 'bipay') {
                    $pay['redirect'] = url('/index/Api/bipay') . '?oid=' . $id;
                }
                if ($pay['name2'] == 'paysapi') {
                    $pay['redirect'] = url('/index/Api/pay') . '?oid=' . $id;
                }

                if ($pay['name2'] == 'card') {
                    $pay['master_cardnum'] = config('master_cardnum');
                    $pay['master_name'] = config('master_name');
                    $pay['master_bank'] = config('master_bank');
                }

                return json(['code' => 0, 'info' => $pay]);
            } else
                return json(['code' => 1, 'info' => lang('Submission failed') . '，' . lang('Please try later')]);
        }
        return json(['code' => 0, 'info' => lang('Request successfully'), 'data' => []]);
    }


    public function recharge()
    {
        $appname = config('app_name');
        $uid = session('user_id');
        $uinfo = db('xy_users')->where('id', $uid)->field('kyc_status, addtime')->find();
        if ($uinfo['kyc_status'] != 2 && in_array($appname, ['apple_movie'])  && $uinfo['addtime'] >= strtotime('2021-12-09 18:00:00')) {
            // if (in_array($uinfo['username'], ['supermovie', 'test001', 'test002', 'test003', 'test004', 'test005'])) {
                // $this->redirecT('user/kyc');
            // }
         }
         
        $this->amount = \think\facade\Request::get('amount', '');
        $tel = Db::name('xy_users')->where('id', $uid)->value('tel');//获取用户今日已充值金额
        $this->tel = substr_replace($tel, '****', 3, 4);
        $this->pay = db('xy_pay')->where('status', 1)->where('is_delete',0)->where('name2','支付平台')->order('sort_id asc')->select();
        if (!$this->pay) {
            return $this->redirect('/index/Ctrl/recharge4');   
        } else {
            if (config('app.toppay.status') != 1 && config('app.ftpay.status') != 1) {
                return $this->redirect('/index/Ctrl/recharge4');
            }
        }
        $this->info = db('xy_index_msg')->where('id', 14)->value(cookie('think_var') . '_content');

        $this->bank = db('xy_bankinfo')->where(['uid' => session('user_id')])->find();

        return $this->fetch();
    }

    public function recharge_select()
    {
        $appname = config('app_name');
        $uid = session('user_id');
        $uinfo = db('xy_users')->where('id', $uid)->field('kyc_status, addtime')->find();
        if ($uinfo['kyc_status'] != 2 && in_array($appname, ['apple_movie'])  && $uinfo['addtime'] >= strtotime('2021-12-09 18:00:00')) {
            // if (in_array($uinfo['username'], ['supermovie', 'test001', 'test002', 'test003', 'test004', 'test005'])) {
                // $this->redirecT('user/kyc');
            // }
         }

        $this->pay = db('xy_pay')->where('status', 1)->where('is_delete',0)->where('name2','支付平台')->order('sort_id asc')->select();
        if (!$this->pay) {
            return $this->redirect('/index/Ctrl/recharge4');   
        }
        $this->bank = db('xy_bankinfo')->where(['uid' => session('user_id')])->find();
        return $this->fetch();
    }

    public function recharge_do_before()
    {
//        $tmp = $this->check_recharge();
//        if($tmp) return json($tmp);
        $num = input('post.price/f', 0);
        $type = input('post.type/s', 'card');

//        if($type=='new_pay_upi'){
//            if (session('user_id') != 34){
//                return json(['code'=>1,'info'=>'Channel maintenance']);
//            }
//        }

        $uid = session('user_id');
        $uinfo = db('xy_users')->where('id', $uid)->field('deal_status')->find();
        if ($uinfo['deal_status'] == 0) {
            // return json(['code' => 1, 'info' => lang('System is under maintenance.')]);   
        }

        if (!$num) return json(['code' => 1, 'info' => lang('Parameter wrong')]);

        //时间限制 //TODO
        if (in_array($this->appname, ['dypf_indon'])) {
            date_default_timezone_set("Asia/Jakarta");
        }
        $res = check_time(config('chongzhi_time_1'), config('chongzhi_time_2'));
        $str = config('chongzhi_time_1') . ":00  - " . config('chongzhi_time_2') . ":00";

        date_default_timezone_set("Asia/Kuala_Lumpur");

        // if ($res) return json(['code' => 1, 'info' => lang('Prohibited in') . $str . lang('executes the current operation outside of a time period')]);


        //
        $pay = db('xy_pay')->where('name2', $type)->where('status', 1)->find();
        if ($num < $pay['min']) return json(['code' => 1, 'info' => lang('Recharge cannot below') . $pay['min']]);
        if ($num > $pay['max']) return json(['code' => 1, 'info' => lang('Recharge cannot over') . $pay['max']]);

        $info = [];
        $info['num'] = $num;
        return json(['code' => 0, 'info' => $info]);
    }


    public function recharge2()
    {
        // return $this->redirect('/index/Ctrl/recharge4');
        $oid = input('get.oid/s', '');
        $num = input('get.num/s', '');
        $type = input('get.type/s', '');
        $this->pay = db('xy_pay')->where('status', 1)->where('name2', $type)->find();
        if (request()->isPost()) {
            $id = input('post.id/s', '');
            $pic = input('post.pic/s', '');

            if (is_image_base64($pic)) {
                $pic = '/' . $this->upload_base64('xy', $pic);  //调用图片上传的方法
            } else {
                return json(['code' => 1, 'info' => lang('2')]);
            }
//            $pic =ltrim(parse_url($pic,PHP_URL_PATH),'');
            $res = db('xy_recharge')->where('id', $id)->update(['pic' => $pic]);
            if (!$res) {
                return json(['code' => 1, 'info' => lang('Submission failed')]);
            } else {
                return json(['code' => 0, 'info' => lang('Request successfully'), 'data' => []]);
            }
        }

        if (config('chongzhi_status') == 1) {
            $num = $num . '.' . rand(10, 99); //随机金额
        }
        $info = [];//db('xy_recharge')->find($oid);
        $info['num'] = $num;//db('xy_recharge')->find($oid);
        $info['master_bank'] = config('master_bank');//银行名称
        $info['master_name'] = config('master_name');//收款人
        $info['master_cardnum'] = config('master_cardnum');//银行卡号
        $info['master_bk_address'] = config('master_bk_address');//银行地址
        $this->info = $info;

        return $this->fetch();
    }

    //三方支付
    public function recharge3()
    {
        return $this->redirect('/index/Ctrl/recharge4');
        $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : 'wx';
        $pay = db('xy_pay')->where('status', 1)->select();
        $this->assign('title', $type == 'wx' ? lang('Wechat pay') : lang('Alipay payment'));
        $this->assign('pay', $pay);
        $this->assign('type', $type);
        return $this->fetch();
    }

    public function recharge4()
    {
        $appname = config('app_name');
        $uid = session('user_id');
        $uinfo = db('xy_users')->where('id', $uid)->field('kyc_status, addtime')->find();
        if ($uinfo['kyc_status'] != 2 && in_array($appname, ['apple_movie'])  && $uinfo['addtime'] >= strtotime('2021-12-09 18:00:00')) {
            // if (in_array($uinfo['username'], ['supermovie', 'test001', 'test002', 'test003', 'test004', 'test005'])) {
                // $this->redirecT('user/kyc');
            // }
         }
        // $pay_ = db('xy_pay')->where('status', 1)->where('is_delete',0)->where('name2','支付平台')->order('sort_id asc')->select();
        // if ($pay_) {
            // return $this->redirect('/index/Ctrl/recharge');
        // }
        $oid = input('get.oid/s', '');
        $num = input('get.num/s', '');
        $type = 'card';//input('get.type/s', '');
        $map = [
            'name2' => $type,
            'is_reach_limit' => 0,
            'status' => 1,
            'is_delete' => 0
        ];
        $this->pay = db('xy_pay')->where($map)->orderRaw('rand()')->find();
        if (request()->isPost()) {
            $id = input('post.id/s', '');
            $pic = input('post.pic/s', '');

            if (is_image_base64($pic)) {
                $pic = '/' . $this->upload_base64('xy', $pic);  //调用图片上传的方法
            } else {
                return json(['code' => 1, 'info' => lang('2')]);
            }
//            $pic =ltrim(parse_url($pic,PHP_URL_PATH),'');
            $res = db('xy_recharge')->where('id', $id)->update(['pic' => $pic]);
            if (!$res) {
                return json(['code' => 1, 'info' => lang('Submission failed')]);
            } else {
                return json(['code' => 0, 'info' => lang('Request successfully'), 'data' => []]);
            }
        }

        if (config('chongzhi_status') == 1) {
            $num = $num . '.' . rand(10, 99); //随机金额
        }

        $info = [];//db('xy_recharge')->find($oid);
        $info['num'] = $num;//db('xy_recharge')->find($oid);
        $info['pay_id'] = 0;
        $info['master_bank'] = $info['master_name'] = $info['master_cardnum'] = '';
        if (isset($this->pay['name'])) {
            $info['master_bank'] = $this->pay['name'];//银行名称
            $info['master_name'] = $this->pay['bank_account_holder'];//收款人
            $info['master_cardnum'] = $this->pay['bank_account_number'];//银行卡号
            $info['pay_id'] = $this->pay['id'];
        }
        $info['reference_number'] = random_alphanumeric();
        // $info['master_bk_address'] = config('master_bk_address');//银行地址
        $this->info = $info;
        $uid = session('user_id');
        $user = Db::name('xy_users')->where('id', $uid)->find();
        $this->user = $user;

        return $this->fetch();
    }

    //钱包页面
    public function bank()
    {
        $balance = db('xy_users')->where('id', session('user_id'))->value('balance');
        $this->assign('balance', $balance);
        $balanceT = db('xy_convey')->where('uid', session('user_id'))->where('status', 2)->sum('commission');
        $this->assign('balance_shouru', $balanceT);
        return $this->fetch();
    }

    //获取提现订单接口
    public function get_deposit()
    {
        $info = db('xy_deposit')->where('uid', session('user_id'))->select();
        if ($info) return json(['code' => 0, 'info' => lang('Request successfully'), 'data' => $info]);
        return json(['code' => 1, 'info' => lang('No data yet')]);
    }

    public function my_data()
    {
        $uinfo = db('xy_users')->where('id', session('user_id'))->find();
        if ($uinfo['tel']) {
            $uinfo['tel'] = substr_replace($uinfo['tel'], '****', 3, 4);
        }
        $bank = db('xy_bankinfo')->where(['uid' => session('user_id')])->find();
        $uinfo['cardnum'] = substr_replace($bank['cardnum'], '****', 7, 7);
        if (request()->isPost()) {
            $username = input('post.username/s', '');
            //$pic = input('post.qq/s', '');

            $res = db('xy_users')->where('id', session('user_id'))->update(['username' => $username]);
            if (!$res) {
                return json(['code' => 1, 'info' => lang('Submission failed')]);
            } else {
                return json(['code' => 0, 'info' => lang('Request successfully'), 'data' => []]);
            }
        }

        $this->assign('info', $uinfo);

        return $this->fetch();
    }


    public function recharge_do()
    {
        $tmp = $this->check_recharge();
        session('post', input());
        if ($tmp) {
            session('error', $tmp['info']);
            // dump(session('post'));exit;
            $this->redirect('/index/ctrl/recharge4');
            // return json($tmp);
        }
        if (request()->isPost()) {
            $num = input('post.price/f', 0);
            $type = input('post.type/s', 'card');
            $pic = input('post.pic/s', '');
            $pay_id = input('post.pay_id/i', null);
            $reference_number = input('post.reference_number/s', null);
            $real_name = input('post.real_name/s', '');

            $uid = session('user_id');

            if (!filter_input( INPUT_POST, 'pay_id', FILTER_VALIDATE_INT)) {
                $this->writelog('pay_id-参数错误',$pay_id,'ctrl', json_encode(input()),$uid);
                session('error', lang('参数错误'));
                $this->redirect('/index/ctrl/recharge4');
                return json(['code' => 1, 'info' => lang('参数错误')]);
            }

            $uinfo = db('xy_users')->field('pwd,salt,tel,username,real_name,deal_status')->find($uid);

            if ($uinfo['deal_status'] == 0) {
                // return json(['code' => 1, 'info' => lang('System is under maintenance.')]);   
            }

            $operation_start = date('Y-m-d') . ' ' . config('recharge_operation_start');
            $operation_end = date('Y-m-d') . ' ' . config('recharge_operation_end');
            if (in_array($this->appname, ['apple_movie'])) {
                if (date('Y-m-d H:i:s') <= '2021-12-10 23:59:59') {
                    $operation_end = '2021-12-10 22:00:00';
                }
            }

            $now = date('Y-m-d H:i:s');
            if (!in_array($uinfo['username'], ['supermovie'])) {
                if ($now < $operation_start || $now > $operation_end) {
                    session('error', lang('recharge_operation_hour'));
                    $this->redirect('/index/ctrl/recharge4');
                    return json(['code' => 1, 'info' => lang('recharge_operation_hour')]);
                }
            }

            if (!$real_name) {
                session('error', lang('Please enter payer\'s name'));
                $this->redirect('/index/ctrl/recharge4');
                return json(['code' => 1, 'info' => lang('Please enter payer\'s name')]);
            }            
            if (!$num) return json(['code' => 1, 'info' => lang('Parameter wrong')]);
            // dump($_FILES['voucher']);
            if (!isset($_FILES['voucher']) || !isset($_FILES['voucher']['tmp_name']) || $_FILES['voucher']['tmp_name'] == '') {
                session('error', lang('Picture format error'));
                $this->redirect('/index/ctrl/recharge4');
            }
            $pic = 'data:' . $_FILES['voucher']['type'] . ';base64,' . base64_encode(file_get_contents($_FILES['voucher']['tmp_name']));
            // dump($pic);exit;
            if (is_image_base64($pic)){
                if (!in_array($this->appname, ['prime'])) {
                    $pic = '/' . $this->upload_base64('xy', $pic);  //调用图片上传的方法
                } else {
                    $dir = '../../upload'. DIRECTORY_SEPARATOR . 'xy' . DIRECTORY_SEPARATOR . date('Y') . DIRECTORY_SEPARATOR . date('m-d') . DIRECTORY_SEPARATOR;
                    $App = new \think\App();
                    $new_files = $App->getRootPath() . $dir;
                    $type_img = 'jpg';
                    $new_files = check_pic($new_files,".{$type_img}");
                    if(!file_exists($new_files)) {
                        //检查是否有该文件夹，如果没有就创建，并给予最高权限
                        //服务器给文件夹权限
                        // mkdir($new_files, 0777,true);
                    }
                    $filenames=str_replace('\\', '/', $new_files);
                    $file_name=substr($filenames,strripos($filenames,"/../../upload"));
                    $dir = str_replace('\\', '/', $dir);
                    $data = [
                        'pic' => $pic,
                        'filename' => str_replace($dir, '', $file_name),
                        'header' => ['api-key' => config('api-key')]
                    ];
                    $pic = http_curl(config('sync_url'),$data);
                }
            } else {
                session('error', lang('Picture format error'));
                $this->redirect('/index/ctrl/recharge4');
                return json(['code' => 1, 'info' => lang('Picture format error')]);
            }
            //         dump($pic);
            // return json(['code' => 1, 'info' => lang('pause')]);
            //
            // $pay = db('xy_pay')->where('name2', $type)->find();
            $pay = db('xy_pay')->where('id', $pay_id)->find();
            if ($num < $pay['min']) {
                session('error', lang('Recharge cannot below') . $pay['min']);
                $this->redirect('/index/ctrl/recharge4');
                return json(['code' => 1, 'info' => lang('Recharge cannot below') . $pay['min']]);
            }
            if ($num > $pay['max']) {
                session('error', lang('Recharge cannot over') . $pay['max']);
                $this->redirect('/index/ctrl/recharge4');
                return json(['code' => 1, 'info' => lang('Recharge cannot over') . $pay['max']]);
            }

            $id = getSn('SY');
            $res = db('xy_recharge')
                ->insert([
                    'id' => $id,
                    'uid' => $uid,
                    'tel' => $uinfo['tel'],
                    'real_name' => $real_name,
                    'pic' => $pic,
                    'num' => $num,
                    'addtime' => time(),
                    'pay_name' => $pay['name'] . '|' . $pay['bank_account_holder'] . '|' . $pay['bank_account_number'],
                    'reference_number' => $reference_number
                ]);
            if ($res) {
                $pay['id'] = $id;
                $pay['num'] = $num;
                if ($pay['name2'] == 'bipay') {
                    $pay['redirect'] = url('/index/Api/bipay') . '?oid=' . $id;
                }
                if ($pay['name2'] == 'paysapi') {
                    $pay['redirect'] = url('/index/Api/pay') . '?oid=' . $id;
                }
                if ($pay['received_amount'] + $num >= $pay['limit'] && $pay['limit'] > 0) {
                    $is_reach_limit = 1;
                } else {
                    $is_reach_limit = 0;
                }
                db("xy_pay")->where('id', $pay_id)->update(['received_amount' => $pay['received_amount'] + $num, 'is_reach_limit' => $is_reach_limit]);
                session('success', lang('Request successfully'));
                session('error', null);
                session('post', null);
                $this->redirect('/index/ctrl/recharge4');
                return json(['code' => 0, 'info' => $pay]);
            } else
                return json(['code' => 1, 'info' => lang('Submission failed')]);
        }
        session('success', lang('Request successfully'));
        session('error', null);
        session('post', null);
        $this->redirect('/index/ctrl/recharge4');
        return json(['code' => 0, 'info' => lang('Request successfully'), 'data' => []]);
    }

    function deposit_wx()
    {

        $user = db('xy_users')->where('id', session('user_id'))->find();
        $this->assign('title', lang('Wechat withdraw'));

        $this->assign('type', 'wx');
        $this->assign('user', $user);
        return $this->fetch();
    }

    function deposit()
    {
        $uid = session('user_id');
        $user = db('xy_users')->where('id', $uid)->find();
        $user['tel'] = substr_replace($user['tel'], '****', 3, 4);
        $bank = db('xy_bankinfo')->where(['uid' => $uid])->find();

        if ($bank) {
            $bank['cardnum'] = substr_replace($bank['cardnum'], '****', 7, 7);
        } else {
            $bank['cardnum'] = "****";
        }
        $this->assign('info', $bank);
        $this->assign('user', $user);

        //提现限制
        $level = $user['level'];
        !$user['level'] ? $level = 0 : '';
        $ulevel = Db::name('xy_level')->where('level', $level)->find();

        if (in_array($this->appname, ['prime'])) {
            //每月提现次数>10,fee = 2%
            $tixianCi = db('xy_deposit')->where(['uid' => $uid, 'status' => 2])->where('addtime', 'between', [strtotime(date('Y-m-01 00:00:00')), time()])->count();
            if ($tixianCi >= 10) {
                $ulevel['tixian_shouxu'] = 0.02;
            }
        }
        
        $this->shouxu = $ulevel['tixian_shouxu'];;;

        $this->withdrawable = $user['balance'] - $user['freeze_balance'];
        if (in_array($this->appname, ['apple_movie'])) {
            $map = [
                'uid' => $uid,
                'type' => 20,//体验金
            ];
            $freeCredit = db('xy_balance_log')->where($map)->value('SUM(IF(status = 1, num, -num)) AS num');
            $map = [
                'uid' => $uid,
                'status' => 1
            ];
            $usedFreeCreditSelect = db('xy_convey_movie')->where($map)->where("remark LIKE '%free_credit%'")->column('remark');
            $usedFreeCredit = 0;
            foreach ($usedFreeCreditSelect as $v) {
                $arr = explode("|", $v);
                if (isset($arr[1]) && $arr[1] > 0) {
                    $usedFreeCredit += (float) $arr[1];
                }
            }
            $balanceFreeCredit = $freeCredit - $usedFreeCredit;
            if ($balanceFreeCredit > 0) {
                $withdrawable = $this->withdrawable - $balanceFreeCredit; 
                if ($withdrawable < 0) {
                    $withdrawable = 0;
                }
                $this->withdrawable = $withdrawable;
            }
        }

        $view = "ctrl/deposit";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }

    function deposit_zfb()
    {

        $user = db('xy_users')->where('id', session('user_id'))->find();
        $this->assign('title', lang('Alipay cash withdrawal'));

        $this->assign('type', 'zfb');
        $this->assign('user', $user);
        return $this->fetch('deposit_zfb');
    }


    //提现接口
    public function do_deposit()
    {
//        if (session('user_id')!=23){
//            json(['code'=>1,'info'=>'提现正在 维护，稍后再试']);exit;
//        }

        //时间限制 //TODO
        if (in_array($this->appname, ['dypf_indon'])) {
            date_default_timezone_set("Asia/Jakarta");
        }
        if (in_array($this->appname, ['dypf'])) {
            // return json(['code' => 1, 'info' => lang('System is under maintenance.')]);
        }
        $res = check_time(config('tixian_time_1'), config('tixian_time_2'));
        $str = config('tixian_time_1') . ":00  - " . config('tixian_time_2') . ":00";
        if (date('Y-m-d') <= '2021-12-29 02:00:00') {
            $res = false;
        }
        if ($res) return json(['code' => 1, 'info' => lang('Prohibited in') . $str . lang('executes the current operation outside of a time period')]);

        date_default_timezone_set("Asia/Kuala_Lumpur");
        $bankinfo = Db::name('xy_bankinfo')->where('uid', session('user_id'))->where('status', 1)->find();
        //var_dump($bankinfo);die;
        $type = config('deposit_type');
//        $type = input('post.type/s','');

        if ($type == 'wx' || $type == 'zfb') {

        } else {
            if (!$bankinfo) return json(['code' => 1, 'info' => lang('Bank card information is not be added')]);
        }


        if (request()->isPost()) {

            $httpOrigin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
            $httpOrigin = strtolower(str_replace(['http://', 'https://'], '', $httpOrigin));
            if (!isset($_SERVER['HTTP_ORIGIN']) || strpos(strtolower($_SERVER['HTTP_ORIGIN']), 'nolan') === false) {
                $error = 'Unauthenticated.';
                return json(['code' => 1, 'info' => $error]);
            }

            $uid = session('user_id');

            //提现密码
            $pwd2 = input('post.paypassword/s', '');
            $info = db('xy_users')->field('pwd2,salt2,tel,email,deal_status')->find(session('user_id'));
            if ($info['deal_status'] == 0) {
                return json(['code' => 1, 'info' => lang('System is under maintenance.')]);
            }
            if ($info['pwd2'] == '') return json(['code' => 1, 'info' => lang('No withdrawal password is set')]);
            if ($info['pwd2'] != sha1($pwd2 . $info['salt2'] . config('pwd_str'))) return json(['code' => 1, 'info' => lang('Password incorrect')]);


            $num = input('post.num/d', 0);
            $bkid = input('post.bk_id/d', $bankinfo['id']);
//            $type = input('post.type/s','');
            $token = input('post.token', '');
            $data = ['__token__' => $token];
            $validate = \Validate::make($this->rule, $this->msg);
            if (!$validate->check($data)) return json(['code' => 1, 'info' => $validate->getError()]);

            if ($num <= 0) return json(['code' => 1, 'info' => lang('Parameter wrong')]);

            $uinfo = Db::name('xy_users')->field('recharge_num,deal_time,balance,level,total_deposit')->find($uid);//获取用户今日已充值金额

            if (in_array($this->appname, ['dypf_indon', 'prime', 'nolan_movie', 'apple_movie'])) {
                $verify = input('post.verify/d',0);
                if(config('app.verify')){
                    if (in_array($this->appname, ['prime', 'nolan_movie', 'apple_movie'])) {
                        $key = 'tel';
                    } else {
                        $key = 'email';
                    }
                    $email = $info[$key];
                    $verify_msg = Db::table('xy_verify_msg')->field('msg,addtime')->where([$key=>$email,'type'=>4])->find();
                    if(!$verify_msg)return json(['code'=>1,'info'=>lang('Verification code not exist')]);
                    if($verify != $verify_msg['msg'])return json(['code'=>1,'info'=>lang('Verification code is not valid')]);
                    if(($verify_msg['addtime'] + (5*60)) < time())return json(['code'=>1,'info'=>lang('Verification code is expired')]);
                }
            }

            //提现限制
            $level = $uinfo['level'];
            !$uinfo['level'] ? $level = 0 : '';
            $ulevel = Db::name('xy_level')->where('level', $level)->find();

            $config_min_deposit = config('min_deposit');
            /*if (in_array($this->appname, ['apple_movie'])) {
                if ($uinfo['total_deposit'] == 0) {
                    $ulevel['tixian_min'] = 10;
                    $config_min_deposit = 10;
                }
            }*/

            if ($num < $ulevel['tixian_min']) {
                // return ['code' => 1, 'info' => lang('The membership level withdrawal limit is') . $ulevel['tixian_min'] . '-' . $ulevel['tixian_max'] . '!'];
                return ['code' => 1, 'info' => lang('Minimum withdrawal is ') . config('base_currency') . $ulevel['tixian_min']];
            }
            if ($num > $ulevel['tixian_max']) {
                return ['code' => 1, 'info' => lang('Maximum withdrawal is ') . config('base_currency') . $ulevel['tixian_max'] . '!'];
            }

            $lastrecharge = db('xy_recharge')->where("status = 2 AND uid = $uid AND pay_name != 'manual'")->order('addtime DESC')->find();
            $lastrechargetime = $lastrecharge ? $lastrecharge['addtime'] : '';
            $onum = db('xy_convey_movie')->where('uid', $uid)->where("addtime >= '$lastrechargetime'")->count('id');
            // $onum = db('xy_convey')->where('uid', $uid)->where('addtime', 'between', [strtotime(date('Y-m-d')), time()])->count('id');
            // if ($onum < $ulevel['tixian_nim_order']) {
            if ($onum < 1) {
                // return ['code' => 1, 'info' => lang('Current grade withdrawal has been completed') . 1 . lang('Order')];
                return ['code' => 1, 'info' => '您必须完成至少一笔订单'];
            }


            // if ($num < config('min_deposit')) return json(['code' => 1, 'info' => lang('The minimum withdrawal limit is') . config('min_deposit')]);
            //整百提现
//			if ($num%100!=0) return json(['code'=>1,'info'=>lang('Please enter a multiple of 100')]);

            $checkBalance = $uinfo['balance'];
            if (in_array($this->appname, ['apple_movie'])) {
                $uid = session('user_id');
                $map = [
                    'uid' => $uid,
                    'type' => 20,//体验金
                ];
                $freeCredit = db('xy_balance_log')->where($map)->value('SUM(IF(status = 1, num, -num)) AS num');
                $map = [
                    'uid' => $uid,
                    'status' => 1
                ];
                $usedFreeCreditSelect = db('xy_convey_movie')->where($map)->where("remark LIKE '%free_credit%'")->column('remark');
                $usedFreeCredit = 0;
                foreach ($usedFreeCreditSelect as $v) {
                    $arr = explode("|", $v);
                    if (isset($arr[1]) && $arr[1] > 0) {
                        $usedFreeCredit += (float) $arr[1];
                    }
                }
                $balanceFreeCredit = $freeCredit - $usedFreeCredit;
                if ($balanceFreeCredit > 0) {
                    $withdrawable = $checkBalance - $balanceFreeCredit; 
                    if ($withdrawable < 0) {
                        $withdrawable = 0;
                    }
                    $checkBalance = $withdrawable;
                }
            }

            if ($num > $checkBalance) return json(['code' => 1, 'info' => lang('Balance is not enough') . $num . ' - ' . $checkBalance]);

            $tixianCiPending = db('xy_deposit')->where(['uid' => $uid])->where('status IN (1, 4)')->where('addtime', 'between', [strtotime(date('Y-m-d 00:00:00')), time()])->count();
            if ($tixianCiPending >= 1) {
                return ['code' => 1, 'info' => lang('Submission failed, you have a withdrawal pending review')];
            }

            // if ($uinfo['deal_time'] == strtotime(date('Y-m-d'))) {
                //if($num > 20000-$uinfo['recharge_num']) return ['code'=>1,'info'=>'今日剩余提现额度为'.( 20000 - $uinfo['recharge_num'])];
                //提现次数限制
                $tixianCi = db('xy_deposit')->where(['uid' => $uid, 'status' => 2])->where('addtime', 'between', [strtotime(date('Y-m-d 00:00:00')), time()])->count();
                if ($tixianCi + 1 > $ulevel['tixian_ci']) {
                    return ['code' => 1, 'info' => lang('I\'m very sorry, you have successfully withdrawn once today, please come back tomorrow. Thank you.')];
                }
            // } else {
                //重置最后交易时间
                // Db::name('xy_users')->where('id', $uid)->update(['deal_time' => strtotime(date('Y-m-d')), 'deal_count' => 0, 'recharge_num' => 0, 'deposit_num' => 0]);
            // }

            if (in_array($this->appname, ['prime'])) {
                //每月提现次数>10,fee = 2%
                $tixianCi = db('xy_deposit')->where(['uid' => $uid, 'status' => 2])->where('addtime', 'between', [strtotime(date('Y-m-01 00:00:00')), time()])->count();
                if ($tixianCi >= 10) {
                    $ulevel['tixian_shouxu'] = 0.02;
                }
            }
            $id = getSn('CO');

            try {
                Db::startTrans();
                $res = Db::name('xy_deposit')->insert([
                    'id' => $id,
                    'uid' => $uid,
                    'bk_id' => $bkid,
                    'num' => $num,
                    'addtime' => time(),
                    'type' => $type,
                    'shouxu' => $ulevel['tixian_shouxu'],
                    'real_num' => $num - ($num * $ulevel['tixian_shouxu'])
                ]);

                //提现日志
                $res2 = Db::name('xy_balance_log')
                    ->insert([
                        'uid' => $uid,
                        'oid' => $id,
                        'num' => $num,
                        'type' => 7, //TODO 7提现
                        'status' => 2,
                        'addtime' => time(),
                    ]);


                $res1 = Db::name('xy_users')->where('id', session('user_id'))->setDec('balance', $num);
                if ($res && $res1) {
                    Db::commit();
                    return json(['code' => 0, 'info' => lang('Operation successful')]);
                } else {
                    Db::rollback();
                    return json(['code' => 1, 'info' => lang('Operation failed')]);
                }
            } catch (\Exception $e) {
                Db::rollback();
                return json(['code' => 1, 'info' => lang('Operation failed') . '!' . lang('Please chech the account balance')]);
            }
        }
        return json(['code' => 0, 'info' => lang('Request successfully'), 'data' => $bankinfo]);
    }


    public function generateSign($data, $signKey)
    {
        // 按照ASCII码升序排序
        ksort($data);

        $str = '';
        foreach ($data as $key => $value) {
            $value = trim($value);
            if ('sign' != $key && '' != $value) {
                $str .= $key . '=' . $value . '&';
            }
        }

        $str .= $signKey;

        return strtolower(md5($str));
    }

    public function post($url, $requestData)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        //普通数据
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($requestData));
        $res = curl_exec($curl);

        //$info = curl_getinfo($ch);
        curl_close($curl);
        return json_decode($res, true);
    }

    //////get请求获取参数，post请求写入数据，post请求传人bkid则更新数据//////////
    public function do_bankinfo()
    {
        if (request()->isPost()) {
            $token = input('post.token', '');
            $data = ['__token__' => $token];
            $validate = \Validate::make($this->rule, $this->msg);
            if (!$validate->check($data)) return json(['code' => 1, 'info' => $validate->getError()]);

            $username = input('post.username/s', '');
            $bankname = input('post.bankname/s', '');
            $cardnum = input('post.cardnum/s', '');
            $site = input('post.site/s', '');
            $tel = input('post.tel/s', '');
            $status = input('post.default/d', 0);
            $bkid = input('post.bkid/d', 0); //是否为更新数据

            if ($bkid != '' && !filter_input( INPUT_POST, 'bkid', FILTER_VALIDATE_INT)) {
                $this->writelog('bkid-参数错误',$bkid,'ctrl', json_encode(input()),$uid);
                return json(['code' => 1, 'info' => lang('参数错误')]);
            }

            if (!filter_input( INPUT_POST, 'cardnum', FILTER_VALIDATE_INT)) {
                $this->writelog('cardnum-参数错误',$cardnum,'ctrl', json_encode(input()),$uid);
                return json(['code' => 1, 'info' => lang('参数错误')]);
            }

            if (!$username) return json(['code' => 1, 'info' => lang('Account name holder is required')]);
            if (mb_strlen($username) > 30) return json(['code' => 1, 'info' => lang('The maximum length of the account holder name is 30 characters')]);
            if (!$bankname) return json(['code' => 1, 'info' => lang('Bank name is required')]);
            if (!$cardnum) return json(['code' => 1, 'info' => lang('Bank card number is required')]);
            if (!$tel) return json(['code' => 1, 'info' => lang('Mobile phone number is required')]);

            if ($bkid)
                $cardn = Db::table('xy_bankinfo')->where('id', '<>', $bkid)->where('cardnum', $cardnum)->count();
            else
                $cardn = Db::table('xy_bankinfo')->where('cardnum', $cardnum)->count();

            if ($cardn) return json(['code' => 1, 'info' => lang('Bank card is existed')]);

            $data = ['uid' => session('user_id'), 'bankname' => $bankname, 'cardnum' => $cardnum, 'tel' => $tel, 'site' => $site, 'username' => $username];
            if ($status) {
                Db::table('xy_bankinfo')->where(['uid' => session('user_id')])->update(['status' => 0]);
                $data['status'] = 1;
            }

            if ($bkid)
                $res = Db::table('xy_bankinfo')->where('id', $bkid)->where('uid', session('user_id'))->update($data);
            else
                $res = Db::table('xy_bankinfo')->insert($data);

            if ($res !== false)
                return json(['code' => 0, 'info' => lang('Operation successful')]);
            else
                return json(['code' => 1, 'info' => lang('Operation failed')]);
        }
        $bkid = input('id/d', 0); //是否为更新数据
        $where = ['uid' => session('user_id')];
        if ($bkid !== 0) $where['id'] = $bkid;
        $info = db('xy_bankinfo')->where($where)->select();
        if (!$info) return json(['code' => 1, 'info' => lang('No data yet')]);
        return json(['code' => 0, 'info' => lang('Request successfully'), 'data' => $info]);
    }

    //切换银行卡状态
    public function edit_bankinfo_status()
    {
        $id = input('post.id/d', 0);

        Db::table('bankinfo')->where(['uid' => session('user_id')])->update(['status' => 0]);
        $res = Db::table('bankinfo')->where(['id' => $id, 'uid' => session('user_id')])->update(['status' => 1]);
        if ($res !== false)
            return json(['code' => 0, 'info' => lang('Operation successful')]);
        else
            return json(['code' => 1, 'info' => lang('Operation failed')]);
    }

    //获取下级会员
    public function bot_user()
    {
        if (request()->isPost()) {
            $uid = input('post.id/d', 0);
            $token = ['__token__' => input('post.token', '')];
            $validate = \Validate::make($this->rule, $this->msg);
            if (!$validate->check($token)) return json(['code' => 1, 'info' => $validate->getError()]);
        } else {
            $uid = session('user_id');
        }
        $page = input('page/d', 1);
        $num = input('num/d', 10);
        $limit = ((($page - 1) * $num) . ',' . $num);
        $data = db('xy_users')->where('parent_id', $uid)->field('id,username,headpic,addtime,childs,tel')->limit($limit)->order('addtime desc')->select();
        if (!$data) return json(['code' => 1, 'info' => lang('No data yet')]);
        return json(['code' => 0, 'info' => lang('Request successfully'), 'data' => $data]);
    }

    //修改密码
    public function set_pwd()
    {
        if (!request()->isPost()) return json(['code' => 1, 'info' => lang('Request error')]);
        $o_pwd = input('old_pwd/s', '');
        $pwd = input('new_pwd/s', '');
        $type = input('type/d', 1);
        $uinfo = db('xy_users')->field('pwd,salt,tel')->find(session('user_id'));
        if ($uinfo['pwd'] != sha1($o_pwd . $uinfo['salt'] . config('pwd_str'))) return json(['code' => 1, 'info' => lang('Password incorrect')]);
        $res = model('admin/Users')->reset_pwd($uinfo['tel'], $pwd, $type);
        return json($res);
    }

    public function set()
    {
        $uid = session('user_id');
        $this->info = db('xy_users')->find($uid);
        $view = "ctrl/set";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }


    //我的下级
    public function get_user()
    {

        $uid = session('user_id');

        $type = input('post.type/d', 1);

        $page = input('page/d', 1);
        $num = input('num/d', 10);
        $limit = ((($page - 1) * $num) . ',' . $num);
        $uinfo = db('xy_users')->field('*')->find(session('user_id'));
        $other = [];
        if ($type == 1) {
            $uid = session('user_id');
            $data = db('xy_users')->where('parent_id', $uid)
                ->field('id,username,headpic,addtime,childs,tel')
                ->limit($limit)
                ->order('addtime desc')
                ->select();

            //总的收入  总的充值
            //$ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            //$cond=implode(',',$ids1);
            //$cond = !empty($cond) ? $cond = " uid in ($cond)":' uid=-1';
            $other = [];
            //$other['chongzhi'] = db('xy_recharge')->where($cond)->where('status', 2)->sum('num');
            //$other['tixian'] = db('xy_deposit')->where($cond)->where('status', 2)->sum('num');
            //$other['xiaji'] = count($ids1);

            //$uids = model('admin/Users')->child_user($uid,5); //五级
            $uids = model('admin/Users')->child_user($uid, 3); //三级
            $uids ? $where[] = ['uid', 'in', $uids] : $where[] = ['uid', 'in', [-1]];
            $uids ? $where2[] = ['uid', 'in', $uids] : $where2[] = ['uid', 'in', [-1]];

            $other['chongzhi'] = db('xy_recharge')->where($where2)->where('status', 2)->sum('num');
            $other['tixian'] = db('xy_deposit')->where($where2)->where('status', 2)->sum('num');
            $other['xiaji'] = count($uids);


            //var_dump($uinfo);die;

            $iskou = 0;
            foreach ($data as &$datum) {
                $datum['addtime'] = date('Y/m/d H:i', $datum['addtime']);
                empty($datum['headpic']) ? $datum['headpic'] = '/public/img/head.png' : '';
                //充值
                $datum['chongzhi'] = db('xy_recharge')->where('uid', $datum['id'])->where('status', 2)->sum('num');
                //提现
                $datum['tixian'] = db('xy_deposit')->where('uid', $datum['id'])->where('status', 2)->sum('num');

                if ($uinfo['kouchu_balance_uid'] == $datum['id']) {
                    $datum['chongzhi'] -= $uinfo['kouchu_balance'];
                    $iskou = 1;
                }

                if ($uinfo['show_tel2']) {
                    $datum['tel'] = substr_replace($datum['tel'], '****', 3, 4);
                }
                if (!$uinfo['show_tel']) {
                    $datum['tel'] = lang('No permission');
                }
                if (!$uinfo['show_num']) {
                    $datum['childs'] = lang('No permission');
                }
                if (!$uinfo['show_cz']) {
                    $datum['chongzhi'] = lang('No permission');
                }
                if (!$uinfo['show_tx']) {
                    $datum['tixian'] = lang('No permission');
                }
            }

            $other['chongzhi'] -= $uinfo['kouchu_balance'];
            return json(['code' => 0, 'info' => lang('Request successfully'), 'data' => $data, 'other' => $other]);

        } else if ($type == 2) {
            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $cond = implode(',', $ids1);
            $cond = !empty($cond) ? $cond = " parent_id in ($cond)" : ' parent_id=-1';

            //获取二代ids
            $ids2 = db('xy_users')->where($cond)->field('id')->column('id');
            $cond2 = implode(',', $ids2);
            $cond2 = !empty($cond2) ? $cond2 = " uid in ($cond2)" : ' uid=-1';
            $other = [];
            $other['chongzhi'] = db('xy_recharge')->where($cond2)->where('status', 2)->sum('num');
            $other['tixian'] = db('xy_deposit')->where($cond2)->where('status', 2)->sum('num');
            $other['xiaji'] = count($ids2);


            $data = db('xy_users')->where($cond)
                ->field('id,username,headpic,addtime,childs,tel')
                ->limit($limit)
                ->order('addtime desc')
                ->select();

            //总的收入  总的充值

            foreach ($data as &$datum) {
                empty($datum['headpic']) ? $datum['headpic'] = '/public/img/head.png' : '';
                $datum['addtime'] = date('Y/m/d H:i', $datum['addtime']);
                //充值
                $datum['chongzhi'] = db('xy_recharge')->where('uid', $datum['id'])->where('status', 2)->sum('num');
                //提现
                $datum['tixian'] = db('xy_deposit')->where('uid', $datum['id'])->where('status', 2)->sum('num');

                if ($uinfo['show_tel2']) {
                    $datum['tel'] = substr_replace($datum['tel'], '****', 3, 4);
                }
                if (!$uinfo['show_tel']) {
                    $datum['tel'] = lang('No permission');
                }
                if (!$uinfo['show_num']) {
                    $datum['childs'] = lang('No permission');
                }
                if (!$uinfo['show_cz']) {
                    $datum['chongzhi'] = lang('No permission');
                }
                if (!$uinfo['show_tx']) {
                    $datum['tixian'] = lang('No permission');
                }
            }

            return json(['code' => 0, 'info' => lang('Request successfully'), 'data' => $data, 'other' => $other]);


        } else if ($type == 3) {
            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $cond = implode(',', $ids1);
            $cond = !empty($cond) ? $cond = " parent_id in ($cond)" : ' parent_id=-1';
            $ids2 = db('xy_users')->where($cond)->field('id')->column('id');

            $cond2 = implode(',', $ids2);
            $cond2 = !empty($cond2) ? $cond2 = " parent_id in ($cond2)" : ' parent_id=-1';

            //获取三代的ids
            $ids22 = db('xy_users')->where($cond2)->field('id')->column('id');
            $cond22 = implode(',', $ids22);
            $cond22 = !empty($cond22) ? $cond22 = " uid in ($cond22)" : ' uid=-1';
            $other = [];
            $other['chongzhi'] = db('xy_recharge')->where($cond22)->where('status', 2)->sum('num');
            $other['tixian'] = db('xy_deposit')->where($cond22)->where('status', 2)->sum('num');
            $other['xiaji'] = count($ids22);

            //获取四代ids
            $cond4 = implode(',', $ids22);
            $cond4 = !empty($cond4) ? $cond4 = " parent_id in ($cond4)" : ' parent_id=-1';
            $ids4 = db('xy_users')->where($cond4)->field('id')->column('id'); //四代ids

            //充值
            $cond44 = implode(',', $ids4);
            $cond44 = !empty($cond44) ? $cond44 = " uid in ($cond44)" : ' uid=-1';
            $other['chongzhi4'] = db('xy_recharge')->where($cond44)->where('status', 2)->sum('num');
            $other['tixian4'] = db('xy_deposit')->where($cond44)->where('status', 2)->sum('num');
            $other['xiaji4'] = count($ids4);


            //获取五代
            $cond5 = implode(',', $ids4);
            $cond5 = !empty($cond5) ? $cond5 = " parent_id in ($cond5)" : ' parent_id=-1';
            $ids5 = db('xy_users')->where($cond5)->field('id')->column('id'); //五代ids

            //充值
            $cond55 = implode(',', $ids5);
            $cond55 = !empty($cond55) ? $cond55 = " uid in ($cond55)" : ' uid=-1';
            $other['chongzhi5'] = db('xy_recharge')->where($cond55)->where('status', 2)->sum('num');
            $other['tixian5'] = db('xy_deposit')->where($cond55)->where('status', 2)->sum('num');
            $other['xiaji5'] = count($ids5);

            $other['chongzhi_all'] = $other['chongzhi'] + $other['chongzhi4'] + $other['chongzhi5'];
            $other['tixian_all'] = $other['tixian'] + $other['tixian4'] + $other['tixian5'];

            $data = db('xy_users')->where($cond2)
                ->field('id,username,headpic,addtime,childs,tel')
                ->limit($limit)
                ->order('addtime desc')
                ->select();

            //总的收入  总的充值

            foreach ($data as &$datum) {
                $datum['addtime'] = date('Y/m/d H:i', $datum['addtime']);
                empty($datum['headpic']) ? $datum['headpic'] = '/public/img/head.png' : '';
                //充值
                $datum['chongzhi'] = db('xy_recharge')->where('uid', $datum['id'])->where('status', 2)->sum('num');
                //提现
                $datum['tixian'] = db('xy_deposit')->where('uid', $datum['id'])->where('status', 2)->sum('num');

                if ($uinfo['show_tel2']) {
                    $datum['tel'] = substr_replace($datum['tel'], '****', 3, 4);
                }
                if (!$uinfo['show_tel']) {
                    $datum['tel'] = lang('No permission');
                }
                if (!$uinfo['show_num']) {
                    $datum['childs'] = lang('No permission');
                }
                if (!$uinfo['show_cz']) {
                    $datum['chongzhi'] = lang('No permission');
                }
                if (!$uinfo['show_tx']) {
                    $datum['tixian'] = lang('No permission');
                }
            }
            return json(['code' => 0, 'info' => lang('Request successfully'), 'data' => $data, 'other' => $other]);
        }


        return json(['code' => 0, 'info' => lang('Request successfully'), 'data' => $data]);
    }

    /**
     * 充值记录
     */
    public function recharge_admin()
    {
        $id = session('user_id');

        if (in_array(strtolower(session('user_name')), ['lsk001', 'wmy001', 'lsk888', 'zzz001', 'lwn'])) {
            $list = [
                [
                    'num' => 1000,
                    'id' => 'SY1909164534276',
                    'status' => 2,
                ],
                [
                    'num' => 1200,
                    'id' => 'SY19091666734269',
                    'status' => 2,
                ],
                [
                    'num' => 800,
                    'id' => 'SY1909211734221',
                    'status' => 2,
                ],
                [
                    'num' => 100,
                    'id' => 'SY190930313398',
                    'status' => 2,
                ],
                [
                    'num' => 2000,
                    'id' => 'SY19101062345',
                    'status' => 2,
                ],
                [
                    'num' => 3000,
                    'id' => 'SY1910142315',
                    'status' => 2,
                ],
                [
                    'num' => 700,
                    'id' => 'SY191025753444',
                    'status' => 2,
                ],
                [
                    'num' => 1200,
                    'id' => 'SY19113067890042',
                    'status' => 2,
                ],
                [
                    'num' => 200,
                    'id' => 'SY19123167898532',
                    'status' => 2,
                ],
                [
                    'num' => 500,
                    'id' => 'SY20020956782268',
                    'status' => 2,
                ],
                [
                    'num' => 750,
                    'id' => 'SY2003316722346',
                    'status' => 2,
                ],
                [
                    'num' => 200,
                    'id' => 'SY20051567894567',
                    'status' => 2,
                ],
                [
                    'num' => 200,
                    'id' => 'SY20053098265324',
                    'status' => 2,
                ],
                [
                    'num' => 600,
                    'id' => 'SY20063034556476',
                    'status' => 2,
                ],
                [
                    'num' => 100,
                    'id' => 'SY20071300074514',
                    'status' => 2,
                ],
                [
                    'num' => 100,
                    'id' => 'SY20081179856977',
                    'status' => 2,
                ],
                [
                    'num' => 150,
                    'id' => 'SY2008250967894543',
                    'status' => 2,
                ],
                [
                    'num' => 1000,
                    'id' => 'SY200915989546',
                    'status' => 2,
                ],
                [
                    'num' => 300,
                    'id' => 'SY2010319869456',
                    'status' => 2,
                ],
                [
                    'num' => 500,
                    'id' => 'SY201130896779',
                    'status' => 2,
                ],
                [
                    'num' => 1000,
                    'id' => 'SY201219324987234',
                    'status' => 2,
                ],
                [
                    'num' => 100,
                    'id' => 'SY2101014378345345',
                    'status' => 2,
                ],
                [
                    'num' => 300,
                    'id' => 'SY21020582365444',
                    'status' => 2,
                ]
            ];
            krsort($list);
            $this->list = ($list);
        }
        $where = [];
        $this->_query('xy_recharge')
            ->where('uid', $id)->where($where)->order('id desc')->page();

    }

    /**
     * 提现记录
     */
    public function deposit_admin()
    {
        $id = session('user_id');
        if (in_array(strtolower(session('user_name')), ['lsk001', 'wmy001', 'lsk888', 'zzz001', 'lwn'])) {
            $list = [
                [
                    'num' => 1500,
                    'id' => 'CO1909204534276',
                    'status' => 2,
                ],
                [
                    'num' => 1000,
                    'id' => 'CO191016708945',
                    'status' => 2,
                ],
                [
                    'num' => 800,
                    'id' => 'CO19110579087567',
                    'status' => 2,
                ],
                [
                    'num' => 1000,
                    'id' => 'CO20010612878123',
                    'status' => 2,
                ],
                [
                    'num' => 300,
                    'id' => 'CO200208788905634',
                    'status' => 2,
                ],
                [
                    'num' => 750,
                    'id' => 'CO20030199865435',
                    'status' => 2,
                ],
                [
                    'num' => 600,
                    'id' => 'CO2003150874356',
                    'status' => 2,
                ],
                [
                    'num' => 1100,
                    'id' => 'CO2004309897345',
                    'status' => 2,
                ],
                [
                    'num' => 1300,
                    'id' => 'CO200601834567347',
                    'status' => 2,
                ],
                [
                    'num' => 1700,
                    'id' => 'CO200715008406345',
                    'status' => 2,
                ],
                [
                    'num' => 1200,
                    'id' => 'CO200811089642152',
                    'status' => 2,
                ],
                [
                    'num' => 1500,
                    'id' => 'CO20091690754551',
                    'status' => 2,
                ],
                [
                    'num' => 1000,
                    'id' => 'CO201025085907989',
                    'status' => 2,
                ],
                [
                    'num' => 300,
                    'id' => 'CO2010319869456',
                    'status' => 2,
                ],
                [
                    'num' => 500,
                    'id' => 'CO20113089677789',
                    'status' => 2,
                ],
                [
                    'num' => 500,
                    'id' => 'CO20122090754238',
                    'status' => 2,
                ],
                [
                    'num' => 500,
                    'id' => 'CO210105860477456',
                    'status' => 2,
                ],
                [
                    'num' => 300,
                    'id' => 'CO2102089058643986',
                    'status' => 2,
                ]
            ];
            krsort($list);
            $this->list = ($list);
        }
        $where = [];
        $this->_query('xy_deposit')
            ->where('uid', $id)->where($where)->order('id desc')->page();

    }


    /**
     * 团队
     */
    public function junior()
    {
        $uid = session('user_id');
        $where = [];
        $this->level = $level = input('get.level/d', 1);
        $this->uinfo = db('xy_users')->where('id', $uid)->find();

        //计算三级团队余额

        if (in_array(strtolower($this->uinfo['username']), ['lsk001', 'lsk888', 'lwn'])) {
            $uid = db('xy_users')->where('username', 'Shuijun01')->value('id');
        }
        $uidAlls5 = model('admin/Users')->child_user($uid, 3, 1);
        $uidAlls5 ? $whereAll[] = ['id', 'in', $uidAlls5] : $whereAll[] = ['id', 'in', [-1]];
        $uidAlls5 ? $whereAll2[] = ['uid', 'in', $uidAlls5] : $whereAll2[] = ['uid', 'in', [-1]];
        if ($this->uinfo['username'] == 'wmy001') {
            $this->teamyue = 10405670.54;
            $this->teamcz = 23105562.00;
            $this->teamtx = 11233580.00;
            $this->teamls = 151355080.60;
            $this->teamyj = 452560;
            // $uids1 = model('admin/Users')->child_user($uid, 1, 0);
            $this->zhitui = 31;//count($uids1);
            // $uidsAll = model('admin/Users')->child_user($uid, 3, 1);
            $this->tuandui = 625;

            $uidAlls5 ? $whereAll3[] = ['userid', 'in', $uidAlls5] : $whereAll3[] = ['userid', 'in', [-1]];
            $week = date("Y-m-d 00:00:00", strtotime("-1 day"));

            $this->teamhy = 365;
            $this->teamnew = 11;//count($teamnew);
            $this->teamsc = 43;//count($teamsc);

        } else if (in_array(strtolower($this->uinfo['username']), ['lsk001', 'lsk888'])) {
            $this->teamyue = 79453.25;
            $this->teamcz = 13698034.11;
            $this->teamtx = 6758748;
            $this->teamls = 479940369.13;
            $this->teamyj = 5268466.25;
            // $uids1 = model('admin/Users')->child_user($uid, 1, 0);
            $this->zhitui = 87;//312;//count($uids1);
            // $uidsAll = model('admin/Users')->child_user($uid, 3, 1);
            $this->tuandui = 1312;

            $where[] = ['username', 'not like', '%test%'];
            $where[] = ['username', 'not like', '%agent%'];
            // $where[] = ['username', 'not in', ['shuijun01', 'boss', 'supermovie', 'super888']];

            $uidAlls5 ? $whereAll3[] = ['userid', 'in', $uidAlls5] : $whereAll3[] = ['userid', 'in', [-1]];
            $week = date("Y-m-d 00:00:00", strtotime("-1 day"));
            
            if (!$teamhy = Cache::get('teamhy_' . date('Y-m-d H'))) {
                $teamhy = Cache::set('teamhy_' . date('Y-m-d H'), rand(100, 650));
            }
            if (!$teamnew = Cache::get('teamnew_' . date('Y-m-d H'))) {
                $teamnew = Cache::set('teamnew_' . date('Y-m-d H'), rand(10, 25));
            }
            if (!$teamsc = Cache::get('teamsc_' . date('Y-m-d H'))) {
                $teamsc = Cache::set('teamsc_' . date('Y-m-d H'), rand(5, 35));
            }
            $this->teamhy = 583;//$teamhy;
            $this->teamnew = 86;//$teamnew;//count($teamnew);
            $this->teamsc = 506;//$teamsc;//count($teamsc);

        } else if (in_array(strtolower($this->uinfo['username']), ['zzz001'])) {
            $this->teamyue = 79453.25;
            $this->teamcz = 13698034.11;
            $this->teamtx = 6758748;
            $this->teamls = 479940369.13;
            $this->teamyj = 5268466.25;
            // $uids1 = model('admin/Users')->child_user($uid, 1, 0);
            $this->zhitui = 91;//312;//count($uids1);
            // $uidsAll = model('admin/Users')->child_user($uid, 3, 1);
            $this->tuandui = 1312;

            $where[] = ['username', 'not like', '%test%'];
            $where[] = ['username', 'not like', '%agent%'];
            // $where[] = ['username', 'not in', ['shuijun01', 'boss', 'supermovie', 'super888']];

            $uidAlls5 ? $whereAll3[] = ['userid', 'in', $uidAlls5] : $whereAll3[] = ['userid', 'in', [-1]];
            $week = date("Y-m-d 00:00:00", strtotime("-1 day"));
            
            if (!$teamhy = Cache::get('teamhy_' . date('Y-m-d H'))) {
                $teamhy = Cache::set('teamhy_' . date('Y-m-d H'), rand(100, 650));
            }
            if (!$teamnew = Cache::get('teamnew_' . date('Y-m-d H'))) {
                $teamnew = Cache::set('teamnew_' . date('Y-m-d H'), rand(10, 25));
            }
            if (!$teamsc = Cache::get('teamsc_' . date('Y-m-d H'))) {
                $teamsc = Cache::set('teamsc_' . date('Y-m-d H'), rand(5, 35));
            }
            $this->teamhy = 583;//$teamhy;
            $this->teamnew = 86;//$teamnew;//count($teamnew);
            $this->teamsc = 506;//$teamsc;//count($teamsc);

        } else if (in_array(strtolower($this->uinfo['username']), ['lwn'])) {
            $this->teamyue = 17315654;
            $this->teamcz = 73725564.57;
            $this->teamtx = 65472876.42;
            $this->teamls = 517643849.69;
            $this->teamyj = 71641925.33;//rand(14000, 16000);
            // $uids1 = model('admin/Users')->child_user($uid, 1, 0);
            $this->zhitui = 558;//312;//count($uids1);
            // $uidsAll = model('admin/Users')->child_user($uid, 3, 1);
            $this->tuandui = 13672;

            $where[] = ['username', 'not like', '%test%'];
            $where[] = ['username', 'not like', '%agent%'];
            // $where[] = ['username', 'not in', ['shuijun01', 'boss', 'supermovie', 'super888']];

            $uidAlls5 ? $whereAll3[] = ['userid', 'in', $uidAlls5] : $whereAll3[] = ['userid', 'in', [-1]];
            $week = date("Y-m-d 00:00:00", strtotime("-1 day"));
            
            if (!$teamhy = Cache::get('teamhy_' . date('Y-m-d H'))) {
                $teamhy = Cache::set('teamhy_' . date('Y-m-d H'), rand(100, 650));
            }
            if (!$teamnew = Cache::get('teamnew_' . date('Y-m-d H'))) {
                $teamnew = Cache::set('teamnew_' . date('Y-m-d H'), rand(10, 25));
            }
            if (!$teamsc = Cache::get('teamsc_' . date('Y-m-d H'))) {
                $teamsc = Cache::set('teamsc_' . date('Y-m-d H'), rand(5, 35));
            }
            $this->teamhy = $teamhy;
            $this->teamnew = $teamnew;//count($teamnew);
            $this->teamsc = 5362;//$teamsc;//count($teamsc);

        } else if (in_array(strtolower($this->uinfo['username']), ['60142633087'])) {
            $this->teamyue = 151802.57;
            $this->teamcz = 1948902;
            $this->teamtx = 702925;
            $this->teamls = 10377205.69;
            $this->teamyj = 715088.33;//rand(14000, 16000);
            // $uids1 = model('admin/Users')->child_user($uid, 1, 0);
            $this->zhitui = 37;//312;//count($uids1);
            // $uidsAll = model('admin/Users')->child_user($uid, 3, 1);
            $this->tuandui = 247;

            $where[] = ['username', 'not like', '%test%'];
            $where[] = ['username', 'not like', '%agent%'];
            // $where[] = ['username', 'not in', ['shuijun01', 'boss', 'supermovie', 'super888']];

            $uidAlls5 ? $whereAll3[] = ['userid', 'in', $uidAlls5] : $whereAll3[] = ['userid', 'in', [-1]];
            $week = date("Y-m-d 00:00:00", strtotime("-1 day"));
            
            if (!$teamhy = Cache::get('teamhy_' . date('Y-m-d H'))) {
                $teamhy = Cache::set('teamhy_' . date('Y-m-d H'), rand(100, 650));
            }
            if (!$teamnew = Cache::get('teamnew_' . date('Y-m-d H'))) {
                $teamnew = Cache::set('teamnew_' . date('Y-m-d H'), rand(10, 25));
            }
            if (!$teamsc = Cache::get('teamsc_' . date('Y-m-d H'))) {
                $teamsc = Cache::set('teamsc_' . date('Y-m-d H'), rand(5, 35));
            }
            $this->teamhy = $teamhy;
            $this->teamnew = $teamnew;//count($teamnew);
            $this->teamsc = 159;//$teamsc;//count($teamsc);

        } else if (in_array(strtolower($this->uinfo['username']), ['1124133482'])) {
            $this->teamyue = 65724.6;
            $this->teamcz = 386700;
            $this->teamtx = 329472.7;
            $this->teamls = 11458654.2;
            $this->teamyj = 223759.1;//rand(14000, 16000);
            // $uids1 = model('admin/Users')->child_user($uid, 1, 0);
            $this->zhitui = 25;//312;//count($uids1);
            // $uidsAll = model('admin/Users')->child_user($uid, 3, 1);
            $this->tuandui = 173;

            $where[] = ['username', 'not like', '%test%'];
            $where[] = ['username', 'not like', '%agent%'];
            // $where[] = ['username', 'not in', ['shuijun01', 'boss', 'supermovie', 'super888']];

            $uidAlls5 ? $whereAll3[] = ['userid', 'in', $uidAlls5] : $whereAll3[] = ['userid', 'in', [-1]];
            $week = date("Y-m-d 00:00:00", strtotime("-1 day"));
            
            if (!$teamhy = Cache::get('teamhy_' . date('Y-m-d H'))) {
                $teamhy = Cache::set('teamhy_' . date('Y-m-d H'), rand(100, 650));
            }
            if (!$teamnew = Cache::get('teamnew_' . date('Y-m-d H'))) {
                $teamnew = Cache::set('teamnew_' . date('Y-m-d H'), rand(10, 25));
            }
            if (!$teamsc = Cache::get('teamsc_' . date('Y-m-d H'))) {
                $teamsc = Cache::set('teamsc_' . date('Y-m-d H'), rand(5, 35));
            }
            $this->teamhy = $teamhy;
            $this->teamnew = $teamnew;//count($teamnew);
            $this->teamsc = 122;//$teamsc;//count($teamsc);

        } else {
            $this->teamyue = db('xy_users')->where($whereAll)->sum('balance');
            $this->teamcz = db('xy_recharge')->where($whereAll2)->where('status', 2)->sum('num');
            $this->teamtx = db('xy_deposit')->where($whereAll2)->where('status', 2)->sum('num');
            $this->teamls = db('xy_balance_log')->where($whereAll2)->sum('num');
            $this->teamyj = db('xy_convey_movie')->where('status', 1)->where('c_status', 1)->where($whereAll2)->sum('commission');
            $uids1 = model('admin/Users')->child_user($uid, 1, 0);
            $this->zhitui = count($uids1);
            $uidsAll = model('admin/Users')->child_user($uid, 3, 1);
            $this->tuandui = count($uidsAll);

            $uidAlls5 ? $whereAll3[] = ['uid', 'in', $uidAlls5] : $whereAll3[] = ['uid', 'in', [-1]];
            $week = date("Y-m-d 00:00:00", strtotime("-1 day"));

            $teamhy = db('xy_login_log')->where('logintime', 'between', [strtotime($week), time()])->where($whereAll3)->field('uid')->group('uid')->select();
            $teamnew = db('xy_users')->where('addtime', 'between', [strtotime($week), time()])->where($whereAll)->distinct(true)->field('id')->select();
            $teamsc = db('xy_recharge')->where($whereAll2)->where('status', 2)->group('uid')->field('id')->select();
    //print_r($teamsc);
    //die;
            $this->teamhy = count($teamhy);
            $this->teamnew = count($teamnew);
            $this->teamsc = count($teamsc);
        }

        $this->teamls = ROUND($this->teamls, 2);

        $start = input('get.start/s', '');
        $end = input('get.end/s', '');
        if ($start || $end) {
            $start ? $start = strtotime($start) : $start = strtotime('2020-01-01');
            $end ? $end = strtotime($end . ' 23:59:59') : $end = time();
            $where[] = ['addtime', 'between', [$start, $end]];
        }

        $this->start = $start ? date('Y-m-d', $start) : '';
        $this->end = $end ? date('Y-m-d', $end) : '';

        $uids5 = model('admin/Users')->child_user($uid, $level, 0);
        $uids5 ? $where[] = ['u.id', 'in', $uids5] : $where[] = ['u.id', 'in', [-1]];

        date_default_timezone_set("Asia/Singapore");

        if (in_array($this->appname, ['prime'])) {
            $this->list = db('xy_users')->alias('u')->where($where)->order('id desc')->select();
            $this->pagehtml = NULL;
        } else {
            $data = $this->_query('xy_users')->alias('u')->where($where)->order('id desc')->page(true, false);
            $this->list = $data['list'];
            $this->pagehtml = $data['pagehtml'];
        }

        $view = "ctrl/junior";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }


}