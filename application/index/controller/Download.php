<?php
namespace app\index\controller;

use library\Controller;
use think\Db;

use library\tools\Node;
use think\facade\Request;
/**
 * 登录控制器
 */
class Download extends Base
{
    public function __construct()
    {   
        cookie('think_var','cn');
    }

    public function index()
    {
    	//Detect special conditions devices
		$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
		$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
		$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
		$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
		$webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

		//do something with this information
		if ( $iPod || $iPhone || $iPad) {
		    //browser reported as an iPhone/iPod touch -- do something here
		    $url = config('ios_download_url');
		} else {
		    //browser reported as an iPad -- do something here
		    $url = config('android_download_url');
		}
		header("Location: $url");
		die();
    }
}