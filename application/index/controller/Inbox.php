<?php

namespace app\index\controller;

use think\Controller;
use think\Request;
use think\Db;
use think\facade\Cache;

/**
 * 信息控制器
 */
class Inbox extends Base
{
    /**
     * 首页
     */
    public function index()
    {
        $uid = session('user_id');
        // $is_agent = db('xy_users')->where('id', $uid)->value('is_agent');
        
        $tester = ['company','bai','test0001','supermovie','supermovie2','agent006vip3','test0002','test0003'];
        
        if (in_array(strtolower($this->uinfo['username']), $tester)) {
            $sql = db('xy_inbox_group')
            ->alias('ing')
            ->where("receiver_uid LIKE '$uid' OR receiver_uid LIKE '$uid" . ",%' OR receiver_uid LIKE '%,$uid,%' OR receiver_uid LIKE '%,$uid'")
            ->field("ing.id, ing.updatetime, ing.sender_uid, ing.receiver_uid, 'group' AS type, name AS sender_username, name AS receiver_username")
            ->buildSql();
        } else {
            $sql = "";
        }
        $list = db('xy_inbox')
        ->alias('in')
        ->join('xy_users sender', 'sender.id = in.sender_uid')
        ->join('xy_users receiver', 'receiver.id = in.receiver_uid')
        ->where("(sender_uid = $uid OR receiver_uid = $uid) AND in.status = 1")
        ->field("in.id, in.updatetime, in.sender_uid, in.receiver_uid, 'onetoone' AS type, COALESCE(sender.weixin, sender.username) AS sender_username, COALESCE(receiver.weixin, receiver.username) AS receiver_username")
        ->union($sql)
        ->order('updatetime desc')
        ->select();
        
        $time = time();
        foreach ($list as $k => $v) {
            $inboxId = $v['id'];
            $updatetime = $v['updatetime'];
            
            $table = 'xy_inbox_message';
            if ($v['type'] == 'group') {
                $table = 'xy_inbox_message_group';
            }
            $message = db($table)->where("inbox_id = '$inboxId' AND status = 1")->order('addtime DESC')->find();
            if ($message) {
                $list[$k]['message'] = ($message['uid'] == $uid ? lang('You') . ': ' : '') . $message['content'];
            } else {
                $list[$k]['message'] = '';
            }
            
            $list[$k]['title'] = $v['sender_uid'] == $uid ? $v['receiver_username'] : $v['sender_username'];
            $diff_s = $time - $updatetime;
            $time_text = strftime('%b %d',$updatetime);
            if ($diff_s < 30) {
                $time_text = lang('Just now');
            } else if ($diff_s < 60) {
                $time_text = $diff_s . lang('Sec ago');
            } else if ($diff_s < 3600) {
                $time_text = floor($diff_s / 60) . lang('Min ago');
            } else if (date('Y', $updatetime) < date('Y')) {
                $time_text = date('d/m/y', $updatetime);
            } else {
                $time_text = date('H:i', $updatetime);
            }
            
            $list[$k]['time_text'] = $time_text;
            $list[$k]['isread'] = isset($message) && $message['uid'] != $uid ? $message['isread'] : 1;
        }

        if (date('Y-m-d H:i:s') <= '2021-12-26 23:59:59') {
            Cache::set('contact_list_' . $uid, NULL);
        }
        if (!$contact_list = Cache::get('contact_list_' . $uid)) {
            $userids = db('xy_users')->where('id', $uid)->value('down_userid');
            $userids = explode(",", $userids);
            $parent = model('admin/Users')->get_parent_users($uid);
            $userids = array_merge($userids, $parent);

            $exclude = ['company', 'Boss', 'Super001'];
            $contact_list = db('xy_users')->where('id', 'IN', $userids)->where('username', 'NOT IN', $exclude)->where('id != ' . $uid)->field("tel, COALESCE(weixin, '') AS weixin, username, id")->select();
            Cache::set('contact_list_' . $uid, $contact_list, 3600);
        }

        $this->contact_list = $contact_list;
        
        if (request()->isAjax()) {
            return json(['code' => 0, 'info' => $list]);
        }
        $this->list = $list;
        
        $view = "index_2";
        if (in_array($this->appname, ['dypf'])) {
            if (in_array($this->uinfo['username'], $tester)) {
                $view = "index_2";
            } else {
                $view = "index";
            }
        }
        

        $view = "inbox/" . $view;
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }
    
    public function chat()
    {
        $id = input('id/i', 0);
        $uid = session('user_id');
        $type = input('type/s','onetoone');
        
        if (!filter_input( INPUT_GET, 'id', FILTER_VALIDATE_INT)) {
            $this->writelog('id-参数错误',$id,'inbox', json_encode(input()));
            return $this->redirect('/index');
        }

        $table = $type == 'group' ? 'xy_inbox_message_group' : 'xy_inbox_message';
        
        if (request()->isAjax()) {
            $messageId = input('message_id/i', 0);

            if (!is_numeric($messageId) || abs($messageId) != $messageId) {
                $this->writelog('message_id-参数错误',$messageId,'inbox', json_encode(input(), $uid));
                return json(['code' => 1, 'info' => lang('参数错误')]);
            }

            $isnew = input('new/i',0);
            $map = [
                'inbox_id' => $id,
                'msg.status' => 1
            ];
            
            $where = $isnew == 1 ? "msg.id > $messageId" : "msg.id < $messageId";
            $list = db($table)->alias('msg')
            ->join('xy_users u', 'u.id = msg.uid')
            ->where($map)
            ->where($where)
            ->field('msg.*, COALESCE(u.weixin, u.username) AS sender_username, headpic')
            ->order('addtime desc')
            ->limit(50)
            ->select();
            
            if ($list) {
                krsort($list);
                $list = array_values($list);
            } else {
                $list = [];
            }
            db($table)->alias('msg')->where($map)->where("isread = 0 AND uid != $uid")->update(['isread' => 1, 'readtime' => time()]);
            return json(['code' => 0, 'info' => $list]);
        } else {
            $this->title = input('title/s', '');
            $map = [
                'inbox_id' => $id,
                'msg.status' => 1
            ];
            $list = db($table)->alias('msg')
            ->join('xy_users u', 'u.id = msg.uid')
            ->where($map)
            ->field('msg.*, COALESCE(u.weixin, u.username) AS sender_username, headpic')
            ->order('addtime desc')
            ->limit(50)
            ->select();
            if (!$list) {
//                 return $this->redirect('inbox/index');
            }
            
            db($table)->alias('msg')->where($map)->where("isread = 0 AND uid != $uid")->update(['isread' => 1, 'readtime' => time()]);
        }
        
        $time = time();
        
        foreach ($list as $k => $v) {
            $updatetime = $v['addtime'];
            $diff_s = $time - $updatetime;
            $time_text = strftime('%b %d',$updatetime);
            if ($diff_s < 30) {
                $time_text = lang('Just now');
            } else if ($diff_s < 60) {
                $time_text = $diff_s . lang('Sec ago');
            } else if ($diff_s < 3600) {
                $time_text = floor($diff_s / 60) . lang('Min ago');
            } else if (date('Y', $updatetime) < date('Y')) {
                $time_text = date('d/m/y', $updatetime);
            } else {
                $time_text = date('H:i', $updatetime);
            }
            
            $list[$k]['time_text'] = $time_text;
        }
        krsort($list);
        
        $this->id = $id;
        $this->list = $list;
        $this->type = $type;
        $view = "inbox/chat";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }
    
    public function checkuserexist()
    {
        $uid = session('user_id');
        $down_userid = db('xy_users')->where('id', $uid)->value('down_userid');
        $down_userid_arr = explode(",", $down_userid);
        $up_userid_arr = model('admin/Users')->get_parent_users($uid);
        
        $arr = array_unique(array_merge($down_userid_arr, $up_userid_arr));
        $uids = implode(",", $arr);
        
        $val = input('val/s', '');
        
        if (in_array(strtolower($this->uinfo['username']), ['cs001', 'test0001'])) {
            $receiver_uid = db('xy_users')->where("username = '$val' OR tel = '$val' OR weixin = '$val'")->where("id != '$uid' AND username NOT IN ('company', 'wmy001')")->value('id');
        } else {
            $receiver_uid = db('xy_users')->where("username = '$val' OR tel = '$val' OR weixin = '$val'")->where("id IN ($uids)")->where("id != '$uid' AND username NOT IN ('company', 'wmy001')")->value('id');
        }
        if (!$receiver_uid) {
            return json(['code' => 1, 'info' => lang('Recipient does not exist')]);
        } else {
            return json(['code' => 0, 'info' => $receiver_uid]);
        }
    }
    
    public function send()
    {
        $uid = session('user_id');
        if (request()->post()) {
            $receiver_uid = input('receiver_uid/i', 0);
            $message = input('message/s', '');

            if (!filter_input( INPUT_POST, 'receiver_uid', FILTER_VALIDATE_INT)) {
                $this->writelog('receiver_uid-参数错误',$receiver_uid,'inbox', json_encode(input()),$uid);
                return json(['code' => 1, 'info' => lang('参数错误')]);
            }
            
            if (!db('xy_users')->find($receiver_uid)) {
                return json(['code' => 1, 'info' => lang('Recipient does not exist')]);
            }
            
            $map = [
                'sender_uid' => $uid,
                'receiver_uid' => $receiver_uid,
                'status' => 1,
            ];
            $inbox = db('xy_inbox')->where($map)->find();
            $time = time();
            if ($inbox) {
                $data_message = [
                    'inbox_id' => $inbox['id'],
                    'uid' => $uid,
                    'content' => $message,
                    'addtime' => $time,
                ];
                $re = db('xy_inbox_message')->insert($data_message);
                if ($re) {
                    db('xy_inbox')->where('id', $inbox['id'])->update(['action' => 1, 'updatetime' => $time]);
                    return json(['code' => 0, 'info' => lang('Sent successfully')]);
                } else {
                    return json(['code' => 1, 'info' => lang('Failed to send')]);
                }
            }
            
            $data = [
                'sender_uid' => $uid,
                'receiver_uid' => $receiver_uid,
                'addtime' => $time,
                'updatetime' => $time,
                'action' => 1,//1等待回复，2对方已回复
            ];
            
            $inboxId = db('xy_inbox')->insertGetId($data);
            
            if (!$inboxId) {
                return json(['code' => 1, 'info' => lang('Failed to send')]);
            }
            $data_message = [
                'inbox_id' => $inboxId,
                'uid' => $uid,
                'content' => $message,
                'addtime' => $time,
            ];
            $re = db('xy_inbox_message')->insert($data_message);
            if ($re) {
                return json(['code' => 0, 'info' => lang('Sent successfully')]);
            } else {
                return json(['code' => 1, 'info' => lang('Failed to send')]);
            }
        }
    }
    
    public function reply()
    {
        $uid = session('user_id');
        if (request()->post()) {
            $id = input('inbox_id/i', 0);
            $message = input('message/s', '');
            $type = input('type/s', 'onetoone');
            $table = $type == 'group' ? '_group' : '';

            if (!filter_input( INPUT_POST, 'inbox_id', FILTER_VALIDATE_INT)) {
                $this->writelog('inbox_id-参数错误',$id,'inbox', json_encode(input()),$uid);
                return json(['code' => 1, 'info' => lang('参数错误')]);
            }

            $map = [
                'id' => $id,
                'status' => 1,
            ];
            $inbox = db('xy_inbox' . $table)->where($map)->find();
            $time = time();
            if ($inbox) {
                $data_message = [
                    'inbox_id' => $inbox['id'],
                    'uid' => $uid,
                    'content' => $message,
                    'addtime' => $time,
                ];
                $re = db('xy_inbox_message' . $table)->insert($data_message);
                if ($re) {
                    db('xy_inbox' . $table)->where('id', $inbox['id'])->update(['action' => 1, 'updatetime' => $time]);
                    return json(['code' => 0, 'info' => lang('Sent successfully')]);
                } else {
                    return json(['code' => 1, 'info' => lang('Failed to send')]);
                }
            }
            
            return json(['code' => 1, 'info' => lang('Failed to send')]);
        }
    }
    
    public function checkifunread()
    {
        $uid = session('user_id');
        if (request()->isAjax()) {
            $count = db('xy_inbox')
            ->alias('in')
            ->join('xy_inbox_message xm', "in.id = xm.inbox_id AND xm.isread = 0 AND xm.uid != $uid")
            ->where("(sender_uid = $uid OR receiver_uid = $uid) AND in.status = 1")
            ->group('xm.inbox_id')
            ->count('xm.inbox_id');
            
            $count2 = db('xy_inbox_group')
            ->alias('in')
            ->join('xy_inbox_message_group xm', "in.id = xm.inbox_id AND xm.isread = 0 AND xm.uid != $uid")
            ->where("(receiver_uid LIKE '$uid' OR receiver_uid LIKE '$uid" . ",%' OR receiver_uid LIKE '%,$uid,%' OR receiver_uid LIKE '%,$uid') AND in.status = 1")
            ->group('xm.inbox_id')
            ->count('xm.inbox_id');
            return json(['code' => 0, 'info' => $count + $count2]);
        }
    }
    
    public function sendgroup()
    {
        $uid = session('user_id');
        if (request()->post()) {
            $receiver_uid = input('receiver_uid/s', '');
            $receiver_usernames = explode(",", input('username/s', ''));
            $message = input('message/s', '');
            $groupname = input('groupname/s', '');
            
            $receiver_uids = explode(",", $receiver_uid);
            $receiver_uids[] = $uid;
            sort($receiver_uids);
            foreach ($receiver_uids as $k => $receiver_uid) {
                if (!db('xy_users')->find((int) $receiver_uid)) {
                    return json(['code' => 1, 'info' => lang('The recipient {:username} does not exist', ['username' => $receiver_usernames[$k]])]);
                }
            }
            
            $map = [
                'sender_uid' => $uid,
                'receiver_uid' => implode(",", $receiver_uids),
                'status' => 1,
            ];
            $inbox = db('xy_inbox_group')->where($map)->find();
            $time = time();
            if ($inbox) {
                $data_message = [
                    'inbox_id' => $inbox['id'],
                    'uid' => $uid,
                    'content' => $message,
                    'addtime' => $time,
                ];
                $re = db('xy_inbox_message_group')->insert($data_message);
                if ($re) {
                    db('xy_inbox_group')->where('id', $inbox['id'])->update(['action' => 1, 'updatetime' => $time]);
                    return json(['code' => 0, 'info' => lang('Sent successfully')]);
                } else {
                    return json(['code' => 1, 'info' => lang('Failed to send')]);
                }
            }
            
            $data = [
                'name' => $groupname,
                'sender_uid' => $uid,
                'receiver_uid' => implode(",", $receiver_uids),
                'owner_uids' => $uid,
                'addtime' => $time,
                'updatetime' => $time,
                'action' => 1,//1等待回复，2对方已回复
            ];
            
            $inboxId = db('xy_inbox_group')->insertGetId($data);
            
            if (!$inboxId) {
                return json(['code' => 1, 'info' => lang('Failed to send')]);
            }
            $data_message = [
                'inbox_id' => $inboxId,
                'uid' => $uid,
                'content' => $message,
                'addtime' => $time,
            ];
            $re = db('xy_inbox_message_group')->insert($data_message);
            if ($re) {
                return json(['code' => 0, 'info' => lang('Sent successfully')]);
            } else {
                return json(['code' => 1, 'info' => lang('Failed to send')]);
            }
        }
    }
    
    public function checkgroupnameexist()
    {
        $uid = session('user_id');
        $name = input("val/s", '');
        
        $exist = db('xy_inbox_group')->where(['sender_uid' => $uid, 'name' => $name])->field('id')->find();
        
        if ($exist) {
            return json(['code' => 1, 'info' => lang('You have created the same group name')]);
        } else {
            return json(['code' => 0, 'info' => '']);
        }
    }
    
    public function adduser()
    {
        if (request()->post()) {
            $uid = session('user_id');
            
            $down_userid = db('xy_users')->where('id', $uid)->value('down_userid');
            $down_userid_arr = explode(",", $down_userid);
            $up_userid_arr = model('admin/Users')->get_parent_users($uid);
            
            $arr = array_unique(array_merge($down_userid_arr, $up_userid_arr));
            $uids = implode(",", $arr);
            
            $username = input('username/s', '');
            
            if (in_array(strtolower($this->uinfo['username']), ['cs001', 'test0001'])) {
                $userid = db('xy_users')->where("username = '$username' OR tel = '$username' OR weixin = '$username'")->where("id != '$uid' AND username NOT IN ('company', 'wmy001')")->value('id');
            } else {
                $userid = db('xy_users')->where("username = '$username' OR tel = '$username' OR weixin = '$username'")->where("id IN ($uids)")->where("id != '$uid' AND username NOT IN ('company', 'wmy001')")->value('id');
            }
            
            if (!$userid) {
                return json(['code' => 1, 'info' => lang('The invited user {:username} does not exist', ['username' => $username])]);
            }
            
            $inboxId = input('inbox_id/i', 0);
            $receiver_uid = db('xy_inbox_group')->where('id', $inboxId)->value('receiver_uid');
            
            
            
            if (!$receiver_uid) {
                return json(['code' => 1, 'info' => lang('Network problem,Try againt') . $receiver_uid]);
            }
            
            $receiver_uids = explode(",", $receiver_uid);
            $receiver_uids[] = $userid;
            $receiver_uids = array_unique($receiver_uids);
            sort($receiver_uids);
            $receiver_uid = implode(",", $receiver_uids);
            
            $re = db('xy_inbox_group')->where('id', $inboxId)->update(['receiver_uid' => $receiver_uid]);
            
            if ($re) {
                return json(['code' => 0, 'info' => lang('Invited successfully')]);
            } else {
                return json(['code' => 1, 'info' => lang('No records updated')]);
            }
        }
    }
    
    public function userlist()
    {
        if (request()->post()) {
            $inboxId = input('inbox_id/i', 0);

            if (!filter_input( INPUT_POST, 'inbox_id', FILTER_VALIDATE_INT)) {
                $this->writelog('inbox_id-参数错误',$inboxId,'inbox', json_encode(input()),session('user_id'));
                return json(['code' => 1, 'info' => lang('参数错误')]);
            }

            $receiver_uid = db('xy_inbox_group')->where('id', $inboxId)->value('receiver_uid');
            
            $uids = $receiver_uid ? $receiver_uid : "''";
            $users = db('xy_users')->where("id IN ($uids)")->field('COALESCE(weixin, username) AS username')->select();
            
            $html = "";
            foreach ($users as $key => $user) {
                $html .= '<p style="text-align: left;">' . ($key + 1) . '. ' . $user['username'] . '</p>';
            }
            
            return json(['code' => 0, 'info' => $html]);
        }
    }
    
    public function recall_message()
    {
        if (request()->post()) {
            $messageId = input('message_id/i', 0);

            if (!filter_input( INPUT_POST, 'message_id', FILTER_VALIDATE_INT)) {
                $this->writelog('message_id-参数错误',$messageId,'inbox', json_encode(input()),session('user_id'));
                return json(['code' => 1, 'info' => lang('参数错误')]);
            }

            $type = input('type/s', '');
            $table = $type == 'group' ? 'xy_inbox_message_group' : 'xy_inbox_message';
            $message = db($table)->where(['id' => $messageId, 'status' => 1])->find();
            if (!$message) {
                return json(['code' => 1, 'info' => lang('Unable to recall the recalled message')]);
            }
            
            $re = db($table)->where('id', $messageId)->update(['status' => 2, 'recall_time' => time()]);
            if ($re) {
                return json(['code' => 0, 'info' => lang('Recalled successfully')]);
            } else {
                return json(['code' => 1, 'info' => lang('No records updated')]);
            }
        }
    }
}