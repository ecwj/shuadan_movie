<?php
namespace app\index\controller;

use library\Controller;
use think\Db;
use think\facade\Cache;

/**
 * 应用入口
 * Class Index
 * @package app\index\controller
 */
class Index extends Base
{
    /**
     * 入口跳转链接
     */
    public function index()
    {
        $this->redirect('home');
    }
    
    public function start()
    {
        return $this->fetch();
    }
    
    public function home()
    {
        $value = cookie('think_var').'_content';
//        $this->info = Db::name('xy_index_msg')->field($value)->select();
        //$this->balance = Db::name('xy_users')->where('id',session('user_id'))->sum('balance');
        // Cache::set('movie_banner', NULL);
        // Cache::set('banner', NULL);
        // Cache::set('homepage_list', NULL);
        $date = config('movie_start_date');
        if (!$movie_banner = Cache::get('movie_banner')) {            
            $movie_banner = Db::name('xy_movie')->where('status', 1)->where("playtime_date >= '$date'")->field('IF(name_en != "", name_en, name) AS name, IF(description_en != "", description_en, description) AS description, id AS movie_id, COALESCE(image_upload, image_url) AS image, price')->orderRaw('rand()')->select();
            Cache::set('movie_banner', $movie_banner, 1800);
        } else {
            shuffle($movie_banner);
        }
        $this->movie_banner = $movie_banner;
        if (!$banner = Cache::get('banner')) {
            $banner = Db::name('xy_banner')->where('type',cookie('think_var'))->orderRaw('rand()')->select();
            Cache::set('banner', $banner, 1800);
        } else {
            shuffle($banner);
        }
        $this->banner = $banner;
        //if($this->banner) $this->banner = explode('|',$this->banner);
//        $this->notice = db('xy_index_msg')->where('id',1)->value('content');
//        $this->hezuo = db('xy_index_msg')->where('id',4)->value('content');
//        $this->jianjie = db('xy_index_msg')->where('id',2)->value('content');
//        $this->guize = db('xy_index_msg')->where('id',3)->value('content');
        // $this->gundong = db('xy_index_msg')->where('id',8)->value($value);
        $this->tanchunag = db('xy_message')->where('is_tanchuang',1)->value('content');


        // $notice_num1 = db('xy_message')->alias('m')
        //     ->leftJoin('xy_reads r','r.mid=m.id and r.uid='.session('user_id'))
        //     ->where('m.uid','in',[0,session('user_id')])
        //     ->count('m.id');
        // $notice_num2 = db('xy_message')->alias('m')
        //     ->leftJoin('xy_reads r','r.mid=m.id and r.uid='.session('user_id'))
        //     ->where('m.uid','in',[0,session('user_id')])
        //     ->count('r.id');
		$notice_num=0;//$notice_num1-$notice_num2;
		$this->notice_num = $notice_num;


        $dev = new \org\Mobile();
        $t = $dev->isMobile();
        if (!$t) {
            if (config('app_only')) {
                header('Location:/app');
            }
        }

        if (!$list = Cache::get('homepage_list')) {
            $list = db('xy_convey_movie')
                ->alias('xc')
                ->join('xy_movie xm',"xm.id=xc.goods_id AND xc.status = 1 AND xm.status = 1 AND xm.playtime_date >= '$date'")
                ->field('IF(xm.name_en != "", xm.name_en, xm.name) AS name, IF(xm.category_en != "", xm.category_en, xm.category) AS category, IF(xm.people_en != "", xm.people_en, xm.people) AS people, COALESCE(xm.image_upload, xm.image_url) AS image, xm.id AS movie_id, SUM(xc.goods_count) AS total_count, xm.playtime_date, IF(description_en != "", description_en, description) AS description')
                ->order('total_count desc, xc.addtime desc')
                ->group('xc.goods_id')
                ->limit(10)
                ->select();
            Cache::set('homepage_list', $list, 1800);
        }
        
        $this->list = $list;

        $uid = session('user_id');

        $this->info = db('xy_users')->find($uid);

        $this->latest_notice = db('xy_message')->where('type', 2)->order('id desc')->limit(1)->value('content');

        $view = "index/home";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }

    
    //获取首页图文
    public function get_msg()
    {
        $type = input('post.type/d',1);
        $data = Db::name('xy_index_msg')->find($type);
        if($data)
            return json(['code'=>0,'info'=>lang('Request successfully'),'data'=>$data]);
        else
            return json(['code'=>1,'info'=>lang('No data yet')]);
    }




    //获取首页图文
    public function getTongji()
    {
        $type = input('post.type/d',1);
        $data = array();

        $data['user'] = db('xy_users')->where('status',1)->where('addtime','between',[strtotime(date('Y-m-d'))-24*3600,time()])->count('id');
        $data['goods'] = db('xy_goods_list')->count('id');;
        $data['price'] = db('xy_convey')->where('status',1)->where('endtime','between',[strtotime(date('Y-m-d'))-24*3600,strtotime(date('Y-m-d'))])->sum('num');
        $user_order = db('xy_convey')->where('status',1)->where('addtime','between',[strtotime(date('Y-m-d')),time()])->field('uid')->Distinct(true)->select();
        $data['num'] = count($user_order);

        if($data){
            return json(['code'=>0,'info'=>lang('Request successfully'),'data'=>$data]);
        } else {
            return json(['code' => 1, 'info' => lang('No data yet')]);
        }
    }



    function getDanmu()
    {
        $barrages=    //弹幕内容
            array(
                array(
                    'info'   => '用户173***4985开通会员成功',
                    'href'   => '',

                ),
                array(
                    'info'   => '用户136***1524开通会员成功',
                    'href'   => '',
                    'color'  =>  '#ff6600'

                ),
                array(
                    'info'   => '用户139***7878开通会员成功',
                    'href'   => '',
                    'bottom' => 450 ,
                ),
                array(
                    'info'   => '用户159***7888开通会员成功',
                    'href'   => '',
                    'close'  =>false,

                ),array(
                'info'   => '用户151***7799开通会员成功',
                'href'   => '',

                )
            );

        echo   json_encode($barrages);
    }

    public function active(){
        $value = cookie('think_var').'_content';
        $this->info = Db::name('xy_index_msg')->where('id',10)->value($value);
        return $this->fetch();
    }

    public function company(){
        $value = cookie('think_var').'_content';
        $this->info = Db::name('xy_index_msg')->where('id',2)->value($value);
        return $this->fetch();
    }

    public function rule(){
        $value = cookie('think_var').'_content';
        $this->info = Db::name('xy_index_msg')->where('id',3)->value($value);
        return $this->fetch();
    }

    public function cooperation(){
        $value = cookie('think_var').'_content';
        $this->info = Db::name('xy_index_msg')->where('id',4)->value($value);
        return $this->fetch();
    }

    public function announcement(){
        if (request()->isAjax()) {
            $id = input('id');
            $limit = 5;
            $where = "";
            if ($id) {
                $where = "id < $id";
            }
            $announcements = db('xy_message')->where('type', 3)->where($where)->where('addtime <= ' . time())->field('id, title, content, addtime')->order('addtime desc')->limit($limit)->select();
            
            foreach ($announcements as $k => $v) {
                if (in_array($this->appname, ['prime'])) {
                    $announcements[$k]['addtime'] = date('d-m-Y', $v['addtime']);
                } else {
                    $announcements[$k]['addtime'] = date('Y-m-d H:i:s', $v['addtime']);
                }
            }
            
            return json($announcements);
        }

        $this->list = NULL;//db('xy_message')->where('type', 3)->where('addtime <= ' . time())->field('id, title, content, FROM_UNIXTIME(addtime) AS addtime')->order('addtime desc')->select();

        $view = "index/announcement";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }
}
