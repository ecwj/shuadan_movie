<?php

namespace app\index\controller;

use think\Controller;
use think\Request;
use think\Db;
use think\facade\Cache;

/**
 * 下单控制器
 */
class Invest extends Base
{
    /**
     * 首页
     */
    public function index()
    {
        $uid = session('user_id');
        try {
        $is_presale = input('get.is_presale/i', 0);
        $map =[
            'status' => 1,
            'is_presale' => $is_presale,
        ];
        $release_date_where = "";
        if ($is_presale == 1) {
            $release_date_where = ' AND release_date >= "' . date('Y-m-d H:i:s') . '"';
        }
        $date = config('movie_start_date');//date('Y-m-d H:i:s');
        
        $time = db('xy_movie')->order('updatetime desc')->value('updatetime');
        $movieupdatetime = Cache::get('movieupdatetime');

        if (!$movieupdatetime || $movieupdatetime < $time) {
            Cache::set('movie_banner', NULL);
            Cache::set('movielist', NULL);
        } else {
            Cache::set('movieupdatetime', $time);
        }

        $uinfo = Db::name('xy_users')->field('deal_status,balance,level')->find($uid);
        $level = $uinfo['level'];
        !$uinfo['level'] ? $level = 0 : '';
        $ulevel = Db::name('xy_level')->where('level',$level)->find();

        $this->level = $ulevel;

        // if (!$movielist = Cache::get('movielist')) {
            // $movielist = db('xy_movie')->where($map)->where("playtime_date >= '$date' $release_date_where")->order('price asc')->orderRaw('rand()')->select();
            $movielist = db('xy_movie')->where($map)->where("playtime_date >= '$date' $release_date_where")->order('playtime_date desc')->select();

            foreach ($movielist as $key => $l) {
                $movielist[$key]['info'] = json_decode($l['content'], true);

                if (cookie('think_var') != 'cn') {
                    $movielist[$key]['name'] = $l['name_en'];
                }
                if (cookie('think_var') != 'cn') {
                    $movielist[$key]['description'] = $l['description_en'];
                }
                if (cookie('think_var') != 'cn') {
                    $movielist[$key]['people'] = $l['people_en'];
                }

                if (in_array($this->appname, ['dypf_indon'])) {
                    $movielist[$key]['price'] = number_format($l['price'], 0, ',', '.');
                }

                if ($l['is_presale'] == 1) {
                    $arr = explode(",", $l['roi_percentage']);
                    $roi_percentage = isset($arr[$level - 1]) ? $arr[$level - 1] : $ulevel['roi'];
                    $movielist[$key]['roi'] = $roi_percentage;
                }
            }
        //     Cache::set('movielist', $movielist, 3600);
        // }
        
        $this->movielist = $movielist;

        
        $map_total = [
            'xm.status' => 1,
            'is_presale' => $is_presale,
        ];
        //if (!$select = Cache::get('invest_movie_list')) {
            $select = db('xy_movie')
                ->alias('xm')
                ->leftJoin('xy_convey_movie xc', 'xc.goods_id = xm.id AND xc.status = 1')
                ->where($map_total)
                ->field('xm.id, SUM(COALESCE(xc.goods_count, 0)) AS num')
                ->group('xm.id')
                ->select();  
            Cache::set('invest_movie_list', $select, 3600);  
        //}

        $totalNum = 0;
        $total = [];
        foreach ($select as $key => $s) {
            // $totalNum += $s['num'];
            $total[$s['id']] = $s['num'];
        }
        // $this->totalNum = $totalNum;
        $this->total = $total;

        if (in_array(strtolower(session('user_name')), ['lsk001', 'wmy001', 'lsk888', 'lwn111'])) {
            $uid = 38;
        }
        $map = [
            'uid' => $uid,
            'is_presale' => $is_presale,
        ];

        // if (!$roi = Cache::get('roi_' . $uid)) {
            $roi = db('xy_convey_movie')
                ->alias('xc')
                ->join('xy_movie g','g.id=xc.goods_id')
                ->field('xc.*,g.name,g.price,g.id AS movie_id,g.image_url,g.image_upload,g.roi_percentage,g.is_presale')
                ->where($map)
                ->where("xc.addtime >= " . strtotime(date('Y-m-d 00:00:00')))
                ->order('addtime desc')
                ->select();
            Cache::set('roi_' . $uid, $roi);
        // }
        // $this->roi = $roi;

        $map_roi = [
            'status' => 1,
            'uid' => $uid
        ];
        $last_roi_date = db('xy_roi_log')->where($map_roi)->order('id desc')->value('addtime');

        $roi_pending = [];
        $todaycount = $todayamount = 0;
        foreach ($roi as $key => $r) {
            $num = $r['num'];// * $r['goods_count'];

            if ($r['is_presale'] == 1) {
                $arr = explode(",", $r['roi_percentage']);
                $roi_percentage = isset($arr[$level - 1]) ? $arr[$level - 1] : $ulevel['roi'];
                $income = $num * $roi_percentage;
            } else {
                $income = $num * $ulevel['roi'];
            }
            $r['num'] = $num;
            $r['income'] = $income;
            if (in_array($this->appname, ['dypf_indon'])) {
                $r['income'] = number_format($income, 0, ',', '.');
                $r['num'] = number_format($num, 0, ',', '.');
            }
            if ($r['addtime'] > $last_roi_date || ($is_presale == 1 && $r['c_status'] == 0)) {
                $r['date'] = date('Y-m-d H:i:s', $r['addtime']);
                $roi_pending[] = $r;
            }            
            $todaycount += $r['goods_count'];
            $todayamount += $r['num'];
        }
        $this->todaycount = $todaycount;
        $this->todayamount = $todayamount;
        // $roi_completed = [];
        // foreach ($roi as $r) {
        //     if ($r['addtime'] <= $last_roi_date) {
        //         $r['date'] = date('Y-m-d H:i:s', $r['addtime']);
        //         $roi_completed[] = $r;
        //     }
        // }

        $this->roi_pending = $roi_pending;
        // $this->roi_completed = $roi_completed;

        $where = [
            'xc.status' => 1,
            'is_presale' => $is_presale,
        ];

        $data = $this->_query('xy_convey_movie')
            ->where('xc.uid',session('user_id'))
            ->alias('xc')
            ->leftJoin('xy_movie xg','xc.goods_id=xg.id')
            ->field('xc.*,xg.name AS goods_name,xg.price AS goods_price,xg.image_url AS goods_pic,xg.image_url,xg.image_upload')
            ->order('xc.addtime desc')
            ->where($where)
            ->page(true, false);

        $this->list = $data['list'];
        $this->pagehtml = isset($data['pagehtml']) ? $data['pagehtml'] : NULL;
        $view = "invest/index";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        $this->is_presale = $is_presale;
        } catch (\Exception $ex) {
            dump($ex);
            exit;
        }
        return $this->fetch($view2);
    }
    public function ranking()
    {
        $date = input('date/s', date('Y-m-d'));
        $date0 = date('Y-m-d', strtotime($date . '-1 day'));
        $date2 = date('Y-m-d', strtotime($date . '+1 day'));

        $this->date0 = $date0;
        $this->date2 = $date2;

        if (request()->isAjax()) {

            $time = db('xy_theater')->order('updatetime desc')->value('updatetime');
            $theaterpdatetime = Cache::get('theaterpdatetime');

            if (!$theaterpdatetime || $theaterpdatetime < $time) {
                Cache::set('theater_list-' . $date, NULL);
            } else {
                Cache::set('theaterpdatetime', $time);
            }
            if (!$content = Cache::get('theater_list-' . $date)) {
                $map = [
                    'date' => $date
                ];
                $info = db('xy_theater')->where($map)->find();
                if ($info) {
                    $content = json_decode($info['content'], 1)['data'];
                } else {
                    $content = [];
                }
                Cache::set('theater_list-' . $date, $content, 3600);
            }

            $appname = config('app_name');
            if ($appname == 'dypf_indon') {
                $cinema = [
                    'Berita Film Indonesia',
                    'Genta Buana Paramita',
                    'Perfini',
                    'Produksi Film Negara',
                    'Rapi Films',
                    'SinemArt'
                ];
                $boxDesc = $content['total']['boxDesc'];
                if (strpos($boxDesc, '万') !== false) {
                    $boxDesc = (((float) rtrim($boxDesc, '万')) * 10000 * 2500);
                }
                $viewCountDesc = $content['total']['viewCountDesc'];
                if (strpos($viewCountDesc, '万') !== false) {
                    $viewCountDesc = (((float) rtrim($viewCountDesc, '万')) * 10000);
                }
                $boxDesc_ = $viewCountDesc_ = '';

                if ($boxDesc > 100000000) {
                    $boxDesc_ = 'bil';
                    $boxDesc = round($boxDesc / 100000000, 2);
                } else if ($boxDesc >= 1000000) {
                    $boxDesc_ = 'mil';
                    $boxDesc = round($boxDesc / 1000000, 2);
                } else if ($boxDesc >= 1000) {
                    $boxDesc_ = 'k';
                    $boxDesc = round($boxDesc / 1000, 2);
                }

                if ($viewCountDesc > 100000000) {
                    $viewCountDesc_ = 'bil';
                    $viewCountDesc = round($viewCountDesc / 100000000, 2);
                } else if ($viewCountDesc >= 1000000) {
                    $viewCountDesc_ = 'mil';
                    $viewCountDesc = round($viewCountDesc / 1000000, 2);
                } else if ($viewCountDesc >= 1000) {
                    $viewCountDesc_ = 'k';
                    $viewCountDesc = round($viewCountDesc / 1000, 2);
                }
                
                $content['total']['boxDesc'] = $boxDesc . $boxDesc_;
                $content['total']['viewCountDesc'] = $viewCountDesc . $viewCountDesc_;
                
                foreach ($content['list'] as $key => $val) {
                    if (!isset($cinema[$key])) {
                        unset($content['list'][$key]);
                        continue;
                    }
                    $content['list'][$key]['name'] = $cinema[$key];

                    $boxDesc = $val['boxDesc'];
                    if (strpos($boxDesc, '万') !== false) {
                        $boxDesc = (((float) rtrim($boxDesc, '万')) * 10000 * 2500);
                    }
                    $boxDesc_ = '';
                    if ($boxDesc > 100000000) {
                        $boxDesc_ = 'bil';
                        $boxDesc = round($boxDesc / 100000000, 2);
                    } else if ($boxDesc >= 1000000) {
                        $boxDesc_ = 'mil';
                        $boxDesc = round($boxDesc / 1000000, 2);
                    } else if ($boxDesc >= 1000) {
                        $boxDesc_ = 'k';
                        $boxDesc = round($boxDesc / 1000, 2);
                    }

                    $content['list'][$key]['boxDesc'] = $boxDesc . $boxDesc_;
                }
            }

            $data = [
                'content' => $content,
                'date0' => $date0,
                'date1' => $date,
                'date2' => $date2,
                'map' => $map
            ];
            return json($data);
        }

        $view = "invest/ranking";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }

    public function record()
    {
        $uid = session('user_id');

        if (in_array(strtolower(session('user_name')), ['lsk001', 'wmy001', 'lsk888', 'zzz001', 'lwn111'])) {
            $uid = 38;
        }
        $start      = input('get.start/s','');
        $end        = input('get.end/s','');
        $map[] = ['uid', '=', $uid];
        $map[] = ['c_status', '=', 1];
        if ($start || $end) {
            $start ? $start = strtotime($start) : $start = strtotime('2021-05-01');
            $end ? $end = strtotime($end.' 23:59:59') : $end = time();
        } else {
            $start ? $start = strtotime($start) : $start = strtotime('2021-05-01');
            $end = strtotime(date('Y-m-d 23:59:59'));
        }

        $map[] = ['xc.addtime','between',[$start,$end]];


        $this->start = $start ? date('Y-m-d',$start) : '';
        $this->end = $end ? date('Y-m-d',$end) : '';

        // $list = db('xy_convey_movie')
        //     ->alias('xc')
        //     ->join('xy_movie g','g.id=xc.goods_id')
        //     ->field('xc.*,g.name,g.price,g.id AS movie_id')
        //     ->where($map)
        //     ->order('addtime desc')
        //     ->select();

        $this->list_ = [];
        // if (in_array(strtolower(session('user_name')), ['lsk001', 'wmy001', 'lsk888'])) {
        //     $this->list_ = $this->wmypurchase(null, $start, $end, session('user_name'));
        // }
        $data = $this->_query('xy_convey_movie')
            ->alias('xc')
            ->join('xy_movie g','g.id=xc.goods_id')
            ->field('xc.*,g.name,g.price,g.id AS movie_id')
            ->where($map)
            ->order('addtime desc')
            ->page(true, false);

        $this->list = $data['list'];
        $this->pagehtml = isset($data['pagehtml']) ? $data['pagehtml'] : NULL;
        // if (in_array(strtolower(session('user_name')), ['lsk001', 'wmy001', 'lsk888'])) {
        //     $this->pagehtml = NULL;
        // }

        $view = "invest/record";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }

    public function record2()
    {
        $uid = session('user_id');

        if (in_array(strtolower(session('user_name')), ['lsk001', 'wmy001', 'lsk888', 'zzz001', 'lwn111'])) {
            $uid = 38;
        }

        $map[] = ['uid', '=', $uid];
        $map[] = ['c_status', '=', 0];
        $start = strtotime(date('Y-m-d 00:00:00'));
        $end = strtotime(date('Y-m-d 23:59:59'));

        // if (!in_array($this->appname, ['apple_movie'])) {
            $map[] = ['xc.addtime','between',[$start,$end]];
        // }


        $this->start = $start ? date('Y-m-d',$start) : '';
        $this->end = $end ? date('Y-m-d',$end) : '';

        $level = db('xy_users')->where('id', $uid)->value('level');
        $level = db('xy_level')->where('level', $level)->find();
        $this->level = $level;

        $list_ = [];
        if (in_array(strtolower(session('user_name')), ['lsk001', 'wmy001', 'lsk888', 'zzz001', 'lwn111'])) {
            $list_ = $this->wmypurchase(null, $start, $end, session('user_name'));
            // return $this->fetch();
        }

        $list = db('xy_convey_movie')
            ->alias('xc')
            ->join('xy_movie g','g.id=xc.goods_id')
            ->field('xc.*,g.name,g.price,g.id AS movie_id,g.roi_percentage,g.is_presale')
            ->where($map)
            ->where("is_presale = 0")
            ->order('addtime desc')
            ->select();

        $map2[] = ['uid', '=', $uid];
        $map2[] = ['c_status', '=', 0];
        $list2 = db('xy_convey_movie')
            ->alias('xc')
            ->join('xy_movie g','g.id=xc.goods_id')
            ->field('xc.*,g.name,g.price,g.id AS movie_id,g.roi_percentage,g.is_presale')
            ->where($map2)
            ->where("is_presale = 1 AND g.name NOT IN ('Test2', 'Test')")
            ->order('addtime desc')
            ->select();
            
        $list = array_merge($list, $list_, $list2);
        foreach ($list as $key => $val) {
            $num = $val['num'];
            $commission = $val['commission'];
            $price = $val['price'];
            if ($commission == 0) {
                if ($val['is_presale'] == 1) {
                    $level_ = $level['level'];
                    $arr = explode(",", $val['roi_percentage']);
                    $roi = isset($arr[$level_ - 1]) ? $arr[$level_ - 1] : $roi;
                } else {
                    $roi = $level['roi'];
                }
                $list[$key]['commission'] = $num * $roi;
            }
            if (in_array($this->appname, ['dypf_indon'])) {

                $list[$key]['num'] = number_format($list[$key]['num'], 0, ',', '.');
                $list[$key]['commission'] = number_format($list[$key]['commission'], 0, ',', '.');
            }
        }

        $this->list = $list;
        
        $view = "invest/record2";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }

    public function order_info()
    {
        $where['xc.id'] = input('id/i', 0);

        $uid = session('user_id');
        
        if (in_array(strtolower(session('user_name')), ['lsk001', 'wmy001', 'lsk888', 'zzz001', 'lwn111'])) {
            // $info = $this->wmypurchase(input('id'));
            // if ($info) {
            //     $this->info = $info;
            //     return $this->fetch();
            // }
            $uid = 38;
        }

        $info = db('xy_convey_movie')
                    ->where('xc.uid',session('user_id'))
                    ->alias('xc')
                    ->leftJoin('xy_movie xg','xc.goods_id=xg.id')
                    ->field('xc.*,xg.name AS goods_name,xg.price AS goods_price,COALESCE(xg.image_upload, xg.image_url) AS goods_pic')
                    ->where($where)
                    ->find();
        $info['goods_price'] = $info['num'] / $info['goods_count'];
        if (in_array($this->appname, ['dypf_indon'])) {
            $info['goods_price'] = number_format($info['goods_price'], 0, ',', '.');
            $info['num'] = number_format($info['num'], 0, ',', '.');
        }
        $this->info = $info;

        $view = "invest/order_info";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }

    function wmypurchase($id = null, $start = '', $end = '', $filename = 'wmy001')
    {
        $content = @file_get_contents(getcwd() . '/' . $filename . '.txt');
        if (!$content) {

            $movies = db('xy_movie')->order('playtime_date asc')->select();
            $lists = [];
            $list_now = [];
            foreach ($movies as $key => $m) {            
                if ($m['playtime_date'] < '2019-09-16') {
                    continue;
                }
                $year = date('Y', strtotime($m['playtime_date']));
                if (!isset($lists[$year])) {
                    $lists[$year] = [];
                }
                if ($m['playtime_date'] >= '2021-05-10') {
                    if (!isset($lists[$year])) {
                        $list_now[$year] = [];
                    }
                    $list_now[$year][] = $m;
                }
                $lists[$year][] = $m;
            }

            $data = [];
            for ($i = 1; $i < 10000; $i ++) {
                foreach ($list_now as $year => $list) {
                    $m = $list[array_rand($list)];
                    $purchasedate = strtotime(date('Y-m-19 H:i:s', strtotime($m['playtime_date']))) + (rand(1, 1440) * 60);
                    
                    $operation_start = date('Y-m-d ' . config('operation_start'), $purchasedate);
                    $operation_end = date('Y-m-d ' . config('operation_end'), $purchasedate);

                    if (date('Y-m-d H:i:s', $purchasedate) > '2021-05-19 17:00') {
                        $i --;
                        continue;
                    }
                    if (date('Y-m-d H:i:s', $purchasedate) < $operation_start || date('Y-m-d H:i:s', $purchasedate) > $operation_end) {
                        if (date('Y-m-d H:i:s', $purchasedate) < $operation_start) {
                            $purchasedate += strtotime($operation_start) - $purchasedate + rand(1, 10) * 60 * 60;
                        } else if (date('Y-m-d H:i:s', $purchasedate) > $operation_end) {
                            //$purchasedate += $purchasedate - strtotime($operation_end);
                        }
                    }

                    if (date('Y-m-d H:i:s'))
                    $order_id_main = date('YmdHis', $purchasedate) . mt_rand(1000, 9999);
                    $oid = "DY" . substr($order_id_main, 2); //生成订单号
                    $price = $m['price'];
                    $count = rand(1, 50);
                    $num = $count * $price;
                    $commission = ROUND($num * 0.038, 2);
                    $goods_pic = $m['image_upload'] != NULL ? $m['image_upload'] : $m['image_url'];
                    $data[$oid] = [
                        'id' => $oid,
                        'name' => $m['name'],
                        'goods_name' => $m['name'],
                        'addtime' => $purchasedate,
                        'num' => $num,
                        'goods_price' => $price,
                        'goods_count' => $count,
                        'commission' => $commission,
                        'status' => 1,
                        'goods_pic' => $goods_pic
                    ];
                }
            }

            for ($i = 1; $i < 10000; $i ++) {
                foreach ($lists as $year => $list) {
                    $m = $list[array_rand($list)];
                    $purchasedate = strtotime(date('Y-m-d H:i:s', strtotime($m['playtime_date'] . '- ' . rand(0, 7). 'day'))) + (rand(1, 1440) * 60);
                    
                    $operation_start = date('Y-m-d ' . config('operation_start'), $purchasedate);
                    $operation_end = date('Y-m-d ' . config('operation_end'), $purchasedate);

                    if (date('Y-m-d H:i:s', $purchasedate) > '2021-08-18 17:00') {
                        $i --;
                        continue;
                    }
                    if (date('Y-m-d H:i:s', $purchasedate) < $operation_start || date('Y-m-d H:i:s', $purchasedate) > $operation_end) {
                        // $i --;
                        if (date('Y-m-d H:i:s', $purchasedate) < $operation_start) {
                            $purchasedate += strtotime($operation_start) - $purchasedate + rand(1, 10) * 60 * 60;
                        } else if (date('Y-m-d H:i:s', $purchasedate) > $operation_end) {
                            $purchasedate += $purchasedate - strtotime($operation_end);
                        }
                        // exit;
                        // continue;
                    }

                    if (date('Y-m-d H:i:s'))
                    $order_id_main = date('YmdHis', $purchasedate) . mt_rand(1000, 9999);
                    $oid = "DY" . substr($order_id_main, 2); //生成订单号
                    $price = $m['price'];
                    $count = rand(1, 50);
                    $num = $count * $price;
                    $commission = ROUND($num * 0.038, 2);
                    $goods_pic = $m['image_upload'] != NULL ? $m['image_upload'] : $m['image_url'];
                    $data[$oid] = [
                        'id' => $oid,
                        'name' => $m['name'],
                        'goods_name' => $m['name'],
                        'addtime' => $purchasedate,
                        'num' => $num,
                        'goods_price' => $price,
                        'goods_count' => $count,
                        'commission' => $commission,
                        'status' => 1,
                        'goods_pic' => $goods_pic
                    ];
                }
            }

            file_put_contents(getcwd() . '/' . $filename . '.txt', json_encode($data));
        } else {
            $data = json_decode($content, 1);
        }
        rsort($data);
        if ($id) {
            foreach ($data as $d) {
                if ($d['id'] == $id) {
                    return $d;
                }
            }
            return null;
        }

        if ($start && $end) {
            foreach ($data as $key => $d) {
                if ($d['addtime'] < $start || $d['addtime'] > $end) {
                    unset($data[$key]);
                }
            }
        }

        return $data;
    }

}
