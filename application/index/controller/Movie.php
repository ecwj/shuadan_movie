<?php

namespace app\index\controller;

use think\Controller;
use think\Request;
use think\Db;
use think\facade\Cache;

/**
 * 下单控制器
 */
class Movie extends Base
{
    /**
     * 首页
     */
    public function info()
    {
        try {
		$id = input('id/i', 0);
        if (!$id = filter_input( INPUT_GET, 'id', FILTER_VALIDATE_INT)) {
            $this->writelog('id-参数错误',$id,'movie', json_encode(input()),session('user_id'));
            $this->redirect('/index');
        }
        $info = db('xy_movie')->find($id);

        if ($info) {
            $info['content'] = json_decode($info['content'], 1);
            $people = $info['people'];

            $people = str_replace(['演员', ' '], ['演员', ''], $people);

            $people = preg_replace('/\s+/', '<br/>', $people);

            $people = str_replace(['演员', '导演', '编剧'], ['<br/><b style="font-size:16px">演员</b>', '<b style="font-size:16px">导演</b>', '<br/><b style="font-size:16px">编剧</b>'], $people);

            $info['people'] = $people;
            if ($info['name_en']) {
                $info['name'] = $info['name_en'];
            }
            if ($info['description_en']) {
                $info['description'] = $info['description_en'];
            }
            if ($info['people_en']) {
                $info['people'] = $info['people_en'];
            }

            if ($info['image_upload']) {
                $info['image_url'] = $info['image_upload'];
            }

            $info['price_display'] = $info['price'];
            if (in_array($this->appname, ['dypf_indon'])) {
                $info['price_display'] = number_format($info['price'], 0, ',', '.');
            }
        } else {
            $this->redirect('/index/invest');
        }
// var_dump($info);
        $this->info = $info;
        $uinfo = Db::name('xy_users')->where('id',session('user_id'))->field('id, total_recharge, down_userid, level, (balance - freeze_balance) AS balance, is_jia, username')->find();

        // if ($uinfo['is_jia'] == 0) {
        //     $newlevel = get_latest_level($uinfo, $uinfo['level']);
        // } else {
            $newlevel = $uinfo['level'];
        // }

        if ($uinfo['level'] != $newlevel) {
            $uinfo['level'] = $newlevel;
            db('xy_users')->where('id', $uinfo['id'])->update(['level' => $newlevel]);
        }
        $min = $max = $presaleDay = $roi = 0;
        if ($info['is_presale'] == 1) {
            $level = $uinfo['level'];
            $arr = explode(",", $info['min_purchase']);
            $min = isset($arr[$level - 1]) ? $arr[$level - 1] : $min;

            $arr = explode(",", $info['max_purchase']);
            $max = isset($arr[$level - 1]) ? $arr[$level - 1] : $min;

            $arr = explode(",", $info['roi_percentage']);
            $roi = isset($arr[$level - 1]) ? $arr[$level - 1] : $roi;

            // $date1 = date_create($date);
            $date1 = date_create(date('Y-m-d H:i:s'));
            $date2 = date_create($info['release_date']);

            $diff = date_diff($date1,$date2);
            $sign = $diff->format("%R");
            if ($sign == '+') {
                $presaleDay = $diff->format("%a") + 1;
                // $presaleDay = $presaleDay == 0 ? 1 : $diff->format("%a");
            }
            if ($info['release_date'] < date('Y-m-d H:i:s')) {
                header('Location: /index/invest?is_presale=1');
                exit;
            }
        }

        $whitelistUser = ['supermovie','1121313010','1121313011','1121313012','1121313013','1121313014','1121313015','1121313016','1121313017','1121313018','1121313019','1121313020'];

        $max = 1000;
        if ($id == 1098) {
            $max = 2000;
        }
        $this->min = $min;
        $this->max = $max;
        if (in_array($uinfo['username'], $whitelistUser)) {
            $this->max = 0;
        }
        $this->presaleDay = $presaleDay;

        $this->uinfo = $uinfo;
        $ulevel = Db::name('xy_level')->where('level',$uinfo['level'])->find();
        if ($roi > 0) {
            $ulevel['roi'] = $roi;
        }
        $this->ulevel = $ulevel;
        $view = "movie/info";
        $apptheme = $this->appname;
        if ($info['is_presale'] == 1) {
            $view .= "_presale";
        }
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        //update userbalance
        $balance = db('xy_balance_log')->where('uid', $uinfo['id'])->field('COALESCE(sum(if(status = 1, num,-num)), 0) as total')->find();
        if ($balance) {
            if ($balance['total'] != $uinfo['balance']) {
                $res1 = Db::name('xy_users')
                ->where('id', $uinfo['id'])
                ->update(['balance' => $balance['total']]);
            }
        }
    } catch (\Exception $ex) {
        dump($ex);exit;
    }
        return $this->fetch($view2);
    }

    /**
     *提交抢单
     */
    public function submit_order()
    {
        $httpOrigin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
        $httpOrigin = strtolower(str_replace(['http://', 'https://'], '', $httpOrigin));
        if (!isset($_SERVER['HTTP_ORIGIN']) || strpos(strtolower($_SERVER['HTTP_ORIGIN']), 'nolan') === false) {
            // $error = 'Unauthenticated.';
            // return json(['code' => 1, 'info' => $error]);
        }

        $operation_start = date('Y-m-d') . ' ' . config('operation_start');
        $operation_end = date('Y-m-d') . ' ' . config('operation_end');

        $now = date('Y-m-d H:i:s');
        if ($now < $operation_start || $now > $operation_end) {
            if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
                return json(['code' => 1, 'info' => lang('Movie tickets are sold from 10am to 9.59pm every day')]);
            }
        }
        $uid = session('user_id');
        $uinfo = db('xy_users')->field('username,status,balance,level,deal_status')->find($uid);
        if($uinfo['status']==2) return json(['code'=>1,'info'=>lang('The account has been disabled')]);

        if ($uinfo['deal_status'] == 0) {
            return json(['code' => 1, 'info' => lang('System is under maintenance.')]);   
        }

        $goods_id = input('goods_id', 0);
        $goods_count = input('count', 0);

        if (!$goods_id = filter_input( INPUT_GET, 'goods_id', FILTER_VALIDATE_INT)) {
            $this->writelog('goods_id-参数错误',$goods_id,'movie', json_encode(input()),$uid);
            return json(['code' => 1, 'info' => lang('参数错误')]);
        }

        $goods = db('xy_movie')->where('id', $goods_id)->find();

        $where_roi = [
            'uid' => $uid,
            'roi_date' => date('Y-m-d')
        ];
        $roi_log = db('xy_roi_log')->where($where_roi)->find();
        if ($roi_log) {
            if ($goods['is_presale'] != 1) {
                return json(['code' => 1, 'info' => '亲你已经买完你今天的门票了']);
            }
        }

        $whitelistUser = ['supermovie','1121313010','1121313011','1121313012','1121313013','1121313014','1121313015','1121313016','1121313017','1121313018','1121313019','1121313020'];
        //get user's today goods count
        $map = [
            'xc.uid' => $uid,
            'xc.status' => 1,
        ];
        $today_total_num = db('xy_convey_movie')->alias('xc')->join('xy_movie xm', 'xm.id = xc.goods_id')->where($map)->where('xm.is_presale != 1 AND xc.addtime between ' . strtotime(date('Y-m-d 00:00:00')) . ' and ' . strtotime(date('Y-m-d 23:59:59')))->sum('num');
        $levelinfo = db('xy_level')->where('level', $uinfo['level'])->find();
        $level = $uinfo['level'];
        if (in_array($this->appname, ['apple_movie'])) {            
            $goods_num = $goods['price'];
            $min = $levelinfo['num_min'];
            if ($goods['is_presale'] == 1) {
                $arr = explode(",", $goods['min_purchase']);
                $min = isset($arr[$level - 1]) ? $arr[$level - 1] : 10;
                if ($min > 0 && $goods_count < $min && !in_array($uinfo['username'], $whitelistUser)) {
                    return json(['code' => 1, 'info' => lang("至少需购买" . $min . "张影票")]);
                }

                $presale_total_num = db('xy_convey_movie')->alias('xc')->join('xy_movie xm', 'xm.id = xc.goods_id')->where(['uid' => $uid, 'xc.status' => 1, 'xm.is_presale' => 1, 'xc.goods_id' => $goods_id])->sum('xc.num');
                $max = 1000;
                if ($goods_id == 1098) {
                    $max = 2000;
                }
                if ($presale_total_num > $max && !in_array($uinfo['username'], $whitelistUser)) {
                    return json(['code' => 1, 'info' => lang("每人限购每部电影" . $max . "张签约影票")]);
                }
            } else {
                if ($min > 0 && $today_total_num >= $min && !in_array($uinfo['username'], $whitelistUser)) {
                    return json(['code' => 1, 'info' => lang("最高可购买" . $min . "元影票")]);
                }

                if ($levelinfo['num_min'] > 0 && ($today_total_num + ($goods_count * $goods_num)) > $levelinfo['num_min'] && !in_array($uinfo['username'], $whitelistUser)) {
                    $bal = $levelinfo['num_min'] - $today_total_num;
                    return json(['code' => 1, 'info' => lang("您今天还能购买总值{$bal}元的影票")]);
                }
            }
        }

        //判断用户金钱是否支持通道
        $user_balance = db('xy_users')->where('id', $uid)->value('balance');
        $res = db('xy_users')->where('id', $uid)->update(['deal_status' => 2]);//将账户状态改为等待交易
        if ($res === false) return json(['code' => 1, 'info' => lang('Matching failed')]);
        
        
        if ($goods_count < 1) {
            return json(['code' => 1, 'info' => lang("Buy at least one ticket")]);
        }
        if ($goods_id == 0) {
            return json(['code' => 1, 'info' => lang('Network problem,Try againt') . $cid]);
        }        

        $res = model('admin/ConveyMovie')->create_order($uid, $goods_id, $goods_count);

        Cache::set('roi_' . $uid, NULL);

        return json($res);
    }

}
