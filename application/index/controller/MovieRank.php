<?php

namespace app\index\controller;

use library\Controller;
use think\Db;

/**
 * 商城
 * Class Index
 * @package app\index\controller
 */
class MovieRank extends Base
{

    public function index()
    {
        $map =[
            // 'date' => date('Y-m-d'),
            'status' => 1
        ];
        $list = db('xy_movie')->where($map)->select();

        foreach ($list as $key => $l) {
            $list[$key]['info'] = json_decode($l['content'], true);
        }
        $this->list = $list;

        return $this->fetch();
    }
}