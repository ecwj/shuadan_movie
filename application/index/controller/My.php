<?php

namespace app\index\controller;

use think\Controller;
use think\Request;
use think\Db;
use app\admin\model\Users;

class My extends Base
{
    protected $msg = ['__token__'  => '请不要重复提交！'];


    /**
     * 首页
     */
    public function index()
    {
        $uid = session('user_id');
        $info = db('xy_users')->field('username,tel,level,id,headpic,balance,freeze_balance,lixibao_balance,invite_code,show_td,point,freeze_point,nickname,total_recharge,down_userid')->find($uid);
        $this->info = $info;
        $this->lv3 = [0,config('vip_3_num')];
        $this->lv2 = [0,config('vip_2_num')];
        $this->sell_y_num = db('xy_convey')->where('status',1)->where('uid',$uid)->sum('commission');

        $level = $this->info['level'];
        !$level ? $level = 0 :'';

        $this->level_name = db('xy_level')->where('level',$level)->value('name');

        if (in_array($this->appname, ['prime'])) {
            $this->level_name = lang($this->level_name);
        }
        // $this->info['lixibao_balance'] = number_format($this->info['lixibao_balance'],2);

        // $this->rililv = config('lxb_bili')*100 .'%' ;
        // $this->lxb_shouyi = db('xy_lixibao')->where('status',1)->where('uid',session('user_id'))->sum('num');

        $minRecharge = config('invite_reward_min_recharge');
        $down_userid = $info['down_userid'] == NULL ? '' : $info['down_userid'];
        $down_userid_arr = $down_userid != '' ? explode(",", $info['down_userid']) : [];
        $downcount = sizeof($down_userid_arr);
        $index = array_search($info['id'], $down_userid_arr);
        if ($index !== false) {
            unset($down_userid_arr[0]);
            $downcount = sizeof($down_userid_arr);
        }
        $new_down_userid = $downcount == 0 ? "''" : implode(",", $down_userid_arr);

        $this->valid_downcount = db('xy_users')->where("id IN ($new_down_userid) AND total_recharge >= $minRecharge")->count('id');

        $levels = db('xy_level')->where('level > 0')->column('name, num, auto_vip_xu_num, order_num, roi', 'level');

        $maxlevel = 1;
        
        foreach ($levels as $k => $level) {
            if (in_array($this->appname, ['prime', 'apple_movie'])) {
                $levels[$k]['name'] = lang($level['name']);
            }
            if ($level['level'] > $maxlevel) {
                $maxlevel = $level['level'];
            }
        }

        $this->levels = $levels;

        $this->maxlevel = $maxlevel;

        $direct_downcount = 0;
        if (in_array($this->appname, ['prime', 'apple_movie'])) {
            $map = [
                'parent_id' => $uid
            ];
            $direct_downcount = db('xy_users')->where($map)->count('id');
        }

        if ($info['username'] == '15952765385') {
            $direct_downcount = '31';
        }
        $this->direct_downcount = $direct_downcount;

        if (in_array(strtolower($info['username']), ['lwn'])) {
            $this->direct_downcount = 178;
        }

        $view = "my/index";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }

    /**
     * 获取收货地址
     */
    public function get_address()
    {
        $id = session('user_id');
        $data = db('xy_member_address')->where('uid',$id)->select();
        if($data)
            return json(['code'=>0,'info'=>lang('Access successfull'),'data'=>$data]);
        else
            return json(['code'=>1,'info'=>lang('No data yet')]);
    }

    public function reload()
    {
        $id = session('user_id');;
        $user = db('xy_users')->find($id);

        $n = ($id%20);

        $dir = './upload/qrcode/user/'.$n . '/' . $id . '.png';
        if(file_exists($dir)) {
            unlink($dir);
        }

        $res = model('admin/Users')->create_qrcode($user['invite_code'],$id);
        if(0 && $res['code']!==0){
            return $this->error(lang('Fail'));
        }
        return $this->success(lang('Successful'));
    }


    /**c
     * 添加收货地址
     */
    public function add_address()
    {
        if(request()->isPost()){
            $uid = session('user_id');
            $name = input('post.name/s','');
            $tel = input('post.tel/s','');
            $address = input('post.address/s','');
            $area = input('post.area/s','');
            $token = input("token");//获取提交过来的令牌
            $data = ['__token__' => $token];
            $validate   = \Validate::make($this->rule,$this->msg);
            if (!$validate->check($data)) {
                return json(['code'=>1,'info'=>$validate->getError()]);
            }
            $data = [
                'uid'       => $uid,
                'name'      => $name,
                'tel'       => $tel,
                'area'      => $area,
                'address'   => $address,
                'addtime'   => time()
            ];
            $tmp = db('xy_member_address')->where('uid',$uid)->find();
            if(!$tmp) $data['is_default']=1;
            $res = db('xy_member_address')->insert($data);
            if($res)
                return json(['code'=>0,'info'=>lang('Operation successful')]);
            else
                return json(['code'=>1,'info'=>lang('Operation failed')]);
        }
        return json(['code'=>1,'info'=>lang('Request error')]);
    }

    /**
     * 编辑收货地址
     */
    public function edit_address()
    {
        if(request()->isPost()){
            $uid        = session('user_id');
            $name       = input('post.shoujianren/s','');
            $tel        = input('post.shouhuohaoma/s','');
            $address    = input('post.address/s','');

            $area       = input('post.area/s','');


            $ainfo = db('xy_member_address')->where('uid',$uid)->find();
            if ($ainfo) {
                $res = db('xy_member_address')
                    ->where('id',$ainfo['id'])
                    ->update([
                        'uid'       => $uid,
                        'name'      => $name,
                        'tel'       => $tel,
                        'area'      => $area,
                        'address'   => $address,
                        //'addtime'   => time()
                    ]);
            }else{
                $res = db('xy_member_address')
                    ->insert([
                        'uid'       => $uid,
                        'name'      => $name,
                        'tel'       => $tel,
                        'area'      => $area,
                        'address'   => $address,
                        'addtime'   => time()
                    ]);
            }

            if($res)
                return json(['code'=>0,'info'=>lang('Operation successful')]);
            else
                return json(['code'=>1,'info'=>lang('Do not submit repeatedly')]); //操作失败
        }elseif (request()->isGet()) {
            $id = session('user_id');
            $this->info = db('xy_member_address')->where('uid',$id)->find();

            return $this->fetch();
        }
    }

    public function team()
    {
        $uid = session('user_id');
        //$this->info = db('xy_member_address')->where('uid',$id)->find();
        //$uids = model('admin/Users')->child_user($uid,5); //五级
        $uids = model('admin/Users')->child_user($uid,3); //三级
        array_push($uids,$uid);
        $uids ? $where[] = ['uid','in',$uids] : $where[] = ['uid','in',[-1]];

        $datum['sl'] = count($uids);
        $datum['yj'] = db('xy_convey')->where('status',1)->where($where)->sum('num');
        $datum['cz'] = db('xy_recharge')->where('status',2)->where($where)->sum('num');
        $datum['tx'] = db('xy_deposit')->where('status',2)->where($where)->sum('num');


        //
        $uids1 = model('admin/Users')->child_user($uid,1);
        $uids1 ? $where1[] = ['sid','in',$uids1] : $where1[] = ['sid','in',[-1]];
        $datum['log1'] = db('xy_balance_log')->where('uid',$uid)->where($where1)->where('f_lv',1)->sum('num');

        $uids2 = model('admin/Users')->child_user($uid,2);
        $uids2 ? $where2[] = ['sid','in',$uids2] : $where2[] = ['sid','in',[-1]];
        $datum['log2'] = db('xy_balance_log')->where('uid',$uid)->where($where2)->where('f_lv',2)->sum('num');

        $uids3 = model('admin/Users')->child_user($uid,3);
        $uids3 ? $where3[] = ['sid','in',$uids3] : $where3[] = ['sid','in',[-1]];
        $datum['log3'] = db('xy_balance_log')->where('uid',$uid)->where($where3)->where('f_lv',3)->sum('num');


/*
        $uids5 = model('admin/Users')->child_user($uid,5); //五级
        $uids5 ? $where5[] = ['sid','in',$uids5] : $where5[] = ['sid','in',[-1]];
*/
        $datum['yj2'] = db('xy_convey')->where('status',1)->where($where)->sum('commission');
        $datum['yj3'] = db('xy_balance_log')->where('uid',$uid)->where($where5)->where('type',6)->sum('num');;


        $this->info = $datum;

        return $this->fetch();
    }

    public function caiwu()
    {
        $id = session('user_id');
        $day      = input('get.day/s','');
        $where=[];
        if ($day) {
            $start = strtotime("-$day days");
            $where[] = ['addtime','between',[$start,time()]];
        }

        $start      = input('get.start/s','');
        $end        = input('get.end/s','');
        if ($start || $end) {
            $start ? $start = strtotime($start) : $start = strtotime('2020-01-01');
            $end ? $end = strtotime($end.' 23:59:59') : $end = time();
            $where[] = ['addtime','between',[$start,$end]];
        }


        $this->start = $start ? date('Y-m-d',$start) : '';
        $this->end = $end ? date('Y-m-d',$end) : '';

        $this->type = $type = input('get.type/d',0);

        if ($type == 1) {
            $where['type'] = 7;
        }elseif($type==2) {
            $where['type'] = 1;
        }

        $this->_query('xy_balance_log')
            ->where('uid',$id)->where($where)->order('id desc')->page();
			
        //var_dump($_REQUEST);die;
    }


    public function headimg()
    {
        $uid = session('user_id');
        if(request()->isPost()) {
            $username = input('post.pic/s', '');
            $res = db('xy_users')->where('id',session('user_id'))->update(['headpic'=>$username]);
            if($res!==false){
                return json(['code'=>0,'info'=>lang('Operation successfu')]);
            }else{
                return json(['code'=>1,'info'=>lang('Operation failed')]);
            }
        }
        $this->info = db('xy_users')->find($uid);
        return $this->fetch();
    }

    public function bind_bank()
    {
        $id = input('post.id/d',0);
        $uid = session('user_id');
        $info = db('xy_bankinfo')->where('uid',$uid)->find();
        $uinfo = db('xy_users')->find($uid);

        if(request()->isPost()){
            if ($info) {
                return json(['code'=>1,'info'=>lang('Operation failed')]);
            }
            $username = input('post.username/s','');
            $bankname = input('post.bankname/s','');
            $cardnum = input('post.card/s','');
            $site  = input('post.zhihang/s','');
            $qq  = input('post.qq/s','');
            $address  = input('post.address/s','');
            // $ifsc  = input('post.ifsc/s','');
            $tel = input('post.tel','');
            // $upi_name = input('post.upi_name','');
            // $upi_account = input('post.upi_account','');
            if (empty($ifsc)){
                // return json(['code'=>1,'info'=>'ifsc no empty']);
            }

            if (!ctype_digit(input('post.card'))) {
                $this->writelog('cardnum-参数错误',$cardnum,'my', json_encode(input()),$uid);
                return json(['code' => 1, 'info' => lang('参数错误')]);
            }

            //同一姓名和卡号只绑定一次
            $res = db('xy_bankinfo')->where('username',$username)->where('cardnum',$cardnum)->find();
            if (!in_array($this->appname, ['apple_movie'])) {
                if ($res && $res['uid'] != $uid) {
                    return json(['code'=>1,'info'=>lang('The name and bank card have been bound to other accounts')]);
                }

                $res1 = db('xy_bankinfo')->where('cardnum',$cardnum)->find();
                if ($res1 && $res1['uid'] != $uid) {
                    return json(['code'=>1,'info'=>lang('haii !! . Your bank account and ATM card have been use please change to new bank account , Thank You')]);
                }
            }

            if (!is_numeric($cardnum) || strlen($cardnum)<8) return json(['code'=>1,'info'=>lang('Please enter the correct bank number')]);

            $data =array(
                'username' =>$username,
                'bankname' =>$bankname,
                'cardnum' =>$cardnum,
                'site' =>$site,
                'address' =>$address,
                'qq' =>$qq,
                'tel' =>$tel,
                'status' =>1,
                'uid' =>$uid,
                // 'ifsc' =>$ifsc,
                // 'upi_name' => $upi_name,
                // 'upi_account' => $upi_account
            );

            if ($info) {
                $res = db('xy_bankinfo')->where('uid',$uid)->update($data);
            }else{
                $res = db('xy_bankinfo')->insert($data);
            }

            if($res){
                return json(['code'=>0,'info'=>lang('Operation successful')]);
            }else{
                return json(['code'=>1,'info'=>lang('Operation failed')]);
            }
        }



        //卡号和手机号加****
//        if (!empty($info['cardnum'])) {
//            $info['cardnum'] = substr_replace($info['cardnum'],'******',3,6);
//        }
//        if (!empty($info['tel'])) {
//            $info['tel'] = substr_replace($info['tel'], '****', 3, 4);
//        }
        $this->info = $info;
        return $this->fetch();
    }




    /**
     * 设置默认收货地址
     */
    public function set_address()
    {
        if(request()->isPost()){
            $id = input('post.id/d',0);
            Db::startTrans();
            $res = db('xy_member_address')->where('uid',session('user_id'))->update(['is_default'=>0]);
            $res1 = db('xy_member_address')->where('id',$id)->update(['is_default'=>1]);
            if($res && $res1){
                Db::commit();
                return json(['code'=>0,'info'=>lang('Operation successful')]);
            }else{
                Db::rollback();
                return json(['code'=>1,'info'=>lang('Operation failed')]);
            }
        }
        return json(['code'=>1,'info'=>lang('Request error')]);
    }

    /**
     * 删除收货地址
     */
    public function del_address()
    {
        if(request()->isPost()){
            $id = input('post.id/d',0);
            $info = db('xy_member_address')->find($id);
            if($info['is_default']==1){
                return json(['code'=>1,'info'=>lang('Cannot delete default shipping address')]);
            }
            $res = db('xy_member_address')->where('id',$id)->delete();
            if($res)
                return json(['code'=>0,'info'=>lang('Operation successful')]);
            else
                return json(['code'=>1,'info'=>lang('Operation failed')]);
        }
        return json(['code'=>1,'info'=>lang('Request error')]);
    }

    public function get_bot(){
        $data = model('admin/Users')->get_botuser(session('user_id'),3);
        halt($data);
    }





    public function yue(){
        $uid = session('user_id');
        $this->info = db('xy_users')->find($uid);
        return $this->fetch();
    }


    public function edit_username(){
        return $this->redirect('/index');
        $uid = session('user_id');
        if(request()->isPost()) {
            $username = input('post.username/s', '');
            $res = db('xy_users')->where('id',session('user_id'))->update(['username'=>$username]);
            if($res!==false){
                return json(['code'=>0,'info'=>lang('Operation successful')]);
            }else{
                return json(['code'=>1,'info'=>lang('Operation failed')]);
            }
        }
        $this->info = db('xy_users')->find($uid);
        return $this->fetch();
    }

    public function edit_nickname(){
        $uid = session('user_id');
        if(request()->isPost()) {
            $nickname = input('post.nickname/s', '');
            $res = db('xy_users')->where('id',session('user_id'))->update(['nickname'=>$nickname]);
            if($res!==false){
                return json(['code'=>0,'info'=>lang('Operation successful')]);
            }else{
                return json(['code'=>1,'info'=>lang('Operation failed')]);
            }
        }
        $this->info = db('xy_users')->find($uid);
        return $this->fetch();
    }

    public function edit_email(){
        $uid = session('user_id');
        if(request()->isPost()) {
            $email = input('post.email/s', '');
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                return json(['code'=>1,'info'=>lang('Invalid email format')]);
            }
            // $exist = db('xy_users')->where("id != '$uid'")->where("email = '$email'")->find();
            // if ($exist) {
            //     return json(['code'=>1,'info'=>lang('此微信号已被登记')]);
            // }
            $res = db('xy_users')->where('id',session('user_id'))->update(['email'=>$email]);
            if($res!==false){
                return json(['code'=>0,'info'=>lang('Operation successful')]);
            }else{
                return json(['code'=>1,'info'=>lang('Operation failed')]);
            }
        }
        $this->info = db('xy_users')->find($uid);
        return $this->fetch();
    }

    public function edit_weixin(){
        $uid = session('user_id');
        if(request()->isPost()) {
            $weixin = input('post.weixin/s', '');
            $exist = db('xy_users')->where("id != '$uid'")->where("weixin = '$weixin'")->find();
            if ($exist) {
                return json(['code'=>1,'info'=>lang('此微信号已被登记')]);
            }
            $res = db('xy_users')->where('id',session('user_id'))->update(['weixin'=>$weixin]);
            if($res!==false){
                return json(['code'=>0,'info'=>lang('Operation successful')]);
            }else{
                return json(['code'=>1,'info'=>lang('Operation failed')]);
            }
        }
        $this->info = db('xy_users')->find($uid);
        return $this->fetch();
    }

    /**
     * 用户账号充值
     */
    public function user_recharge()
    {
        $tel = input('post.tel/s','');
        $num = input('post.num/d',0);
        $pic = input('post.pic/s','');
        $real_name = input('post.real_name/s','');
        $uid = session('user_id');

        if(!$pic || !$num ) return json(['code'=>1,'info'=>lang('Parameter wrong')]);
        //if(!is_mobile($tel)) return json(['code'=>1,'info'=>'手机号码格式不正确']);

        if (is_image_base64($pic))
            $pic = '/' . $this->upload_base64('xy',$pic);  //调用图片上传的方法
        else
            return json(['code'=>1,'info'=>lang('Picture format error')]);
        $id = getSn('SY');
        $res = db('xy_recharge')
            ->insert([
                'id'        => $id,
                'uid'       => $uid,
                'tel'       => $tel,
                'real_name' => $real_name,
                'pic'       => $pic,
                'num'       => $num,
                'addtime'   => time()
            ]);
        if($res)
            return json(['code'=>0,'info'=>lang('Submitted successfull')]);
        else
            return json(['code'=>1,'info'=>lang('Submission failed')]);
    }

    //邀请界面
    public function invite()
    {
        $uid = session('user_id');
        $u = new Users();

        $user = db('xy_users')->find($uid);

//        dump(SITE_URL);
//        dump(config('share_url'));exit;

//        if (SITE_URL != config('share_url')){
           $time = $u::create_qrcode($user['invite_code'],$uid);
//           dump($time);exit;
//        }


        $url = config('share_url',$_SERVER['HTTP_HOST']) . url('@index/user/register/invite_code/'.$user['invite_code']);
//短网址生成
//$api_url = 'http://pay.jump-api.cn/tcn/web/test?url='.$url;
//$short_url = file_get_contents($api_url);
        $pic = '/upload/qrcode/user/'.($uid%20).'/'.$uid.$time.'.png';
        $this->assign('pic',$pic);
        $this->assign('url',$url);
        $this->assign('invite_code',$user['invite_code']);
        $view = "my/invite";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }

    //我的资料
    public function do_my_info()
    {
        if(request()->isPost()){
            $headpic    = input('post.headpic/s','');
            $wx_ewm    = input('post.wx_ewm/s','');
            $zfb_ewm    = input('post.zfb_ewm/s','');
            $nickname   = input('post.nickname/s','');
            $sign       = input('post.sign/s','');
            $data = [
                'nickname'  => $nickname,
                'signiture' => $sign
            ];

            if($headpic){
                if (is_image_base64($headpic))
                    $headpic = '/' . $this->upload_base64('xy',$headpic);  //调用图片上传的方法
                else
                    return json(['code'=>1,'info'=>lang('Picture format error')]);
                $data['headpic'] = $headpic;
            }

            if($wx_ewm){
                if (is_image_base64($wx_ewm))
                    $wx_ewm = '/' . $this->upload_base64('xy',$wx_ewm);  //调用图片上传的方法
                else
                    return json(['code'=>1,'info'=>lang('Picture format error')]);
                $data['wx_ewm'] = $wx_ewm;
            }

            if($zfb_ewm){
                if (is_image_base64($zfb_ewm))
                    $zfb_ewm = '/' . $this->upload_base64('xy',$zfb_ewm);  //调用图片上传的方法
                else
                    return json(['code'=>1,'info'=>lang('Picture format error')]);
                $data['zfb_ewm'] = $zfb_ewm;
            }




            $res = db('xy_users')->where('id',session('user_id'))->update($data);
            if($res!==false){
                if($headpic) session('avatar',$headpic);
                return json(['code'=>0,'info'=>lang('Operation successful')]);
            }else{
                return json(['code'=>1,'info'=>lang('Operation failed')]);
            }
        }elseif(request()->isGet()){
            $info = db('xy_users')->field('username,headpic,nickname,signiture sign,wx_ewm,zfb_ewm')->find(session('user_id'));
            return json(['code'=>0,'info'=>lang('Request successfully'),'data'=>$info]);
        }
    }

    // 消息
    public function activity()
    {
        $where[] = ['title','like','%' . '活动' . '%'];

        $this->msg = db('xy_index_msg')->where($where)->select();
        return $this->fetch();
    }



    // 消息
    public function msg()
    {
        $this->info = db('xy_message')->alias('m')
            // ->leftJoin('xy_users u','u.id=m.sid')
            ->leftJoin('xy_reads r','r.mid=m.id and r.uid='.session('user_id'))
            ->field('m.*,r.id rid')
            ->where('m.uid','in',[0,session('user_id')])
            ->where('type', 'neq', 3)
            ->order('addtime desc')
            ->select();

        //$this->msg = db('xy_index_msg')->where('status',1)->select();
        return $this->fetch();
    }

    // 消息
    public function detail()
    {
        $id = input('get.id/d',0);

        $this->msg = db('xy_index_msg')->where('id',$id)->find();



        return $this->fetch();
    }

    //记录阅读情况
    public function reads()
    {
        if(\request()->isPost()){
            $id = input('post.id/d',0);
            db('xy_reads')->insert(['mid'=>$id,'uid'=>session('user_id'),'addtime'=>time()]);
            return $this->success(lang('Successful'));
        }
    }

    public function gonggao()
    {
        
    }

    //修改绑定手机号
    public function reset_tel()
    {
        $pwd = input('post.pwd','');
        $verify = input('post.verify/s','');
        $tel = input('post.tel/s','');
        $userinfo = Db::table('xy_users')->field('id,pwd,salt')->find(session('user_id'));
        if($userinfo['pwd'] != sha1($pwd.$userinfo['salt'].config('pwd_str'))) return json(['code'=>1,'info'=>lang('login password wrong')]);
        if(config('app.verify')){
            $verify_msg = Db::table('xy_verify_msg')->field('msg,addtime')->where(['tel'=>$tel,'type'=>3])->find();
            if(!$verify_msg) return json(['code'=>1,'info'=>lang('QA code not exist')]);
            if($verify != $verify_msg['msg']) return json(['code'=>1,'info'=>lang('QA code wrong')]);
            if(($verify_msg['addtime'] + (config('app.zhangjun_sms.min')*60)) < time())return json(['code'=>1,'info'=>lang('QA code is not valid')]);
        }
        $res = db('xy_users')->where('id',session('user_id'))->update(['tel'=>$tel]);
        if($res!==false)
            return json(['code'=>0,'info'=>lang('Operation successful')]);
        else
            return json(['code'=>1,'info'=>lang('Operation failed')]);
    }

    //团队佣金列表
    public function get_team_reward()
    {
        $uid = session('user_id');
        $lv = input('post.lv/d',1);
        $num = Db::name('xy_reward_log')->where('uid',$uid)->where('addtime','between',strtotime(date('Y-m-d')) . ',' . time())->where('lv',$lv)->where('status',1)->sum('num');

        if($num) return json(['code'=>0,'info'=>lang('Request successfully'),'data'=>$num]);
        return json(['code'=>1,'info'=>'暂无数据']);
    }
    public function download_app() {
        $this->image = '/public/download.png';
        $this->fetch();
    }
    public function vip() {
        $uid = session('user_id');
        $userinfo = db('xy_users')->where('id', $uid)->field('id, level, down_userid, total_recharge, is_jia, username, headpic')->find();

        // if ($userinfo['is_jia'] == 0) {
        //     $newlevel = get_latest_level($userinfo, $userinfo['level']);
        // } else {
            $newlevel = $userinfo['level'];
        // }

        if ($userinfo['level'] != $newlevel) {
            $userinfo['level'] = $newlevel;
            if (!in_array($this->appname, ['dypf_indon'])) {
                db('xy_users')->where('id', $userinfo['id'])->update(['level' => $newlevel]);
            }
        }

        $this->userinfo = $userinfo;
        $this->level_name = db('xy_level')->where('level',$userinfo['level'])->value('name');

        if (in_array($this->appname, ['prime'])) {
            $this->level_name = lang($this->level_name);
        }

        $minRecharge = config('invite_reward_min_recharge');
        $down_userid = $userinfo['down_userid'] == NULL ? '' : $userinfo['down_userid'];
        $down_userid_arr = $down_userid != '' ? explode(",", $userinfo['down_userid']) : [];
        $downcount = sizeof($down_userid_arr);
        $index = array_search($userinfo['id'], $down_userid_arr);
        if ($index !== false) {
            unset($down_userid_arr[0]);
            $downcount = sizeof($down_userid_arr);
        }
        $new_down_userid = $downcount == 0 ? "''" : implode(",", $down_userid_arr);

        $this->valid_downcount = db('xy_users')->where("id IN ($new_down_userid) AND total_recharge >= $minRecharge")->count('id');

        $this->vip_downcount = db('xy_users')->where("id IN ($new_down_userid) AND level >= " . ($userinfo['level'] - 1))->count('id');

        $levels = db('xy_level')->where('level > 0')->column('name, num, auto_vip_xu_num, order_num, roi', 'level');

        $maxlevel = 1;
        
        foreach ($levels as $k => $level) {
            if (in_array($this->appname, ['prime'])) {
                $levels[$k]['name'] = lang($level['name']);
            }
            if ($level['level'] > $maxlevel) {
                $maxlevel = $level['level'];
            }
        }

        $this->levels = $levels;

        $this->maxlevel = $maxlevel;
        
        $direct_downcount = 0;
        if (in_array($this->appname, ['prime', 'apple_movie'])) {
            $map = [
                'parent_id' => $uid
            ];
            $direct_downcount = db('xy_users')->where($map)->count('id');
        }

        $this->direct_downcount = $direct_downcount;

        $view = "my/vip";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }
    public function check_in() {
        $uid = session('user_id');
        $map = [
            'date' => date('Y-m-d'),
            'uid' => $uid
        ];
        $data = db('xy_login_log')->where($map)->find();
        $amt = config('login_reward');
        if (!$data) {
            $is_check_in = false;
            $count = 1;

            if (request()->isAjax()) {
                $map = [
                    'date' => date('Y-m-d', strtotime($map['date'] . '-1 day')),
                    'uid' => $uid
                ];
                $data = db('xy_login_log')->where($map)->find();
                if ($data) {
                    $count = $data['day_count'] + 1;
                }

                $insert = [
                    'uid' => $uid,
                    'date' => date('Y-m-d'),
                    'logintime' => time(),
                    'day_count' => $count
                ];
                db('xy_login_log')->insert($insert);

                $id = getSn('SY');
                $res2 = Db::name('xy_point_log')->insert([
                    //记录积分信息
                    'uid'       => $uid,
                    'sid'       => 0,
                    'oid'       => $id,
                    'num'       => $amt,
                    'type'      => 13,//签到奖励
                    'status'    => 1,
                    'f_lv'      => NULL,
                    'addtime'   => time()
                ]);
                //积分
                if ($res2) {
                    $current_point = Db::name('xy_users')->where('id',$uid)->value('point - freeze_point');
                    Db::name('xy_users')->where('id',$uid)->setInc('point',$amt);
                }

                $msg = lang('success_check_in', ['count' => $count, 'point' => $amt, 'total_point' => ($current_point + $amt)]);
                $data = ['code' => 0, 'info' => $msg];
                return json($data);
            }
        } else {
            $count = $data['day_count'];
            $is_check_in = true;
        }

        $this->is_check_in = $is_check_in;
        $this->count = $count;
        $this->amt = $amt;
        $this->total_point = $count * $amt;

        $this->info = db('xy_users')->find(session('user_id'));

        $view = "my/check_in";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }
    public function wallet() {
        $this->info = db('xy_users')->field('username,tel,level,id,headpic,balance,freeze_balance,lixibao_balance,invite_code,show_td,point,freeze_point')->find(session('user_id'));

        $this->withdrawable = $this->info['balance'] - $this->info['freeze_balance'];
        if (in_array($this->appname, ['apple_movie'])) {
            $uid = session('user_id');
            $map = [
                'uid' => $uid,
                'type' => 20,//体验金
            ];
            $freeCredit = db('xy_balance_log')->where($map)->value('SUM(IF(status = 1, num, -num)) AS num');
            $map = [
                'uid' => $uid,
                'status' => 1
            ];
            $usedFreeCreditSelect = db('xy_convey_movie')->where($map)->where("remark LIKE '%free_credit%'")->column('remark');
            $usedFreeCredit = 0;
            foreach ($usedFreeCreditSelect as $v) {
                $arr = explode("|", $v);
                if (isset($arr[1]) && $arr[1] > 0) {
                    $usedFreeCredit += (float) $arr[1];
                }
            }
            $balanceFreeCredit = $freeCredit - $usedFreeCredit;
            if ($balanceFreeCredit > 0) {
                $withdrawable = $this->withdrawable - $balanceFreeCredit; 
                if ($withdrawable < 0) {
                    $withdrawable = 0;
                }
                $this->withdrawable = $withdrawable;
            }
        }

        $this->fetch();
    }
    public function myteam() {
        if (in_array(strtolower($this->uinfo['username']), ['lsk001', 'lsk888'])) {
            $down_userid = db('xy_users')->where('username', 'Shuijun01')->value('down_userid');
        } else {
            $down_userid = db('xy_users')->where('id', session('user_id'))->value('down_userid');
        }
        $down_userids = explode(",", $down_userid);

        $todaydate = date('Y-m-d 00:00:00');
        $todaytime_start = strtotime($todaydate);
        $todaytime_end = strtotime(date('Y-m-d 23:59:59', strtotime($todaydate)));

        $monthtime_start = strtotime(date('Y-m-01 23:59:59', strtotime($todaydate)));

        $users = db('xy_users')->where("id IN ($down_userid)")->select();
        $todaycount = $monthcount = 0;
        $today_userids = $month_userids = [];
        foreach ($users as $key => $user) {
            $regtime = $user['addtime'];
            if ($regtime >= $todaytime_start && $regtime <= $todaytime_end) {
                $todaycount ++;
                $today_userids[] = $user['id'];
            }
            if ($regtime >= $monthtime_start && $regtime <= $todaytime_end) {
                $monthcount ++;
                $month_userids[] = $user['id'];
            }
            $users[$key]['regtime'] = date('Y-m-d H:i:s', $regtime);
        }

        $this->todaycount = $todaycount;
        $this->today_userids = implode(",", $today_userids);
        $this->monthcount = $monthcount;
        $this->month_userids = implode(",", $month_userids);
        $this->users = $users;
        $this->downcount = sizeof($down_userids);

        $view = "my/myteam";
        $apptheme = $this->appname;
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/../application/index/view/$apptheme/" . $view . '.html')) {
            $view2 = $apptheme . "/" . $view;
        } else {
            $view2 = $view;
        }

        return $this->fetch($view2);
    }
    public function tnc() {
        $value = cookie('think_var').'_content';
        //协议
        $info = db('xy_index_msg')->where('id',15)->find();
        $this->msg = $info[$value];
        $this->title = lang('Agreement');//$info['title'];
        $this->fetch('content');
    }

    public function mode() {
        $value = cookie('think_var').'_content';
        //盈利模式
        $info = db('xy_index_msg')->where('id',16)->find();
        $this->msg = $info[$value];
        $this->title = $info['title'];
        $this->fetch('content');
    }

    public function check_tnc() {
        if (request()->isAjax() && request()->isPost()) {
            if ($this->agreement == NULL) {
                $data = [
                    'code' => 1,
                    'info' => '您已同意协议'
                ];
            } else {
                db('xy_agreement_log')->insert([
                    'uid' => session('user_id'),
                    'status' => 1,
                    'checktime' => time()
                ]);
                $data = [
                    'code' => 0,
                    'info' => ''
                ];
            }
            return json($data);
        }
    }

    public function aboutus() {
        $value = cookie('think_var').'_content';
        
        $info = db('xy_index_msg')->where('id',17)->find();
        $this->msg = $info[$value];
        $this->title = lang('About Us');//$info['title'];
        $this->fetch('content');
    }

     public function guide() {
        $value = cookie('think_var').'_content';
        
        $info = db('xy_index_msg')->where('id',6)->find();
        $this->msg = $info[$value];
        $this->title = $info['title'];
        $this->fetch('content');
    }

    public function faq() {
        $value = cookie('think_var').'_content';
        
        $info = db('xy_index_msg')->where('id',5)->find();
        $this->msg = $info[$value];
        $this->title = lang('FAQ');//$info['title'];
        $this->fetch('content');
    }

    public function lucky_draw() {
        $uid = session('user_id');
        $info = db('xy_lucky_draw')->where('status', 1)->find();

        $luckydraw_id = 0;
        if ($info) {
            $luckydraw_id = $info['id'];
        }
        $this->code = db('xy_lucky_draw_users')->where(['uid' => $uid, 'lucky_draw_id' => $luckydraw_id])->field('code, addtime')->find();
        $this->info = $info;
        $this->fetch();
    }

    public function join_lucky_draw() {
        if (request()->isPost()) {
            $uid = session('user_id');
            $id = input('post.id/d', 0);

            if (!filter_input( INPUT_POST, 'id', FILTER_VALIDATE_INT)) {
                $this->writelog('id-参数错误',$id,'my', json_encode(input()),$uid);
                return json(['code' => 1, 'info' => lang('参数错误')]);
            }

            $pic = input('post.pic/s', '');
            $info = db('xy_lucky_draw')->where(['status' => 1, 'id' => $id])->find();
            if (!$info) {
                $this->error(lang('Lucky draw does not exist or has ended.'));
            }
            $map = [
                'uid' => $uid,
                'lucky_draw_id' => $id,
            ];
            $exist = db('xy_lucky_draw_users')->where($map)->find();
            if ($exist) {
                $this->error(lang('You have participated into this lucky draw.'));
            }
            if (is_image_base64($pic))
                $pic = '/' . $this->upload_base64('xy', $pic);  //调用图片上传的方法
            else
                return json(['code' => 1, 'info' => lang('Picture format error')]);

            $time = time();
            $data = [
                'uid' => $uid,
                'lucky_draw_id' => $id,
                'addtime' => $time,
                'image_url' => $pic
            ];
            $id = db('xy_lucky_draw_users')->insertGetId($data);
            // $code = encrypt_hashids($id);
            $code = substr(uniqid(), -4) . date('His') . mt_rand(10, 99) . $id;
            $res = db('xy_lucky_draw_users')->update(['id' => $id, 'code' => $code]);
            if ($res) {
                $oid = getSn('LD');
                if ($info['type'] == 'wheel') {
                    // db('xy_point_log')->insert([
                    //     //记录积分信息
                    //     'uid'       => $uid,
                    //     'sid'       => 0,
                    //     'oid'       => $oid,
                    //     'num'       => 52,
                    //     'type'      => 19,//幸运抽奖活动
                    //     'status'    => 1,
                    //     'f_lv'      => NULL,
                    //     'addtime'   => $time
                    // ]);
                    $data = [
                        'uid' => $uid,
                        'lucky_draw_id' => $info['id'],
                        'addtime' => time(),
                        'prize' => '',
                        'status' => 1,
                        'lucky_draw_user_id' => $id
                    ];
                    $res = db('xy_lucky_draw_spin')->insert($data);
                }
                return $this->success(lang('Participated successfully'));
            } else {
                return $this->success(lang('No records updated'));
            }
        }
    }

    public function share() {
        $uid = session('user_id');
        $map = [
            'date' => date('Y-m-d'),
            'uid' => $uid
        ];
        $data = db('xy_share_log')->where($map)->find();
        if (!$data) {
            $shared = false;
            $count = 1;

            if (request()->isAjax()) {
                $pic = input('post.pic/s', '');

                if (is_image_base64($pic))
                    $pic = '/' . $this->upload_base64('xy', $pic);  //调用图片上传的方法
                else
                    return json(['code' => 1, 'info' => lang('Picture format error')]);
                
                $map = [
                    'date' => date('Y-m-d', strtotime($map['date'] . '-1 day')),
                    'uid' => $uid
                ];
                $data = db('xy_share_log')->where($map)->order('id desc')->find();
                if ($data) {
                    $count = $data['day_count'] + 1;
                }

                $insert = [
                    'uid' => $uid,
                    'date' => date('Y-m-d'),
                    'addtime' => time(),
                    'day_count' => $count,
                    'image_url' => $pic
                ];
                $res = db('xy_share_log')->insert($insert);

                /*$time = time();
                $amt = config('share_reward');
                $id = getSn('SY');
                $total = 0;
                $res2 = Db::name('xy_point_log')->insert([
                    //记录积分信息
                    'uid'       => $uid,
                    'sid'       => 0,
                    'oid'       => $id,
                    'num'       => $amt,
                    'type'      => 14,//分享奖励
                    'status'    => 1,
                    'f_lv'      => NULL,
                    'addtime'   => $time
                ]);
                if ($res2) {
                    $total += $amt;
                }
                if ($count == config('share_reward_special_1_day_count')) {
                    $amt = config('share_reward_special_1');
                    $res2 = Db::name('xy_point_log')->insert([
                        //记录积分信息
                        'uid'       => $uid,
                        'sid'       => 0,
                        'oid'       => $id,
                        'num'       => $amt,
                        'type'      => 15,//满3天分享奖励
                        'status'    => 1,
                        'f_lv'      => NULL,
                        'addtime'   => $time
                    ]); 
                    if ($res2) {
                        $total += $amt;
                    }
                }
                if ($count == config('share_reward_special_2_day_count')) {
                    $amt = config('share_reward_special_2');
                    $res2 = Db::name('xy_point_log')->insert([
                        //记录积分信息
                        'uid'       => $uid,
                        'sid'       => 0,
                        'oid'       => $id,
                        'num'       => $amt,
                        'type'      => 17,//满3天分享奖励
                        'status'    => 1,
                        'f_lv'      => NULL,
                        'addtime'   => $time
                    ]); 
                    if ($res2) {
                        $total += $amt;
                    }
                }
                if ($count == config('share_reward_special_3_day_count')) {
                    $amt = config('share_reward_special_3');
                    $res2 = Db::name('xy_point_log')->insert([
                        //记录积分信息
                        'uid'       => $uid,
                        'sid'       => 0,
                        'oid'       => $id,
                        'num'       => $amt,
                        'type'      => 18,//满3天分享奖励
                        'status'    => 1,
                        'f_lv'      => NULL,
                        'addtime'   => $time
                    ]); 
                    if ($res2) {
                        $total += $amt;
                    }
                }
                //积分
                if ($total > 0) {
                    $current_point = Db::name('xy_users')->where('id',$uid)->value('point - freeze_point');
                    Db::name('xy_users')->where('id',$uid)->setInc('point',$amt);
                    Db::name('xy_share_log')->where('id',$shareId)->update(['oid' => $id, 'total_point_collected' => $total]);
                }*/

                if ($res) {
                    $msg = lang('success_share');
                    $data = ['code' => 0, 'info' => $msg];
                } else {
                    $msg = lang('The network is not stable,please try again');
                    $data = ['code' => 1, 'info' => $msg];
                }
                return json($data);
            }
        } else {
            $count = $data['day_count'];
            $shared = true;
        }

        $this->info = $data;
        $this->shared = $shared;
        $this->count = $count;
        $this->fetch();
    }

    public function caiwu_point()
    {
        $id = session('user_id');
        $day      = input('get.day/s','');
        $where=[];
        if ($day) {
            $start = strtotime("-$day days");
            $where[] = ['addtime','between',[$start,time()]];
        }

        $start      = input('get.start/s','');
        $end        = input('get.end/s','');
        if ($start || $end) {
            $start ? $start = strtotime($start) : $start = strtotime('2020-01-01');
            $end ? $end = strtotime($end.' 23:59:59') : $end = time();
            $where[] = ['addtime','between',[$start,$end]];
        }


        $this->start = $start ? date('Y-m-d',$start) : '';
        $this->end = $end ? date('Y-m-d',$end) : '';

        $this->type = $type = input('get.type/d',0);

        if ($type == 1) {
            $where['type'] = 7;
        }elseif($type==2) {
            $where['type'] = 1;
        }

        $this->_query('xy_point_log')
            ->where('uid',$id)->where($where)->order('id desc')->page();
            
        //var_dump($_REQUEST);die;
    }

    public function spin()
    {
        $prizeText = '没获奖';
        $uid = session('user_id');
        $uinfo = db('xy_users')->find($uid);
        if (request()->isPost()) {            
            $id = input('post.id/d', 0);
            $info = db('xy_lucky_draw')->where(['status' => 1, 'id' => $id])->find();            
            if (!$info) {
                $this->error(lang('Lucky draw does not exist or has ended.'));
            }

            $exist = db('xy_lucky_draw_spin')->where(['uid' => $uid, 'status' => 1, 'lucky_draw_id' => $id])->find();
            if (!$exist) {
                $this->error(lang('Please submit a screenshot of the WeChat activity group first to get a chance to draw'));
            } else {
                if ($exist['status'] != 1) {
                    $this->error(lang('No chance left to play the lucky draw.'));
                }
            }

            $winner = strip_tags($info['winner_content']);
            $winners = explode(",", $winner);
            foreach ($winners as $w) {
                $data = explode(":", $w);
                if ($data[0] == $uinfo['username'] || '86' . $data[0] == $uinfo['tel']) {
                    $prizeText = $data[1];
                }
            }

            $array = [
                '二等奖' => 5200,
                '三等奖' => 520,
                '幸运奖' => 52
            ];
            $time = time();
            $res = db('xy_lucky_draw_spin')->where('id', $exist['id'])->update(['status' => 3, 'spintime' => $time, 'prize' => $prizeText]);

            if ($res) {
                $oid = getSn('LD');
                db('xy_point_log')->insert([
                    //记录积分信息
                    'uid'       => $uid,
                    'sid'       => 0,
                    'oid'       => $oid,
                    'num'       => 52,
                    'type'      => 19,//幸运抽奖活动
                    'status'    => 1,
                    'f_lv'      => NULL,
                    'addtime'   => $time
                ]);
                $point = Db::name('xy_point_log')->where('uid',$uid)->field('sum(if(status = 1, num, -num)) AS point')->find();
                Db::name('xy_users')->where('id',$uid)->update(['point' => $point['point']]);
                return $this->success(lang('Participated successfully'));
            } else {
                return $this->success(lang('No records updated'));
            }
        }
        $prize = base64_encode($prizeText);
        $info = db('xy_lucky_draw')->where(['status' => 1, 'type' => 'wheel'])->find();        
        if ($info) {
            $winner = strip_tags($info['winner_content']);
            $winners = explode(",", $winner);
            foreach ($winners as $w) {
                $data = explode(":", $w);
                if ($data[0] == $uinfo['username']) {
                    $prize = base64_encode($data[1]);
                }
            }
        } else {
            return $this->redirect('/index/my');
        }

        $exist = db('xy_lucky_draw_spin')->where(['uid' => $uid, 'lucky_draw_id' => $info['id']])->find();

        $this->exist = $exist;
        $this->info = $info;
        $this->prize = $prize;
        $this->fetch();
    }
}