<?php

namespace app\index\controller;

use think\Controller;
use think\Request;
use think\Db;

/**
 * 下单控制器
 */
class Redemption extends Base
{
    /**
     * 首页
     */
    public function index()
    {
        return $this->redirect(url('/index/shop'));
        return $this->fetch();
    }

    public function info()
    {
        return $this->fetch();
    }
}
