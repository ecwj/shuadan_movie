<?php
namespace app\index\controller;

use library\Controller;
use think\Db;

use library\tools\Node;
use think\facade\Request;
/**
 * 登录控制器
 */
class Report extends Controller
{
    public function __construct()
    {
        cookie('think_var','cn');
        if (empty(cookie('think_var'))){
            cookie('think_var','en');
        }
        $appname = config('app_name');
        $this->appname = $appname;
    }
    
    public function index()
    {
        $key = input('key');
        if (!$key || $key != sysconf('report_key')) {
            return $this->redirect('/');
        }
        
        $upusername = input('upusername');
        if ($upusername) {
            $down_userid = db('xy_users')->where("username = '$upusername'")->value('down_userid');
            if (!$down_userid) {
                $this->error('代理不存在');
            }
            
            $recharge_where[] = ["uid", "IN", "$down_userid"];
            $deposit_where[] = ["uid", "IN", "$down_userid"];
            $user_where[] = ["id", "IN", "$down_userid"];
        }
        
        $shuijun_down_userids = db('xy_users')->where("username IN ('testSJ','shuijun', 'Shuijun01')")->column('down_userid');
        $shuijun_down_userid = implode(",", $shuijun_down_userids);
        $recharge_where[] = ["uid", "NOT IN", "$shuijun_down_userid"];
        $deposit_where[] = ["uid", "NOT IN", "$shuijun_down_userid"];
        $user_where[] = ["id", "NOT IN", "$shuijun_down_userid"];
        
        $this->upusername = $upusername;
        $this->key = $key;
        
        $today_start = strtotime(date('Y-m-d 00:00:00'));
        $today_end = strtotime(date('Y-m-d 23:59:59'));
        $yes_start = strtotime(date('Y-m-d 00:00:00', strtotime(date('Y-m-d 00:00:00') . '-1 day')));
        $yes_end = strtotime(date('Y-m-d 23:59:59', $yes_start));
        $this_month_start = strtotime(date('Y-m-01 00:00:00'));
        $this_month_end = strtotime(date('Y-m-t 23:59:59'));
        
        $today_where[] = ['addtime', 'BETWEEN', "$today_start, $today_end"];
        $yes_where[] = ['addtime', 'BETWEEN', "$yes_start, $yes_end"];
        $this_month_where[] = ['addtime', 'BETWEEN', "$this_month_start, $this_month_end"];
        
        $today_where_trans[] = ['endtime', 'BETWEEN', "$today_start, $today_end"];
        $yes_where_trans[] = ['endtime', 'BETWEEN', "$yes_start, $yes_end"];
        $this_month_where_trans[] = ['endtime', 'BETWEEN', "$this_month_start, $this_month_end"];
        
        $uid = db('xy_users')->where("username IN ('supermovie')")->value('id');
        $recharge_where[] = ['uid', 'neq', $uid];
        $recharge_where[] = ["uid", "neq", "0"];
        $deposit_where[] = ['uid', 'neq', $uid];
        $user_where[] = ['id', 'neq', $uid];
        
        $recharge_where2 = "id NOT IN ('SY2107251151259242','SY2107251151002279','SY2107251150449815','SY2107251148427238','SY2107251148114087','SY2107251147578332','SY2107251147427923','SY2107251147287011','SY2107251147144023','SY2107251146569183','SY2107251145473196','SY2107251145252488','SY2107251144517484','SY2107251144243426','SY2107251143591316','SY2107251143368936','SY2107251143173945','SY2107251142539171','SY2107251142265092','SY2107251142076757','SY2107251141421445','SY2107251140389732','SY2107251140184928','SY2107251140024973','SY2107251139252880','SY2107251139062084','SY2107251138217953','SY2107251052198794','SY2107251051251526','SY2107251051251583','SY2107251051251704','SY2107251051251790','SY2107251051252006','SY2107251051252270','SY2107251051252546','SY2107251051253724','SY2107251051253847','SY2107251051254697','SY2107251051254865','SY2107251051255476','SY2107251051255771','SY2107251051255823','SY2107251051255870','SY2107251051256456','SY2107251051256539','SY2107251051256584','SY2107251051257184','SY2107251051257850','SY2107251051257984','SY2107251051258232','SY2107251051258793','SY2107251051259486','SY2107251051259733','SY2107251051259752','SY2107251051259901','SY2107251039421436','SY2107251039425007','SY2107251039425452','SY2107251039426377','SY2107251039427043','SY2107251039427068','SY2107251039427553','SY2107251039427719','SY2107251039427858','SY2107251039429470')";
        $this->todaycz = db('xy_recharge')->where($today_where_trans)->where($recharge_where)->where('status', 2)->where($recharge_where2)->sum('num');
        $this->todaytx = db('xy_deposit')->where($today_where_trans)->where($deposit_where)->where('status', 2)->sum('num');
        $this->todaymember = db('xy_users')->where($today_where)->where($user_where)->where("is_jia = 0 AND is_free = 0")->count('id');
        
        $this->yescz = db('xy_recharge')->where($yes_where_trans)->where($recharge_where)->where('status', 2)->where($recharge_where2)->sum('num');
        $this->yestx = db('xy_deposit')->where($yes_where_trans)->where($deposit_where)->where('status', 2)->sum('num');
        $this->yesmember = db('xy_users')->where($yes_where)->where($user_where)->where("is_jia = 0 AND is_free = 0")->count('id');
        
        $this->this_monthcz = db('xy_recharge')->where($this_month_where_trans)->where($recharge_where)->where('status', 2)->where($recharge_where2)->sum('num');
        $this->this_monthtx = db('xy_deposit')->where($this_month_where_trans)->where($deposit_where)->where('status', 2)->sum('num');
        $this->this_monthmember = db('xy_users')->where($this_month_where)->where($user_where)->where("is_jia = 0 AND is_free = 0")->count('id');
        
        $this->totalcz = db('xy_recharge')->where($recharge_where)->where('status', 2)->where($recharge_where2)->sum('num');
        $this->totaltx = db('xy_deposit')->where($deposit_where)->where('status', 2)->sum('num');
        $this->totalmember = db('xy_users')->where($user_where)->where("is_jia = 0 AND is_free = 0")->count('id');
        
        $today_manual_deduct = db('xy_balance_log')
            ->where('type = 16')
            ->where($today_where)
            ->where($recharge_where)
            ->where($recharge_where2)
            ->sum('num');
        $this->todaycz = ($today_manual_deduct > $this->todaycz) ? 0 : $this->todaycz - $today_manual_deduct;

        $month_manual_deduct = db('xy_balance_log')
            ->where('type = 16')
            ->where($this_month_where)
            ->where($recharge_where)
            ->where($recharge_where2)
            ->sum('num');
        $this->this_monthcz = ($month_manual_deduct > $this->this_monthcz) ? 0 : $this->this_monthcz - $month_manual_deduct;

        $yes_manual_deduct = db('xy_balance_log')
            ->where('type = 16')
            ->where($yes_where)
            ->where($recharge_where)
            ->where($recharge_where2)
            ->sum('num');
        $this->yescz = ($yes_manual_deduct > $this->yescz) ? 0 : $this->yescz - $yes_manual_deduct;

        $total_manual_deduct = db('xy_balance_log')
            ->where('type = 16')
            ->where($recharge_where)
            ->where($recharge_where2)
            ->sum('num');
        $this->totalcz = ($total_manual_deduct > $this->totalcz) ? 0 : $this->totalcz - $total_manual_deduct;
        return $this->fetch();
    }
}