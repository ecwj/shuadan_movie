<?php

namespace app\index\controller;

use think\Controller;
use think\Request;
use think\Db;

/**
 * 下单控制器
 */
class RotOrder extends Base
{
    /**
     * 首页
     */
    public function index()
    {
//        date_default_timezone_set("Asia/Calcutta");
        $where = [
            ['uid', '=', session('user_id')],
            ['addtime', 'between', strtotime(date('Y-m-d')) . ',' . time()],
        ];
        $this->day_deal = Db::name('xy_convey')->where($where)->where('status', 'in', [1, 3, 5])->where('c_status', '=', 1)->sum('commission');
        $yes1 = strtotime(date("Y-m-d 00:00:00", strtotime("-1 day")));
        $yes2 = strtotime(date("Y-m-d 23:59:59", strtotime("-1 day")));
        $this->yes_team_num = Db::name('xy_balance_log')->where('uid', session('user_id'))->where('addtime', 'between', [$yes1, $yes2])->where(['status' => 1, 'type' => 6])->sum('num');
        $this->yes_user_yongjin = db('xy_convey')->where('uid', session('user_id'))->where('status', 1)->where('addtime', 'between', [$yes1, $yes2])->where('c_status', '=', 1)->sum('commission');
        $this->user_yongjin = db('xy_convey')->where('uid', session('user_id'))->where('status', 1)->where('c_status', '=', 1)->sum('commission');

        //分类
        $type = input('get.type/d', 1);

        $cate = Db::name('xy_goods_cate')->alias('c')
            ->leftJoin('xy_level u', 'u.id=c.level_id')
            ->field('c.id,c.name,c.cate_info,c.cate_pic,u.name as levelname,c.status as cate_status,u.pic,u.level,u.bili,u.order_num')
            ->find($type);
        $this->cate = $cate;

        $member_level = db('xy_level')->order('level asc')->select();
        $order_num = $member_level[0]['order_num'];
//        $bili = ($member_level[0]['bili']*1000).'%';//会员等级
        $bili = db('xy_goods_cate')->where('id', $cate['id'])->value('bili');//专区
//        dump($bili);
        $uinfo = db('xy_users')->field('balance,freeze_balance,deal_count,deal_time,level')->where('id', session('user_id'))->find();

//        dump($uinfo['deal_time']);
//        dump(date("Y-m-d H:i:s"));exit;
        $this->price = $uinfo['balance'];
        if ($uinfo['deal_time'] == strtotime(date("Y-m-d 00:00:00"))) {
            $this->day_d_count = $uinfo['deal_count'];
        } else {
            $this->day_d_count = 0;
        }
        $this->lock_deal = $uinfo['freeze_balance'];

        if (!empty($uinfo['level'])) {
            $order_num = db('xy_level')->where('level', $uinfo['level'])->value('order_num');
            //按会员等级分佣比例
//            $bili = db('xy_level')->where('level',$uinfo['level'])->value('bili');
            //$bili = rtrim ( $bili, '0' );

            //按专区算分佣比例
            $bili = db('xy_goods_cate')->where('id', $cate['id'])->value('bili');
            $bili = ($bili * 100);
        }
//        $bili = ($bili*100);
        $this->order_num = $order_num;
        $this->assign('bili', $bili);
//        dump($bili);
        return $this->fetch();
    }

    /**
     *提交抢单
     */
    public function submit_order()
    {
        $tmp = $this->check_deal();
        if ($tmp) return json($tmp);
        $uid = session('user_id');
        if ($uid != 34 && $uid != 477 && $uid != 76) {
            $res = check_time(config('order_time_1'), config('order_time_2'));
            $str = config('order_time_1') . ":00  - " . config('order_time_2') . ":00";
            // if ($res) return json(['code' => 1, 'info' => lang('Prohibited in') . $str . lang('executes the current operation outside of a time period')]);
        }

        //判断用户金钱是否支持通道
        $user_balance = db('xy_users')->where('id', $uid)->value('balance');
        $cate_min_money = config('cate_min_money');//通道最低金额
//        $cate_max_money = config('cate_max_money');
        $cate_id = input('get.cate_id/d');
        $cat_min = db('xy_goods_cate')->where('id', $cate_id)->value('min');
        $cat_max = db('xy_goods_cate')->where('id', $cate_id)->value('max');
//        if ($user_balance<$cate_min_money) return json(['code'=>1,'info'=>lang('Balance is not enough').'，'.lang('Match minimal amount').$cate_min_money]);
        if ($user_balance < $cat_min) return json(['code' => 1, 'info' => lang('Balance is not enough') . '，' . lang('Match minimal amount') . $cat_min]);//通道最低匹配金额
        // if ($user_balance > $cat_max) return json(['code' => 1, 'info' => lang('Your account balance is too high to allow access to this channel') . '!' . lang('Match maximum amount') . $cat_max]);//通道最高匹配金额


//        $add_id = db('xy_member_address')->where('uid',$uid)->value('id');//获取收款地址信息
//        if(!$add_id) return json(['code'=>1,'info'=>'还没有设置收货地址']);
        $res = db('xy_users')->where('id', $uid)->update(['deal_status' => 2]);//将账户状态改为等待交易
        if ($res === false) return json(['code' => 1, 'info' => lang('Matching failed')]);
        // session_write_close();//解决sleep造成的进程阻塞问题
        // sleep($sleep);
        //
        $cid = input('cid', 0);
        if ($cid == 0) {
            return json(['code' => 1, 'info' => lang('Network problem,Try againt') . $cid]);
        }
        $count = db('xy_goods_list')->where('cid', '=', $cid)->count();
        if ($count < 1) return json(['code' => 1, 'info' => lang('Matching failed,insufficient product inventory') . '2']);


        $res = model('admin/Convey')->create_order($uid, $cid);
//        $res = json_decode($res,true);
//        dump($res);exit;
        return json($res);
    }

    /**
     * 停止抢单
     */
    public function stop_submit_order()
    {
        $uid = session('user_id');
        $res = db('xy_users')->where('id', $uid)->where('deal_status', 2)->update(['deal_status' => 1]);
        if ($res) {
            return json(['code' => 0, 'info' => lang('Operation successful')]);
        } else {
            return json(['code' => 1, 'info' => lang('Operation failed')]);
        }
    }

    public function platform()
    {
        $uid = session('user_id'); //获取当前会员ID;
        $vipHook = 0;
        $vipHookFee = 0;

        //首先判断当前会员VIP等级输出对应等级类目
        $userInfo = db('xy_users')->where('id', $uid)->field('balance,level')->find();

        $level = $userInfo['level'];
        //获取当前完成总单量 判断会员等级是否需要进阶或可直接进阶
        /*$vipOrderNum = vip_num_order($uid, $userInfo['level'], $upgradeFee); //vip 限制到期数量

        //获取用户当前数量
        $checkOrderNum = db('xy_convey')->where(array(['uid','=',$uid],['status','=',1],['addtime','>=','1611072000']))->count();

        //预先计算差额
        $upgradeFee = $userInfo['balance'] - $upgradeFee;        

        //判断是否溢出数量 或等于数量
        if ($checkOrderNum >= $vipOrderNum && $upgradeFee < 0) {  //已经过期并且需要开通补差额
            $vipHook = 1;
            $vipHookFee = abs($upgradeFee);
        } elseif($checkOrderNum >= $vipOrderNum && $upgradeFee >= 0) { //满足升级余额
            $level = $level + 1;
            if(!db('xy_users')->where(['id' => $uid])->setField('level', $level)){
                // exit('system upgrade error');
            };
        }*/

        //有效下线
        $childCount = Db::name('xy_users')->where(['parent_id'=>$uid,'active'=>1])->count();
        //充值金额
        $totalRecharge = Db::name('xy_balance_log')->where(['uid'=>$uid,'type'=>1,'status'=>1])->sum('num');

        $nextLevelPackage = db('xy_level')->where(['level'=>($level + 1)])->find();
        if ($nextLevelPackage) {
            $packageMaxLevel = db('xy_level')->max('level');
            if ($childCount >= $nextLevelPackage['auto_vip_xu_num'] && $totalRecharge >= $nextLevelPackage['num']) {                
                $level = $level + 1 > $packageMaxLevel ? $packageMaxLevel : $level + 1;
                if(!db('xy_users')->where(['id' => $uid])->setField('level', $level)){
                    // exit('system upgrade error');
                };
            } else if ($childCount >= $nextLevelPackage['auto_vip_xu_num'] && $totalRecharge < $nextLevelPackage['num']) {
                $vipHook = 1;
                $vipHookFee = $nextLevelPackage['num'] - $totalRecharge;
                $level = $level + 1 > $packageMaxLevel ? $packageMaxLevel : $level + 1;
            }
        }

        //当前可操作栏目
        $this->cate = db('xy_goods_cate')->where(array(['status', '=', 1], ['level_id', '>=', $level]))->select();
        //当前会员等级
        $this->user_level = $level;
        //vip限制以及补充费用
        $this->vip_hook = $vipHook;
        $this->vip_hook_fee = $vipHookFee;
        //当前会员余额
        $this->user_balance = $userInfo['balance'];
        $this->fetch();
    }

}