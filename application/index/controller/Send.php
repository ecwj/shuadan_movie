<?php
namespace app\index\controller;

use library\Controller;
use think\facade\Request;
use think\db;

class Send extends Controller
{
    //获取验证码
    public function sendsms(Request $request)
    {
        $httpOrigin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
        $httpOrigin = strtolower(str_replace(['http://', 'https://'], '', $httpOrigin));
        if (!isset($_SERVER['HTTP_ORIGIN']) || strpos(strtolower($_SERVER['HTTP_ORIGIN']), 'nolan') === false) {
            $error = 'Unauthenticated.';
            return json(['code' => 1, 'info' => $error]);
        }

        $tel = Request::post('tel/s','');
        $tel = str_replace("+", "", $tel);
        $stel = $tel;
        $type = Request::post('type',1);
        // if(!is_mobile($tel)){
        //     return json(['code'=>1,'info'=>lang('Incorrect phone number format')]);
        // }


        if ($type == 4) {
            $res = check_time(config('tixian_time_1'), config('tixian_time_2'));
            $str = config('tixian_time_1') . ":00  - " . config('tixian_time_2') . ":00";
            if ($res) return json(['code' => 1, 'info' => lang('Prohibited in') . $str . lang('executes the current operation outside of a time period')]);
            $u = Db::table('xy_users')->where(['id'=>session('user_id')])->find();
        } else {
            if (!$tel) {
                return json(['code' => 1, 'info' => lang('Phone number format is incorrect')]);
            }
            $u = Db::table('xy_users')->where(['tel'=>$tel])->find();
        }
        
        if($type == 1){
            if($u){
                return json(['code'=>1,'info'=>lang('The phone number is already registered')]);
            }
            $u['country_code'] = Request::post('country_code');
        } else {
            if (!$u) {
                return json(['code'=>1,'info'=>lang('手机号不存在。')]);
            }
            if ($type == 4) {
                $tel = $u['tel'];
                $stel = $u['tel'];
                if ($u['country_code'] == 0) {
                    $u['country_code'] = '86';
                }
                if (substr($stel, 0, strlen($u['country_code'])) == $u['country_code']) {
                    $stel = substr($tel, strlen($u['country_code']));
                }
                if (substr($stel, 0, strlen($u['country_code'])) == $u['country_code']) {
                    $stel = substr($tel, strlen($u['country_code']));
                }
            } else {
                $stel = strlen($stel) > strlen($u['country_code']) ? substr($stel, strlen($u['country_code'])) : $stel;
            }
        }

        $res = Db::table('xy_verify_msg')->field('addtime,tel')->where(['tel'=>$tel])->find();
        $t = $res['addtime'] + 60;
        $t2 = time();
        if($res && (($t) > $t2))
            return json(['code'=>0,'info'=>lang('Can send just 1 message in 1 minutes'), 'data'=>($t-$t2)]);

        $time = date('YmdHis',time());
        $num = rand(10000,99999);
        //$msg = config('app.zhangjun_sms.content') . $num  . '，' . config('app.zhangjun_sms.min') . '分钟内有效！';
        //$result = \org\ZhangjunSms::sendsms(config('app.zhangjun_sms.userid'),$time,md5(config('app.zhangjun_sms.account').config('app.zhangjun_sms.pwd').$time),$tel,$msg);

        //$result = $this->smsbao($tel,$num);//短信宝
//        $result = $this->sendSmsCode($tel,$num);
//        $result = $this->whzt($tel,$num);
        // $result = $this->nxcloudSend($stel,$num);
        $result = $this->moduyunSend($stel,$u['country_code'],$num);

        if(!$res){
            $r = Db::table('xy_verify_msg')->insert(['tel'=>$tel, 'msg'=>$num, 'addtime'=>time(),'type'=>$type]);
        }else{
            $r = Db::table('xy_verify_msg')->where(['tel'=>$tel])->data(['msg'=>$num,'addtime'=>time(),'type'=>$type])->update();
        }

//        if($result['status'] == 1){  //发送成功
//        if($result == 'Success'){  //发送成功
        if($result == 0){  //发送成功
            if($r)
                return json(['code'=>0,'info'=>lang('Send successfully')]);
            else
                return json(['code'=>0,'info'=>lang('Send fail')]);
        }else
            return json(['code'=>0,'info'=>$result]);
    }


    public function smsbao($tel,$code)
    {
        //----------------短信宝---------------------
        $statusStr = array(
            "0" => "短信发送成功",
            "-1" => "参数不全",
            "-2" => "服务器空间不支持,请确认支持curl或者fsocket，联系您的空间商解决或者更换空间！",
            "30" => "密码错误",
            "40" => "账号不存在",
            "41" => "余额不足",
            "42" => "帐户已过期",
            "43" => "IP地址限制",
            "50" => "内容含有敏感词"
        );
        $smsapi = "http://api.smsbao.com/";
        $user = config('app.smsbao.user');       //短信平台帐号15196952584
        $pass = config('app.smsbao.pass') ;
        $pass = md5("$pass");   //短信平台密码
        $sign = config('app.smsbao.sign') ;
        $content = "【".$sign."】您的验证码为{$code}，验证码5分钟内有效。";
        $phone = $tel;//要发送短信的手机号码
        $sendurl = $smsapi . "sms?u=" . $user . "&p=" . $pass . "&m=" . $phone . "&c=" . urlencode($content);
        $result = file_get_contents($sendurl);
        if ($result == '0') {
            return ['status' => 1, 'msg' => "发送成功"];
        } else {
            return ['status' => 0, 'msg' => $statusStr[$result]];
        }

    }

/**
 * @description：新版短信发送接口http://www.smschinese.cn/
 * @param $phone
 * @param $code
 * @return array
 */
    function sendSmsCode($tel,$code){
        $statusStr = array(
            "-1"=>"没有该用户账户",
            "-2"=>"接口密钥不正确 [查看密钥]不是账户登陆密码",
            "-21"=>"MD5接口密钥加密不正确",
            "-3"=>"短信数量不足",
            "-11"=>"该用户被禁用",
            "-14"=>"短信内容出现非法字符",
            "-4"=>"手机号格式不正确",
            "-41"=>"手机号码为空",
            "-42"=>"短信内容为空",
            "-51"=>"短信签名格式不正确,接口签名格式为：【签名内容】",
            "-52"=>"短信签名太长,建议签名10个字符以内",
            "-6"=>"IP限制"
        );
        $smsapi = 'http://utf8.api.smschinese.cn/';
        $sign = config('app.smsbao.sign');
        $user = config('app.smsbao.user');
        $pass = config('app.smsbao.pass') ;
        $phone = $tel;
        $content = "您本次的验证码为：{$code}，验证码5分钟内有效。";
        $sendurl = $smsapi . "?Uid=" . $user . "&Key=" . $pass . "&smsMob=" . $phone . "&smsText=" . urlencode($content);
        $result = file_get_contents($sendurl);
        if ($result == '1') {
            return ['status' => 1, 'msg' => "发送成功"];
        } else {
            return ['status' => 0, 'msg' => $statusStr[$result]];
        }
    }

//    public function whzt($phone,$code){
//        $url = 'http://122.114.79.52:6688/sms.aspx';
//        $userid = '3814';
//        $account = 'ruifengdeyong666';
//        $password = 'abc112233';
//        $content = '【慧丰科技】您好，您的验证码'.$code.'，请注意保存！2分钟内有效。';
//
//        $send_url = $url."?action=send&userid=".$userid."&account=".$account."&password=".$password."&mobile=".$phone."&content=".$content."&sendTime=&extno=";
//        $xml = file_get_contents($send_url);
//        $xml =simplexml_load_string($xml); //xml转object
//        $xml= json_encode($xml);  //objecct转json
//        $result=json_decode($xml,true); //json转array
////        dump($result);
//        return $result['returnstatus'];
//
//    }

    public function posturl($url,$data){
//        $data  = json_encode($data);
//        dump($data);exit;
        $headerArray =array("Content-type:application/x-www-form-urlencoded;charset=utf-8;","Accept:application/json;charset=utf-8;");
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
        curl_setopt($curl, CURLOPT_POST, 1);
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl,CURLOPT_HTTPHEADER,$headerArray);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);
        return json_decode($output,true);
    }

    public function post($url, $post_data = '', $timeout = 5, $isjson = 0)
    {//curl
        $post_data  = json_encode($post_data);
        // dump($post_data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        if ($post_data != '') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_HEADER, false);
        if ($isjson === 1) {
            $header = ['Content-Type: application/json'];
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        $file_contents = curl_exec($ch);
        curl_close($ch);
        return json_decode($file_contents,true);
        return $file_contents;
    }

        public function singleSend($mobile,$code) {
        $apikey = '0ab1733c4a4ddc053e3fe21b9320a561';
        $text = 'Kode verifikasi Anda adalah '.$code.'.Jika Anda tidak beroperasi sendiri, abaikan pesan ini';
//            Kode verifikasi Anda adalah #code#. Jika Anda tidak beroperasi sendiri, abaikan pesan ini
//        $text = 'Your verification code is '.$code.'. If you are not operating by yourself, please ignore this message';
//        $param = [
//            'apikey' => $apikey,
//            'mobile' => '+620'.$mobile,
//            'text' => $text
//        ];
        $param = 'apikey='.urlencode($apikey).'&mobile='.urlencode($mobile).'&text='.$text;
        return $this->posturl("https://sms.yunpian.com/v2/sms/single_send.json", $param);
    }

    public function nxcloudSend($phone,$code){
//        cITnlvTI        DT7QPq5i
        $url = 'http://api2.nxcloud.com/api/sms/mtsend';
        $appkey = 'cITn1vTI';
        $secretkey = 'DT7QPq5i';
        $content = '[PART-Time Hero]Hello, your verification code '.$code.', please save it! Effective within 2 minutes.';
        $params = [
            'appkey' => $appkey,
            'secretkey' => $secretkey,
            'content'   => urlencode($content),
            'phone' =>  $phone
        ];
        $res = $this->posturl($url,$params);
        return $res['code'];


    }

    public function moduyunSend($phone, $country_code, $code) {
        $access_key = config('app.moduyun.key');
        $access_secret = config('app.moduyun.secret');
        $random = rand(100000,999999);
        $time = time();
        $sig = hash("sha256", "secretkey=$access_secret&random=$random&time=$time&mobile=$phone", false);
        $url = "https://live.moduyun.com/sms/v2/sendsinglesms?accesskey=$access_key&random=" . $random;
        // dump($url);
        $data = [
            "tel" => [ 
                "nationcode" => $country_code, //国家码
                "mobile" => $phone //手机号码
            ], 
            "signId" => config('app.moduyun.sign_id'),//"609b75727d196b12537dd461", //短信签名，如果使用默认签名，该字段可缺省
            "templateId" => config('app.moduyun.template_id'),//"609b84e47d196b12537dd471", //业务在控制台审核通过的模板ID
             //假定这个模板为：您的注册的验证码是:{1}，有效时间{2}分钟。
            "params" => [
                $code, 
                "2"
            ], //参数，分别对应上面假定模板的{1}，{2}
            "sig" => $sig, //app凭证，具体计算方式见下注
            "time" => $time, //unix时间戳，请求发起时间，如果和系统时间相差超过10分钟则会返回失败
            "extend" => "", //通道扩展码，可选字段，默认没有开通(需要填空)。
            //在短信回复场景中，moduyunserver会原样返回，开发者可依此区分是哪种类型的回复
            "ext" => "", //用户的session内容，moduyunserver回包中会原样返回，可选字段，不需要就填空。
            "type" => 0, //0:普通短信;1:营销短信（强调：要按需填值，不然会影响到业务的正常使用）
        ];
        $res = $this->post($url,$data,10,1);
        // dump($data);
        // dump($res);
        return $res['result'];
    }

    public function sendEmail() {
        $type = input('post.type');
        if ($type == 4) {
            $u = Db::table('xy_users')->where(['id'=>session('user_id')])->find();
            $email = $u['email'];

            $res = Db::table('xy_verify_msg')->field('addtime')->where(['email'=>$email])->find();
            $t = $res['addtime'] + 60;
            $t2 = time();
            if($res && (($t) > $t2))
                return json(['code'=>0,'info'=>lang('Each email must be sent one minute apart'), 'data'=>($t-$t2)]);

            $time = date('YmdHis',time());
            $num = rand(10000,99999);

            $subject = sysconf('site_name') . ' ' . lang('Verification code');
            $message = lang('verification_email_withdrawal', ['username' => $u['username'], 'code' => $num]);
            $result = sendEmail($subject, $email, $message);

            if($result == 1){  //发送成功
                if(!$res){
                    $r = Db::table('xy_verify_msg')->insert(['tel' => '', 'email'=>$email, 'msg'=>$num, 'addtime'=>time(),'type'=>$type]);
                }else{
                    $r = Db::table('xy_verify_msg')->where(['email'=>$email])->data(['msg'=>$num,'addtime'=>time(),'type'=>$type])->update();
                }

                if($r)
                    return json(['code'=>0,'info'=>lang('Verification code has been sent to your email')]);
                else
                    return json(['code'=>0,'info'=>lang('Send fail')]);
            }else
                return $result;
        } else {
            return;
        }
    }

}
