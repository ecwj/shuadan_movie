<?php

namespace app\index\controller;

use library\Controller;
use think\Db;

/**
 * 商城
 * Class Index
 * @package app\index\controller
 */
class Shop extends Base
{
    public $table_goods;

    function __construct() {
//        date_default_timezone_set("Asia/Calcutta");
        parent::__construct();

        $this->table_goods = 'xy_goods_list_cn';
        if (in_array(config('app_name'), ['apple_movie'])) {
            $this->table_goods = 'xy_shop_goods_list';
        }
    }
    /**
     * 入口跳转链接
     */
    public function index2()
    {
        $this->redirect('home');
    }

    public function index()
    {
        // $this->redirect('/');
        $this->banner = Db::name('xy_mall_banner')->select();
        //if($this->banner) $this->banner = explode('|',$this->banner);
        $this->gundong = null;//db('xy_index_msg')->where('id',8)->value(cookie('think_var') . '_content');
        $this->shoplist = db('xy_shop_goods_list')->where('is_tj',1)->limit(10)->select();;


        // $this->assign('pic','/upload/qrcode/user/'.(session('user_id')%20).'/'.session('user_id').'-1.png');
        $this->cate = db('xy_shop_goods_cate')->order('id asc')->select();

        return $this->fetch('/shop/index');
    }

    public function index1215()
    {
        // $this->redirect('/');
        $this->banner = Db::name('xy_mall_banner')->select();
        //if($this->banner) $this->banner = explode('|',$this->banner);
        // $this->gundong = db('xy_index_msg')->where('id',8)->value(cookie('think_var') . '_content');
        // $this->shoplist = db('xy_shop_goods_list')->where('is_tj',1)->limit(10)->select();;



        // $this->assign('pic','/upload/qrcode/user/'.(session('user_id')%20).'/'.session('user_id').'-1.png');
        // $this->cate = db('xy_shop_goods_cate')->order('id asc')->select();

        //一天的
        // $this->lixibao = db('xy_lixibao_list')->order('id asc')->find();

        $param = [];
        // $data = http_curl($this->haojingke_url . 'cats', $param);

        // $reData = json_decode($data,1);
        // if($reData == NULL || empty($reData) || !isset($reData['data'])) {
        //     $list = [];
        // } else {
        //     $list = $reData;
        // }
        // $datas = $list['data']['goods_cats_list'];
        /*1）美妆工具
2）电脑 / 平板
3）手机
4）服饰 / 包包
5）腕表
6）生活电器
7）家居娱乐
8）豪华房车*/
        $cate = [
            [
                'cat_name' => '美妆工具',
                'level' => '1',
                'cat_id' => '18482',
                'parent_cat_id' => 0,
                'img' => '/public/img/new/makeup kit.jpg'
            ],
            // [
            //     'cat_name' => '美容护肤/美体/精油',
            //     'level' => '1',
            //     'cat_id' => '18637',
            //     'parent_cat_id' => 0
            // ],
            [
                'cat_name' => '腕表',
                'level' => '1',
                'cat_id' => '17455',
                'parent_cat_id' => 0,
                'img' => '/public/img/new/watch.jpg'
            ],
            [
                'cat_name' => '电脑 / 平板',
                'level' => '1',
                'cat_id' => '5851',
                'parent_cat_id' => 0,
                'img' => '/public/img/new/laptop tablet.jpg'
            ],
            [
                'cat_name' => '生活电器',
                'level' => '1',
                'cat_id' => '6128',
                'parent_cat_id' => 0,
                'img' => '/public/img/new/Household appliances.jpg'
            ],
            [
                'cat_name' => '服饰 / 包包',
                'level' => '1',
                'cat_id' => '1',
                'parent_cat_id' => 0,
                'img' => '/public/img/new/bag clothes.jpg'
            ],
            [
                'cat_name' => '豪华房车',
                'level' => '1',
                'cat_id' => '2',
                'parent_cat_id' => 0,
                'img' => '/public/img/new/car house.jpg'
            ],
            [
                'cat_name' => '手机',
                'level' => '1',
                'cat_id' => '3',
                'parent_cat_id' => 0,
                'img' => '/public/img/new/phone.jpg'
            ],
            [
                'cat_name' => '家居娱乐',
                'level' => '1',
                'cat_id' => '4',
                'parent_cat_id' => 0,
                'img' => '/public/img/new/Home Entertainment.jpg'
            ],

        ];
        // foreach ($datas as $d) {
        //     if (in_array($d['cat_name'], ['彩妆/香水/美妆工具', '美容护肤/美体/精油', '手机', '电脑', '生活电器'])) {
        //         $cate[] = $d;
        //     }
        // }
        $this->cate = $cate;

        if (request()->isAjax()) {
            return json($this->cate);
        }

        return $this->fetch('/shop/index');
    }


    public function goodslist()
    {
        $where = [];
        // if(input('cid/d',0))$where[] = ['cid','=',input('cid/d',0)];
        // if(input('name/s',''))$where[] = ['goods_name','like','%' . input('name/s','') . '%'];
        $name = input("name/s", '');

        //一天的
        $page = input('page/i',1);
        $cat_id = input('cat_id/i','');
        $where[] = ['cat_id','=',input('cat_id/d',0)];
        $where2 = "";

        $mall_price_range = sysconf('mall_price_range');
        if ($mall_price_range) {
            $arr = explode("-", $mall_price_range);
            if ($arr[1] > $arr[0]) {
                $param['minprice'] = $arr[0];
                $param['maxprice'] = $arr[1];
                $where2 = "goods_price BETWEEN " . $arr[0] . " AND " . $arr[1];
            }
        }
        
        $this->cat_name = input('cat_name/s', '');

        // $reData = json_decode($data,1);
        // if($reData == NULL || empty($reData) || !isset($reData['data'])) {
        //     $list = [];
        // } else {
        //     $list = $reData;
        // }
        // $this->list = $list['data']['goods_list'];
        $this->cat_id = $cat_id;
        $limit = 20;
        $offset = (input('page',1) - 1) * $limit;
        if (request()->isAjax()) {
            $this->list = db($this->table_goods)->where($where)->where($where2)->limit($offset, $limit)->order('goods_price desc')->select();
            return json($this->list);
        }
        $this->list = db($this->table_goods)->where($where)->where($where2)->limit($limit)->order('goods_price desc')->select();
        return $this->fetch();
    }


    public function orderlist()
    {
        $where = [];
        if(input('cid/d',0))$where[] = ['cid','=',input('cid/d',0)];
        if(input('name/s',''))$where[] = ['goods_name','like','%' . input('name/s','') . '%'];

        //一天的
        if(request()->isPost()) {
            $uid = session('user_id');
            $page = input('post.page/d',1);
            $num = input('post.num/d',10);
            $limit = ( (($page - 1) * $num) . ',' . $num );
            $status = input('post.status/d',1);

            $data = db('xy_shop_order')
                ->where('uid',session('user_id'))
                ->where('status',$status)
                ->order('addtime desc')
                ->limit($limit)
                ->select();

            foreach ($data as &$datum) {
                //$datum['endtime'] = date('Y/m/d H:i:s',$datum['endtime']);
                $datum['addtime'] = date('Y/m/d H:i:s',$datum['addtime']);
            }


            if(!$data) json(['code'=>1,'info'=>'暂无数据']);
            return json(['code'=>0,'info'=>'请求成功','data'=>$data]);
        }
        return $this->fetch();
    }


    public function detail()
    {
        $id      = input('get.id/d',1);

        $info = db($this->table_goods)->find($id);
        $info['goods_price'] = $info['goods_price'] * config('shop_price_multiple');
        $this->info = $info;

        // $param = [
        //     'goods_sign' => $goods_sign
        // ];
        // $data = http_curl($this->haojingke_url . 'goodsdetail', $param);

        // $reData = json_decode($data,1);
        // if($reData == NULL || empty($reData) || !isset($reData['data'])) {
        //     $list = [];
        // } else {
        //     $list = $reData;
        // }
        // $data = $list['data'];
        // $data['price_after'] = $data['price_after'] * config('shop_price_multiple');
        // $this->info = $data;
        return $this->fetch();
    }

    public function order_info()
    {
        $uid = session('user_id');
        $id      = input('get.id/d',1);
        $this->endtime = date('Y/m/d H:i:s', time()+30*60);
        $this->address = db('xy_member_address')->where('uid',$uid)->find();
        $this->balance = db('xy_users')->where('id',$uid)->value('point - freeze_point');
        $goods = db($this->table_goods)->find($id);
        $goods['goods_price'] = $goods['goods_price'] * config('shop_price_multiple');
        $this->goods = $goods;
        // $goods_sign = input('get.goods_sign/s','');

        // $param = [
        //     'goods_sign' => $goods_sign
        // ];
        // $data = http_curl($this->haojingke_url . 'goodsdetail', $param);

        // $reData = json_decode($data,1);
        // if($reData == NULL || empty($reData) || !isset($reData['data'])) {
        //     $list = [];
        // } else {
        //     $list = $reData;
        // }

        // $data = $list['data'];
        // $data['price_after'] = $data['price_after'] * config('shop_price_multiple');
        // $this->goods = $data;

        return $this->fetch();
    }



     public function order_detail()
    {
        $uid = session('user_id');
        $id      = input('get.oid/s',1);
        $this->endtime = date('Y/m/d H:i:s', time()+3*24*60*60);
        $this->address = db('xy_member_address')->where('uid',$uid)->find();
        $this->balance = db('xy_users')->where('id',$uid)->value('point - freeze_point');

        $order = db('xy_shop_order')->where('id',$id)->find();
//ar_dump($order);die;

        if ($order['platform'] == 'haojingke') {
            $goods_sign = $order['goods_sign'];

            $param = [
                'goods_sign' => $goods_sign
            ];
            $data = http_curl($this->haojingke_url . 'goodsdetail', $param);

            $reData = json_decode($data,1);
            if($reData == NULL || empty($reData) || !isset($reData['data'])) {
                $list = [];
            } else {
                $list = $reData;
            }

            $goods = $list['data'];
            $goods['goods_pic'] = $goods['picurl'];
            $goods['goods_price'] = $goods['price_after'] * config('shop_price_multiple');
            $this->goods = $goods;
        } else {
            $this->goods = db($this->table_goods)->find($order['gid']);
        }


        $this->order = $order;


        return $this->fetch();
    }


    //生成订单号
    function getSn($head='')
    {
        @date_default_timezone_set("PRC");
        $order_id_main = date('YmdHis') . mt_rand(1000, 9999);
        //唯一订单号码（YYMMDDHHIISSNNN）
        $osn = $head.substr($order_id_main,2); //生成订单号
        return $osn;
    }

    public function do_order()
    {
        $id = input('post.id','');
        $uid = session('user_id');
        $num = input('post.num',1);
        // $goods_sign = input('post.goods_sign', '');

        $goods = db($this->table_goods)->find($id);

        // $param = [
        //     'goods_sign' => $goods_sign
        // ];
        // $data = http_curl($this->haojingke_url . 'goodsdetail', $param);

        // $reData = json_decode($data,1);
        // if($reData == NULL || empty($reData) || !isset($reData['data'])) {
        //     $list = [];
        // } else {
        //     $list = $reData;
        // }

        // $goods = $list['data'];

        if (!$goods) {
            return json(['code'=>1,'info'=>'商品参数异常','data'=>[]]);
        }

        $goods['goods_price'] = $goods['goods_price'] * config('shop_price_multiple');
        if ($num <= 0) {
            return json(['code'=>1,'info'=>'商品参数异常','data'=>[]]);
        }



        if (!$num) return json(['code'=>1,'info'=>'参数异常','data'=>[]]);
        $balance = db('xy_users')->where('id',$uid)->value('point - freeze_point');
        if ( $balance < ($goods['goods_price']*$num) ) {
            return json(['code'=>1,'info'=>'可用积分余额不足','data'=>[]]);
        }

        if( $goods['goods_price']*$num < 0 ) {
            return json(['code'=>1,'info'=>'商品参数异常','data'=>[]]);
        }

        $id1 = getSn('SP');
        $data = [
            'id'        =>$id1,
            'uid'       => $uid,
            'gid'       => $id,
            'price'       => $goods['goods_price'],
            'num'      => $num,
            'price2'      => $goods['goods_price']*$num,
            'status'   => 1,
            'addtime'   => time(),
            'platform' => '',
            // 'goods_sign' => $goods_sign,
            'goods_name' => $goods['goods_name']
        ];
        $res = Db::name('xy_users')->where('id',$uid)->setDec('point',$goods['goods_price']*$num);

        //
        $res1 = Db::name('xy_point_log')->insert([
            //记录返佣信息
            'uid'       => $uid,
            'oid'       => $id1,
            'num'       => $goods['goods_price']*$num,
            'type'      => 20,
            'status' => 2,
            'addtime'   => time()
        ]);

        $res = db('xy_shop_order')->insert($data);
        if($res)
            return json(['code'=>0,'info'=>'操作成功']);
        else
            return json(['code'=>1,'info'=>'操作失败']);

    }




}
