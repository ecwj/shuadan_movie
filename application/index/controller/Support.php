<?php
namespace app\index\controller;

use think\Controller;
use think\Request;

class Support extends Base
{
    /**
     * 首页
     */
    public function index()
    {
		//对接客服开始
        //$uid = session('user_id');
        //$this->kfguest = db('xy_users')->find($uid);
        $kfguest = db('xy_users')->find(session('user_id'));
        if (!empty($kfguest['username'])) {
            $key = 'darren123';
            $this->token = encrypt($kfguest['tel'],$key);
        }
        $kfguest['type']='未知';
        if($kfguest['parent_id']==0 && $kfguest['is_jia']==0&& $kfguest['is_free']==0){$kfguest['type']='代';}
        if($kfguest['parent_id']<>0 && $kfguest['is_jia']==0&& $kfguest['is_free']==0){$kfguest['type']='会';}
        if($kfguest['parent_id']==0 && $kfguest['is_jia']==1&& $kfguest['is_free']==0){$kfguest['type']='虚';}
        if($kfguest['parent_id']==0 && $kfguest['is_jia']==1&& $kfguest['is_free']==1){$kfguest['type']='试';}

		$this->assign('kfguestname',$kfguest['username']);
		$this->assign('kfguesttel',$kfguest['tel']);
		$this->assign('kfguestpic',substr(strrchr($kfguest['headpic'], "/"), 1));
		$this->assign('kfguestid',$kfguest['id']);
		$this->assign('kfguesttype',$kfguest['type']);
		//对接客服结束
		
        $this->info = db('xy_cs')->where('status',1)->select();
        $this->assign('list',$this->info);

        $this->msg = db('xy_index_msg')->where('status',1)->select();
        return $this->fetch();
    }


    public function index2()
    {
        $this->url = isset($_REQUEST['url']) ? $_REQUEST['url'] :'';
        return $this->fetch();
    }

    /**
     * 常见问题
     */
    public function detail()
    {
        $value = cookie('think_var').'_content';
        $this->info = db('xy_index_msg')->where('id',5)->value($value);


        return $this->fetch();
    }




    /**
     * 换一个客服
     */
    public function other_cs()
    {
        $data = db('xy_cs')->where('status',1)->where('id','<>',$id)->find();
        if($data) return json(['code'=>0,'info'=>'请求成功','data'=>$data]);
        return json(['code'=>1,'info'=>'暂无数据']);
    }
}