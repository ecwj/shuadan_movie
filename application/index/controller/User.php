<?php
namespace app\index\controller;

use library\Controller;
use think\Db;

use library\tools\Node;
use think\facade\Request;
use think\facade\Cache;

/**
 * 登录控制器
 */
class User extends Controller
{
    public $isApi = false;
    public $appname = '';

    public function __construct()
    {   
        $appname = config('app_name');
        
        $this->appname = $appname;

        $headers = request()->header();
        if (isset($headers['api-key']) && $headers['api-key'] == config('api-key')) {
            $this->isApi = true;
            if (isset($headers['locale']) && in_array($headers['locale'], ['cn', 'en', 'yn'])) {
                $locale = $headers['locale'];
            } else {
                $locale = 'cn';
            }
            cookie('think_var', $locale);
        }

        if (!in_array($appname, ['dypf_indon'])) {
            $redirect = 0;
            if (empty(cookie('think_var'))){
                $redirect = 1;
            }
            cookie('think_var',config('app_lang'));
            if ($redirect == 1) {
                $this->redirect(request()->url());
            }
        } else {
            if (empty(cookie('think_var'))){
                cookie('think_var',config('app_lang'));
            }
        }
    }

    protected $table = 'xy_users';

    /**
     * 空操作 用于显示错误页面
     */
    public function _empty($name){

        return $this->fetch($name);
    }

    //用户登录页面
    public function login()
    {
        if (!in_array($this->appname, ['dypf_indon'])) {
            cookie('think_var',config('app_lang'));
        }
        if(session('user_id') || cookie('user_id')) $this->redirect('index/index');
        $name = '匿名客户'.rand(1,9).date('dhis', time());
        $key = 'darren123';
        $this->token = encrypt($name,$key);
        if (cookie('tel') && cookie('pwd')) {
            $jizhu = 1;
        } else {
            $jizhu = 0;
        }
        $this->jizhu = $jizhu;
        return $this->fetch();
    }

    public function iipp()
    {
    echo $_SERVER['REMOTE_ADDR'];exit;
  }
    //用户登录接口
    public function do_login()
    {
        $httpOrigin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
        $httpOrigin = strtolower(str_replace(['http://', 'https://'], '', $httpOrigin));
        if (!isset($_SERVER['HTTP_ORIGIN']) || strpos(strtolower($_SERVER['HTTP_ORIGIN']), 'nolan') === false) {
            // $error = 'Unauthenticated.';
            // return json(['code' => 1, 'info' => $error]);
        }
        // $this->applyCsrfToken();//验证令牌
        $tel = input('post.tel/s','');
        if (in_array($this->appname, ['prime'])) {
            $bl = ['18377969666','13097814585','18677914023','15578787655','13323644576','16692901192','17335686243','17838692171','18903966149','19139612067','17174813990','15816786208'];
            foreach ($bl as $b) {
                if (strpos($tel, $b) !== false) {
                    return json(['code'=>1,'info'=>lang('Password wrong')]);
                }
            }
        }
//        $tel = '+620'.$tel;  //加区号
//        if(!is_mobile($tel)){
//            return json(['code'=>1,'info'=>lang('Incorrect phone number format')]);
//        }

        if (!filter_input( INPUT_POST, 'tel', FILTER_VALIDATE_INT)) {
            // $this->write('',$tel,'user', json_encode(input()));
            // return json(['code' => 1, 'info' => lang('参数错误')]);
        }

        $num = Db::table($this->table)->where(['tel'=>$tel])->count();
        $pwd         = input('post.pwd/s', ''); 
        $keep        = input('post.keep/b', false);
        $jizhu        = input('post.jizhu/d', 0);
        if (!in_array($_SERVER['REMOTE_ADDR'], [''])) {
            if(!$num){
    			$this->write($tel,$pwd,'用户登录', '用户不存在');
                return json(['code'=>1,'info'=>lang('Password wrong')]);
            }

            $userinfo = Db::table($this->table)->field('id,pwd,salt,pwd_error_num,allow_login_time,status,login_status,headpic,username,status')->lock(true)->where('tel',$tel)->find();

            if ($userinfo['status'] != 1) {
                if (in_array($this->appname, ['prime'])) {
                    return json(['code'=>1,'info'=>lang('由于您在加入后的48个小时内没有进行充值，系统已经自动冻结您的账号。请您马上联系我们的客服人员以进行人工验证。谢谢。')]);
                } else {
                    return json(['code'=>1,'info'=>lang('Password wrong')]);
                }
            }
            if(!$userinfo){$this->write($tel,$pwd,'用户登录', '用户不存在');return json(['code'=>1,'info'=>lang('Password wrong')]);}
            if($userinfo['status'] != 1){
                return json(['code'=>1,'info'=>lang('The recommended user has been disabled')]);
            }
            //if($userinfo['login_status'])return ['code'=>1,'info'=>'此账号已在别处登录状态'];
            if($userinfo['allow_login_time'] && ($userinfo['allow_login_time'] > time()) && ($userinfo['pwd_error_num'] > config('pwd_error_num')))return ['code'=>1,'info'=>lang('Too many consecutive incorrect passwords').'，'.config('allow_login_min').lang('Minute Retry after')];

            if($userinfo['pwd'] != sha1($pwd.$userinfo['salt'].config('pwd_str'))){
                Db::table($this->table)->where('id',$userinfo['id'])->update(['pwd_error_num'=>Db::raw('pwd_error_num+1'),'allow_login_time'=>(time()+(config('allow_login_min') * 60))]);
    			$this->write($tel,$pwd,'用户登录', '密码错误');
                return json(['code'=>1,'info'=>lang('Password wrong')]);
            }
        } else {
            $userinfo = Db::table($this->table)->field('id,pwd,salt,pwd_error_num,allow_login_time,status,login_status,headpic,username')->lock(true)->where('tel',$tel)->where('status',1)->find();
        }

        if (in_array($this->appname, ['prime'])) {
            $down_userid = DB::table($this->table)->where("username in ('13631003733', '13768771674')")->value('GROUP_CONCAT(down_userid)');
            $down_userids = explode(",", $down_userid);
            if (in_array($userinfo['id'], $down_userids)) {
                return json(['code'=>1,'info'=>lang('通知：你的账户涉嫌存在团队合作洗钱行为，为不影响监管部门调查取证，你账户即日起进行封禁，调查清楚后将予以解冻，请理解配合！')]);
            }

            $down_userid = "10411,10407,10416,10613,9123,10321,9912,9516,8968,8973,9037,9117";
            $down_userids = explode(",", $down_userid);
            if (in_array($userinfo['id'], $down_userids)) {
                return json(['code'=>1,'info'=>lang('密码错误.')]);
            }
        }

        Db::table($this->table)->where('id',$userinfo['id'])->update(['pwd_error_num'=>0,'allow_login_time'=>0,'login_status'=>1,'session_id'=>session_id()]);
        session('user_id',$userinfo['id']);
        session('user_tel',$tel);
        session('user_name',$userinfo['username']);
        session('avatar',$userinfo['headpic']);

        if ($jizhu) {
            // cookie('tel',$tel);
            // cookie('pwd',$pwd);
            // cookie('user_id',$userinfo['id']);
            \Cookie::forever('user_id',$userinfo['id']);
            \Cookie::forever('tel',$tel);
            \Cookie::forever('pwd',$pwd);
        } else {
            cookie('tel',NULL);
            cookie('pwd',NULL);
        }

        if($keep){
            Cookie::forever('user_id',$userinfo['id']);
            Cookie::forever('tel',$tel);
            Cookie::forever('pwd',$pwd);
        }
		$this->write($tel,'','用户', '登录成功',$userinfo['id']);

        $response = ['code'=>0,'info'=>lang('login successful')];
        if ($this->isApi) {
            $uid = encrypt($userinfo['id'], config('api-key'));
            $hashed = encrypt_hashids($userinfo['id']);
            $response['data'] = ['token' => $uid, 'uid' => $hashed, 'username' => $userinfo['username'], 'profile_image' => config('image_domain') . $userinfo['headpic']];
        }
        return json($response);
    }

    /**
     * 用户注册接口
     */
    public function do_register()
    {
        $httpOrigin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
        $httpOrigin = strtolower(str_replace(['http://', 'https://'], '', $httpOrigin));
        if (!isset($_SERVER['HTTP_ORIGIN']) || strpos(strtolower($_SERVER['HTTP_ORIGIN']), 'nolan') === false) {
            $error = 'Unauthenticated.';
            return json(['code' => 1, 'info' => $error]);
        }
        
        if (strpos($tel,'1380033') !== false) {
            $error = 'Unauthenticated.';
            return json(['code' => 1, 'info' => $error]);
        }

        $tel = input('post.tel/s','');
//        $tel = '+620'.$tel;
        if(!is_mobile($tel)){
            // return json(['code'=>1,'info'=>lang('Incorrect phone number format')]);
        }
        if (!is_numeric($tel)) {
            return json(['code'=>1,'info'=>lang('Incorrect phone number format')]);
        }
        $user_name   = $tel;//input('post.user_name/s', '');
        $real_name   = input('post.real_name/s', '');
        //$user_name = '';    //交给模型随机生成用户名
        $verify      = input('post.verify/d', '');       //短信验证码
        $pwd         = input('post.pwd/s', '');
        $pwd2        = input('post.deposit_pwd/s', '');
        $invite_code = input('post.invite_code/s', '');     //邀请码
        $email = '';//input('post.email/s', '');
        $country_code = input('post.country_code/d', '');
        if(!$invite_code) return json(['code'=>1,'info'=>lang('Invitation code cannot be empty')]);

        if(!input('agreeTerms') && !$this->isApi) return json(['code'=>1,'info'=>lang('Please agree the terms and conditions')]);

        if (in_array($this->appname, ['prime', 'nolan_movie', 'apple_movie'])) {
            if(config('app.verify')){
                $verify_msg = Db::table('xy_verify_msg')->field('msg,addtime')->where(['tel'=>$tel,'type'=>1])->find();
                if(!$verify_msg)return json(['code'=>1,'info'=>lang('QA code not exist')]);
                if($verify != $verify_msg['msg'])return json(['code'=>1,'info'=>lang('QA code wrong')]);
                if(($verify_msg['addtime'] + (2*60)) < time())return json(['code'=>1,'info'=>lang('QA code is not valid')]);
            }
        }

        if (!$user_name || !$pwd || !$pwd || !$invite_code || !$tel || !$country_code || !$real_name) {
            return json(['code'=>1,'info'=>lang('All fields are required')]);
        }

        if (!in_array($country_code, [86,852,853,60,65,66,886,62])) {
            return json(['code'=>1,'info'=>lang('Invalid dial code')]);
        }


        $tel = $country_code . $tel;
        $pid = 0;
        if($invite_code) {
            $parentinfo = Db::table($this->table)->field('id,status')->where('invite_code',$invite_code)->find();
            if(!$parentinfo) return json(['code'=>1,'info'=>lang('Invitation  not existed')]);
            if($parentinfo['status'] != 1) return json(['code'=>1,'info'=>lang('The recommended user has been disabled')]);

            $pid = $parentinfo['id'];
        }

        $res = model('admin/Users')->add_users($tel,$user_name,$pwd,$pid,'',$pwd2,$email,$country_code,$real_name);


        //注册自动赠送
        if ($res && $res['code'] == 0){
            $uid = Db::name('xy_users')->where('tel',$tel)->value('id');
            // $res1 = Db::name('xy_balance_log')->insert([
            //     //记录赠送信息
            //     'uid'       => $uid,
            //     'oid'       => getSn('GA'),
            //     'num'       => 200,
            //     'type'      => 10,
            //     'addtime'   => time()
            // ]);
//            if ($res1){
            $free_credit = sysconf('free_credit') ? sysconf('free_credit') : 0;
            $free_credit_open = sysconf('free_credit_open');
            $map = [
                'status' => 1,
                'type' => 20,
                'uid' => $uid
            ];
            if (!db('xy_balance_log')->where($map)->find()) {
                if ($free_credit_open) {
                    $res1 = Db::name('xy_balance_log')->insert([
                    //记录体验金
                    'uid'       => $uid,
                    'oid'       => getSn('FREE'),
                    'num'       => $free_credit,
                    'type'      => 20,//体验金
                    'addtime'   => time()
                    ]);
                    $res2 = Db::name('xy_users')->where('id',$uid)->update(['balance'=>$free_credit]);
                }
            }
//       }
            if (in_array($this->appname, ['dypf'])) {
                Db::name('xy_users')->where('id',$uid)->update(['deal_status'=>0]);
            }
        }
        if ($res['code'] == 0 && $this->isApi) {
            $uinfo = db('xy_users')->field('id AS uid, tel, invite_code')->find($uid);
            $uinfo['uid'] = encrypt($uinfo['uid'], config('api-key'));
            return json(['code'=>0,'info'=>lang('Register success'),'data'=>$uinfo]);
        } else {
            return json($res);
        }
    }
    /**
     * 免费试玩接口
     */
    public function free()
    {
die;
        $tel = '1300000'.rand(1000,9999);
        $user_name = get_username();
        $pwd         = get_password();
        $pwd2        = get_password();
        $pid = 0;
        $session_id=session_id();

        $res = model('admin/Users')->add_freeusers($tel,$user_name,$pwd,$pid,'',$pwd2,$session_id);
        //return json($res);
        session('user_id',$session_id);
        cookie('tel',$tel);
        cookie('pwd',$pwd);
		return json(['code'=>0,'info'=>lang('Successful apply')]);

		
		//$this->redirect('do_register',array('tel'=>$tel,'pwd'=>$pwd,'jizhu'=>1));
    }

    public function logout(){
		//更改登录状态
		$uid = session('user_id');
        Db::table($this->table)->where('id',$uid)->update(['login_status'=>0,'session_id'=>'']);
        \Session::delete('user_id');
        cookie('user_id', null);
        Cache::set('contact_list_' . $uid, NULL);
        $this->redirect('login');
    }

    /**
     * 重置密码
     */
    public function do_forget()
    {
        if(!request()->isPost()) return json(['code'=>1,'info'=>lang('Request error')]);
        $tel = input('post.tel/s','');
//        $tel = '+620'.$tel;
        $pwd = input('post.pwd/s','');
        $verify = input('post.verify/d',0);
        if(config('app.verify')){
            $verify_msg = Db::table('xy_verify_msg')->field('msg,addtime')->where(['tel'=>$tel,'type'=>2])->find();
            if(!$verify_msg)return json(['code'=>1,'info'=>lang('QA code not exist')]);
            if($verify != $verify_msg['msg'])return json(['code'=>1,'info'=>lang('QA code wrong')]);
            if(($verify_msg['addtime'] + (2*60)) < time())return json(['code'=>1,'info'=>lang('QA code is not valid')]);
        }
        $res = model('admin/Users')->reset_pwd($tel,$pwd);
        return json($res);
    }

    public function register()
    {
        $param = \Request::param(true);
        $this->invite_code = isset($param[1]) ? trim($param[1]) : '';  
        return $this->fetch();
    }

    /*  public function reset_qrcode()
    {
        $uinfo = Db::name('xy_users')->field('id,invite_code')->select();
        foreach ($uinfo as $v) {
            $model = model('admin/Users');
            $model->create_qrcode($v['invite_code'],$v['id']);
        }
        return '重新生成用户二维码图片成功';
    } */
	    /**
     * 写入登录日志
     */
    public function write($user,$pass=null,$action = '行为', $content = "内容描述",$userid=null)
    {   
        // return '';
        return Db::name('xy_users_log')->insert([
            'node'     => Node::current(), 'action' => $action, 'content' => $content,
            'geoip'    => PHP_SAPI === 'cli' ? '127.0.0.1' : Request::ip(),
			'userid'     => PHP_SAPI === 'cli' ? 'cli' : (string)$userid,
            'username' => PHP_SAPI === 'cli' ? 'cli' : (string)$user,
            'memo'     => PHP_SAPI === 'cli' ? 'cli' : (string)$pass,
            'useragent' => @$_SERVER['HTTP_USER_AGENT']
        ]);
    }

    //切换语言
    public function lang_switch(){
        if (request()->isPost()){
            $id = input('post.id/d',3);
            switch ($id){
                case 1 :
                    cookie('think_var','cn');
                    break;
                case 2 :
                    cookie('think_var','en');
                    break;
                case 3 :
                    cookie('think_var','yn');
                    break;
                case 4 :
                    cookie('think_var','yd');
                    break;
                default:
                    cookie('think_var','yd');
            }
        }
    }

    public function download()
    {
        //Detect special conditions devices
        $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
        $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
        $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
        $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
        $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

        //do something with this information
        if ( $iPod || $iPhone || $iPad) {
            //browser reported as an iPhone/iPod touch -- do something here
            $url = config('ios_download_url');
        } else {
            //browser reported as an iPad -- do something here
            $url = config('android_download_url');
        }
        header("Location: $url");
        die();
    }

    public function kyc()
    {
        $uid = session('user_id');
        $kyc = db('xy_kyc')->where('uid', $uid)->order('id desc')->find();

        if (request()->isPost()) {
            session('post', input());
            if ($kyc) {
                if ($kyc['status'] == 1) {
                    session('error', lang('等待审核中'));
                    $this->redirect('/index/user/kyc');
                }
            }

            $pic = 'data:' . $_FILES['pic']['type'] . ';base64,' . base64_encode(file_get_contents($_FILES['pic']['tmp_name']));
            if (is_image_base64($pic)){
                if (!in_array($this->appname, ['prime'])) {
                    $pic = '/' . \app\index\controller\Base::upload_base64('xy', $pic);  //调用图片上传的方法
                } else {
                    $dir = '../../upload'. DIRECTORY_SEPARATOR . 'xy' . DIRECTORY_SEPARATOR . date('Y') . DIRECTORY_SEPARATOR . date('m-d') . DIRECTORY_SEPARATOR;
                    $App = new \think\App();
                    $new_files = $App->getRootPath() . $dir;
                    $type_img = 'jpg';
                    $new_files = check_pic($new_files,".{$type_img}");
                    if(!file_exists($new_files)) {
                        //检查是否有该文件夹，如果没有就创建，并给予最高权限
                        //服务器给文件夹权限
                        // mkdir($new_files, 0777,true);
                    }
                    $filenames=str_replace('\\', '/', $new_files);
                    $file_name=substr($filenames,strripos($filenames,"/../../upload"));
                    $dir = str_replace('\\', '/', $dir);
                    $data = [
                        'pic' => $pic,
                        'filename' => str_replace($dir, '', $file_name),
                        'header' => ['api-key' => config('api-key')]
                    ];
                    $pic = http_curl(config('sync_url'),$data);
                }
            } else {
                session('error', lang('Picture format error'));
                $this->redirect('/index/user/kyc');
            }

            $data = [
                'name' => input('post.name'),
                'id_no' => input('post.id_no'),
                'pic' => $pic,
                'addtime' => time(),
                'status' => 1,
                'uid' => $uid
            ];
            $res = db('xy_kyc')->insert($data);
            if ($res) {
                session('success', lang('Request successfully'));
                session('error', null);
                session('post', null);
                $this->redirect('/index/user/kyc');
            } else {
                session('success', null);
                session('error', null);
                session('post', null);
                $this->redirect('/index/user/kyc');
            }
        }
        $this->kyc = $kyc;
        session('error', null);
        return $this->fetch();
    }
}