<?php

return [
    'app_name' => 'apple_movie',
    'app_lang' => 'cn',
    // 应用调试模式
    'app_debug'                 => true,
    // 应用Trace调试
    'app_trace'                 => false,
    // 0按名称成对解析 1按顺序解析
    'url_param_type'            => 1,
    // 当前 ThinkAdmin 版本号
    'thinkadmin_ver'            => 'v',
    'empty_controller'          => 'Error',
    'pwd_str'                   => '!qws6F!xffD2vx80?95jt',  //盐
    'pwd_error_num'             => 10,    //密码连续错误次数
    'allow_login_min'           => 5,     //密码连续错误达到次数后的冷却时间，分钟
    'default_filter'            => 'strip_tags,htmlspecialchars,trim',
    //短信
    'smsbao' => [
        'user'=>'', //账号 
        'pass'=>'', //密码 
        'sign'=>'', //签名
        'url' => '',//短信接口
    ],
    //bi支付
    'bipay' => [
        'appKey' => '',          //bi支付 商户appkey
        'appSecret' => '', //密钥
    ],
    //paysapi支付
    'paysapi' => [
        'uid'    => '',          //bi支付 商户appkey
        'token'  => '', //密钥
        'istype' => 1, //默认支付方式  1 支付宝  2 微信  3 比特币
    ],

    'app_only' => 0,            //只允许app访问
    'vip_sj_bu'=> 1,            //vip升级 是否补交
    'app_url'=>'http://www.baidu.com',          //app下载地址
    'version'=>'001',  //版本号


    'verify'    => false,
    'min_time'=>'5',                    //匹配订单最小延迟
    'max_time'=>'10',                   //匹配订单最大延迟
    'min_recharge'=>'100000',              //最小充值金额
    'max_recharge'=>'5000000000',             //最大充值金额
    'deal_min_balance'=>'100',          //交易所需最小余额
    'deal_min_num'=>'10',               //匹配区间
    'deal_max_num'=>'47',               //匹配区间
    'deal_min_num1'=>'12',               //匹配区间
    'deal_max_num1'=>'66',               //匹配区间
    'deal_min_num2'=>'8',               //匹配区间
    'deal_max_num2'=>'49',               //匹配区间
    'deal_min_num3'=>'2',               //匹配区间
    'deal_max_num3'=>'19',               //匹配区间
    'deal_min_num4'=>'2',               //匹配区间
    'deal_max_num4'=>'15',               //匹配区间
    'deal_min_num5'=>'2',               //匹配区间
    'deal_max_num5'=>'12',               //匹配区间
    'deal_min_num6'=>'2',               //匹配区间
    'deal_max_num6'=>'10',               //匹配区间
    'deal_count'=>'30',                 //当日交易次数限制
    'deal_reward_count'=>'0',//'2',          //推荐新用户获得额外的交易次数
    'deal_timeout'=>'1000',              //订单超时时间
    'deal_feedze'=>'0',              //交易冻结时长
    'deal_error'=>'0',                  //允许违规操作次数
    'vip_1_commission'=>'0',          //交易佣金
    'min_deposit'=>'100',               //最低提现额度
    '1_reward'=>'0',                  //直推上级推荐奖励
    '2_reward'=>'0',                 //上两级推荐奖励
    '3_reward'=>'0',                 //上三级推荐奖励
    '1_d_reward'=>'0.15',               //上级会员获得交易奖励
    '2_d_reward'=>'0.10',               //上二级会员获得交易奖励
    '3_d_reward'=>'0.05',               //上三级会员获得交易奖励
    '4_d_reward'=>'0',               //上四级会员获得交易奖励
    '5_d_reward'=>'0',                  //上五级会员获得交易奖励
    'teambonus_wallet_min_required' => 1000, //钱包至少有多少余额才能获得团队奖励
    'invite_reward'=>'30',          //邀请奖励金额
    'invite_target_reward'=>'300',          //邀请满人金额
    'invite_target_reward_count'=>'15',          //邀请满人的人数
    '1_invite_reward'=>'50',               //上级会员获得邀请奖励积分
    '2_invite_reward'=>'30',               //上二级会员获得请奖励积分
    '3_invite_reward'=>'20',               //上三级会员获得请奖励积分
    'login_reward'=>'10',               //签到奖励
    'invite_reward_min_recharge'=>'300', //邀请奖励充值最低金额
    'invite_target_reward_min_recharge'=>'300', //邀请奖励充值最低金额
    'first_recharge_reward'=>'0', //首次充值300以上奖励
    'first_recharge_reward_min_amount'=>'300', //首次充值300以上奖励
    'vip_bonus_1' => 88,
    'vip_bonus_2' => 128,
    'vip_bonus_3' => 188,
    'vip_bonus_4' => 268,
    'vip_bonus_5' => 388,
    'master_cardnum'=>'尊敬的会员您好！每次充值前请联系在线客服获取最新充值渠道， 平台不定时更换充值渠道，擅自充值至停用账户，平台概不负责谢谢',             //银行卡号
    'master_name'=>'请联系在线客服获取充值卡号',                              //收款人
    'master_bank'=>'请联系在线客服获取充值卡号',                          //所属银行
    'master_bk_address'=>'请联系在线客服',         //银行地址
    'deal_zhuji_time'=>'5',         //远程主机分配时间
    'deal_shop_time'=>'5',          //等待商家响应时间
    'tixian_time_1'=>'10',           //提现开始时间
    'tixian_time_2'=>'02',          //提现结束时间

    'chongzhi_time_1'=>'9',           //充值开始时间
    'chongzhi_time_2'=>'23',          //充值结束时间
    'chongzhi_status'=>'0',         //充值随机小数',

    'order_time_1'=>'10',           //抢单结束时间
    'order_time_2'=>'24',          //抢单结束时间

    //利息宝
    'lxb_bili'=>'0.025',         //利息宝 日利率
    'lxb_time'=>'1',             //利息宝 转出到余额  实际 /小时
    'lxb_sy_bili1'=>'0',         //利息宝 上一级会员收益比例
    'lxb_sy_bili2'=>'0',         //利息宝 上一级会员收益比例
    'lxb_sy_bili3'=>'0',         //利息宝 上一级会员收益比例
    'lxb_sy_bili4'=>'0',         //利息宝 上一级会员收益比例
    'lxb_sy_bili5'=>'0',         //利息宝 上一级会员收益比例
    'lxb_ru_max'=>'100000000',         //利息宝 转入最大金额
    'lxb_ru_min'=>'100',         //利息宝 转入最低金额

    'home_div_status'=>'2',          //1首页显示用户信息，2显示轮播图
    'shop_status'=>'1',         //商城状态',


    'cate_min_money' => '20000', //订单最小金额
//    'cate_max_money' => '5000000', //订单最大金额

    'share_url'     => 'https://diyipiaofang.com',

    'lang_switch_on'    => true,
    'default_lang'  => 'zh-cn',
    'deal_recharge_status'  =>  1,
    'daifu_status'  =>  1,

    'deposit_type'  =>  'fengmang',  //card、fengmang、

    //基础货币
    'base_currency' => 'RP',//'₹',
    'apikey_haojingke' => 'c71a3523b9cefc5b',
    'haojingke_url' => 'http://api-gw.haojingke.com/index.php/v1/api/pdd/',

    'operation_start' => '10:00',
    'operation_end' => '21:59',
    'recharge_operation_start' => '10:00',
    'recharge_operation_end' => '23:59',
    'withdrawal_operation_start' => '10:00',
    'withdrawal_operation_end' => '00:00',
    'app_version' => '1.00',
    'base_price' => '30',
    'movie_start_date' => '2021-05-10 00:00:00',
    'ios_download_url' => '',
    'android_download_url' => '',

    'toppay' => [
        'status' => 0,
        'appid' => 'S820210624160005000002',
        'appKey' => 'MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAK0xHR0Ki00VwWuhLZUliahQdTlNLM21hEdihIimOtLhNHIDlCN0lYu7ovDU3NkaaTzMbR2upcXBmli5OdAtZvCyvcv+TKsXBVuE+X9lVm9qmSFWXOS+1OECTMIh/wfwIKEbBzmnSoK9tH2rnp6q+puIJ2zbhCSjqXktV2ZLqkQjAgMBAAECgYEAhfIcMB+9iMqwowsu89DqXDCN2NllM1uViJgbJIxbpkjihONbqOg72Kwv4s7eV1Jj3c/E/FXcg8uuEbx/iA/1MiAu9ncXCXl2rrJVpQgxljXbdd1Xo0LSE1yoQUJ3GIRjJtVj2aHC4mtUpOTZQtERmm9OwoEpk93M7NWrhz+/4UECQQDmGQtG/sJeWGW/agzYtOy+c70HSZFTPXVpX2FTzeIh0ZZ3ld6y9BUk6oiRvcytq4qVGikt4Ow6M/xOqL61gYqTAkEAwLAnbUiiHr/jp544CvS74SjwVzWq6AD4EFkZmNMO5gS4Jcm1B9nWIAgCXeZ6j2SvjAqt6kk4wi+K23fZj9cKMQJBAInyFbq4XNqm8EjzUURSI2SCip7l0gQvYkaL8hOF6L4Ror2K0fpDLrH1EW8edKjhoLGG/40LvVOfevHFZJ/4S5UCQHZTdlK42L2TTsSe94Fa2okhL74OOd5wweSgIoTjjvGXklG6NQ4WKlTPNkzdfQPTa63O3Q/TWSR+N4IgbhzTi/ECQEdV26y+fkvtgFLDKg70lfReuGGa92fefE606n3ghxjfD6TEnaBirsfJNIFrUTRXDhE8h7PZ51D+C0EFRDhsYO4='
    ],
    'ftpay' => [
        'status' => 1,
        'appid' => '2021078909',
        'appKey' => 'JfpweqlawTPPXgmPeSeJgviNJWFDaybM'
    ],
    'kefu_link' => 'https://support.dypf666.com',
    'livechat_id' => '12933528',
    'daifu_url' => 'https://openapi.pgpay.cc',
    'api-key' => '12586786742356',
    'sync_url' => '',
    'sync_url' => 'http://shuadan_movie.local/index/api2/upload',
    'sync_url' => 'https://image.primemovie.c/index/api2/upload',
    'shop_price_multiple' => '10',
    'image_upload' => '/read.php/?path=',
    'image_domain' => 'http://shuadan_movie.local',
    'chat_file_domain' => 'http://shuadan_movie.local/read.php/?path=',
    'share_reward' => '30',
    'share_reward_special_1' => '40',
    'share_reward_special_2' => '100',
    'share_reward_special_3' => '200',
    'share_reward_special_1_day_count' => '3',
    'share_reward_special_2_day_count' => '7',
    'share_reward_special_3_day_count' => '10',
    'moduyun' => [
        'key' => '612889c931a03f41e25e0a87',
        'secret' => 'fed095e76f0d4c7da3af3799bb62eb77',
        'sign_id' => '612a508b31a03f41e25e2fcb',
        'template_id' => '612a50ed31a03f41e25e2fd0'
    ]
];

