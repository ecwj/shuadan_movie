<?php
return [
    // cookie 保存时间
    'expire'    => 0,
    // cookie 启用安全传输
    'secure'    => false,
    // httponly 设置
    'httponly'  => true,
    // 是否使用 setcookie
    'setcookie' => true,
];
