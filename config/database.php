<?php
return [
    // 数据库调试模式
    'debug'       => false,
    // 数据库类型
    'type'        => 'mysql',
    // 服务器地址
    'hostname'    => 'localhost',
    // 数据库名
    'database'    => 'root_prime',
    // 用户名
    'username'    => 'root',
    // 密码
    'password'    => '',
    // 编码
    'charset'     => 'utf8mb4',
    // 端口
    'hostport'    => '3306',
    // 主从
    'deploy'      => 0,
    // 分离

    'rw_separate' => false,

];
