ALTER TABLE `xy_movie` ADD INDEX(`playtime_date`);
ALTER TABLE `xy_movie` ADD INDEX(`price`);
ALTER TABLE `xy_movie` ADD INDEX(`addtime`);
ALTER TABLE `xy_movie` ADD INDEX(`image_url`);
ALTER TABLE `xy_convey_movie` 
ADD INDEX `addtime` (`addtime` ASC) ,
ADD INDEX `num` (`num` ASC) ,
ADD INDEX `status` (`status` ASC) ,
ADD INDEX `c_status` (`c_status` ASC) ,
ADD INDEX `add_id` (`add_id` ASC) ,
ADD INDEX `goods_id` (`goods_id` ASC) ,
ADD INDEX `goods_count` (`goods_count` ASC) ;
;
ALTER TABLE `xy_roi_log` 
ADD INDEX `uid` (`uid` ASC) ,
ADD INDEX `roi_date` (`roi_date` ASC) ,
ADD INDEX `addtime` (`addtime` ASC) ,
ADD INDEX `status` (`status` ASC) ;
;
ALTER TABLE `xy_roi_matching_log` 
ADD INDEX `uid` (`uid` ASC) ,
ADD INDEX `status` (`status` ASC) ,
ADD INDEX `addtime` (`addtime` ASC) ,
ADD INDEX `roi_id` (`roi_id` ASC) ;
;
ALTER TABLE `xy_recharge` 
ADD INDEX `addtime` (`addtime` ASC) ,
ADD INDEX `endtime` (`endtime` ASC) ,
ADD INDEX `type` (`type` ASC) ,
ADD INDEX `status` (`status` ASC) ,
ADD INDEX `reference_number` (`reference_number` ASC) ,
ADD INDEX `tel` (`tel` ASC) ,
ADD INDEX `real_name` (`real_name` ASC) ;
;
ALTER TABLE `xy_recharge_pay` 
ADD INDEX `addtime` (`addtime` ASC) ,
ADD INDEX `endtime` (`endtime` ASC) ,
ADD INDEX `type` (`type` ASC) ,
ADD INDEX `status` (`status` ASC) ,
ADD INDEX `reference_number` (`reference_number` ASC),
ADD INDEX `tel` (`tel` ASC),
ADD INDEX `real_name` (`real_name` ASC) ;
;
ALTER TABLE `xy_deposit` 
ADD INDEX `uid` (`uid` ASC) ,
ADD INDEX `bk_id` (`bk_id` ASC) ,
ADD INDEX `addtime` (`addtime` ASC) ,
ADD INDEX `endtime` (`endtime` ASC) ,
ADD INDEX `status` (`status` ASC) ,
ADD INDEX `type` (`type` ASC) ,
ADD INDEX `act_user_id` (`act_user_id` ASC) ;
;
ALTER TABLE `xy_message` 
ADD INDEX `addtime` (`addtime` ASC) ,
ADD INDEX `type` (`type` ASC) ;
;

ALTER TABLE `xy_point_log` 
ADD INDEX `type` (`type` ASC) ,
ADD INDEX `status` (`status` ASC) ,
ADD INDEX `addtime` (`addtime` ASC) ;
;
ALTER TABLE `xy_balance_log` 
ADD INDEX `addtime` (`addtime` ASC) ;
;
ALTER TABLE `xy_inbox` 
ADD INDEX `sender_uid` (`sender_uid` ASC) ,
ADD INDEX `receiver_uid` (`receiver_uid` ASC) ,
ADD INDEX `status` (`status` ASC) ,
ADD INDEX `action` (`action` ASC) ,
ADD INDEX `addtime` (`addtime` ASC) ,
ADD INDEX `updatetime` (`updatetime` ASC) ,
ADD INDEX `chat_room_id` (`chat_room_id` ASC) ,
ADD INDEX `chat_image_url` (`chat_image_url` ASC) ;
;
ALTER TABLE `xy_inbox_group` 
ADD INDEX `sender_uid` (`sender_uid` ASC) ,
ADD INDEX `status` (`status` ASC) ,
ADD INDEX `action` (`action` ASC) ,
ADD INDEX `addtime` (`addtime` ASC) ,
ADD INDEX `updatetime` (`updatetime` ASC) ,
ADD INDEX `chat_room_id` (`chat_room_id` ASC) ,
ADD INDEX `chat_image_url` (`chat_image_url` ASC) ;
;

ALTER TABLE `xy_inbox_message` 
ADD INDEX `uid` (`uid` ASC) ,
ADD INDEX `inbox_id` (`inbox_id` ASC) ,
ADD INDEX `addtime` (`addtime` ASC) ,
ADD INDEX `isread` (`isread` ASC) ,
ADD INDEX `readtime` (`readtime` ASC) ,
ADD INDEX `replytime` (`replytime` ASC) ,
ADD INDEX `status` (`status` ASC) ,
ADD INDEX `recall_time` (`recall_time` ASC) ,
ADD INDEX `file_url` (`file_url` ASC) ;
;
ALTER TABLE `xy_inbox_message_group` 
ADD INDEX `uid` (`uid` ASC) ,
ADD INDEX `inbox_id` (`inbox_id` ASC) ,
ADD INDEX `addtime` (`addtime` ASC) ,
ADD INDEX `isread` (`isread` ASC) ,
ADD INDEX `readtime` (`readtime` ASC) ,
ADD INDEX `replytime` (`replytime` ASC) ,
ADD INDEX `status` (`status` ASC) ,
ADD INDEX `recall_time` (`recall_time` ASC) ,
ADD INDEX `file_url` (`file_url` ASC) ;
;