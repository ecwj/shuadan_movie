<?php
date_default_timezone_set("Asia/Singapore");
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$conn = new mysqli("localhost","root","","root_prime");

// Check connection
if ($conn->connect_errno) {
  echo "Failed to connect to MySQL: " . $conn->connect_error;
  exit();
}

$conn->set_charset("utf8");

function parent_user($conn, $uid, $num=1, $lv=1)
{
  $sql = "SELECT parent_id FROM xy_users WHERE id = $uid";
  $result = $conn->query($sql);
  
  $res = $result->fetch_array(MYSQLI_ASSOC);
  $pid = $res['parent_id'];

  $sql = "SELECT id, parent_id, status, (balance - freeze_balance) AS balance, level, deal_time, bonus_status FROM xy_users WHERE id = $pid";
  $result = $conn->query($sql);
  $uinfo = $result->fetch_array(MYSQLI_ASSOC);
  
    if($uinfo){
        if($uinfo['parent_id']&&$num>1) $data = parent_user($conn,$uinfo['id'],$num-1,$lv+1);
        $data[] = ['id'=>$uinfo['id'],'pid'=>$uinfo['parent_id'],'lv'=>$lv,'status'=>$uinfo['status'],'balance'=>$uinfo['balance'],'level'=>$uinfo['level'],'deal_time'=>$uinfo['deal_time'],'bonus_status'=>$uinfo['bonus_status']];
        return $data;
    }
    return false;
}

function parent_user_balance($conn, $uid, $num=1, $lv=1)
{
  $sql = "SELECT parent_id FROM xy_users WHERE id = $uid";
  $result = $conn->query($sql);
  
  $res = $result->fetch_array(MYSQLI_ASSOC);
  $pid = $res['parent_id'];

  $sql = "SELECT u.id, u.parent_id, u.status, COALESCE(SUM(IF(b.status = 1, b.num, -b.num)), 0) AS balance, u.level, u.deal_time, u.bonus_status FROM xy_users u LEFT JOIN xy_balance_log b ON b.uid = u.id AND b.type NOT IN (2, 15) WHERE u.id = $pid";
  $result = $conn->query($sql);
  $uinfo = $result->fetch_array(MYSQLI_ASSOC);
  
    if($uinfo){
        if($uinfo['parent_id']&&$num>1) $data = parent_user_balance($conn,$uinfo['id'],$num-1,$lv+1);
        $data[] = ['id'=>$uinfo['id'],'pid'=>$uinfo['parent_id'],'lv'=>$lv,'status'=>$uinfo['status'],'balance'=>$uinfo['balance'],'level'=>$uinfo['level'],'deal_time'=>$uinfo['deal_time'],'bonus_status'=>$uinfo['bonus_status']];
        return $data;
    }
    return false;
}