<?php

include 'connection.php';

$config = include '../../config/app.php';

$baseprice = $config['base_price'];

$url = "http://piaofang.maoyan.com/getBoxList?date=1&isSplit=true";
$res = myCurl($url);

$sql = "INSERT INTO xy_movie_api_log VALUES (NULL, '$url', NULL, '$res', '" . time() . "')";
$re = $conn->query($sql);
// echo $sql;
// var_dump($re);exit;
$data = json_decode($res, true);

$time = time();
$date = date('Y-m-d');
$removeDate = $config['movie_start_date'];

if (isset($data['status']) && $data['status'] == true) {
    if (isset($data['boxOffice'])) {
        if (isset($data['boxOffice']['data'])) {
            if (isset($data['boxOffice']['data']['list'])) {
                $b_data = $data['boxOffice']['data']['list'];

                $multiSql = [];//"";
                $movieIds = [];
                $i = $j = 0;
                foreach ($b_data as $k => $d) {
                	if ($k > 12) {
                		// continue;
                	}
                    $movieId = $d['movieInfo']['movieId'];
                    $movieName = $d['movieInfo']['movieName'];
                    // $content = json_encode($d);

                    if (in_array($movieName, ['反击者','筑梦之路'])) {
                        continue;
                    }
                    $map = [
                        'movie_id' => $movieId,
                        'date' => $date,
                        'status' => 1
                    ];                    

                    $sql = "SELECT * FROM xy_movie WHERE movie_id = $movieId";
					$result = $conn->query($sql);
					$exist = $result->fetch_array(MYSQLI_ASSOC);

					$result->free_result();

                    if ($j % 5 == 0) {
                        $i ++;
                    }
                    if (!isset($multiSql[$i])) {
                        $multiSql[$i] = "";
                    }

					// var_dump('sql: ' . $sql . ' | exist: ' . json_encode($exist));
                    if (!$exist) {

                       	$img = "";
                        $category = "";
                        $playtime = "";
                        $desc = "";
                        // if ($exist['image_url'] === NULL || $exist['image_url'] === '' || $exist['category'] === NULL || $exist['category'] === '') {

                            $u = 'http://piaofang.maoyan.com/movie/'. $movieId;
                            $html = cg($u);

                            $doc = new \DOMDocument();
                            @$doc->loadHTML($html);

                            $tags = $doc->getElementsByTagName('img');

                            $tags_p = $doc->getElementsByTagName('p');

                            $tags_span = $doc->getElementsByTagName('span');

                            $tags_div = $doc->getElementsByTagName('div');

                            foreach ($tags as $tag) {
                                if (strpos($tag->getAttribute('class'), 'need-handle-pic') !== false) {
                                   $img = $tag->getAttribute('src');
                                }
                            }
                            foreach ($tags_p as $tag) {
                                if (strpos($tag->getAttribute('class'), 'info-category') !== false) {
                                	$category = trim($tag->textContent);
                                }
                            }
                            foreach ($tags_span as $tag) {
                                if (strpos($tag->getAttribute('class'), 'score-info') !== false) {
                                	$playtime = trim($tag->textContent);
                                }
                            }

                            $u = 'http://piaofang.maoyan.com/movie/' . $movieId . '/moresections';
                            $html = myCurl($u);

                            $res = json_decode($html, 1);
                            
                            $html = $res['sectionHTMLs']['celebritySection']['html'];
                            $htmlDetail = $res['sectionHTMLs']['detailSection']['html'];

                            $doc = new \DOMDocument();
                            @$doc->loadHTML($html);

                            $tags = $doc->getElementsByTagName('div');

                            $people = "";
                            foreach ($tags as $tag) {
                            	// var_dump($tag->getAttribute('class'));
                                if (strpos($tag->getAttribute('class'), 'category') !== false) {
                                	// var_dump($tag);
                                	$title = trim(utf8_decode($tag->textContent));
                                	$people .= '<br />' . $title;
                                }
                            }


                            $doc = new \DOMDocument();
                            @$doc->loadHTML($htmlDetail);

                            $tags = $doc->getElementsByTagName('div');

                            foreach ($tags as $tag) {
                            	// var_dump($tag->getAttribute('class'));
                                if (strpos($tag->getAttribute('class'), 'detail-block-content') !== false) {
                                	$desc = utf8_decode($tag->textContent);
                                }
                            }

                        // }
                        // $content = $d;//json_decode($content, true);
                        $people = str_replace("'", "\'", $people);
                        $category = str_replace("'", "\'", $category);
                        $playtime = $playtime;

                        $playdate = preg_replace("/\p{Han}+/u", '', $playtime);

                        $_date = date('Y-m-d H:i:s', strtotime($playdate));

                        $playtime_date = $_date;

                        echo $playtime . ' | ' . $playdate . ' | ' . $_date . "\n";

                        if ($_date < $removeDate) {
                            // continue;
                        }
                        $movieIds[] = $movieId;

                        $desc = str_replace("'", "\'", $desc);
                        $content = json_encode($d);
                        $multiSql[$i] = "INSERT INTO xy_movie (movie_id, name, content, playtime, category, description, image_url, people, addtime, status, api, price, stock, playtime_date) VALUES ($movieId, '$movieName', '$content', '$playtime', '$category', '$desc', '$img', '$people', '$time', 1, '$url', '$baseprice', rand() * 1000000, '$playtime_date');";
                    } else {
                        $img = $exist['image_url'];
                        $category = $exist['category'];
                        $playtime = $exist['playtime'];
                        $desc = $exist['description'];
                        $people = $exist['people'];
                        $oriStatus = $exist['status'];
                        if($oriStatus != 1) {
                            continue;
                        }
                        if ($exist['playtime'] === NULL || $exist['playtime_date'] === '' || $exist['category'] === NULL || $exist['image_url'] === '') {

                            $u = 'http://piaofang.maoyan.com/movie/'. $movieId;
                            $html = cg($u);

                            $doc = new \DOMDocument();
                            @$doc->loadHTML($html);

                            $tags = $doc->getElementsByTagName('img');

                            $tags_p = $doc->getElementsByTagName('p');

                            $tags_span = $doc->getElementsByTagName('span');

                            $tags_div = $doc->getElementsByTagName('div');

                            foreach ($tags as $tag) {
                                if (strpos($tag->getAttribute('class'), 'need-handle-pic') !== false) {
                                   $img = $tag->getAttribute('src');
                                }
                            }
                            foreach ($tags_p as $tag) {
                                if (strpos($tag->getAttribute('class'), 'info-category') !== false) {
                                	$category = trim($tag->textContent);
                                }
                            }
                            foreach ($tags_span as $tag) {
                                if (strpos($tag->getAttribute('class'), 'score-info') !== false) {
                                	$playtime = trim($tag->textContent);
                                }
                            }

                            $u = 'http://piaofang.maoyan.com/movie/' . $movieId . '/moresections';
                            $html = myCurl($u);

                            $res = json_decode($html, 1);
                            
                            $html = $res['sectionHTMLs']['celebritySection']['html'];
                            $htmlDetail = $res['sectionHTMLs']['detailSection']['html'];

                            $doc = new \DOMDocument();
                            @$doc->loadHTML($html);

                            $tags = $doc->getElementsByTagName('div');

                            $people = "";
                            foreach ($tags as $tag) {
                            	// var_dump($tag->getAttribute('class'));
                                if (strpos($tag->getAttribute('class'), 'category') !== false) {
                                	// var_dump($tag);
                                	$title = trim(utf8_decode($tag->textContent));
                                	$people .= '<br />' . $title;
                                }
                            }


                            $doc = new \DOMDocument();
                            @$doc->loadHTML($htmlDetail);

                            $tags = $doc->getElementsByTagName('div');

                            foreach ($tags as $tag) {
                            	// var_dump($tag->getAttribute('class'));
                                if (strpos($tag->getAttribute('class'), 'detail-block-content') !== false) {
                                	$desc = utf8_decode($tag->textContent);
                                }
                            }

                        }
                        // $content = $d;//json_decode($content, true);
                        $people = str_replace("'", "\'", $people);
                        $category = str_replace("'", "\'", $category);
                        $playtime = $playtime;

                        $playdate = preg_replace("/\p{Han}+/u", '', $playtime);

                        $_date = date('Y-m-d H:i:s', strtotime($playdate));

                        $playtime_date = $_date;

                        echo $playtime . ' | ' . $playdate . ' | ' . $_date . "\n";

                        if ($_date < $removeDate) {
                            // continue;
                        }
                        $movieIds[] = $movieId;

                        $status = 1;

                        $date1 = date_create($playtime_date);
                        $date2 = date_create(date('Y-m-d H:i:s'));

                        $diff = date_diff($date1,$date2);
                        $sign = $diff->format("%R");
                        $dayDiff = $diff->format("%a");
                        echo 'movieId: ' . $movieId . ' | sign: ' . $sign . ' | dayDiff: ' . $dayDiff;
                        if ($sign == "+" && $dayDiff >= 0) {                            
                            if ($dayDiff > 14) {
                                echo '-------' . 'movieId: ' . $movieId . "\n";
                                $status = 2;
                            }
                        }

                        $desc = str_replace("'", "\'", $desc);
                        $content = json_encode($d);
                        $multiSql[$i] = "UPDATE xy_movie SET name = '$movieName', content = '$content', category = '$category', playtime = '$playtime', description = '$desc', image_url = '$img', people = '$people', updatetime = $time, status = $status, playtime_date = '$playtime_date' WHERE id = '" . $exist['id'] . "';";
                    }
                }                

                if ($multiSql != []) {
                	// echo $multiSql;

                    if (sizeof($movieIds) > 0) {
                        $ids = implode(",", $movieIds);
                        // $multiSql[$i + 1] = "UPDATE xy_movie SET status = 0 WHERE movie_id NOT IN ($ids);";
                    }

                    foreach ($multiSql as $sql) {
                    	$conn->multi_query($sql);
                        while (@$conn->next_result());
                        echo "Error: " . ($conn->error ? $conn->error : 'None');
                    }
                }


            }
        }
    }
}
$conn->close();
// var_dump($conn);
return $result;

function myCurl($url){
    $ch = curl_init();     // Curl 初始化  
    $timeout = 30;     // 超时时间：30s  
    $ua='Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36';    // 伪造抓取 UA  
        $ip = mt_rand(11, 191) . "." . mt_rand(0, 240) . "." . mt_rand(1, 240) . "." . mt_rand(1, 240);
    curl_setopt($ch, CURLOPT_URL, $url);              // 设置 Curl 目标  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);      // Curl 请求有返回的值  
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);     // 设置抓取超时时间  
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);        // 跟踪重定向  
    curl_setopt($ch, CURLOPT_REFERER, $url);   // 伪造来源网址  
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:'.$ip, 'CLIENT-IP:'.$ip));  //伪造IP  
    curl_setopt($ch, CURLOPT_USERAGENT, $ua);   // 伪造ua   
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts  
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0); //强制协议为1.0
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); //强制使用IPV4协议解析域名
    $content = curl_exec($ch);   

    curl_close($ch);    // 结束 Curl  
    return $content;    // 函数返回内容  
}

function cg($url)
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Cookie: theme=moviepro'
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}