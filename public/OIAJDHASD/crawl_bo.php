<?php

set_time_limit(3600);

include 'connection.php';

$config = include '../../config/app.php';

$baseprice = $config['base_price'];
$appname = $config['app_name'];

if ($appname == 'dypf_indon') {
	$baseprice = $baseprice * 1000;
}

$limit = 300;
$start = !isset($_GET['start']) ? 0 : $start;
$offset = $start * $limit;

$url = 'https://www.boxofficemojo.com/year/world/?ref_=bo_nb_in_tab';
$res = myCurl($url);

$doc = new \DOMDocument();
$doc1 = new \DOMDocument();
$doc2 = new \DOMDocument();
$doc3 = new \DOMDocument();

@$doc->loadHTML($res);

$multiSql = '';
$i = 0;
$trs = $doc->getElementsByTagName('tr');
$movieIds = [];
foreach ($trs as $key => $tr) {
	$tds = $tr->getElementsByTagName('td');
	$movieName = '';
	$category_en = '';
	$playtime = '';
	$people_en = '';
	$desc_en = '';
	$playtime_date = '';
	$image_url = '';
	$movieId = '';
	if ($key < $offset || $i > $limit) {
		continue;
	}
	foreach ($tds as $td) {
	    if (strpos($td->getAttribute('class'), 'type-release_group') !== false) {
	    	$movieName = str_replace("'", "\'", $td->textContent);
	       	$a = $td->getElementsByTagName('a');
	       	$href = 'https://www.boxofficemojo.com' . $a[0]->getAttribute('href');
			$res1 = myCurl($href);

			@$doc1->loadHTML($res1);
			$as = $doc1->getElementsByTagName('a');
			// echo $href . ' <br>';
			// echo sizeof($as) . '<br>';
			foreach ($as as $a) {
				// echo $k . ': ' . $a->getAttribute('class') . ' | ' . $a->textContent . '<br>';
				if (strpos($a->getAttribute('class'), 'mojo-title-link') !== false) {
					$href = 'https://www.boxofficemojo.com' . $a->getAttribute('href');
					$res2 = myCurl($href);
					@$doc2->loadHTML($res2);

					$movieId = explode("/", $a->getAttribute('href'))[2];

					$movieIds[] = $movieId;

					$sql = "SELECT * FROM xy_movie WHERE movie_id = '$movieId'";
					$result = $conn->query($sql);
					$exist = $result->fetch_array(MYSQLI_ASSOC);

					$result->free_result();

					if ($exist) {
						continue;
					}

					$imgs = $doc2->getElementsByTagName('img');
					foreach ($imgs as $img) {
						if ($img->getAttribute('data-a-hires')) {
							$image_url = $img->getAttribute('src');
						}
					}
					$spans = $doc2->getElementsByTagName('span');
					foreach ($spans as $span) {
						if ($span->textContent == 'Genres') {
							$category_en = str_replace("'", "\'", $span->nextSibling->textContent);
						}
						if ($span->textContent == 'Earliest Release Date') {
							$playtime = $span->nextSibling->textContent;
							$playtime = explode(" (", $playtime)[0];
							$playtime_date = date('Y-m-d H:i:s', strtotime($playtime));
						}
						if ($span->getAttribute('class') == 'a-size-medium') {
							$desc_en = str_replace("'", "\'", $span->textContent);
						}
					}

					$href = explode("?", $href)[0] . 'credits';
					$res3 = myCurl($href);
					@$doc3->loadHTML($res3);
					
					$table = $doc3->getElementById('principalCast');
					if ($table != NULL) {
						$trs_cast = $table->getElementsByTagName('tr');
						foreach ($trs_cast as $k => $trc) {
							if ($k == 0) {
								continue;
							}
							$tds_cast = $trc->getElementsByTagName('td');
							$people_en .= $tds_cast[0]->textContent . ': ' . str_replace("See more", "", $tds_cast[1]->textContent) . '<br>';
						}
					}
				}
			}
	    }	    
	}
	$i ++;
	if ($image_url != '') {
		$people_en = str_replace("'", "\'", $people_en);
		$image_url = str_replace("'", "\'", $image_url);

		$sql = "SELECT * FROM xy_movie WHERE movie_id = '$movieId'";
		$result = $conn->query($sql);
		$exist = $result->fetch_array(MYSQLI_ASSOC);

		$result->free_result();

		if (!$exist && !in_array($movieId, $movieIds) && !in_array($movieName, ['1921','Forever Passion','French Tech','Mobile Suit Gundam: Hathaway','Character','Sunny Sisters','Queen Bees','Never Stop','Family Swap','Presidents','Home Front','Villa Caprice'])) {

			$multiSql .= "INSERT INTO xy_movie (movie_id, name_en, content, playtime, category_en, description_en, image_url, people_en, addtime, status, api, price, stock, playtime_date) VALUES ('$movieId', '$movieName', NULL, '$playtime', '$category_en', '$desc_en', '$image_url', '$people_en', '" . time() . "', 1, '$url', '$baseprice', rand() * 1000000, '$playtime_date');";


    		echo $movieId . ' | ' . $movieName . ' | ' . $category_en . ' | ' . $playtime . ' | ' . $desc_en . '| ' . $image_url . ' | ' . ' <br>' . $people_en . '<br>';
    	// exit;
    	}
    }	
}

if ($multiSql != '') {
	echo $multiSql;

    // if (sizeof($movieIds) > 0) {
    //     $ids = implode(",", $movieIds);
    //     $multiSql .= "UPDATE xy_movie SET status = 0 WHERE movie_id NOT IN ($ids);";
    // }

	$conn->multi_query($multiSql);
	echo "Error: " . ($conn->error ? $conn->error : 'None') . '<br>';
}

function myCurl($url){
    $ch = curl_init();     // Curl 初始化  
    $timeout = 30;     // 超时时间：30s  
    $ua='Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36';    // 伪造抓取 UA  
        $ip = mt_rand(11, 191) . "." . mt_rand(0, 240) . "." . mt_rand(1, 240) . "." . mt_rand(1, 240);
    curl_setopt($ch, CURLOPT_URL, $url);              // 设置 Curl 目标  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);      // Curl 请求有返回的值  
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);     // 设置抓取超时时间  
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);        // 跟踪重定向  
    curl_setopt($ch, CURLOPT_REFERER, $url);   // 伪造来源网址  
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:'.$ip, 'CLIENT-IP:'.$ip));  //伪造IP  
    curl_setopt($ch, CURLOPT_USERAGENT, $ua);   // 伪造ua   
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts  
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0); //强制协议为1.0
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); //强制使用IPV4协议解析域名
    $content = curl_exec($ch);   

    curl_close($ch);    // 结束 Curl  
    return $content;    // 函数返回内容  
}
?>
