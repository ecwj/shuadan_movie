<?php

include 'connection.php';

$url0 = "http://piaofang.maoyan.com/theater/query?date=0";
$url1 = "http://piaofang.maoyan.com/theater/query?date=1";
$url2 = "http://piaofang.maoyan.com/theater/query?date=2";

$date = date('Y-m-d');

$date0 = date('Y-m-d', strtotime($date . '-1 day'));
$date1 = $date;
$date2 = date('Y-m-d', strtotime($date . '+1 day'));

$result = 0;
$result += crawl($conn, $url0, $date0);
$result += crawl($conn, $url1, $date1);
$result += crawl($conn, $url2, $date2);

$conn->close();
return $result;

function crawl ($conn, $url, $date) {
    $res = myCurl($url);

    $sql = "INSERT INTO xy_movie_api_log VALUES (NULL, '$url', NULL, '$res', '" . time() . "')";
    $re = $conn->query($sql);
    // echo $sql;
    // var_dump($re);exit;
    $data = json_decode($res, true);

    $result = 0;
    if (isset($data['success']) && $data['success'] == true) {
        if (isset($data['data'])) {
            $sql = "SELECT * FROM xy_theater WHERE `date` = '$date'";
            $result = $conn->query($sql);
            $exist = $result->fetch_array(MYSQLI_ASSOC);
            $content = $res;

            if ($exist) {
                $sql = "UPDATE xy_theater SET content = '$content', updatetime = '" . time() . "' WHERE id = '" . $exist['id'] . "'";
            } else {
                $sql = "INSERT INTO xy_theater VALUES (NULL, '$content', '$date', '" . time() . "', NULL)";
            }
            $result = $conn->query($sql);
        }
    }

    return $result;
}


function myCurl($url){
    $ch = curl_init();     // Curl 初始化  
    $timeout = 30;     // 超时时间：30s  
    $ua='Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36';    // 伪造抓取 UA  
        $ip = mt_rand(11, 191) . "." . mt_rand(0, 240) . "." . mt_rand(1, 240) . "." . mt_rand(1, 240);
    curl_setopt($ch, CURLOPT_URL, $url);              // 设置 Curl 目标  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);      // Curl 请求有返回的值  
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);     // 设置抓取超时时间  
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);        // 跟踪重定向  
    curl_setopt($ch, CURLOPT_REFERER, $url);   // 伪造来源网址  
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:'.$ip, 'CLIENT-IP:'.$ip));  //伪造IP  
    curl_setopt($ch, CURLOPT_USERAGENT, $ua);   // 伪造ua   
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts  
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0); //强制协议为1.0
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); //强制使用IPV4协议解析域名
    $content = curl_exec($ch);   

    curl_close($ch);    // 结束 Curl  
    return $content;    // 函数返回内容  
}
