<?php

include 'connection.php';

$config = include '../../config/app.php';

$apikey = $config['apikey_haojingke'];

$catid_ = isset($_SERVER['argv'][1]) ? $_SERVER['argv'][1] : 0;

$catid = explode(",", '239,2603,2629,2933,5217,5752,5834,5839,5851,5906,5921,5955,6076,6128,6209,6290,6398,6536,6586,6630,6758,6785,6883,7323,7629,7639,8172,8439,8502,8508,8509,8538,8583,8634,8669,8721,8722,8723,8724,8725,8726,8727,8728,8729,8730,8731,8732,8733,8734,8736,9313,9314,9315,9316,9317,9318,9319,9320,9321,9322,9323,9324,11683,11684,11685,11686,11687,11688,11689,13176,13177,13178,14697,14740,14933,14966,15083,15356,15543,16155,16192,16209,16237,16288,16548,16676,16794,16901,16989,17134,17249,17285,17412,17455,17671,17803,18088,18270,18349,18482,18574,18601,18637,18814,19298,20078,20118,20340,20645');
$catid = explode(",", '18482,18637,5851,6128');

if (!in_array($catid_, $catid)) {
	echo 'invalid catid';
	exit;
}
$catid = [$catid_];

foreach ($catid as $cat_id) {
	echo "\ncatid: " . $cat_id . " => ";
	
	$sql = "SELECT cat_id, COALESCE(MAX(page), 0) AS page FROM xy_goods_list_cn WHERE cat_id = $cat_id AND page > 0 HAVING page > 0 LIMIT 1";
	$result = $conn->query($sql);
	
	$page = 1;
	while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
	   $page = $row['page'] + 1;
	}
	
	$maxPage = 160;
	for ($i = $page; $i <= $maxPage; $i ++) {
	    // if ($cat_id == 239 && $i == 1) {
	    //     $i = 24;
	    // }
		$multiSql = "";
		echo $i . ',';
	    $page = $i;
	    $param = [
	        'page' => $page,
	        'pageSize' => 20,
	        'cat_id' => $cat_id
	    ];
	    $data = http_curl($config['haojingke_url'] . 'goodslist', $param, $apikey);
	    $reData = json_decode($data,1);
	    if($reData == NULL || empty($reData) || !isset($reData['data'])) {
	        $list = [];
	    } else {
	        $list = $reData['data']['goods_list'];
	        foreach ($list as $d) {
	            $picurl = $d['picurl'];
	            @$content = file_get_contents($picurl);
	            if (!$content) {
	                continue;
	            }
	            $url = '/../img/goods/' . $d['goods_id'] . '.jpg';
	            file_put_contents(getcwd() . $url, $content);

	            $sql = "SELECT id FROM xy_goods_list_cn WHERE goods_name = '" . $d['goods_name'] . "'";
				$result = $conn->query($sql);
				
				if (!$result) {
// 				    echo $sql;
// 				    var_dump($result);
				    continue;
				}
				$exist = $result->fetch_array(MYSQLI_ASSOC);

				$result->free_result();

	            if ($exist) {
	                continue;
	            }
	            // $data = [
	            //     'shop_name' => $d['shopname'],
	            //     'goods_name' => $d['goods_name'],
	            //     'goods_info' => $d['goods_desc'],
	            //     'goods_price' => $d['price'],
	            //     'addtime' => time(),
	            //     'status' => 1,
	            //     'goods_pic' => $url,
	            //     'shop_status' => 1,
	            //     'price_status' => 1,
	            //     'cid' => rand(1,6)
	            // ];
	            // db('xy_goods_list_cn')->insert($data);
	            $multiSql .= "INSERT INTO xy_goods_list_cn (shop_name, goods_name, goods_info, goods_price, addtime, status, goods_pic, shop_status, price_status, cid, page, cat_id) VALUES ('" . $d['shopname'] . "','" . $d['goods_name'] . "','" . $d['goods_desc'] . "','" . $d['price'] . "'," . time() . ",1,'$url',1,1," . rand(1,6) . ", $i, $cat_id);";
	        }
	    }

	    if ($multiSql != '') {
	    	// echo $multiSql;
	    	$re = $conn->multi_query($multiSql);
	    	if ($conn->error) {
	    	  echo "Error: " . ($conn->error ? $conn->error : 'None');
	    	}
	    	while (@$conn->next_result());
	    }
	}
}

function http_curl($url, $data, $apikey){
    $data['apikey'] = $apikey;
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    // post数据
    curl_setopt($ch, CURLOPT_POST, 1);
    // post的变量
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    // curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}