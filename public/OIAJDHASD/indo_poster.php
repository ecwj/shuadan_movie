<?php
date_default_timezone_set("Asia/Singapore");
$a = [
	'2020-01-01' => 'Tahun Baru Masehi',
	'2020-01-25' => 'Year Rat of the 2020',
	'2020-03-22' => 'Isra Mi\\\'raj',
	'2020-03-25' => 'Hari Suci Nyepi',
	'2020-04-10' => 'Jumat Agung',
	'2020-05-01' => 'Hari Buruh',
	'2020-05-07' => 'Hari Waisak',
	'2020-05-21' => 'Kenaikan Isa Almasih',
	'2020-05-24' => 'Hari Raya Idul Fitri',
	'2020-06-01' => 'Hari Lahir Pancasila',
	'2020-07-31' => 'Idul Adha',
	'2020-08-17' => 'Hari Kemerdekaan',
	'2020-08-20' => 'Tahun Baru Islam',
	'2020-10-28' => 'Cuti Bersama Maulid Nabi Muhammad SAW',
	'2020-11-15' => '5th Anniversary Celeberation',
	'2020-12-25' => 'Merry Christmas & Happy New Year',
	'2021-01-01' => 'Happy New Year',
	'2021-02-12' => 'Happy Chinese New Year The Year of Ox',
	'2021-03-11' => 'Isra Mi\\\'raj 2021',
	'2021-03-14' => 'Selamat Hari Rara Nyepi',
	'2021-04-02' => 'Good Friday',
	'2021-05-01' => 'International Workers\\\' Day',
	'2021-05-13' => 'Selamat Hari Raya Kenaikan Isa Almasih',
	'2021-05-14' => 'Selamat Hari Raya Idul Fitri',
	'2021-05-26' => 'Selamat Hari Raya Waisak',
	'2021-06-01' => 'Pancasila Day',
	'2021-07-20' => 'Selamat Hari Raya Idul Adha',
	'2021-08-20' => 'Tahun Baru Islam'
];

$sql = "";
$i = 1;
foreach ($a as $key => $val) {
	$name = $val;
	$img = ($i) . '.png';
	$content = '<p><img alt="" src="/public/img/holiday/_img_?v=1.1" style="max-width:100%;border:0" /><br>_name_</p>
<style type="text/css">.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}
</style>';
	$content = str_replace(['_img_', '_name_'], [$img, $name], $content);
	$sql .= "INSERT INTO `xy_message` (`id`, `uid`, `sid`, `title`, `content`, `addtime`, `type`) VALUES (NULL, '0', '0', '$name', '$content', '" . strtotime($key) . "', '3');";
echo $i . ': ' . $name.  '<br>';
	$i ++;
}

file_put_contents('indo_poster.txt', $sql);