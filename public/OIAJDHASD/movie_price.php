<?php

include 'connection.php';

$config = include '../../config/app.php';

$sql = "SELECT * FROM xy_movie WHERE status = 1 AND is_presale != 1";
$result = $conn->query($sql);
$baseprice = $config['base_price'];

$appname = $config['app_name'];

$res = false;
$updateSql = "";
while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
	$id = $row['id'];
	$price = $row['price'];
	$playtime = $row['playtime'];
	$addtime = date('Y-m-d H:i:s', $row['addtime']);
	$playdate = preg_replace("/\p{Han}+/u", '', $playtime);
	$date = date('Y-m-d H:i:s', strtotime($playdate));

	// $date1 = date_create($date);
	$date1 = date_create($addtime);
    $date2 = date_create(date('Y-m-d H:i:s'));    

    $diff = date_diff($date1,$date2);
    $sign = $diff->format("%R");
    $dayDiff = $diff->format("%a");
    if ($sign == "+" && $dayDiff >= 0) {
    	if ($addtime < '2021-05-23 00:00:00') {
    		$newprice = getnewprice($price, $dayDiff, $baseprice);	
    	} else {
    		if ($appname == 'dypf_indon') {
    			$newprice = getnewprice3($price, $dayDiff, $baseprice);
    		} else {
				$newprice = getnewprice2($price, $dayDiff, $baseprice);
			}
		}
    } else {
    	$newprice = $price;
    }

    if ($newprice != $price) {
    	$updateSql .= "Update xy_movie SET price = '$newprice' WHERE id = $id;";
    }
    echo 'id: ' . $row['id'] . ' | addtime: ' . $addtime . ' | playtime: ' . $playtime . ' | price: ' . $price . ' | newprice: ' . $newprice . ' | sign,diff: ' . $sign . ',' . +$dayDiff . "\n";
}

if ($updateSql != '') {
	$res = $conn->multi_query($updateSql);
	while (@$conn->next_result());
}


if ($appname == 'dypf_indon') {
	$baseprice = $baseprice * 1000;
	$c = 1;
} else {
	$c = 5;
}

$sql = "SELECT id AS c FROM xy_movie WHERE price = '$baseprice' AND status = 1 AND is_presale != 1";
$result = $conn->query($sql);

if (!$result || ($result && $result->num_rows < $c)) {
    $sql = "SELECT id FROM xy_movie WHERE status = 1 AND name != '图兰朵：魔咒缘起' AND is_presale != 1 ORDER BY RAND(), playtime_date DESC LIMIT 5";
    $res = $conn->query($sql);

    if ($res && $res->num_rows > 0) {
	    while ($row = $res->fetch_array(MYSQLI_ASSOC)) {
		    $sql = "UPDATE xy_movie SET price = '$baseprice' WHERE id = '" . $row['id'] . "'";
		    echo $sql;
		    $conn->query($sql);
		}
	}
}

$sql = "SELECT id FROM xy_movie WHERE name in ('图兰朵：魔咒缘起', '铁道英雄')";
$res = $conn->query($sql);

if ($res && $res->num_rows > 0) {
    while ($row = $res->fetch_array(MYSQLI_ASSOC)) {
	    $sql = "UPDATE xy_movie SET price = '$baseprice' WHERE id = '" . $row['id'] . "'";
	    echo $sql;
	    $conn->query($sql);
	}
}

$conn->close();
return $res;

//+3+3+3+3 +4+4 +5+5 +6+6 +7+7 +8+8 +9+9 +10+10
//         5,6,  7,8, 9,10, 11,12, 13,14, 15,16 17,18		
//         1,2,  3,4, 5,6, 7,8, 9,10, 11,12, 13,14
//         46,50,  55,60, 66,72, 79,86, 94,102, 111,120, 130,140
function getnewprice($price, $dayDiff, $baseprice) {
	if ($dayDiff <= 0) {
		return $price;
	} else if ($dayDiff > 0 && $dayDiff <= 4) {
		$price = $baseprice + $dayDiff * 3;
	} else {
		$day = $dayDiff - 4;
		$price = $baseprice + (4 * 3);
		for ($i = 1; $i <= $day; $i ++) {
			$price += (3 + ceil($i/2));
		}
	}
	if ($price >= 80) {
		$price = 80;
	}
	return $price;
}

function getnewprice2($price, $dayDiff, $baseprice) {
	if ($price >= 80) {
		$price = 80;
		return $price;
	}
	if ($dayDiff <= 0) {
		return $price;
	} else if ($dayDiff > 0 && $dayDiff <= 6) {
		$price = $baseprice + $dayDiff * 2;
	} else if ($dayDiff > 2 && $dayDiff <= 14) {
		$price = $baseprice + (6 * 2);
		$price = $price + ($dayDiff - 6) * 3;
	} else {
		$price += 4;
	}
	if ($price >= 80) {
		$price = 80;
	}
	return $price;
}

function getnewprice3($price, $dayDiff, $baseprice) {
	if ($price >= 80000) {
		$price = 80000;
		return $price;
	}
	if ($dayDiff <= 0) {
		return $price;
	} else if ($dayDiff > 0) {
		$price = $baseprice + $dayDiff * 2;
	}
	$price = $price * 1000;
	if ($price >= 80000) {
		$price = 80000;
	}
	return $price;
}