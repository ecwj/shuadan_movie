<?php

include 'connection.php';

$config = include '../../config/app.php';

if (isset($argv[1])) {
	$id = $argv[1];
} else {
	$id = 16;
}

$payId = '';
if (isset($argv[2])) {
    $payId = $argv[2];
}

if ($id == 16) {
	$url = 'https://openapi.toppay.asia/gateway/query';
	$re = check_16($conn, $config, $url);

	echo 'Success sql: ' . $re;
} else if ($id == 32) {
    $url = 'https://zaplati.cc/Apipay';
    $re = check_32($conn, $config, $url, $id);

    echo 'Success sql: ' . $re;
}

function check_16($conn, $config, $url) {
	$re = 0;
	$appid = $config['toppay']['appid'];
	$appKey = $config['toppay']['appKey'];
	$date = date('Y-m-d H:i:s', time());
	$d1 = strtotime(date('Y-m-d 00:00:00', strtotime($date . '-1 day')));
	$d2 = strtotime($date);

	$sql = "SELECT * FROM xy_recharge_pay WHERE addtime BETWEEN '$d1' AND '$d2' AND status = 1 AND uid > 0";
    if ($payId != '') {
        $sql .= " AND id = '$payId' ";
    }

	$result = $conn->query($sql);

	$datas = [];
	while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		$insertSql = '';
		$orderNum = $row['id'];
		$uid = $row['uid'];
        $num = $row['num'];

		$params = [
			'queryType' => 'ORDER_QUERY',
            'merchantCode' => $appid,
            'orderNum' => $orderNum,
            'dateTime' => date('YmdHis')
        ];

        $params['sign'] = generateSign2($params, $appKey);

        $error_msg = '';
        $res = post_json($url, $params, $error_msg);
        $data = json_decode($res,true);
        $time = time();
        if (isset($data['platRespCode'])) {
        	if ($data['platRespCode'] == 'SUCCESS') {
        		if ($data['status'] == 'SUCCESS') {
        			$insertSql .= "INSERT INTO xy_recharge (id, uid, real_name, tel, num, type, pic, addtime, endtime, status, pay_name, pay_id) SELECT id, uid, real_name, tel, num, type, pic, addtime, '$time', 2, pay_name, pay_id FROM xy_recharge_pay WHERE id = '$orderNum' AND uid = '$uid';";

                    $insertSql .= "INSERT INTO xy_balance_log (uid, sid, `oid`, num, type, status, addtime) VALUES ($uid, 0, '$orderNum', '$num', 1, 1, '$time');";

                    $insertSql .= "UPDATE xy_users SET balance = balance + '$num' WHERE id = '$uid';";

                    $insertSql .= "UPDATE xy_recharge_pay SET status = 2, endtime = '$time' WHERE id = '$orderNum' AND uid = '$uid';";

        		} else if ($data['status'] == 'PAY_CACLE') {
        			$insertSql .= "UPDATE xy_recharge_pay SET status = 3, error_msg = '" . $data['msg'] . "' WHERE id = '$orderNum' AND uid = '$uid';";
        		}
	        } else if ($data['platRespCode'] == 'NOTEXIST') {
	        	$insertSql .= "UPDATE xy_recharge_pay SET status = 3, error_msg = '订单不存在' WHERE id = '$orderNum' AND uid = '$uid';";
	        }
	        $insertSql .= "UPDATE xy_recharge_pay SET order_status_response = '" . $res . "' WHERE id = '$orderNum' AND uid = '$uid';";
        }
        if ($insertSql != '') {
        	$re += $conn->multi_query($insertSql);
        	echo "Error: " . ($conn->error ? $conn->error : 'None') . '<br>';
			while (@$conn->next_result());

            if ($data['status'] == 'SUCCESS') {
                $total_recharge_sql = "SELECT SUM(NUM) AS total FROM xy_recharge WHERE uid = '$uid' AND status = 2";
                $result = $conn->query($total_recharge_sql);
                $select = $result->fetch_array(MYSQLI_ASSOC);

                $result->free_result();

                $total_recharge = $select['total'];
                $insertSql = "";
                if ($total_recharge == $num && $config['first_recharge_reward'] > 0 && $num >= $config['first_recharge_reward_min_amount']) {
                    $rechargeReward = $config['first_recharge_reward'];

                    $insertSql .= "INSERT INTO xy_balance_log (uid, sid, `oid`, num, type, status, addtime) VALUES ($uid, 0, '$orderNum', '$rechargeReward', 18, 1, '$time');";

                    $insertSql .= "UPDATE xy_users SET balance = balance + '$rechargeReward' WHERE id = '$uid';";
                    $re += $conn->multi_query($insertSql);
                    echo "Error: " . ($conn->error ? $conn->error : 'None') . '<br>';
                    while (@$conn->next_result());
                }

                invitationBonus($conn, $uid, $orderNum, $config);
            }
        }
	}
	return $re;
}

function check_32($conn, $config, $url, $id) {
    $re = 0;
    $appid = $config['ftpay']['appid'];
    $appKey = $config['ftpay']['appKey'];
    $date = date('Y-m-d H:i:s', time());
    $d1 = strtotime(date('Y-m-d 00:00:00', strtotime($date . '-3 day')));
    $d2 = strtotime($date);

    $sql = "SELECT * FROM xy_recharge_pay WHERE addtime BETWEEN '$d1' AND '$d2' AND status = 1 AND uid > 0 AND pay_id = $id";

    $params = [
            'userid' => $appid,
            'orderno' => 'SY2111081047208711',//$orderNum,
            'action' => 'orderquery',            
        ];

        $params['sign'] = generateSign3($params, $appKey);

        $error_msg = '';
        $res = post1($url, $params, $error_msg);
        $data = json_decode($res,true);

        var_dump($data);exit;

    $result = $conn->query($sql);

    $datas = [];
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $insertSql = '';
        $orderNum = $row['id'];
        $uid = $row['uid'];
        $num = $row['num'];

        $params = [
            'userid' => $appid,
            'orderno' => $orderNum,
            'action' => 'orderquery',            
        ];

        $params['sign'] = generateSign3($params, $appKey);

        $error_msg = '';
        $res = post1($url, $params, $error_msg);
        $data = json_decode($res,true);
        $time = time();
        // var_dump($data);exit;
        if (isset($data['status'])) {
            if ($data['status'] == '1') {
                $time = $data['paytime'];
                $insertSql .= "INSERT INTO xy_recharge (id, uid, real_name, tel, num, type, pic, addtime, endtime, status, pay_name, pay_id) SELECT id, uid, real_name, tel, num, type, pic, addtime, '$time', 2, pay_name, pay_id FROM xy_recharge_pay WHERE id = '$orderNum' AND uid = '$uid';";

                $insertSql .= "INSERT INTO xy_balance_log (uid, sid, `oid`, num, type, status, addtime) VALUES ($uid, 0, '$orderNum', '$num', 1, 1, '$time');";

                $insertSql .= "UPDATE xy_users SET balance = balance + '$num' WHERE id = '$uid';";

                $insertSql .= "UPDATE xy_recharge_pay SET status = 2, endtime = '$time' WHERE id = '$orderNum' AND uid = '$uid';";

            } else if ($data['status'] == '0') {
                if (isset($data['error'])) {
                    $error = $data['error'];
                    $status = 3;
                } else {
                    $error = "";
                    $status = $row['status'];
                }
                $insertSql .= "UPDATE xy_recharge_pay SET status = $status, error_msg = '$error' WHERE id = '$orderNum' AND uid = '$uid';";
            }
            $insertSql .= "UPDATE xy_recharge_pay SET order_status_response = '" . $res . "' WHERE id = '$orderNum' AND uid = '$uid';";
        }
        if ($insertSql != '') {
            $re += $conn->multi_query($insertSql);
            echo "Error: " . ($conn->error ? $conn->error : 'None') . '<br>';
            while (@$conn->next_result());

            if ($data['status'] == 'SUCCESS') {
                $total_recharge_sql = "SELECT SUM(NUM) AS total FROM xy_recharge WHERE uid = '$uid' AND status = 2";
                $result = $conn->query($total_recharge_sql);
                $select = $result->fetch_array(MYSQLI_ASSOC);

                $result->free_result();

                $total_recharge = $select['total'];
                $insertSql = "";
                if ($total_recharge == $num && $config['first_recharge_reward'] > 0 && $num >= $config['first_recharge_reward_min_amount']) {
                    $rechargeReward = $config['first_recharge_reward'];

                    $insertSql .= "INSERT INTO xy_balance_log (uid, sid, `oid`, num, type, status, addtime) VALUES ($uid, 0, '$orderNum', '$rechargeReward', 18, 1, '$time');";

                    $insertSql .= "UPDATE xy_users SET balance = balance + '$rechargeReward' WHERE id = '$uid';";
                    $re += $conn->multi_query($insertSql);
                    echo "Error: " . ($conn->error ? $conn->error : 'None') . '<br>';
                    while (@$conn->next_result());
                }

                invitationBonus($conn, $uid, $orderNum, $config);
            }
        }
    }
    return $re;
}

function generateSign2($data, $signKey)
{
    // 按照ASCII码升序排序
    ksort($data);

    $str = '';
    foreach ($data as $key => $value) {
        $value = trim($value);
        if ('sign' != $key && '' != $value) {
            $str .= $value;
        }
    }

    $pivate_key = '-----BEGIN PRIVATE KEY-----'."\n".$signKey."\n".'-----END PRIVATE KEY-----';
    $pi_key = openssl_pkey_get_private($pivate_key);
    $crypto = '';
    foreach (str_split($str, 117) as $chunk) {
        openssl_private_encrypt($chunk, $encryptData, $pi_key);
        $crypto .= $encryptData;
    }

    return base64_encode($crypto);
}

function generateSign3($data, $signKey)
{
    $str = '';
    foreach ($data as $value) {
        $value = trim($value);
        if ('' != $value) {
            $str .= $value;
        }
    }

    $str .= $signKey;
    // dump('sign str: ' . $str);
    return md5($str);
}

function post_json($url, $data, &$error_msg = '')
{
    $data = json_encode($data);
    $ch = curl_init($url); //请求的URL地址
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);//$data JSON类型字符串
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));
    $tmpInfo = curl_exec($ch);

    if (curl_errno($ch)) {
        $error_msg = curl_error($ch);
    }
    curl_close($ch);
    return $tmpInfo;
}

function post1($url, $requestData)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($curl,CURLOPT_HTTPHEADER,$this_header);
    //普通数据
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($requestData));
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $requestData);
    $res = curl_exec($curl);

    //$info = curl_getinfo($ch);
    curl_close($curl);
    return $res;
    return json_decode($res,true);
}

function invitationBonus($conn,$uid,$oid,$config,$total_recharge = 0)
{
    $time = time();
    $re = 0;
    $insertSql = "";

    $sql = "SELECT name, value FROM system_config WHERE name in ('invite_reward_open', 'invite_target_reward_open')";
    $result = $conn->query($sql);
    $invite_reward_open = $invite_target_reward_open = 0;
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        if ($row['name'] == 'invite_reward_open') {
            $invite_reward_open = $row['value'];
        } else if ($row['name'] == 'invite_target_reward_open') {
            $invite_target_reward_open = $row['value'];
        }
    }

    $sql = "SELECT id, active FROM xy_users WHERE id = '$uid'";
    $result = $conn->query($sql);
    $uinfo = $result->fetch_array(MYSQLI_ASSOC);
    $result->free_result();

    if($uinfo['active']==0){
        $insertSql .= "UPDATE xy_users SET active = 1 WHERE id = '$uid';";
        //将账号状态改为已发放推广奖励
        $userList = parent_user($conn,$uinfo['id'],3);
        if($userList){
            foreach($userList as $v){
                if($v['status']==1){
                    if ($v['lv'] == 1) {
                        $bonus = $config['invite_reward'];
                        if ($bonus > 0 && $invite_reward_open == 1) {
                            //记录邀请一个朋友三十元的现金奖励
                            if ($total_recharge > 0) {
                                $bonus = $total_recharge * 0.1;
                            }
                            $insertSql .= "INSERT INTO xy_balance_log (uid, sid, `oid`, num, type, status, addtime, f_lv) VALUES ('" . $v['id'] . "', '" . $uinfo['id'] . "', '$oid', '$bonus', 11, 1, '$time', '" . $v['lv'] . "');";
                            $insertSql .= "UPDATE xy_users SET balance = balance + '$bonus' WHERE id = '" . $v['id'] . "';";                            
                        }

                        //邀请满人奖励
                        $bonus2 = $config['invite_target_reward'];
                        //至少300元
                        $bonus2_min_recharge = $config['invite_target_reward_min_recharge'];
                        if ($bonus2 > 0 && $invite_target_reward_open == 1) {
                            //读取之前从哪个用户得到过满人邀请奖励
                            $sql = "SELECT down_userid FROM xy_balance_log WHERE type = 12 AND uid = '" . $v['id'] . "'";
                            $result = $conn->query($sql);
                            $select = $result->fetch_array(MYSQLI_ASSOC);
                            $result->free_result();

                            if ($select) {
                                continue;
                                $down_userids = implode(",", $select['down_userid']);
                                $down_userids_where = " AND id NOT IN ($down_userids)";
                            } else {
                                $down_userids_where = "";
                            }

                            $sql = "SELECT id FROM xy_users WHERE parent_id = '" . $v['id'] . "' AND active = 1 AND total_recharge >= $bonus2_min_recharge $down_userids_where";
                            $result = $conn->query($sql);
                            $userids = [];
                            $childCount = 0;
                            while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                                $userids[] = $row['id'];
                                $childCount ++;
                            }

                            if ($childCount >= $config['invite_target_reward_count']) {
                                //查询用户是否有得过此奖励
                                $insertSql .= "INSERT INTO xy_balance_log (uid, sid, `oid`, num, type, status, addtime, f_lv, down_userid) VALUES ('" . $v['id'] . "', '" . $uinfo['id'] . "', '$oid', '$bonus2', 12, 1, '$time', '" . $v['lv'] . "', '" . implode(",", $userids) . "');";
                                $insertSql .= "UPDATE xy_users SET balance = balance + '$bonus2' WHERE id = '" . $v['id'] . "';";
                            }
                        }
                    }

                    $amt = $config[$v['lv'].'_invite_reward'];
                    $insertSql .= "INSERT INTO xy_point_log (uid, sid, `oid`, num, type, status, addtime, f_lv) VALUES ('" . $v['id'] . "', '" . $uinfo['id'] . "', '$oid', '$amt', 12, 1, '$time', '" . $v['lv'] . "');";

                    //积分
                    $insertSql .= "UPDATE xy_users SET point = point + '$amt' WHERE id = '" . $v['id'] . "';";
                }
            }
        }
    }

    if ($insertSql != '') {
        $re += $conn->multi_query($insertSql);
        echo "invitationBonus Error: " . ($conn->error ? $conn->error : 'None') . '<br>';
        while (@$conn->next_result());
        echo 'invitationBonus sql success: ' . $re;
    }

    return $re;
}