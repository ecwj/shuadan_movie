<?php

include 'connection.php';

$config = include '../../config/app.php';
include '../../application/common.php';

$type = '';
$is_presale = 0;
if (isset($argv[1])) {
	if ($argv[1] == 1) {
		$is_presale = 1;
	}
}
if (isset($argv[2])) {
	$type = $argv[2];
}

$date = date('Y-m-d', time());
$ytd = $date;//date('Y-m-d', strtotime($date . '-1 day'));
if (in_array($config['app_name'], ['dypf_indon'])) {
	$ytd = date('Y-m-d', strtotime($date . '-1 day'));
}
$d1 = strtotime(date('Y-m-d 00:00:00', strtotime($ytd)));
$d2 = strtotime(date('Y-m-d 23:59:59', strtotime($ytd)));

if (in_array($config['app_name'], ['prime', 'apple_movie', 'nolan_movie'])) {
	if (date('H:i:s') >= '22:30:00' && date('H:i:s') <= '23:59:59') {
		$ytd = date('Y-m-d', time());
		$d1 = strtotime(date('Y-m-d 00:00:00', strtotime($ytd)));
		$d2 = strtotime(date('Y-m-d 18:00:00', strtotime($ytd)));
	} else if (date('H:i:s') >= '10:30:00' && date('H:i:s') <= '22:29:59') {
		$ytd = date('Y-m-d', strtotime($date . '-1 day'));
		$d1 = strtotime(date('Y-m-d 18:00:00', strtotime($ytd)));
		$d2 = strtotime(date('Y-m-d 23:59:59', strtotime($ytd)));
	}
	if ($type == 'ytd') {
		$ytd = date('Y-m-d', strtotime($date . '-1 day'));
		$d1 = strtotime(date('Y-m-d 00:00:00', strtotime($ytd)));
		$d2 = strtotime(date('Y-m-d 18:00:00', strtotime($ytd)));	
	} else if ($type == 'now') {
		$ytd = date('Y-m-d');
		$d1 = strtotime(date('Y-m-d 00:00:00', strtotime($ytd)));
		$d2 = strtotime(date('Y-m-d 23:59:59', strtotime($ytd)));	
	}
}
// echo 'typd: ' . $type;
$wherePresale = '';
if ($is_presale == 1) {
	$wherePresale = " AND xc.release_time > 0 AND xc.release_time <= " . time();
} else {
	$wherePresale = " AND COALESCE(xc.release_time, 0) = 0";
}
$sql = "SELECT u.id, l.roi, xc.id AS xc_id, (xc.num) AS total_num, COALESCE((xc.goods_count), 0) AS total_count, l.order_num, r.id AS roi_id, u.bonus_status, COALESCE(free.free_credit, 0) AS free_credit, COALESCE(xc.remark, '') AS remark, COALESCE(xc.addtime, '') AS xc_addtime, xc.roi_percentage, u.level, COALESCE(xc.release_time, 0) release_time
	FROM xy_users u 
	JOIN xy_level l ON l.level = u.level
	LEFT JOIN xy_roi_log r ON r.uid = u.id AND r.roi_date = '$ytd'
    LEFT JOIN 
    	(
    		SELECT xc.*, COALESCE(xm.roi_percentage, '') roi_percentage
    		FROM xy_convey_movie xc 
    		LEFT JOIN xy_movie xm ON xm.id = xc.goods_id
    		WHERE xc.addtime BETWEEN '$d1' AND '$d2' AND c_status = 0 $wherePresale
    	) xc ON xc.uid = u.id
    LEFT JOIN (
        SELECT uid, SUM(IF(status = 1, num, -num)) AS free_credit 
        FROM xy_balance_log 
        WHERE uid > 0 AND type = 20 
        GROUP BY uid
    ) free ON free.uid = u.id
    WHERE u.username != 'supermovie'
	GROUP BY xc_id
	HAVING total_num IS NOT NULL";

$result = $conn->query($sql);
// echo $sql;
$insertSql = [];
$datas = [];
while ($user = $result->fetch_array(MYSQLI_ASSOC)) {
	if (!in_array($config['app_name'], ['prime', 'apple_movie', 'nolan_movie'])) {
		if ($user['roi_id'] != NULL) {
			continue;
		}
	}
	$uid = $user['id'];
	$total_num = $user['total_num'];
	$total_count = $user['total_count'];
	$roi = (float) $user['roi'];
	$release_time = $user['release_time'];
	
	if ($is_presale == 1 && $release_time > 0 && $release_time < time()) {
		$roi_percentage = explode(",", $user['roi_percentage']);
		$roi = isset($roi_percentage[$user['level'] - 1]) ? ((float) $roi_percentage[$user['level'] - 1]) : $roi;
	}
	// var_dump('roi: ' . $roi);

	if (!isset($datas[$uid])) {
		$datas[$uid] = [];
		$datas[$uid]['total_num'] = 0;
		$datas[$uid]['total_count'] = 0;
		$datas[$uid]['roi'] = 0;
		$datas[$uid]['xc_id'] = [];
		$datas[$uid]['xc_addtime'] = [];
	}

	if (array_search("'" . $user['xc_id'] . "'", $datas[$uid]['xc_id']) !== false) {
		continue;
	}

	if (in_array($config['app_name'], ['apple_movie'])) {
		if (!isset($datas[$uid]['free_credit'])) {
			$datas[$uid]['free_credit'] = $user['free_credit'];
		}
		if (!isset($datas[$uid]['free_credit_purchase'])) {
			$datas[$uid]['free_credit_purchase'] = 0;
		}
		if (strpos($user['remark'], 'free_credit') !== false) {
			// var_dump($user);
			$free_credit_purchase_arr = explode("|", $user['remark']);
			if (isset($free_credit_purchase_arr[1]) && $free_credit_purchase_arr[1] > 0) {
				$datas[$uid]['free_credit_purchase'] += $free_credit_purchase_arr[1];
			}
		}
	}

	$datas[$uid]['total_num'] += $total_num;
	$datas[$uid]['total_count'] += $total_count;
	$datas[$uid]['roi'] = $roi;
	$datas[$uid]['xc_id'][] = "'" . $user['xc_id'] . "'";
	$datas[$uid]['xc_addtime'][] = $user['xc_addtime'];
	$datas[$uid]['order_num'] = $user['order_num'];
	$datas[$uid]['bonus_status'] = $user['bonus_status'];
}
// var_dump($datas);exit;
$i = $j = 0;
foreach ($datas as $uid => $user) {
	$total_num = $user['total_num'];
	$total_count = $user['total_count'];
	$roi = (float) $user['roi'];
	$order_num = $user['order_num'];
	$bonus_status = $user['bonus_status'];
	$ids = implode(",", $user['xc_id']);
// var_dump($user);
	if ($j % 5 == 0) {
		$i ++;
	}

	if (!isset($insertSql[$i])) {
		$insertSql[$i] = "";
	}
	//回购
	$huigou = $total_num;
	if (!in_array($config['app_name'], ['dypf_indon'])) {
		if (in_array($config['app_name'], ['apple_movie'])) {

			$minAddTime = min($user['xc_addtime']);
			$old_fc_purchase_sql = "SELECT COALESCE(SUM(num), 0) AS num FROM xy_convey_movie WHERE uid = $uid AND addtime < '$minAddTime' AND remark LIKE '%free_credit%'";
			$result = $conn->query($old_fc_purchase_sql);
			var_dump($result);
			$old_fc_purchase_res = $result->fetch_array(MYSQLI_ASSOC);
			
			if(!$old_fc_purchase_res) {
				$old_fc_purchase  = 0;
			} else {
				$old_fc_purchase = $old_fc_purchase_res['num'];
			}
			$result->free_result();

			$fc_balance = $user['free_credit'] - $old_fc_purchase;
			// var_dump('fc_balance: ' . $fc_balance);
			if ($fc_balance > 0) {
				if (($user['free_credit_purchase'] - $fc_balance) < 0) {
					$huigou = 0;
				} else {
					$huigou = $total_num - $user['free_credit_purchase'];
				}
			}
		}
		if ($huigou > 0) {
			$oid = getSn('DY');
			$insertSql[$i] .= "INSERT INTO xy_balance_log (uid, sid, `oid`, num, type, status, addtime) VALUES ($uid, 0, '$oid', '$huigou', 15, 1, '" . time() . "');";
		}
	}

	if ($total_count < $order_num) {
		$insertSql[$i] .= "UPDATE xy_users SET balance = (balance + " . $total_num . ") WHERE id = $uid;";
		$insertSql[$i] .= "UPDATE xy_convey_movie SET commission = (num * " . $roi . "), c_status = 1 WHERE id IN ($ids) AND uid = $uid;";
		$j ++;
		continue;
	}

	$remark = '';
	//检验奖金是否可以派发
	if (in_array($config['app_name'], ['apple_movie', 'nolan_movie']) && $bonus_status != 1) {
		$remark = 'bonus_status = ' . $bonus_status . ' | date: ' . date('Y-m-d H:i:s');
		$insertSql[$i] .= "UPDATE xy_convey_movie SET commission = (num * " . $roi . "), c_status = 1, remark = '$remark' WHERE id IN ($ids) AND uid = $uid;";
		$insertSql[$i] .= "UPDATE xy_users SET balance = (balance + " . ($huigou) . ") WHERE id = $uid;";
	} else {
		$bonus = $roi * $total_num;	
		$insertSql[$i] .= "INSERT INTO xy_balance_log (uid, sid, `oid`, num, type, status, addtime) VALUES ($uid, 0, 0, '$bonus', 13, 1, '" . time() . "');";
		$insertSql[$i] .= "INSERT INTO xy_roi_log (uid, roi_date, deal_amount, bonus, percentage, status, addtime, remark) VALUES ($uid, '$ytd', '$total_num', '$bonus', '$roi', 1, '" . time() . "', '$remark');";
		$insertSql[$i] .= "UPDATE xy_convey_movie SET commission = (num * " . $roi . "), c_status = 1 WHERE id IN ($ids) AND uid = $uid;";
		$insertSql[$i] .= "UPDATE xy_users SET balance = (balance + " . ($huigou + $bonus) . ") WHERE id = $uid;";
	}

	$j ++;
}
// var_dump($insertSql);exit;
$re = 0;
if ($insertSql != []) {
	foreach ($insertSql as $sql) {
		// echo $sql . '<br>';
		$res = $conn->multi_query($sql);
		while (@$conn->next_result());
		if ($res) {
			$re ++;
		}
		echo "Error: " . ($conn->error ? $conn->error : 'None') . '<br>';
	}
}

echo "Done sql count: " . $re;
return $re;