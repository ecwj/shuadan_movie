<?php

include 'connection.php';

$config = include '../../config/app.php';

$date = date('Y-m-d');
$ytd = $date;//date('Y-m-d', strtotime($date . '-1 day'));
if (in_array($config['app_name'], ['dypf_indon'])) {
	$ytd = date('Y-m-d', strtotime($date . '-1 day'));
} else if (in_array($config['app_name'], ['prime', 'apple_movie', 'nolan_movie'])) {
	if (date('H:i:s') >= '10:30:00' && date('H:i:s') <= '22:29:59') {
		$ytd = date('Y-m-d', strtotime($date . '-1 day'));
	}
}
$sql = "SELECT r.id AS roi_id, r.uid, r.bonus, rm.id AS rm_id, u.level
	FROM xy_roi_log r
	JOIN xy_users u ON u.id = r.uid
	LEFT JOIN xy_roi_matching_log rm ON rm.roi_id = r.id AND rm.status = 1
	WHERE roi_date = '$ytd' AND r.status = 1
	HAVING rm_id IS NULL";

$result = $conn->query($sql);

$insertSql = "";
$re = 0;
while ($user = $result->fetch_array(MYSQLI_ASSOC)) {
	$uid = $user['uid'];
	$amount = $user['bonus'];
	$roi_id = $user['roi_id'];

	if (in_array($config['app_name'], ['prime', 'apple_movie', 'nolan_movie'])) {
		$userList = parent_user_balance($conn, $uid, 3);
	} else {
		$userList = parent_user($conn, $uid, 3);
	}
    if($userList){
        foreach($userList as $v){
        	$insertSql = "";
        	$balance = $v['balance'];
        	if ($v['id'] == '3767') {
        		echo $uid . ' - ' . $amount . ' - ' . $roi_id;
        		var_dump($v); 
        	}
        	if (in_array($config['app_name'], ['prime', 'apple_movie', 'nolan_movie'])) {
        		if ($v['level'] <= 1) {
        			continue;
        		}
        		if (in_array($config['app_name'], ['apple_movie', 'nolan_movie'])) {
        			//检验奖金是否可以派发
        			if ($v['bonus_status'] != 1) {
        				continue;
        			}
        			//单个户口连续48小时无购票，系统自动暂停该账户团队收益。
        			$deal_time = $v['deal_time'];

        			$date1 = date_create(date('Y-m-d H:i:s', $deal_time));
				    $date2 = date_create(date('Y-m-d H:i:s'));

				    $diff = date_diff($date1,$date2);
				    $sign = $diff->format("%R");
				    $dayDiff = $diff->format("%a");
				    // var_dump($date1);
				    // var_dump($date2);
				    // var_dump($v['id']);
				    // var_dump($dayDiff);
         				// echo $deal_time;exit;
				    // var_dump($v['level']);
				    // var_dump($user['level']);exit;
        			if ($dayDiff >= 2) {
        				continue;
        			}
        			//下级等级高于上级时，该上级断层收益，将无法享受比自己高这条线的团队佣金。
        			if (in_array($config['app_name'], ['apple_movie'])) {
	        			if ($user['level'] > $v['level']) {
	        				continue;
	        			}
	        		}
        		}
        	}
            if($v['status']==1 && $balance >= $config['teambonus_wallet_min_required']){
            	$f_lv = $v['lv'];
            	$receive_uid = $v['id'];
            	$percentage = (float) $config[$v['lv'].'_d_reward'];
            	$bonus = $percentage * $amount;

            	$insertSql .= "INSERT INTO xy_balance_log (uid, sid, oid, num, type, status, addtime) VALUES ($receive_uid, 0, 0, '$bonus', 14, 1, '" . time() . "');";
				$insertSql .= "INSERT INTO xy_roi_matching_log (uid, f_lv, deal_amount, bonus, percentage, status, addtime, roi_id) VALUES ($receive_uid, '$f_lv', '$amount', '$bonus', '$percentage', 1, '" . time() . "', '$roi_id');";
				$insertSql .= "UPDATE xy_users SET balance = (balance + " . ($bonus) . ") WHERE id = $receive_uid;";				
            }
            if ($insertSql != '') {
				$re += $conn->multi_query($insertSql);
				while (@$conn->next_result());
			}
        }
    }
}

// $re = 0;
// if ($insertSql != '') {
// 	$re = $conn->multi_query($insertSql);
// }

echo "Done sql count: " . $re;
return $re;

