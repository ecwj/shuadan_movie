<?php



include 'connection.php';

$config = include '../../config/app.php';
include '../../application/common.php';

$date = date('Y-m-d', time());
$ytd = $date;//date('Y-m-d', strtotime($date . '-1 day'));
if (in_array($config['app_name'], ['dypf_indon'])) {
	$ytd = date('Y-m-d', strtotime($date . '-1 day'));
}
$d1 = strtotime(date('Y-m-d 00:00:00', strtotime($ytd)));
$d2 = strtotime(date('Y-m-d 23:59:59', strtotime($ytd)));

$sql = "SELECT u.id, l.roi, xc.id AS xc_id, (xc.num) AS total_num, COALESCE((xc.goods_count), 0) AS total_count, l.order_num, r.id AS roi_id
	FROM xy_users u 
	JOIN xy_level l ON l.level = u.level
	LEFT JOIN xy_roi_log r ON r.uid = u.id AND r.roi_date = '$ytd'
    LEFT JOIN xy_convey_movie xc ON xc.uid = u.id AND xc.addtime BETWEEN '$d1' AND '$d2' AND c_status = 0
    WHERE u.username = 'supermovie'
	-- GROUP BY u.id
	HAVING total_num IS NOT NULL";

$result = $conn->query($sql);

$insertSql = [];
$datas = [];
while ($user = $result->fetch_array(MYSQLI_ASSOC)) {
	if (!in_array($config['app_name'], ['prime', 'apple_movie'])) {
		if ($user['roi_id'] != NULL) {
			continue;
		}
	}
	$uid = $user['id'];
	$total_num = $user['total_num'];
	$total_count = $user['total_count'];
	$roi = (float) $user['roi'];

	if (!isset($datas[$uid])) {
		$datas[$uid] = [];
		$datas[$uid]['total_num'] = 0;
		$datas[$uid]['total_count'] = 0;
		$datas[$uid]['roi'] = 0;
		$datas[$uid]['xc_id'] = [];
	}

	$datas[$uid]['total_num'] += $total_num;
	$datas[$uid]['total_count'] += $total_count;
	$datas[$uid]['roi'] = $roi;
	$datas[$uid]['xc_id'][] = "'" . $user['xc_id'] . "'";
	$datas[$uid]['order_num'] = $user['order_num'];
}
// var_dump($datas);exit;
$i = $j = 0;
foreach ($datas as $uid => $user) {
	$total_num = $user['total_num'];
	$total_count = $user['total_count'];
	$roi = (float) $user['roi'];
	$order_num = $user['order_num'];
	$ids = implode(",", $user['xc_id']);
	if ($j % 5 == 0) {
		$i ++;
	}

	if (!isset($insertSql[$i])) {
		$insertSql[$i] = "";
	}
	//回购
	if (!in_array($config['app_name'], ['dypf_indon'])) {
		$oid = getSn('DY');
		$insertSql[$i] .= "INSERT INTO xy_balance_log (uid, sid, `oid`, num, type, status, addtime) VALUES ($uid, 0, '$oid', '$total_num', 15, 1, '" . time() . "');";
	}

	if ($total_count < $order_num) {
		$insertSql[$i] .= "UPDATE xy_users SET balance = (balance + " . $total_num . ") WHERE id = $uid;";
		$insertSql[$i] .= "UPDATE xy_convey_movie SET commission = (num * " . $roi . "), c_status = 1 WHERE id IN ($ids) AND uid = $uid;";
		$j ++;
		continue;
	}

	$bonus = $roi * $total_num;	

	$insertSql[$i] .= "INSERT INTO xy_balance_log (uid, sid, `oid`, num, type, status, addtime) VALUES ($uid, 0, 0, '$bonus', 13, 1, '" . time() . "');";
	$insertSql[$i] .= "INSERT INTO xy_roi_log (uid, roi_date, deal_amount, bonus, percentage, status, addtime) VALUES ($uid, '$ytd', '$total_num', '$bonus', '$roi', 1, '" . time() . "');";
	$insertSql[$i] .= "UPDATE xy_convey_movie SET commission = (num * " . $roi . "), c_status = 1 WHERE id IN ($ids) AND uid = $uid;";
	$insertSql[$i] .= "UPDATE xy_users SET balance = (balance + " . ($total_num + $bonus) . ") WHERE id = $uid;";

	$j ++;
}
// var_dump($insertSql);exit;
$re = 0;
if ($insertSql != []) {
	foreach ($insertSql as $sql) {
		// echo $sql . '<br>';
		$res = $conn->multi_query($sql);
		while (@$conn->next_result());
		if ($res) {
			$re ++;
		}
		echo "Error: " . ($conn->error ? $conn->error : 'None') . '<br>';
	}
}

echo "Done sql count: " . $re;
return $re;