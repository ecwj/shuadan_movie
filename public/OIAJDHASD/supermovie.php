<?php
//buy movie ticket with fake account


include 'connection.php';
include '../../application/common.php';
$config = include '../../config/app.php';

$userSql = "SELECT id, (balance - freeze_balance) AS balance FROM xy_users WHERE username = 'supermovie'";
$r = $conn->query($userSql);
$user = $r->fetch_array(MYSQLI_ASSOC);

if ($user == NULL) {
	echo 'No user';
	return;
}

$uid = $user['id'];
$balance = $user['balance'];
$_date = $config['movie_start_date'];

$sql = "SELECT m.id, m.price, m.stock, COALESCE(SUM(goods_count), 0) AS total_count 
	FROM `xy_movie` m
	LEFT JOIN xy_convey_movie xc ON xc.goods_id = m.id AND xc.status 
	WHERE m.status = 1 AND playtime_date >= '$_date'
	GROUP BY m.id
	HAVING m.stock > total_count;";
$result = $conn->query($sql);

$res = false;
$insertSql = "INSERT INTO xy_convey_movie (id, uid, num, addtime, endtime, status, commission, goods_id, goods_count) VALUES ";
$insertBalanceLogSql = "INSERT INTO xy_balance_log (uid, `oid`, num, type, status, addtime) VALUES ";
$sqls = [];
$balanceLogSqls = [];
while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
	$id = $row['id'];
	$price = $row['price'];
	$stock = $row['stock'];
	$total_count = $row['total_count'];

	$remaining = $stock - $total_count;
	$purchase_count = FLOOR($remaining * 0.02); //(2% of the stock)

	if ($purchase_count < 1 || $purchase_count > $remaining) {
		continue;
	}
	$total_num = $price * $purchase_count;
	if ($balance < $total_num) {
		
		echo "\n Not enough balance. Balance: $balance ";
		break;
	}

	$balance -= $total_num;

	$time = time();
	$oid = getSn('DY');
	$sqls[] = " ('$oid', '$uid', '$total_num', '$time', '$time', 1, 0, $id, $purchase_count) ";

	$balanceLogSqls[] = " ('$uid', '$oid', '$total_num', 2, 2, '$time') ";
}

if (sizeof($sqls) > 0) {
	$insertSql .= implode(",", $sqls) . ';';
	$insertBalanceLogSql .= implode(",", $balanceLogSqls) . ';';

	$insertSql .= $insertBalanceLogSql;

	$insertSql .= "UPDATE xy_users SET balance = $balance WHERE id = $uid;";
	// echo $insertSql;
	$res = $conn->multi_query($insertSql);
	echo "Error: " . ($conn->error ? $conn->error : 'None');
}

echo "\nCron done.";
return;