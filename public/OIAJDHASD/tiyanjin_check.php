<?php

include 'connection.php';

$config = include '../../config/app.php';
include '../../application/common.php';

$sql = "SELECT value FROM system_config WHERE name = 'free_credit_day'";
$res0 = $conn->query($sql);
if ($res0->num_rows > 0) {
	$fc_day = $res0->fetch_array(MYSQLI_ASSOC)['value'];
} else {
	$fc_day = 3;
}
$sql = "SELECT uid, SUM(IF(status = 1, num, -num)) AS total, MIN(addtime) AS fc_time 
	FROM xy_balance_log 
	WHERE uid > 0 AND type IN (2, 20)
	GROUP BY uid";

$result = $conn->query($sql);
// echo $sql;
$insertSql = [];
$i = $j = 0;
while ($user = $result->fetch_array(MYSQLI_ASSOC)) {
	if ($user['total'] > 0) {
		$fc_time = $user['fc_time'];
		$uid = $user['uid'];
		$remainingFC = $user['total'];

		if ($j % 5 == 0) {
			$i ++;
		}

		if (!isset($insertSql[$i])) {
			$insertSql[$i] = "";
		}

		$date1 = date_create(date('Y-m-d H:i:s', $fc_time));
		$date2 = date_create(date('Y-m-d H:i:s'));    

		$diff = date_diff($date1,$date2);
		$sign = $diff->format("%R");
		$dayDiff = $diff->format("%a");
		if ($sign == "+" && $dayDiff >= $fc_day) {
			// $sql = "SELECT remark FROM xy_convey_movie WHERE remark LIKE '%free_credit%' AND uid = $uid";
			// $res1 = $conn->query($sql);
			// $usedFc = 0;
			// while ($row = $res1->fetch_array(MYSQLI_ASSOC)) {
			// 	$arr = explode("|", $row['remark']);
   //              if (isset($arr[1])) {
   //                  $usedFc += (float) $arr[1];
   //              }
			// }
			if ($remainingFC > 0) {
				$oid = getSn('FREE');
				// $deduct = 
				$insertSql[$i] .= "INSERT INTO xy_balance_log (uid, sid, `oid`, num, type, status, addtime) VALUES ($uid, 0, '$oid', '$remainingFC', 20, 2, '" . time() . "');";
				$insertSql[$i] .= "UPDATE xy_users SET balance = (balance - " . ($remainingFC) . ") WHERE id = $uid;";
			}
		}

		$j ++;
	}
}

$re = 0;
if ($insertSql != []) {
	foreach ($insertSql as $sql) {
		// echo $sql . '<br>';
		$res = $conn->multi_query($sql);
		while (@$conn->next_result());
		if ($res) {
			$re ++;
		}
		echo "Error: " . ($conn->error ? $conn->error : 'None') . '<br>';
	}
}