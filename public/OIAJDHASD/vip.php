<?php

include 'connection.php';

$config = include '../../config/app.php';
$minRecharge = $config['invite_reward_min_recharge'];

//checkLevel_1($conn, $minRecharge);
$min = 2;
$max = 5;
if (in_array($config['app_name'], ['dypf_indon', 'apple_movie', 'prime', 'nolan_movie'])) {
    $sql = "UPDATE xy_users SET level = 1 WHERE is_jia = 0";
    $conn->query($sql);
    if (in_array($config['app_name'], ['dypf_indon'])) {
        $max = 3;
    } else if (in_array($config['app_name'], ['apple_movie'])) {
        $max = 7;
    } else {
        $max = 6;
    }
    for ($i = $max; $i >= $min; $i --) {
        checkLevel_other($conn, $minRecharge, $config['app_name'], $i);
    }
} else {
    for ($i = $min; $i <= $max; $i ++) {
    	checkLevel_other($conn, $minRecharge, $config['app_name'], $i);
    }
}

$d = date("Y-m-d H:i:s");
if ($d >= '2021-06-14 00:00:00' && $d <= '2021-06-18 23:59:59') {
    vip_bonus($conn, $config);
}

function checkLevel_1($conn, $minRecharge)
{
	$res = 0;

	$level1Sql = "SELECT * FROM xy_level WHERE level = 1 LIMIT 1";
	$result = $conn->query($level1Sql);
	$level = $result->fetch_array(MYSQLI_ASSOC);

	$result->free_result();

	$sql = "SELECT u.id, u.level, u.total_recharge, u.down_userid, COALESCE(SUM(xc.goods_count), 0) AS total_mission
		FROM xy_users u 
		LEFT JOIN xy_convey_movie xc ON xc.uid = u.id
		WHERE u.level = 0 AND u.total_recharge >= $minRecharge AND u.is_jia = 0 AND u.is_jia = 0
		GROUP BY u.id";
	$result = $conn->query($sql);
 
	$qualified_uids = [];
	while($user = $result->fetch_array(MYSQLI_ASSOC)) {
        $uid = $user['id'];
        $total_recharge = $user['total_recharge'];
        $total_mission = $user['total_mission'];
        $down_userid = $user['down_userid'] == NULL ? '' : $user['down_userid'];
        // echo ($down_userid) . '<br>';
        $down_userid_arr = $down_userid != '' ? explode(",", $user['down_userid']) : [];
        $downcount = sizeof($down_userid_arr);
        $index = array_search($uid, $down_userid_arr);
        if ($index !== false) {
        	unset($down_userid_arr[0]);
        	$downcount = sizeof($down_userid_arr);
        }

        $new_down_userid = $downcount == 0 ? "''" : implode(",", $down_userid_arr);
        $sql = "SELECT COUNT(id) AS total FROM xy_users WHERE id IN ($new_down_userid) AND total_recharge >= $minRecharge";
        $result2 = $conn->query($sql);
        $inv = $result2->fetch_array(MYSQLI_ASSOC);
        $result2->free_result();

        //VIP1	最低15次	邀请3人充值最低300	邀请3人充值最低300	1000	2.2%
        if ($inv['total'] >= $level['auto_vip_xu_num'] && $total_recharge >= $level['num']) {
        	$qualified_uids[] = $uid;
        }
    }

    if ($qualified_uids != []) {
    	$qualified_uids_str = implode(",", $qualified_uids);
    	$sql = "UPDATE xy_users SET level = 1 WHERE id IN ($qualified_uids_str)";
    	$res = $conn->query($sql);
    }

    return $res;
}

function checkLevel_other($conn, $minRecharge, $appname, $lv = 2)
{
	if ($lv < 2 && $lv > 6) {
		$lv = 2;
	}
	$res = 0;

	$level1Sql = "SELECT * FROM xy_level WHERE level = $lv LIMIT 1";
	$result = $conn->query($level1Sql);
	$level = $result->fetch_array(MYSQLI_ASSOC);
	
	if(!$level) {
		return 0;
	}
	$result->free_result();

	$sql = "SELECT u.id, u.level, u.total_recharge, u.down_userid, (u.balance - u.freeze_balance) AS balance
		FROM xy_users u 
		LEFT JOIN xy_convey_movie xc ON xc.uid = u.id
		WHERE u.level = " . ($lv - 1) . " AND u.total_recharge >= $minRecharge AND u.is_jia = 0
		GROUP BY u.id";
    if (in_array($appname, ['dypf_indon'])) {
        $sql = "SELECT u.id, u.level, (u.total_recharge - u.total_deposit) AS total_recharge, u.down_userid, (u.balance - u.freeze_balance) AS balance
        FROM xy_users u 
        WHERE u.level = 1 AND u.total_recharge >= $minRecharge AND u.is_jia = 0
        GROUP BY u.id";
    } else if (in_array($appname, ['prime'])) {
        $sql = "SELECT u.id, u.level, COALESCE(b.bal, 0) AS balance, u.total_recharge, GROUP_CONCAT(DISTINCT(up.id)) AS down_userid
        FROM xy_users u 
        LEFT JOIN xy_users up ON up.parent_id = u.id
        LEFT JOIN (
            SELECT uid, SUM(IF(b.status = 1, b.num, -b.num)) AS bal
            FROM
            xy_balance_log b WHERE b.type NOT IN (2, 15)
            GROUP BY uid
        ) b ON b.uid = u.id
        WHERE u.level = 1 AND u.total_recharge >= $minRecharge AND u.is_jia = 0
        GROUP BY u.id";
    } else if (in_array($appname, ['apple_movie', 'nolan_movie'])) {
        $sql = "SELECT u.id, u.level, COALESCE(b.bal, 0) AS balance,u.total_recharge, GROUP_CONCAT(DISTINCT(up.id)) AS down_userid
        FROM xy_users u 
        LEFT JOIN xy_users up ON up.parent_id = u.id
        LEFT JOIN (
            SELECT uid, SUM(IF(b.status = 1, b.num, -b.num)) AS bal
            FROM
            xy_balance_log b WHERE b.type NOT IN (2, 15)
            GROUP BY uid
        ) b ON b.uid = u.id
        WHERE u.level = 1 AND u.total_recharge >= $minRecharge AND u.is_jia = 0
        GROUP BY u.id";
    }
	$result = $conn->query($sql);

	$qualified_uids = [];
	while($user = $result->fetch_array(MYSQLI_ASSOC)) {
        $uid = $user['id'];        
        $total_recharge = $user['total_recharge'];
        $down_userid = $user['down_userid'] == NULL ? '' : $user['down_userid'];
        // echo ($down_userid) . '<br>';
        $down_userid_arr = $down_userid != '' ? explode(",", $user['down_userid']) : [];
        $downcount = sizeof($down_userid_arr);
        $index = array_search($uid, $down_userid_arr);
        if ($index !== false) {
        	unset($down_userid_arr[0]);
        	$downcount = sizeof($down_userid_arr);
        }

        //充值最低300的团队人数
        $new_down_userid = $downcount == 0 ? "''" : implode(",", $down_userid_arr);
        //$sql = "SELECT COUNT(id) AS total FROM xy_users WHERE id IN ($new_down_userid) AND total_recharge >= $minRecharge";

        $sql = "select id, (select sum(num) from xy_balance_log where uid = u.id and type = 1) as times from xy_users u  where id in ($new_down_userid) having times >= $minRecharge";
        $result2 = $conn->query($sql);
        $inv = $result2->fetch_array(MYSQLI_ASSOC);
        $totalValid = $result2->num_rows;
        $result2->free_result();
        
        echo 'uid: ' . $uid . 'lv: ' . $lv . '$totalValid: ' . $totalValid . 'auto_vip_xu_num: ' . $level['auto_vip_xu_num'] . ' total_recharge: ' . $total_recharge . ' $level["num"]: ' . $level['num'] . "\n";
        if ($totalValid >= $level['auto_vip_xu_num'] && $total_recharge >= $level['num']) {
            if (in_array($appname, ['prime', 'apple_movie', 'nolan_movie'])) { 
                if ($user['balance'] >= $level['num']) {
                   $qualified_uids[] = $uid;
               }
            }
        }
    }

    if ($qualified_uids != []) {
    	$qualified_uids_str = implode(",", $qualified_uids);
    	$sql = "UPDATE xy_users SET level = $lv WHERE id IN ($qualified_uids_str)";
    	$res = $conn->query($sql);
    }
    echo $res;
    return $res;
}

function vip_bonus($conn, $config) {

    $t1 = strtotime(date('Y-m-d 00:00:00'));
    $t2 = strtotime(date('Y-m-d 23:59:59'));
    // $existSql = "SELECT uid FROM `xy_balance_log` WHERE type = 10 AND addtime BETWEEN '$t1' AND '$t2'";
    $existSql = "SELECT uid, CONCAT(uid, '-', remark) AS remark FROM `xy_balance_log` WHERE type = 10";
    $resultExist = $conn->query($existSql);

    $uids = [];
    while($row = $resultExist->fetch_array(MYSQLI_ASSOC)) {
        $uids[] = $row['remark'];
    }
    $sql = "SELECT users.id, balance, users.level, level.name AS level_name FROM xy_users users JOIN xy_level level ON level.level = users.level WHERE status = 1 AND is_jia = 0";

    $multiSql = [];
    $result = $conn->query($sql);

    $i = $j = 0;
    while($user = $result->fetch_array(MYSQLI_ASSOC)) {
        $level = $user['level'];
        $uid = $user['id'];
        $remark = $user['level_name'] . '奖励';
        $uid_remark = $uid . '-' . $remark;
        if (isset($config['vip_bonus_' . $level]) && !in_array($uid_remark, $uids)) {

            $bonus = $config['vip_bonus_' . $level];
            
            if ($j % 5 == 0) {
                $i ++;
            }

            if (!isset($multiSql[$i])) {
                $multiSql[$i] = "";
            }
            
            $multiSql[$i] .= "INSERT INTO xy_balance_log (uid, sid, `oid`, num, type, status, addtime, remark) VALUES ($uid, 0, '', '$bonus', 10, 1, '" . time() . "', '$remark');";
            $multiSql[$i] .= "UPDATE xy_users SET balance = (balance + " . $bonus . ") WHERE id = $uid;";

            $j++;
        }        
    }

    $re = 0;
    if ($multiSql != []) {
        foreach ($multiSql as $sql) {
            $res = $conn->multi_query($sql);
            while (@$conn->next_result());
            if ($res) {
                // echo $res . '<br>';
                $re ++;
            }
            echo "Error: " . ($conn->error ? $conn->error : 'None') . '<br>';
        }
    }
}