<?php
$config = include '../config/app.php';
?>
<!DOCTYPE html>
<html data-dpr="1" style="font-size: 37.5px;">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1">
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <link href="/public/css/custom.css?v=<?= $config['app_version']; ?>" rel="stylesheet">
</head>
<?php

//Detect special conditions devices
$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
$webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");
$wechat  = stripos($_SERVER['HTTP_USER_AGENT'],"MicroMessenger");

$copy = false;
//do something with this information
if ( $iPod || $iPhone || $iPad) {
    //browser reported as an iPhone/iPod touch -- do something here
    $url = $config['ios_download_url'];   
} else if ($wechat) {
	$url = $config['android_download_url'];	
	$copy = true;
} else {
    //browser reported as an iPad -- do something here
    $url = $config['android_download_url'];
}

if($copy) {
	echo '<div style="text-align:center;margin-top: 10px;"><img src="/public/img/new/logo.png" style="width:200px;"><br><button class="btn btn-primary" id="copy" onclick="copy_txt(\'' . $url . '\');">拷贝下载链接</button><p id="copied"></p></div>';
	echo '<input name="" id="webcopyinput" type="text"
            style="color: #FF0000; font-size: 20px; width: 1px; height: 1px; border: hidden; font-weight: bold; text-align: center; position: absolute; top: -99999px;"
            value="">';
} else {
	header("Location: $url");
	die();
}

?>
<!-- <script charset="utf-8" src="/static_new/js/jquery.min.js"></script> -->
<script>
	function copy_txt(xx) {
        var text = document.getElementById("webcopyinput");
        text.id = 'webcopyinput';
        text.value = '' + xx + '';
        text.focus(); //给input输入框聚焦
        text.setSelectionRange(0, text.value.length); //设置input框选中的范围
        copied = document.execCommand('Copy'); //执行复制操作
        text.blur();
        copied = false;
        document.querySelector('#copied').innerHTML = '拷贝成功';
    }
</script>