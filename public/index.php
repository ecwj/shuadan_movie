<?php
namespace think;

//Header("Location:/404.html");
//die;

date_default_timezone_set("Asia/Singapore");

$http = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && $_SERVER['HTTPS'] != 'off') ? 'https' : 'http';
define('SITE_URL',$http.'://'.$_SERVER['HTTP_HOST']); // 网站域名
define('APP_PATH', __DIR__ . '/../application/');
define('PHPEXCEL_ROOT', __DIR__ . '/../extend/PHPExcel/');

//define('ADMIN_KEY', '1');
if (isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] != '127.0.0.1') {
	if (!isset($_SERVER['HTTPS'])) {
		header("Location: https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
		die();
	}
}
require __DIR__ . '/../thinkphp/base.php';
require __DIR__ . '/../extend/org/Mobile.php';
/*
if ( stripos($_SERVER['REQUEST_URI'],"admin") === false && ( $_SERVER['REQUEST_URI'] != '/index/crontab/start')  ){
    if( $_SERVER['REQUEST_URI'] != '/index/crontab/lxb_jiesuan' ){
        $dev = new \org\Mobile();
        $t = $dev->isMobile();
        if (!$t) {
            header('Location:/app/?hash='.time().rand(100,999));
        }
    }
}else{

}
*/
Container::get('app')->run()->send();
