

layui.define(function(exports){
  var admin = layui.admin;
  
  //区块轮播切换
  layui.use(['admin', 'carousel'], function(){
    var $ = layui.$
    ,admin = layui.admin
    ,carousel = layui.carousel
    ,element = layui.element
    ,device = layui.device();

    //轮播切换
    $('.layadmin-carousel').each(function(){
      var othis = $(this);
      carousel.render({
        elem: this
        ,width: '100%'
        ,arrow: 'none'
        ,interval: othis.data('interval')
        ,autoplay: othis.data('autoplay') === true
        ,trigger: (device.ios || device.android) ? 'click' : 'hover'
        ,anim: othis.data('anim')
      });
    });
    
    element.render('progress');
    
  });

  //八卦新闻
  layui.use(['carousel', 'echarts'], function(){
    var $ = layui.$
    ,carousel = layui.carousel
    ,echarts = layui.echarts;
    
    var echartsApp = [], options = [
      {
        title : {
          subtext: '完全实况球员数据',
          textStyle: {
            fontSize: 14
          }
        },
        tooltip : {
          trigger: 'axis'
        },
        legend: {
          x : 'left',
          data:['罗纳尔多','舍普琴科']
        },
        polar : [
          {
            indicator : [
              {text : '进攻', max  : 100},
              {text : '防守', max  : 100},
              {text : '体能', max  : 100},
              {text : '速度', max  : 100},
              {text : '力量', max  : 100},
              {text : '技巧', max  : 100}
            ],
            radius : 130
          }
        ],
        series : [
          {
            type: 'radar',
            center : ['50%', '50%'], 
            itemStyle: {
              normal: {
                areaStyle: {
                  type: 'default'
                }
              }
            },
            data:[
              {value : [97, 42, 88, 94, 90, 86], name : '舍普琴科'},
              {value : [97, 32, 74, 95, 88, 92], name : '罗纳尔多'}
            ]
          }
        ]
      }
    ]
    ,elemDataView = $('#LAY-index-pageone').children('div')
    ,renderDataView = function(index){
      echartsApp[index] = echarts.init(elemDataView[index], layui.echartsTheme);
      echartsApp[index].setOption(options[index]);
      window.onresize = echartsApp[index].resize;
    };   
    //没找到DOM，终止执行
    if(!elemDataView[0]) return;
 
    renderDataView(0); 
  });

  //访问量
  layui.use(['carousel', 'echarts'], function(){
    var $ = layui.$
    ,carousel = layui.carousel
    ,echarts = layui.echarts;
    
    var echartsApp = [], options = [
      {
        tooltip : {
          trigger: 'axis'
        },
        calculable : true,
        legend: {
          data:['充值量','提现量']
        },
        
        xAxis : [
          {
            type : 'category',
            //data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
			data : datevalue
          }
        ],
        yAxis : [
          {
            type : 'value',
            name : '充值量',
            axisLabel : {
              formatter: '{value} 元'
            }
          },
          {
            type : 'value',
            name : '提现量',
            axisLabel : {
                formatter: '{value} 元'
            }
          }
        ],
        series : [
          {
            name:'充值量',
            type:'line',
            //data:[{value}900, 850, 950, 1000, 1100, 1050, 1000, 1150, 1250, 1370, 1250, 210000]
			data:czvalue
          },
          {
            name:'提现量',
            type:'line',
            yAxisIndex: 1,
            //data:[850, 850, 800, 950, 1000, 950, 950, 1150, 1100, 1240, 1000, 950]
			data:txvalue
          }
        ]
      }
    ]
    ,elemDataView = $('#LAY-index-pagetwo').children('div')
    ,renderDataView = function(index){
      echartsApp[index] = echarts.init(elemDataView[index], layui.echartsTheme);
      echartsApp[index].setOption(options[index]);
      window.onresize = echartsApp[index].resize;
    };
    //没找到DOM，终止执行
    if(!elemDataView[0]) return;
    renderDataView(0);
    
  });



  //项目进展
  layui.use('table', function(){
    var $ = layui.$
    ,table = layui.table;
    
    table.render({
      elem: '#LAY-index-prograss'
      ,url: layui.setter.base + 'json/console/prograss.js' //模拟接口
      ,cols: [[
        {type: 'checkbox', fixed: 'left'}
        ,{field: 'prograss', title: '任务'}
        ,{field: 'time', title: '所需时间'}
        ,{field: 'complete', title: '完成情况'
          ,templet: function(d){
            if(d.complete == '已完成'){
              return '<del style="color: #5FB878;">'+ d.complete +'</del>'
            }else if(d.complete == '进行中'){
              return '<span style="color: #FFB800;">'+ d.complete +'</span>'
            }else{
              return '<span style="color: #FF5722;">'+ d.complete +'</span>'
            }
          }
        }
      ]]
      ,skin: 'line'
    });
  });
  
  //回复留言
  admin.events.replyNote = function(othis){
    var nid = othis.data('id');
    layer.prompt({
      title: '回复留言 ID:'+ nid
      ,formType: 2
    }, function(value, index){
      //这里可以请求 Ajax
      //…
      layer.msg('得到：'+ value);
      layer.close(index);
    });
  };

  exports('sample', {})
});