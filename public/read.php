<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);

if (isset($_GET['path'])) {
    $path = $_GET['path'];
    $basename = basename($path);
    $paths = explode(".", $basename);
    $ext = array_pop($paths);
    $ext = explode("?", $ext)[0];
    $size = sizeof($paths);

    if (in_array($ext, ['png', 'jpeg', 'jpg', 'gif'])) {
        header('Content-type: image/jpeg');
    } else {
        header('Content-Type: application/' . $ext);
        header('Content-Disposition: attachment; filename="'.$basename.'"');
    }
    readfile(getcwd() . "/../../$path");
    exit;
} else {
    $folder = $_GET['folder'];
    $type = $_GET['type']; // kyc / beneficiary
    $id = $_GET['id'];
    $image = $_GET['image'];
}

$image = htmlentities($image);
basename($image);
$dir = getcwd();

if (in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1'])) {
	$retrieveImageUrl = $dir . "/../$folder/$type/";
} else {
    $retrieveImageUrl = $dir . "/../$folder/$type/";
}

readfile($retrieveImageUrl . $id . '/' . $image);
?>
