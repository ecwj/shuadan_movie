// JavaScript Document
layui.use(['layer', 'element', 'form', 'upload', 'table', 'laydate'], function(){
    var $    = layui.$
    ,element = layui.element
    ,layer   = layui.layer
    ,form    = layui.form
    ,upload  = layui.upload
    ,table   = layui.table
    ,laydate = layui.laydate;

	form.render();
	// layer设置
    layer.config({
        skin: 'layui-layer-molv',
        closeBtn: 1,
        shade: 0.3,
        shadeClose: true,
        maxmin: true,
        resize: true
    });
    //同时绑定多个
    // lay("input[name='end_time']").each(function(){});
    laydate.render({
        elem: "input[name='end_time']"
        ,type: 'date'
        ,theme: 'molv'
    });
 
    
});