// JavaScript Document
layui.use(['layer', 'element', 'form', 'upload', 'table'], function(){
    var $    = layui.$
    ,element = layui.element
    ,layer   = layui.layer
    ,form    = layui.form
    ,upload  = layui.upload
    ,table   = layui.table;

    form.render();
    // layer设置
    layer.config({
        skin: 'layui-layer-molv',
        closeBtn: 1,
        shade: 0.3,
        shadeClose: true,
        maxmin: true,
        resize: true
    });
    //同时绑定多个
    lay(".bank-cq-time").each(function(){
        layui.laydate.render({
            elem: this
            ,type: 'time'
            ,theme: 'molv'
        });
    });
 

    // 监听工具条
    table.on('tool(fd_record)', function(obj){
     
    });
 
    /**
     * 导出报表
     */
    window.exportData = function(exporttype){
        layer.open({
            type:2,
            title:"导出报表",
            area:['600px','400px'],
            content:"/manage/bet/exportData?exporttype="+exporttype
        });
    }
});