<?php
Route::domain(['域名'], function () {
    Route::group('', function () {
        Route::get('demo', 'admin'); //变成这个来访问
        Route::get('admin', '404'); //原地址路由到404
    });
})->cache(3600);