配置：
Php：5.6+
Mysql：5.6+

伪静态：
location / {
	if (!-e $request_filename){
		rewrite  ^(.*)$  /index.php?s=$1  last;   break;
	}
}

运行目录：
/public/


执行时间：实时
说明：订单冻结操作
URL：http://域名/index/Crontaboks/starts

执行时间：每日0点执行1次
说明：理财利息计算
URL：http://域名/index/Crontaboks/lxb_jiesuans

